Informations about how to run ROMlab.
contact: Matthieu VITSE - vitse@lmt.ens-cachan.fr

tata


%-------------------------------------------% 
%    Remarks concerning the cluster run     %
%-------------------------------------------%  
 - when the code crashes while being run in parallel, the parallel pool stays open. To close it, use the job monitor tool (Home->Parallel->Monitor Jobs)
        -> add a function at the begining of the code who kills all the previously opened pools
 
 
 
%-------------------------------------------%  
%                Known bugs                 %
%-------------------------------------------% 
