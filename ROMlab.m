clear all;
close all;
clc;
warning off backtrace
%-------------------------------------------------------------------------
%  IMMPORTANT REMARKS
%-------------------------------------------------------------------------
% when opening a too big m file :
%    - toto = matfile('file.mat')
%    - X = toto.X;
% count the total number of lines in directory: slocDir('../esbroufePGD_V2_0_4')
% save initial solution for MPS: U = s_linear.U; save('sol_init.mat','U')
%-------------------------------------------------------------------------
% 
tcode = tic;
% load the paths to the different usefull directories
paths;

% load informations about the run
in_out = input_ROMlab();

if in_out.parallel
    
    delete(gcp('nocreate'));
%     parpool('local',32);
     parpool;
    in_out.arg_parallel = Inf;
else
    in_out.arg_parallel = 1;
end

% set up global parameters for line plots
fontsize  = 18;
linewidth = 2;
set(0,'DefaultAxesFontSize',fontsize)
set(0,'DefaultLineLineWidth',linewidth)
plot_opt.scrsz = get(0,'ScreenSize');

%%%%%%%%%%%%%%%%%%%%%%%%
%   LOADING PROBLEM    %
%%%%%%%%%%%%%%%%%%%%%%%%
profile off
%profile -memory on
profile on
if in_out.debug == 1
    dbstop if error  % /if warning
end

if strcmp(in_out.name,'') == 1
    disp('specify input file ....')
    keyboard
end

format short
c = clock;

disp(' ');
disp(' ');
disp(' ');
disp(' ');
disp(' #######     #####     ##     ##    ll                    ');
disp(' ##    ##   ##   ##   ## #   # ##   ll                    ');
disp(' ##    ##   ##   ##   ##  # #  ##   ll            bb      ');
disp(' #######    ##   ##   ##   #   ##   ll   aaaa     bb      ');
disp(' ##  ##     ##   ##   ##       ##   ll  aa  aa    bbbbb   ');
disp(' ##   ##    ##   ##   ##       ##   ll  aa  aaa   bb  bb  ');
disp(' ##    ##    #####     ##     ##    ll   aaaa aa   bbbb   ');
disp(' ');
disp(['V0.0.1 - ',num2str(c(1)),'/',num2str(c(2)),'/',num2str(c(3)),' - vitse@lmt.ens-cachan.fr']);
disp(' ');
disp(' ');
disp(' ');
disp(' ');

name = in_out.name;

% MPS init
if in_out.MPS
    DATA_MPS = load_pb(name);
    fID=fopen('params_input.txt');
    formatSpec = '%f';
    line = fscanf(fID,formatSpec);
    X = cell(1,numel(line));
    for i_cell = 1 : numel(line)
        X{i_cell} = line(i_cell);
    end
    ModifyInput(name,X);
end

% load DATA
DATA = load_pb(name);
if DATA.dim == 1
    warning('1D problems are not taken into account yet (or maybe never)')
    foo;keyboard
end

[in_out] = check_params(in_out,DATA);
in_out   = set_results_environment(in_out);

disp(' ');
disp(' ');
disp(' ');
disp(' ');
disp('**********************************************************************')
disp('parameters : ')
disp('------------ ')
disp(['name of the test case              : ',in_out.name])
if in_out.exp_pv
    disp(['export format                      : ','ParaView - http://www.paraview.org/'])
end
if in_out.exp_pxdmf
    disp(['export format                      : ','PXDMF - https://rom.ec-nantes.fr/'])
end
if in_out.debug
    disp(['debug mode                        : ON'])
end
if in_out.dam
    if in_out.unilateral
        disp(['unilateral effect                  : ON']);
    end
end
if isfield(DATA.model(:),'regularization')
    switch in_out.regularization
        case 0
            disp('no regularization')
        case 1
            disp('damage-delay regularization        : ON')
            disp(['     parameter tauc = ',num2str(DATA.model(1).regularization.tauc)]);
        case 2
            warning('nonlocal integral regularization => not implemented')
            foo;keyboard
    end
    if in_out.pgd
        disp(['PGD decomposition at local stage   : ON']);
        if in_out.orthog
            disp(['    orthog. of the reduced basis   : ON'])
        end
        if in_out.update_pgd
            disp(['    update of the reduced basis    : ON'])
        end
    end
end
disp('**********************************************************************')
disp(' ');
disp(' ');
disp(' ');
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  GENERATION OF THE MESH  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save_ref        = 0;    % 1 to save reference operators
load_operators  = 0;

if save_ref == 1
    load_operators = 0;
end

if load_operators == 1
    fload = open(strcat('data/ref/',name,'.mat'));
    m     = fload.m;
    
    
    if 1 == 1
        %m = update_hooke_damage(m);        
    end
    
    warning('operators have been loaded (but not computed)')
    disp(' ');
    disp(' ');
else
    m = read_mesh(DATA);
    m = connectivity(DATA,m);
    
    if in_out.plot_mesh
        disp('ROMlab: Exporting .vtu mesh(es)...')
        for i_model = 1:numel(m.model)
            vtk_mesh(in_out,m.model(i_model),i_model);
        end
        disp('    ... .vtu mesh(es) generated...')
    end
    
    if in_out.plot_bc
        vtk_bc(in_out,DATA);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   GENERATION OF OPERATORS   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if strcmp(in_out.solver, 'bigPGD') % Separated representation of the operators
        m = pgdmakebc(DATA,m);
        m = integration(DATA,m);
        m = pgdstiffness(DATA,m);
        
    else % Complete representation of the operators
        m = makebc(DATA,m);
        m = integration(DATA,m);
        m = stiffness(in_out,DATA,m);
    end
        
    flag = 1;
    if strcmp(DATA.assembly,'contact')
        flag = 0;
    end
    
    m = mass(DATA,m,flag);
    m = pgdintegrators(m,flag);
    
    if save_ref == 1
        save([name,'.mat'],'-v7.3');
        if ~exist('data/ref','dir')
            mkdir('data/ref');
        end
        movefile([name,'.mat'],'data/ref/')
        %foo;keyboard
    end
end

% check that the problem is not too big to be solved
eval_mem_usage(in_out,DATA,m,0);  % last argument is available RAM memory (GB)
% 0 for Linux
% 1 for OSX
% 2 for windows (errrh)

%%%%%%%%%%%%%%%
%   SOLVERS   %
%%%%%%%%%%%%%%%

switch in_out.solver
    case 'DIRECT'
        [m,s_direct]          = solver_direct(in_out,DATA,m);              % to compute the Uref solution => useless in NL
    case 'LATIN'
        [m,s_local,s_linear]  = solver_LATIN(in_out,DATA,m);               % LATIN solver
    case 'CONTACT'
        [m,s_contact]         = solver_contact(in_out,DATA,m);
    case 'PGD'
        [m,s_pgd]             = solver_PGD(in_out,DATA,m);
    case 'bigPGD'
        [m, s_big_particular] = solver_direct_part(in_out,DATA,m);  % Particular solution
%         [m, s_PGD]                   = solver_PGD_var_sep(in_out,DATA,m); % Classical PGD with separated representation of the operators 
        [m,s_big]             = solver_big_PGD(in_out,DATA,m);  % Solveur BigPGD  
       
end

% checking memory used
memory_check = monitor_memory_whos;
disp(['ROMlab: computing memory used...']);
disp(['    ... memory used: ',num2str(memory_check),' MB']);

tcodefin = toc(tcode);
disp(['ROMlab: total resolution time... ' num2str(tcodefin,2) ' s']);
disp('ROMlab: *** Success *** ');

%%%%%%%%%%%%
%   POST   %
%%%%%%%%%%%%

disp(' ')
disp(' ')

if ~in_out.cluster     % does not exist anymore, so condition always satisfied
    disp(' ')
    disp(' ')
    disp('*** Visualization - press *F5* or type *dbcont* to open paraview - type *dbquit* to exit ***')
    disp(' ')
    disp(' ')
    command = ['paraview ',in_out.out_dir,'output_1.pvd ','&'];
    %command = ['paraview --state=pv_viz/tutu.pvsm  --data-directory=',in_out.out_dir,'output_1.pvd ','&'];
    system(command);
end

%if in_out.debug == 1
%disp('saving profile...');
%profsave(profile('info'),strcat(in_out.out_dir,'profile/'));
%disp('...profile saved');

fold = [in_out.out_dir,'profile'];
profsave(profile('info'),fold);
profile viewer
profile off
profile clear
%end
diary off

% Run python script before script is over.
comm = ['python bin/other_functions/toto.py'];
% Save library paths
MatlabPath = getenv('LD_LIBRARY_PATH');
% Make Matlab use system libraries
setenv('LD_LIBRARY_PATH',getenv('PATH'))
system(comm);
% Reassign old library paths
setenv('LD_LIBRARY_PATH',MatlabPath)

% closing the parpool
delete(gcp('nocreate'));
warning on backtrace
