function ModifyInput(filename,X)
%% ModifyInput pour les fichiers Input de ROMlab
% Le programme vient lire le fichier dont le path est "filename"
% pour scanner les mots clés '%%%variable%%%'.
%
% Le i-eme mot-clé scanné est modifié par la i-eme valeur du cell X avec la contrainte suivante :
% La valeur doit être inscrite sur 14 bits (donc 7 chiffres après la virgule pour un >0, 6 sinon)
%
% On retourne ensuite au bit avant le mot-clé et on continue.
%
% A Améliorer : Seuls des nombres sur 14 bits peuvent remplacer les mots-clés à cause de
% l'ouverture du fichier en mode binaire... (Problème de décalage de carriage return sinon)
%
% Le mot-clé s'écrit dans le fichier input de la sorte :
% (Pas de signe égal pour éviter l'erreur dans l'éditeur Matlab)
% DATA.model(1).material.E          = 1;                  % Young's Modulus
% DATA.model(1).material.nu         %%%variable%%%;       % Poisson ratio
% DATA.model(1).material.density    = 2550;               % density

% open the file
fileID = fopen(filename,'r+');

% read the file
if fileID ~= -1
    fgetl(fileID);
    varID = 1;
    while ~feof(fileID)     % While it's not the End Of File
        out=fgets(fileID);  % Gets the line
        test=strfind(out,'%%%variable%%%'); % Search for the keyword
        if ~isempty(test)                   % If keyword is found
            % String Remplacement
            if X{varID} > 0
                out2=strrep(out, '%%%variable%%%', sprintf('=%1.7e',X{varID}));
            else
                out2=strrep(out, '%%%variable%%%', sprintf('=%1.6e',X{varID}));
            end
            fseek(fileID, -length(out), 0); % Back to the start of the keyword
            fwrite(fileID,out2);            % Write the number in X{varID}
            varID=varID+1;                  % Indent varID
        end
    end
end

% close the file
status=fclose(fileID);
if status~=0
    error('Probl?me lors de la fermeture du fichier OUT');
end
end