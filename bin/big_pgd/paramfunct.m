function gi = paramfunct(A,Xi,Ri,LH,UH,m,tiroir,ngroup,in_out)
% PGD fixed point algorithm, computes gi so that A*Xi*gi is close to Ri 
% gi : parameter functions (separated variables representation)
% A : stiffness matrix (separated variables representation)
% Xi : spatial vector of the Krylov Basis
% Ri : residual to minimize (separated variables representation)
% H : precond
% m : model
% tiroir : use of tiroir for parametric PGD decomposition 1=y 
% ngroup : number of tiroirs if tiroir
min_rez =0;


% keyboard
fixe_point = in_out.fixe_pointP;                  %  max number of iterations for fixed points algorithm
stagn_crit = in_out.stagn_crit;            %   Stagnation criterion for fixed points algorithm

for i = 1 : length(m.params)-1
    ndofparams(i) = m.params(i+1).ndof;
    pgdones{i} = ones(ndofparams(i),1);
end
gi= ktensor(pgdones);    % Init param function

for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
    pgdones{i} = ones(ndofparams(i),1);
end

IntXRlinit= ktensor(pgdones);

if tiroir
    % Group construction : 
    nparam = length(m.params)-1;
    sizegroup =  (length(m.params)-1)/ngroup;
    Alpha = zeros(ngroup,sizegroup); % Alpha : param of each group
    for ng = 1:ngroup  % Beta : params which are not in the group
        Beta{ng} = [1:nparam];
    end
    for group = 1:ngroup  
        Alpha(group,:) = [((group-1)*sizegroup +1):group*sizegroup];
        Beta{group}(((group-1)*sizegroup +1):group*sizegroup) = [];
        Beta{group} = fliplr(Beta{group}); % reverse Beta to use the ttv fonction during the integration
    end
    for i = 1:sizegroup % to init groups of params in fixed point iteration
        pgdgroup{i} = ones(ndofparams(i+1),1);
    end
    
end 

stagn = 1;
g2old = 1;
ptfixe = 1;


while (stagn > stagn_crit) && (ptfixe <=fixe_point)
    ptfixe = ptfixe + 1;
    
if tiroir ~= 1 % classical fixed point
    

if min_rez ==1  % Residual minimisation 
    for par = 2 : length(m.params) % loop on the params
        
        Den = zeros(m.params(par).ndof,size(A,2)^2);

% %         Numerator : int(RX)

        for k = 1:size(A,2)
            IntXR = Ri;
            for r = 2 : ndims(Ri)
                fin = ndims(Ri)-r+2;
                if fin ~= par
                    IntXR = ttv(IntXR,m.params(fin).integ2*(gi.U{fin-1}.*A(k).u{fin-1}),fin);
                end
            end
            IntXR = ttv(IntXR,(UH\(LH\A(k).k))*Xi,1);
            IntXR = redistribute(IntXR,1);
            IntXR.U{1} =  IntXR.U{1}.*repmat( A(k).u{par-1}, 1, size(Ri.lambda,1) );
            if k==1
                SNum = double(IntXR);
            else
                SNum = SNum + double(IntXR);
            end
        end

        % Denominator : int(XAX);
        for k1 = 1:size(A,2)
            for k2 = 1:size(A,2)
                IntXAX = 1;
                for i = 2 : ndims(Ri)
                    fin = ndims(Ri)-i+2;
                    if fin ~= par
                            IntXAX = IntXAX* ((gi.U{fin-1}.*A(k1).u{fin-1})'*m.params(fin).integ2*(gi.U{fin-1}.*A(k2).u{fin-1}));
                    end
                end

                Den(:,(k1-1)*size(A,2)+k2) = (Xi'*A(k1).k*(UH\(LH\A(k2).k))*Xi) * IntXAX *(A(k1).u{par-1}.*A(k2).u{par-1});
            end

        end  
         SDen = sum(Den,2);% denominator
         
         gi.U{par-1} = SNum./SDen; % Param function
         
         % Normalisation : for all but the last param
         if par ~= length(m.params)
             gi.U{length(m.params)-1}= gi.U{length(m.params)-1}*sqrt(gi.U{par-1}'*m.params(par).integ2*gi.U{par-1});
             gi.U{par-1} = gi.U{par-1}/ sqrt(gi.U{par-1}'*m.params(par).integ2*gi.U{par-1});
         end
    end
    
    
else   % Galerkin method
    
    %  % % En cours de modif !!
%     keyboard
%     IntXR = Ri;
%     for i = 2 : ndims(Ri)
%         fin = ndims(Ri)-i+2;
%         IntXR = ttv(IntXR,m.params(fin).integ2*gi.U{fin-1},fin);
%     end
%     IntXR = redistribute(IntXR,1);
%     IntXR = IntXR.U{1}'*Xi;
    for par = 2 : length(m.params) % loop on the params

        Num = zeros(m.params(par).ndof,size(Ri.lambda,1));
        Den = zeros(m.params(par).ndof,size(A,2));

% %         Numerator : int(RX)
        IntXR = Ri;
            for i = 2 : ndims(Ri)
                fin = ndims(Ri)-i+2;
                if fin ~= par
                    IntXR = ttv(IntXR,m.params(fin).integ2*gi.U{fin-1},fin);
                end
            end
            IntXR = ttv(IntXR,Xi,1);
            IntXR = redistribute(IntXR,1);
            SNum = double(IntXR);

%             %keyboard % En cours de modif !!
%             IntXRbar = IntXR./(Ri.U{par}'*m.params(fin).integ2*gi.U{fin-1})
%             num= IntXR*Ri.U{par}*diag(1./(Ri.U{par}'*m.params(fin).integ2*gi.U{fin-1}));
%             SNum = sum(num,2);
%             IntXR = 
%             num = IntXR./(Ri.U{par}'*m.params(fin).integ2*gi.U{fin-1})*Ri.U{par}
%         num = Ri.U{par}*diag((IntXR./(Ri.U{par}'*m.params(fin).integ2*gi.U{fin-1})))
%         SNum = sum(num,2);

%            IntXRbar = IntXR./(Ri.U{par}'*m.params(par).integ2*gi.U{par-1});
%             num = Ri.U{par}*diag( IntXRbar);
%             SNum = sum(num,2);
%           keyboard          

        % Denominator : int(XAX);
        for La = 1:size(A,2)
            IntXAX = 1;
            for i = 2 : ndims(Ri)
                fin = ndims(Ri)-i+2;
                if fin ~= par
                        IntXAX = IntXAX* ((gi.U{fin-1}.*A(La).u{fin-1})'*m.params(fin).integ2*gi.U{fin-1});
                end
            end

                Den(:,La) = (Xi'*A(La).k*Xi) * IntXAX *A(La).u{par-1};

        end  
         SDen = sum(Den,2);% denominator
         
         gi.U{par-1} = SNum./SDen; % Param function
         
         % Normalisation : for all but the last param
         if par ~= length(m.params)
             gi.U{length(m.params)-1}= gi.U{length(m.params)-1}*sqrt(gi.U{par-1}'*m.params(par).integ2*gi.U{par-1});
             gi.U{par-1} = gi.U{par-1}/ sqrt(gi.U{par-1}'*m.params(par).integ2*gi.U{par-1});
         end
    end
    
end
    
    % L2-Norm of the mode and stagnation :
    g2 = gi.U{length(m.params)-1}'*m.params(length(m.params)).integ2*gi.U{length(m.params)-1};
    stagn = sqrt(abs(g2-g2old)/g2old);
    disp(['    Stagnation for fixed point iteration ',num2str( ptfixe-1),' : ',num2str(stagn)]);
    g2old = g2;

else % tiroir == 1
%%%%%  COMPUTE FUNCTIONS OF groups of PARAMETERS G %%%


        for group =1:ngroup % loop on the groups
                IntXR = Ri;      
                
                % Compute Num : int Ri*X.u

                IntBeta{size(Beta{group},2)}=[];
                ii=0;
                for i = Beta{group}
                    ii=ii+1;
                    IntBeta{ii} = m.params(i+1).integ2*gi.U{i};
                end
                IntXR= ttv(IntXR,IntBeta,Beta{group}+1);

                IntXR = ttv(IntXR,Xi,1);
                IntXR  = redistribute(IntXR ,1);
                Num = double(IntXR);
                               

                % Compute Den : int X*A*X
                for La = 1:size(A,2)
                    
                    Au = cell(A(La).u){2};
                    AuBeta = Au(Beta{group});
                    gcell = cell(gi){2}(Beta{group});
                    Augi = cellfun(@(a,b) a.*b ,gcell,AuBeta,'UniformOutput',false);
                    Augi = ktensor(Augi);
                    IntXAX   = ttv(Augi,IntBeta);                 

                    IntXAX = IntXAX*(Xi'*A(La).k*Xi);

                    Dtensor = ktensor(IntXAX,Au(Alpha(group,:)));
                    if La == 1
                        Den = full(Dtensor);
                    else
                        Den = Den + full(Dtensor);
                    end

                end  

                 G = Num./Den;
                 G = cp_als(G,1,'printitn',0); % First order HO-SVD
                 G.U{1} = G.lambda *  G.U{1};
                 alpha = 0;

                 if any ((length(m.params)-1)==Alpha(group,:))% /!\ Start with the last param, 
                     % not to overwite on the normalization of the other params of the group 
                     gi.U{length(m.params)-1} = G.U{ndims(G)};                     
                 end
                 
                 for dt = Alpha(group,:)
                     if dt ~=  (length(m.params)-1)% For all but the last param
                     alpha = alpha+1;
                     gi.U{dt} = G.U{alpha};
                     % normatlisation
                         gi.U{length(m.params)-1} =  gi.U{length(m.params)-1}*sqrt(gi.U{dt}'*m.params(dt+1).integ2*gi.U{dt});
                         gi.U{dt} =  gi.U{dt}/sqrt(gi.U{dt}'*m.params(dt+1).integ2*gi.U{dt});
                     end
                 end

        end   
    % L2-Norm of the mode and stagnation :
    g2 = gi.U{length(m.params)-1}'*m.params(length(m.params)).integ2*gi.U{length(m.params)-1};
    stagn = sqrt(abs(g2-g2old)/g2old);
    disp(['       Stagnation for fixed point iteration ',num2str( ptfixe-1),' : ',num2str(stagn)]);
    g2old = g2;
end

end