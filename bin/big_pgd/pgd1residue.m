function dU = pgd1residue(A,X,b,H,m,in_out)
% Calcul de dU, la 1PGD de R = b - AX au sens du produit scalaire H
% gi : parameter functions (separated variables representation)
% /!\ This version doesn't compute gi !
% A : stiffness matrix (separated variables representation)
% X : Vector, (separated var representation)
% b : loading vector (separated variables representation)
% OR 0, if we only approximate R = -AX
% H : precond matrix
% m : model
% in_out : parameters of the resolution

ddlu=m.dofuu;
fixe_point = in_out.fixe_pointK;                  %  max number of iterations for fixed points algorithm
stagn_crit = in_out.stagn_crit;            %   Stagnation criterion for fixed points algorithm
tiroir         = 0;%in_out.tiroir;
ngroup = in_out.ngroup;
spatial     = in_out.spatial;         % Chosen algo to compute the vector

for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
    pgdones{i} = ones(ndofparams(i),1);
end

dU = ktensor(pgdones);
IntXblinit= ktensor(pgdones);

% Hinv = H;%inv(H);%eye(size(H,1));%inv(H);

if tiroir==1
    % Group construction : 
    nparam = length(m.params)-1;
    sizegroup =  (length(m.params)-1)/ngroup;
    Alpha = zeros(ngroup,sizegroup); % Alpha : param of each group
    for ng = 1:ngroup  % Beta : params which are not in the group
        Beta{ng} = [1:nparam];
    end

    for group = 1:ngroup  
        Alpha(group,:) = [((group-1)*sizegroup +1):group*sizegroup];
        Beta{group}(((group-1)*sizegroup +1):group*sizegroup) = [];
        Beta{group} = fliplr(Beta{group}); % reverse Beta to use the ttv fonction during the integration
    end
    for i = 1:sizegroup % to init groups of params in fixed point iteration
        pgdgroup{i} = ones(ndofparams(i+1),1);
    end
end 

%%%% GENERATion of spatial MODE %%%%
ptfixe = 1;
u2old = 10000;
stagn = 1;

AA = struct2cell(A);
Au = AA(2,1,:);
AK =  AA(1,1,:);
    
%%% Computation of the r�sidual 

            %Product A*X :

                for k = 1 : length(A)
                    a = X;
                    a.U{1} = A(k).k*X.U{1};
                    for l = 2 : ndims(X)
                        for Ls=1:size(X.lambda,1)
                            a.U{l}(:,Ls) = X.U{l}(:,Ls).*A(k).u{l-1};
                        end
                    end
                    if k ==1 
                        ASol = a;
                    else
                    ASol = ASol +  a;
                    end
                end
    if max(size(b)) > 1
        Residual = b-ASol;
    else
        Residual = -ASol;
    end
    Residual = redistribute(Residual, 2);
    
if strcmp(spatial,'eigPb')
% % Calcul of int_I (R H-1 R^T)

    int = ones(size(Residual.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(Residual{par}'*m.params(par).integ2*Residual{par});
    end
    RR = Residual{1}*int*Residual{1}';

Ix =H;%inv(H);%inv(H);%m.params(1).integ2(ddlu,ddlu);%eye(size(H));%

% First eigenvector
[vect, val] = eigs(RR,Ix,1);

test.U{1}  = vect;

test2 = test.U{1}'*(H*test.U{1});
test.U{1} = test.U{1}/sqrt(test2);

dU.U{1} = test.U{1} ;

elseif strcmp(spatial,'fixedP')   % Fixed point algorithm !



%%% /!\ H-preconditioned fixed point ... a r�parer !!

% % while (stagn > stagn_crit) && (ptfixe <=fixe_point)
% while (stagn > 10^-5) && (ptfixe <=fixe_point)
%     ptfixe = ptfixe + 1;
%     tptfixe =tic;
%     %%%% COMPUTE FUNCTIONS OF SPACE %%%%
%     % int gb = intgb
%     % int gAX = int AgX = int Ay = intAy
%     u2 = 1;
%     for i = 2 : ndims(dU)
%         u2 = u2 * dU.U{i}'*m.params(i).integ2*dU.U{i};
%     end
%     
%     y = X;
%     intAy = zeros(size(y.U{1},1),1);
%     
%  %%%%%%%%%%%%%%  A revoir :-S
% %     Xcell = cell(X){2}; 
% % 
% %     AX = cellfun(@(K) K*Xcell{1},AK,'UniformOutput',false);
% %     AX = reshape(cell2mat(AX),[size(X.U{1},1),size(X.lambda,1) * length(A)]);
% %     
% %     Xcell = cell(X){2};
% %     Lam = zeros(ndims(dU)-1,size(X.lambda,1) * length(A));
% %     for par = 1:ndims(dU)-1
% %         KX = cellfun(@(K) (Xcell{par+1}.*repmat(dU{par+1},1,size(X.U{par+1},2)))'*m.params(par+1).integ2*K.U{par},Au,'UniformOutput',false);
% %         Lam(par,:) = reshape(cell2mat(KX),[1,size(X.lambda,1) * length(A)]);
% %     end
% %     
% %     intAy2 = double(ktensor(prod(Lam)',AX));
% 
%     for Lr = 1:size(y.lambda,1)
%         for i = 2 : ndims(dU)
%             y.U{i}(:,Lr) = y.U{i}(:,Lr).*dU.U{i};
%         end
%         
%         for k = 1 : length(A)
%             a = 1;
%             for l = 2 : ndims(dU)
%                 a = a * y.U{l}(:,Lr)'*m.params(l).integ2*A(k).u{l-1};
%             end
%             intAy = intAy + a * A(k).k*y.U{1}(:,Lr);
%         end
%         
%     end 
% 
%     if max(size(b)) > 1
%         intub = b;
%         Vectint{ndims(dU)-1}=[]; 
%         for i = 2 : ndims(dU)
%             Vectint{i-1} = m.params(i).integ2*dU.U{i};
%         end
%         intub = ttv(intub,Vectint,[2:ndims(dU)]);  
% 
%         intub = sum(double(intub),2);
%         dU.U{1} = (intub-intAy)/u2;
%     else
%         dU.U{1} = (-intAy)/u2;
%     end
%     
%     %%%% COMPUTE FUNCTIONS OF PARAMETERS %%%%
%     
% if tiroir ~= 1
%     
% %     %%%%%%%%%% Pt fixe, classique
% %             for par = 2 : length(m.params) 
% %                 
% %                 if max(size(b)) > 1
% %                 IntXb = b;
% %                 Num = zeros(m.params(par).ndof,size(b.lambda,1));
% %                 
% %                 % Compute Num : int b*X.u
% %                 for l = 1:size(b.lambda,1) 
% %                     IntXbl = IntXblinit;
% %                     IntXbl.U{1} = IntXb.U{1}(:,l);
% %                     for j = 2:ndims(b)
% %                         IntXbl.U{j} = IntXb.U{j}(:,l);
% %                     end
% % 
% %                     for i = 2 : ndims(b)
% %                         fin = ndims(b)-i+2;
% %                         if fin ~= par
% %                             IntXbl = ttv(IntXbl,m.params(fin).integ2*dU.U{fin},fin);
% %                         end
% %                     end
% %                     IntXbl = redistribute(IntXbl,2);
% %                     Num(:,l)  = (dU.U{1}'*Hinv*IntXbl.U{1})*IntXbl.U{2}; 
% %                 end  
% %                 intDub = sum(Num,2);
% %                 end
% %                 % Compute Num : int dU*A*X0
% % 
% %                         for k = 1 : length(A)
% %                             for Lr = 1:size(X.lambda,1)
% %                             IntXA = 1;
% %                             for i = 2 : ndims(dU)
% %                                 fin = ndims(dU)-i+2;
% %                                 if fin ~= par
% %                                     IntXA = IntXA* (dU.U{fin}'*m.params(fin).integ2*(A(k).u{fin-1}.*X.U{fin}(:,Lr))) ;
% %                                 end
% %                             end
% % 
% %                             if k == 1 && Lr == 1
% %                                 UHinvAX =  IntXA *(dU.U{1}'*Hinv*A(k).k*X.U{1}(:,Lr)) * (A(k).u{par-1}.*X.U{par}(:,Lr));
% %                             else
% %                                 UHinvAX = UHinvAX +  IntXA *(dU.U{1}'*Hinv*A(k).k*X.U{1}(:,Lr)) * (A(k).u{par-1}.*X.U{par}(:,Lr));
% %                             end
% %                             end
% %                         end
% %                 
% %                 % Compute Den : int X*A*X
% % 
% %                     IntXAX = 1;
% %                     for i = 2 : ndims(dU)
% %                         if i ~= par
% %                            IntXAX = IntXAX* (dU.U{i}'*m.params(i).integ2*dU.U{i});
% %                         end
% %                     end
% %                     IntXAX = IntXAX*dU.U{1}'*Hinv*dU.U{1};
% %                     
% %                 if max(size(b)) > 1
% %                  dU.U{par} = ( intDub-UHinvAX) / IntXAX;
% %                 else
% %                     dU.U{par} = (-UHinvAX) / IntXAX;
% %                 end
% % 
% %                  % Normalisation : for all but the last param
% %                  if par ~= length(m.params)
% %                      dU.U{length(m.params)}= dU.U{length(m.params)}*sqrt(dU.U{par}'*m.params(par).integ2*dU.U{par});
% %                      dU.U{par} = dU.U{par}/ sqrt(dU.U{par}'*m.params(par).integ2*dU.U{par});
% %                  end
% % 
% %             end
% %     
% %     
%     
%     
%     %%%%%%%%% Fast
% 
%     U2 = dU.U{1}'*(H\dU.U{1});
% 
%     Xcell = cell(X){2};
%     KX = cellfun(@(K) dU.U{1}'*(H\K)*Xcell{1},AK,'UniformOutput',false);
%     lambda  =reshape(cell2mat(KX),[1,size(X.lambda,1) * length(A)]);
%     
%     for par = 1 : ndims(dU)-1
%         cellu = cellfun(@(u) Xcell{par+1}.*repmat(u{par},1,size(X.U{par+1},2)) ,Au, 'UniformOutput',false);
%         UHinvAXcell{par} = reshape(cell2mat(cellu),[size(X.U{par+1},1),size(X.lambda,1) * length(A)]);
%     end
%     UHinvAX = ktensor(lambda',UHinvAXcell);
%     
% %     clear a;
% %     a = ktensor;
% %     a.lambda = 1;
% %     for Lr = 1:size(X.lambda,1) % Plus lent
% %     for k = 1 : length(A)
% %         for l = 2 : ndims(dU)
% %             a.U{l-1} = X.U{l}(:,Lr).*A(k).u{l-1};
% %         end
% %         if k == 1 && Lr == 1
% %             UHinvAX = a * (dU.U{1}'*Hinv*A(k).k*X.U{1}(:,Lr));
% %         else
% %             UHinvAX = UHinvAX + a * (dU.U{1}'*Hinv*A(k).k*X.U{1}(:,Lr));
% %         end
% %     end
% %     end
% 
% %     keyboard
% 
%     if max(size(b)) > 1
%         UHinvb = ttv(b,H\dU.U{1},1);
%         u = (UHinvb-UHinvAX)*(1/U2);
%     else
%         u = (-UHinvAX)*(1/U2);
%     end
%     if ndims(u) ==1
%         u1 = ktensor(1,double(u));
%     else
%         u1 = cp_als(u,1,'printitn',0);
%         u1 = redistribute(u1,ndims(u1));
%     end
% 
%     for i = 2 : ndims(dU)
%         dU.U{i} = u1.U{i-1};
%     end
%     
% else % tiroir ==1 % Don't use (piti probleme) !
% 
% %       for group =1:ngroup % loop on the groups
% %           
% %                 IntDen = dU;
% %                 % Compute Den : intG (RHRg²)
% %                 
% %                 for i = Beta{group}
% %                     IntDen = ttv(IntDen,m.params(i+1).integ2*dU.U{i+1},i+1);
% %                 end                
% %                 
% %                 IntDen = ttv(IntDen, Hinv*dU.U{1},1);
% %                 Den = full(IntDen);
% %                 
% %                 % Compute Num : int Ri*X.u
% %                  
% %                      for k = 1 : length(A)
% %                         IntAXg = X;
% %                         for i = Beta{group}
% %                             IntAXg =  ttv(IntAXg,m.params(i+1).integ2*(A(k).u{i}.*dU{i+1}),i+1);
% %                            
% %                         end
% % %                         keyboard
% %                         IntAXg = ttv(IntAXg,Hinv*A(k).k*dU{1},1);
% % 
% % 
% %                         for Lr = 1:size(X.lambda,1)
% %                             alpha = 0;
% %                             for dt = Alpha(group,:)
% %                                 alpha = alpha+1;
% %                                 IntAXg.U{alpha}(:,Lr) = IntAXg.U{alpha}(:,Lr).*A(k).u{dt};%.*dU{dt+1};
% %                             end
% %                         end
% %                         if k == 1
% %                             UHinvAX =  IntAXg;
% %                         else
% %                             UHinvAX = UHinvAX +  IntAXg;
% %                         end
% % 
% %                      end
% %                      if max(size(b)) > 1
% %                          Intbg = b;
% %                          for i = Beta{group}
% %                              Intbg = ttv(Intbg,m.params(i+1).integ2*dU.U{i+1},i+1);
% %                          end
% %                          Intbg = ttv(Intbg,Hinv*dU.U{1},1);
% %                          Num = full(Intbg-UHinvAX);
% %                      else
% %                          Num = full(-UHinvAX);
% % 
% %                      end
% %                  
% %                  
% %                  G = Num./Den;
% %                  G = cp_als(G,1,'printitn',0); % First order HO-SVD
% %                  G.U{1} = G.lambda *  G.U{1};
% %                  alpha = 0;                        if k == 1
% %                             UHinvAX =  IntAXg;
% %                         else
% %                             UHinvAX = UHinvAX +  IntAXg;
% %                         end
% % 
% %                  
% %                  if any ((length(m.params)-1)==Alpha(group,:))% /!\ Start with the last param, 
% %                      % not to overwite on the normalization of the other params of the group 
% %                      dU.U{length(m.params)} = G.U{ndims(G)};                     
% %                  end
% %                  
% %                  for dt = Alpha(group,:)
% %                      if dt ~=  (length(m.params)-1)% For all but the last param
% %                      alpha = alpha+1;
% %                      dU.U{dt+1} = G.U{alpha};
% %                      % normatlisation
% %                          dU.U{length(m.params)} =  dU.U{length(m.params)}*sqrt(dU.U{dt+1}'*m.params(dt+1).integ2*dU.U{dt+1});
% %                          dU.U{dt+1} =  dU.U{dt+1}/sqrt(dU.U{dt+1}'*m.params(dt+1).integ2*dU.U{dt+1});
% %                      end
% %                  end
% % 
% %                      
% %       end
%                    
%     
%     
% end
%     %%%% CONTROL THE STAGNATION %%%%
%     
%     stagn = abs(u2-u2old)/sqrt(u2old);
%     disp(['       Stagnation for fixed point iteration ',num2str( ptfixe-1),' : ',num2str(stagn), '   (... in ',num2str(toc(tptfixe)) ,' s)']);
% %      keyboard  
%     u2old = u2;
% 
%     
% end
% % dU = Residual;
% 
% U2 = dU.U{1}'*(H\dU.U{1});
% %%%% NORMALIZATION WITH RESPECT TO Hinv %%%%
% dU.U{1} = dU.U{1}/sqrt(U2);
% dU.U{ndims(dU)} = dU.U{ndims(dU)}*sqrt(U2);
% % 

%%%% fast non-preconditioned fixed-point :

Rez_comp= cp_als(Residual,1,'printitn',0);
Rez_comp= redistribute(Rez_comp,ndims(Rez_comp));
dU.U{1} = H\Rez_comp.U{1};

end