function k = pgdevaloperator(kpgd,params)

lparams = sprintf('%.0f,' , params);
lparams = lparams(1:end-1);% strip final comma
k = sparse([],[],[],size(kpgd(1).k,1),size(kpgd(1).k,2));
for i = 1 : length(kpgd)
    ui = eval(strcat('kpgd(i).u(',lparams,');'));
    k = k + ui*kpgd(i).k;
end