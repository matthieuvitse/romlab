function m=pgdmakebc(DATA,m)
%  J.C. Passieux (2011)


% All informations concerning parameters
m.params(1).name = 'M';
m.params(1).ndof = m.ndof ;
m.params(1).mesh = {'m.elem' 'm.node'};
sizedofs = m.params(1).ndof;

for i = 2 : length(DATA.params)
    m.params(i).name = DATA.params(i).name;
    m.params(i).ndof = length(DATA.params(i).mesh);
    m.params(i).mesh = DATA.params(i).mesh;
    m.params(i).type = DATA.params(i).type;
    m.params(i).unit = DATA.params(i).unit;
    m.params(i).val = DATA.params(i).val;
    m.params(i).box = DATA.params(i).box;
    sizedofs = [ sizedofs m.params(i).ndof ];
end
if length(sizedofs) == 1
    sizedofs = [ sizedofs 1 ]
end

m.dofud=[]; %m.repud=[];
for i_model = 1 : numel(m.model)
    m.model(i_model).dofud = [];
    m.model(i_model).dofuu = [];
    m.model(i_model).dofud_corres = [];

    param_fu = '';
    for i_param = 2 : length(DATA.params)
        param_fu = [param_fu,',sizedofs(',num2str(i_param),')'];        
    end
    eval (strcat('m.model(i_model).fu=sptensor([m.model(i_model).ndof',param_fu,']);'));
    
end




m.elem_in_box = [];   % elements in boxes where damage will be imposed to zero


m.repvd=[]; 
m.fu=ktensor;
m.ud=ktensor;
m.vd=ktensor;

for k=1:size(DATA.load,2)
    switch lower(DATA.load(k).type)
        case {'displ','depl'}
            m=pgdmakebc_disp(m,DATA.load(k));
        case {'velocity'}
            m=makebc_velo(m,DATA.load(k));
        case {'dnorm','sym'}
            m=makebc_dnorm(m,DATA.load(k));
        case {'stres','stress'}
            m=pgdmakebc_stress(DATA,m,DATA.load(k));
        case {'force'}
            m=makebc_force(m,DATA.load(k));
        otherwise
            error([DATA.load(k).type,' : unknown loading type.']);
    end
    if isfield(DATA.load(k),'dam_box')
        if ~isempty(DATA.load(k).dam_box)
            m=makebc_damage(DATA,m,DATA.load(k));
        end
    end
end

% delete multiple prescribed dofs and build global mapping
allbutM = '';
for i_M = 2 : length(m.params)
    allbutM = [ allbutM ',:' ];
end

for i_model = 1 : numel(m.model)
    [m.model(i_model).dofud,ia,~] = unique(m.model(i_model).dofud);
    m.model(i_model).dofud_corres = m.model(i_model).dofud_corres(ia);
    corres = sort([ m.model(i_model).dofuu m.model(i_model).dofud; m.model(i_model).dofuu_corres m.model(i_model).dofud_corres ]')';
    m.model(i_model).dof_corres = corres(2,:);
%     keyboard
%     if ~isempty(m.model(i_model).dofud_corres)
%         eval(strcat('m.ud(m.model(i_model).dofud_corres',allbutM,') = sptensor(m.model(i_model).ud(m.model(i_model).dofud',allbutM,'));'));
%     end
end

% m.dofud = unique([m.model(:).dofud_corres]); /!\ test !
m.dofud = m.model.dofud;
prescribed_dof = unique([m.dofud m.repvd]);
m.dofuu=1:m.ndof;
m.dofuu(prescribed_dof)=[];

disp(['    ... boundary conditions set... ',num2str(toc),' s']);
end
