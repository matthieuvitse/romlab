function m=pgdmakebc_disp(m,ld)
%  J.C. Passieux (2010)


    m.ud = ld.val;

for i_model = 1 : numel(m.model)
    m.model(i_model).dofuu=sort([m.model(i_model).node(:).dof],'ascend');
    m.model(i_model).dofuu_corres=sort([m.model(i_model).node(:).dof_corres],'ascend');
     for i=1:size(m.model(i_model).node,2)
        if(is_in_box(m.model(i_model).node(i).pos,ld.box))
            if numel(m.model(i_model).node(i).dof)>0
                Ubc(m.model(i_model).node(i).dof',1)=ld.val.U{1};
                m.model(i_model).dofud=[m.model(i_model).dofud m.model(i_model).node(i).dof];
                m.model(i_model).dofud_corres = [m.model(i_model).dofud_corres m.model(i_model).node(i).dof_corres];
            end
        end
    end
    m.model(i_model).dofuu([m.model(i_model).dofud]) = [];
    
    m.model(i_model).dofuu_corres([m.model(i_model).dofud]) = [];
end

m.ud.U{1} = Ubc;
end