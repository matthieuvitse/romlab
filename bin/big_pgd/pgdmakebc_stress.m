function m=pgdmakebc_stress(DATA,m,ld)
%  J.C. Passieux (2010)

m.fu = ld.val;
Fbc = zeros(m.params(1).ndof,1);

for i_model = 1 : numel(m.model)
for i=1:size(m.model(i_model).elem,2)
    nn=0;
    nd=[];
    for jj=1:numel(m.model(i_model).elem(i).node_num)
        j=m.model(i_model).elem(i).node_num(jj);
        if(is_in_box(m.model(i_model).node(j).pos,ld.box))
            nn=nn+1;
            nd=[nd j];
        end
    end
    
    if nn>(DATA.dim-1)
        switch nn
            case 2,
                Length=norm(m.model(i_model).node(nd(1)).pos-m.model(i_model).node(nd(2)).pos);
            case 3,
                vec1=m.model(i_model).node(nd(2)).pos-m.model(i_model).node(nd(1)).pos;
                vec2=m.model(i_model).node(nd(3)).pos-m.model(i_model).node(nd(1)).pos;
                   if DATA.dim == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                   end
                Length=norm(cross(vec1,vec2))/2;
            case 4,
                vec1=m.model(i_model).node(nd(2)).pos-m.model(i_model).node(nd(1)).pos;
                vec2=m.model(i_model).node(nd(3)).pos-m.model(i_model).node(nd(1)).pos;
                   if DATA.dim == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                   end                
                Length=norm(cross(vec1,vec2))/2;
                vec1=m.model(i_model).node(nd(3)).pos-m.model(i_model).node(nd(1)).pos;
                vec2=m.model(i_model).node(nd(4)).pos-m.model(i_model).node(nd(1)).pos;
                   if DATA.dim == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                   end                 
                Length=Length+norm(cross(vec1,vec2))/2;
            otherwise,
                disp(['elem ' num2str(i) ', nodes ',num2str(nd)]);
                error('unknown skin element!');
        end
        for i=1:nn
            repi=m.model(i_model).node(nd(i)).dof;
            Fbc(repi',1)=Fbc(repi',1)+Length*ld.val.U{1}/nn;
        end
    end
end
% keyboard
end
m.fu.U{1} = Fbc;

