function m=pgdstiffness(DATA,m)
%  J.C. Passieux (2010)
tic

disp(['esb3d: Assembling stiffness operator... ']);
m.B_g          = spalloc(m.ndofgp,m.ndof,1);

% ELEMENTARY STIFFNESS MATRICES


for i_model = 1 : numel(DATA.model)
    
    
    
    ne=size(m.model(i_model).elem,2);
    dof         = [m.model(i_model).node(:).dof];
    dof_corres  = [m.model(i_model).node(:).dof_corres];
    
    [B_g,detjac] = grad_op_glob(DATA,m.model(i_model),DATA.model(i_model).type);
    
    % GLOBAL STIFFNESS AND H1 MATRIX
    for i = 2 : length(m.params)
        foou{i-1} = ones(m.params(i).ndof,1);
    end
    m.model(i_model).k(1).k=sparse([],[],[],m.ndof,m.ndof);
    ui = ktensor(foou);
    m.model(i_model).k(1).u = ui;
    for i = 2 : length(m.params)
        m.model(i_model).k(i).k=sparse([],[],[],m.ndof,m.ndof);
        % REMARK: K = K1 + ui(Ei)*Ki with ui in percents
        ui = ktensor(foou);
%         keyboard
        ui.U{i-1} = (m.params(i).val)';
        m.model(i_model).k(i).u = ui;
    end
    m.model(i_model).h1=sparse([],[],[],m.ndof,m.ndof);

%     n = progress('init','Processing...');
%     t0 = toc; % or toc if tic has already been called
%     tm = t0;
% 
    z = [];
    for i=1:ne
%         if tm+1 < toc % refresh time every 1s only
%             tm = toc;
%             tt = ceil((toc-t0)*(ne-i)/i);
%             n  = progress(i/ne,sprintf('Processing... (estimated time: %ds)',tt));
%         else
%             n  = progress(i/ne,n);
%         end

        %%%% RUN %%%%
        % Hooke operator \int defor U * K * defor V
        elem(i).k=kelem(DATA,m.model(i_model).elem(i),DATA.model(i_model).material.hooke);

        % Find the relevant parameter zone
        for k = 2 : length(m.params)
            if(is_in_box(mean(m.model(i_model).elem(i).node_pos),m.params(k).box))
                z = [ z k ];
            end
        end
        m.model(i_model).k(1).k(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)=m.model(i_model).k(1).k(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)+elem(i).k;
        m.model(i_model).k(z(end)).k(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)=m.model(i_model).k(z(end)).k(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)+elem(i).k;
        % H1 operator \int defor U * defor V
        elem(i).k=kelem(DATA,m.model(i_model).elem(i),DATA.model(i_model).h1);
        m.model(i_model).h1(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)=m.model(i_model).h1(m.model(i_model).elem(i).dof,m.model(i_model).elem(i).dof)+elem(i).k;
    end
    
    
    m.k= m.model.k; % /!\ pour un seul model !
    m.h1= m.model.h1;
    m.model(i_model).B_g = B_g;

    


end
m.B_g = m.model.B_g;
%norm(full(m.k(1).k - (m.k(2).k + m.k(3).k + m.k(4).k + m.k(5).k + m.k(6).k+m.k(7).k+m.k(8).k+m.k(9).k)))

% progress('close')
disp([ '    ... operators assembled... ', num2str(toc,2) ' s']);
