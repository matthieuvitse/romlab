function [] = check_damage_law(DATA,s,varargin)

if nargin < 3
    eps_max  = max(max(max(double(full(s.eps)))));
else
    eps_max = varargin{1};
end
Y_max    = 1/2 * DATA.model(1).material.E * (eps_max)^2;

F_Y      = DATA.model(1).behavior.F_Y;
Y0       = DATA.model(1).material.Y0;
Adt      = 1/20 * DATA.model(1).material.Ad;
Y        = Y0 : .1 : round(Y_max);
toto     = F_Y(Y,Y0,Adt);
figure;plot(Y,toto)

end