function [ damage ] = reg_damage_delay(DATA,m,damage,t)
% damage-delay regularization

%tauc  = 1/2 * 1/n_step *  xt * tT;          % characteristic time
tauc  =  1 * 1/n_step *  xt * tT;            % characteristic time
a_ret = 1;                                   % some parameter supposedly always equal to one

dt      = zeros(1,nelem(m.elem));

dnr = damage(:,t,yyy);

if t == 1
    damage(:,t,yyy) = dt;
else
    damage(:,t,yyy) = damage(:,t-1,yyy) + (lT_loc(t) - lT_loc(t-1)) * 1/ tauc * (1 - exp(- a_ret * pos_part(dnr - hat_d_time(:,t-1,yyy))) ) ;
end

end

