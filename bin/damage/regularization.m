function [damage_reg] = regularization(in_out,DATA,damage,damage_prev_time,j,lT)

switch in_out.regularization
    case 0
        damage_reg = damage;
    case 1
        tauc = DATA.regularization.tauc;
        a_ret = 1;
        dnr = damage;           % value of damage without regularization
        if j == 1
            damage_reg = 0;
        else
            damage_reg = damage + (lT(j) - lT(j-1)) * 1/ tauc * (1 - exp(- a_ret * abs(dnr - damage_prev_time) ) );
        end
    otherwise
        warning on backtrace
        disp('WARNING: this regularization is not (yet) implemented')
        warning off backtrace
        foo;keyboard
end

end