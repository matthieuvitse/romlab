function [sigma_f,eps_f_max,eps_f] = unilateral(in_out,DATA,dam,eps,eps_f_old,sigma_f_old,delta_eps_f,hooke)

alpha_0 = 6.5;  % parameter controlling a reference variance of the event 'a crack closes'

% computation of the homogenized contribution of crack openings to the
% total strain of the RVE
eps_f = dam .* eps;

% computation of the maximum crack strain tensor
if sum_v(eps_f) > sum_v(eps_f_old)
    eps_f_max = eps_f;
else
    eps_f_max = eps_f_old;
end

if 1 == 1
    nu = 1 - 1 / ( 1 + exp(- alpha_0 * sum_v(eps_f) / sum_v(eps_f_max) ) );
    sigma_f = sigma_f_old +  nu * hooke * delta_eps_f;
else
    %this one does not work at all
    sigma_f = hooke * (eps_f - eps_f_max / alpha_0 * log(1 + exp(- alpha_0 * sum_v(eps_f) / sum_v(eps_f_max)  ) ) );
end
if ~isempty(find(isnan(sigma_f)))
    keyboard
end

end
