function OpInt=Simpson(f,x)

% simpson integration: int_a^b f(x) dx = (b-a)/6 [ f(a) + 4f( (a+b)/2 ) + f(b);

OpInt=sparse(length(x),length(x));

for i=1:length(x)-1
    OpInt(i,i:i+1)=1/6*(f(x(i))+4*f((x(i)+x(i+1))/2)+f(x(i+1)))*[-1 1];
end

end
