function A=aire_heron(n1,n2,n3)

%Calcul de l'aire d'un triangle bas� sur la formule de H�ron

    h1=norm(n1-n2);
    h2=norm(n3-n2);
    h3=norm(n1-n3);

    %Demi-p�rim�tre
    p=(h1+h2+h3)/2;

    %Aire (formule de H�ron)
    A=sqrt(p*(p-h1)*(p-h2)*(p-h3));

end