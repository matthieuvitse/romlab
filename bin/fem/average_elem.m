function m = average_elem(DATA,m)
% MV
% averages the gauss point fields on the elements. Also provides the
% reshaping of the voigt notation for visualization : 
% [XX YY ZZ sqrt(2)*XY sqrt(2)*XZ sqrt(2)*YZ]' ->  [XX YY ZZ XY YZ XZ]'

%tic
dim=DATA.dim;

I = [];
J = [0];
Av2 = []; %Av2 = spalloc(comp*numel(m.elem),comp*m.npg_tot,1); 

    compt_tot = 0;
for i_model = 1 : numel(m.model)

    if strcmp(DATA.model(i_model).type,'FE3D')
        comp = 6;
        correceps=[eye(dim) zeros(dim,2*dim-3) ; zeros(2*dim-3,dim)  1/sqrt(2)*eye(2*dim-3)];
        % this following line because Paraview's tensor ordering is XX YY ZZ XY YZ XZ
        correceps(5:6,5:6)=correceps(5:6,6:-1:5);  %[0 1*1/sqrt(2) ; 1*1/sqrt(2) 0];
    elseif strcmp(DATA.model(i_model).type,'FE2D')
        comp = 3;
        correceps=[eye(dim) zeros(dim,2*dim-3) ; zeros(2*dim-3,dim)  1/sqrt(2)*eye(2*dim-3)];
    elseif strcmp(DATA.model(i_model).type,'TRUSS')
        comp = 1;
        correceps = 1;
    end
    
    nelem = numel(m.model(i_model).elem);
    for i = 1 : nelem
        
        ngp = numel(m.model(i_model).elem(i).gp);
        I2 = repmat( compt_tot  + 1 : compt_tot + comp,comp,1) ;
        I3 = repmat(I2(:)',1,ngp);
        I = [I, I3 ];
        
        for pgj = 1 : ngp
            correceps2 = reshape(correceps',1,size(correceps,1)*size(correceps,2));
            Av2 = [Av2, 1/ngp .* correceps2];
            J2 = repmat((J(end)+1) : (J(end)+comp),1,comp);
            J = [J , J2];
        end
        compt_tot = compt_tot +  comp;    
    end
       
end

J = J(2:end);

Av3 = sparse(I,J,Av2);

m.Av = Av3;

end