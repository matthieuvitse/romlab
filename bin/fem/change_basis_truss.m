function [T] = change_basis_truss(n)

le = norm(n(1,:) - n(2,:));     %length of the element

lmn =  1/le * [-1 1] * n;

T = sparse(6,6);

T(1,1:3) = lmn;
T(4,4:6) = lmn;

end