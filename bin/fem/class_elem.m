function e=class_elem
%  J.C. Passieux (2010)

e.typel=''; 		    % element type
e.k=[];                 % elementary stiffness matrix
e.m=[];                 % elementary mass matrix
e.dof=[];               % rep of the element dof in the global dof vector
e.num=0;                % number of the element
%e.u=[];                % displacement (+ enrichments)
%e.sig=[];              % Stress tensor
%e.eps=[];              % Strain tensor
%e.principal_stress=[]; % Principal stress tensor
%e.vonmises=0;          % Von Mises stress
e.posc=[];              % center of the element
e.node_pos=[];		    % global position of the nodes of the element
e.node_posref=[];	    % local position of the nodes of the element
e.node_num=[];		    % number of the element
e.gp=[struct];		    % integration Gauss Points
e.N=[];                 % Shape functions
e.dN_xi=[];             % Gradient of the Shape functions wrt xi
e.dN_eta=[];		    % Gradient of the Shape functions wrt eta
e.dN_zeta=[];           % Gradient of the Shape functions wrt zeta
e.plot=1;               % Post processing plot element
e.h=[];
e.dofgp = [];
e.dofgp_corres = [];
e.num_loc = [];
e.num_glob = [];
e.dof_glob=[]; 

end