function m=class_mesh
%  J.C. Passieux (2010)

m.ndof=0;		% number of dof
m.k=[]; 		% stiffness matrix
m.m=[]; 		% mass matrix
m.fu=[]; 		% prescribed force
m.ud=[]; 		% prescribed displacement
m.u=[]; 		% displacement (+ enrichments)
m.a=[]; 		% acceleration (+ enrichments)
m.v=[]; 		% velocity (+ enrichments)
m.dofuu=[];		% all dofs except from the dirichlet for substitution
m.dof_eff=[];   % dofs in rigid part
m.c=sparse([]);		% constraint operator for applying conditions by Lagrange multiplier
m.cf=[];		% right hand side for applying conditions by Lagrange multiplier
m.h=0;			% characteristic size of the elements

m.node=class_node; % nodes
m.elem=class_elem;	% elements

end
