function n=class_node
%  J.C. Passieux (2010)

n.pos=[];           % position [x y]
n.num=0;            % num in original mesh
n.done=0;           % for connectivity
n.dof=[];           % rep in the element vector of unknown
n.pnum=0;           % num for postprocessing
n.u=[];             % displacement (+ enrichments)
n.elem=[];          % elements where the node belongs       
n.corres_model1=[]; % ddl correspondance to master model
n.dof_corres=[];    % local ddls relative to models

n.num_loc=[];     % local numbering relative to DDM
n.num_glob = [];  % global numbering relative to DDM
n.dof_loc=[];           % local numbering relative to DDM
n.domains=[];
n.interf_primal=[];
n.interf_dual=[];
n.connected_domains=[];
n.interf_dual_val=[];

end
