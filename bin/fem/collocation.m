function Pc=collocation(m,mref)

Pc=sparse(m.ndof,m.ndof);


figure
hold on
axis equal
list=[];
cpt=0;


for i=1:numel(m.node)
    
    if any(m.node(i).rep)
        x=m.node(i).pos(1);
        y=m.node(i).pos(2);

        for j=1:numel(mref.elem)
                %%%Element
                    if point_in_triangle(m.node(i).pos,mref.elem(j).node_pos(1,:),mref.elem(j).node_pos(2,:),mref.elem(j).node_pos(3,:))
                        break
                    end
    %             keyboard
        end

            
            eref=mref.elem(j);

            x1=eref.node_pos(1,1);
            y1=eref.node_pos(1,2);
            x2=eref.node_pos(2,1);
            y2=eref.node_pos(2,2);
            x3=eref.node_pos(3,1);
            y3=eref.node_pos(3,2);
            
            list=[list j];

            coord_iso=[x2-x1 x3-x1;y2-y1 y3-y1]\[x-x1;y-y1];
            xi=coord_iso(1);
            eta=coord_iso(2);
            
    %         liste_node=eref.node_num;
    %         rep_ref=zeros(1,length(liste_node));
    %         
    %         for r=1:length(liste_node)
    %             rep_ref(2*r-1:2*r)=mref.node(liste_node(r)).rep;
    %             rep_ref=sort(rep_ref);
    %             rep_ref(r)=mref.node(liste_node(r)).rep(1);
    %             rep_ref(length(liste_node)+r)=mref.node(liste_node(r)).rep(2);
    %         end


            rep_ref=zeros(1,length(eref.rep));

            for r=1:length(rep_ref)/2
                rep_ref(r)=eref.rep(2*r-1);
                rep_ref(length(eref.rep)/2+r)=eref.rep(2*r);
            end


            N=[ eref.N(xi,eta) 0*eref.N(xi,eta) ;
                0*eref.N(xi,eta)  eref.N(xi,eta)];
%             keyboard
            X=N*[eref.node_pos(:,1);eref.node_pos(:,2)];
            
            plot(X(1),X(2),'o')
    %       
            Pc(m.node(i).rep,rep_ref)=Pc(m.node(i).rep,rep_ref)+N;
          
            cpt=cpt+1;
%             keyboard
    end
end
% cpt
length(list)
list=unique(list);
length(list)

        for i=1:numel(m.elem)

                line([m.elem(i).node_pos(1,1) m.elem(i).node_pos(2,1)],[m.elem(i).node_pos(1,2) m.elem(i).node_pos(2,2)],'Marker','.','Color','b','LineStyle','-');
                line([m.elem(i).node_pos(2,1) m.elem(i).node_pos(3,1)],[m.elem(i).node_pos(2,2) m.elem(i).node_pos(3,2)],'Marker','.','Color','b','LineStyle','-');
                line([m.elem(i).node_pos(3,1) m.elem(i).node_pos(1,1)],[m.elem(i).node_pos(3,2) m.elem(i).node_pos(1,2)],'Marker','.','Color','b','LineStyle','-');

        end
end













