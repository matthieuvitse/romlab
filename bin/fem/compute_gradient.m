function [B, det_jac]=compute_gradient(dim,gp,e,type,flag)
%  J.C. Passieux (2010)

if strcmp(type,'TRUSS')
    %dN_xi=gp.dN_xi;
    n=e.node_pos;
    
    dx12 = n(2,:) - n(1,:);
    
    t12 = (dx12) / norm(dx12);
    
    if flag
        if dim == 3
        B = 1/norm(dx12) * [ -t12(1) -t12(2) -t12(3) t12(1) t12(2) t12(3)];
        elseif dim == 2
            B = 1/norm(dx12) * [ -t12(1) -t12(2)  t12(1) t12(2) ];
        end
        %B = [ -dN_xi(1) -dN_xi(2) 0 dN_xi(1) dN_xi(2) 0];
    else
        B = [];
    end
    
    jac = norm(dx12)/2;
    
elseif strcmp(type,'FE2D')
    
    dN_eta=gp.dN_eta;
    dN_xi=gp.dN_xi;
    n=e.node_pos;
    
    % Jacobian matrix
    jac = [ dN_xi*n(:,1) dN_eta*n(:,1)
        dN_xi*n(:,2) dN_eta*n(:,2)  ];
    % inv du Jac
    jaci = inv(jac);
    dN = jaci' * [dN_xi;dN_eta];
    dN_x = dN(1,:);
    dN_y = dN(2,:);
    
    if flag
        B=zeros(3,2*size(n,1));
        for j= 1:size(n,1)
            B(:,(2*j-1):2*j)=[dN_x(j)  0.; 0.  dN_y(j) ; dN_y(j)  dN_x(j)];
        end
        B(3,:) = 1/sqrt(2)*B(3,:);
    else
        B = [];
    end
    
elseif strcmp(type,'FE3D')
    
    dN_eta=gp.dN_eta;
    dN_xi=gp.dN_xi;
    dN_zeta=gp.dN_zeta;
    n=e.node_pos;
    
    % Jacobian matrix
    jac = [ dN_xi*n(:,1) dN_eta*n(:,1) dN_zeta*n(:,1)
        dN_xi*n(:,2) dN_eta*n(:,2) dN_zeta*n(:,2)
        dN_xi*n(:,3) dN_eta*n(:,3) dN_zeta*n(:,3)];
    % inv du Jac
    jaci = inv(jac);
    dN = jaci' * [dN_xi;dN_eta;dN_zeta];
    dN_x = dN(1,:);
    dN_y = dN(2,:);
    dN_z = dN(3,:);
    
    if flag
        B=zeros(6,3*size(n,1));
        for j=1:size(n,1)
            B(:,(3*j-2):3*j)=[  dN_x(j)  0.      0.
                0.       dN_y(j) 0.
                0.       0.      dN_z(j)
                dN_y(j)  dN_x(j) 0.
                dN_z(j)  0.      dN_x(j)
                0.       dN_z(j) dN_y(j) ];
        end
        B(4:6,:) = 1/sqrt(2)*B(4:6,:);
    else
        B = [];
    end
    
end

det_jac=det(jac);

end
