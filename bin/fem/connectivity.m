function m=connectivity(DATA,m)

tic
disp('ROMlab: Building connectivity...')
[m,~] = map_nodes(DATA,m,1,1);
if numel(m.model) > 1 %&& ~strcmp(DATA.assembly,'contact')   % DATA.contact : TEMPORARY variable for David - Rafael's project
    [m,~] = map_nodes(DATA,m,1,2);
end

cdof=0;
elem_list = [0];
elem_dof_list = [0];
for i_model = 1 : numel(DATA.model)
    
    switch m.model(i_model).elem(1).typel
        case 'bar2'
            comp = 1;
        case 'qua4'
            comp = 3;
        case 'tri3'
            comp = 3;
        case 'tri6'
            comp = 3;
        case 'tet4'
            comp = 6;
        case 'cub8'
            comp = 6;
        otherwise
            foo;keyboard
    end
    
    ne=numel(m.model(i_model).elem);
    
    cdof2 = 0;
    
    for i=1:ne
        for jj=1:size(m.model(i_model).elem(i).node_num,1)
            j=m.model(i_model).elem(i).node_num(jj);
            if(m.model(i_model).node(j).done==0)
                
                if i_model > 1
                    if m.model(i_model).node(j).corres_model1 ~=0  && strcmp(DATA.assembly,'reinforcement')
                        corres_nodes_model_1 = m.model(i_model).node(j).corres_model1;
                        m.model(i_model).node(j).dof_corres = m.model(1).node(corres_nodes_model_1).dof;
                        m.model(i_model).node(j).dof = cdof2 + [1:DATA.dim];
                    elseif strcmp(DATA.assembly,'contact')
                        m.model(i_model).node(j).dof = cdof2 + [1:DATA.dim];
                        m.model(i_model).node(j).dof_corres = cdof+[1:DATA.dim];
                        cdof = cdof+DATA.dim;
                    end
                else
                    m.model(i_model).node(j).dof=cdof+[1:DATA.dim];
                    m.model(i_model).node(j).dof_corres = m.model(1).node(j).dof;
                    cdof = cdof+DATA.dim;
                end
                
                cdof2 = cdof2+DATA.dim;
                
                m.model(i_model).node(j).done=1;
            end
            m.model(i_model).elem(i).dof=[m.model(i_model).elem(i).dof m.model(i_model).node(j).dof];
        end
    end
    m.model(i_model).ndof=cdof2;
    
    m.model(i_model).elem_list      = [(elem_list(end)+1) : (elem_list(end) + numel(m.model(i_model).elem))];
    m.model(i_model).elem_dof_list  = [(elem_dof_list(end)+1) : (elem_dof_list(end) + numel(m.model(i_model).elem)*comp)];
    
    elem_list     = m.model(i_model).elem_list(end);
    elem_dof_list = m.model(i_model).elem_dof_list(end);
    
    disp(['    ... number of dofs for mesh ',num2str(i_model),'... ', num2str(cdof2) ]);
    
end

m.ndof = cdof;
m.elem = numel([m.model(:).elem]);
m.node = numel([m.model(:).node]);

if numel(m.model) > 1
    disp(['    ... total number of dofs for meshes... ', num2str(m.ndof) ]);
end

disp(['    ... connectivity built... ' num2str(toc,2) ' s']);


end