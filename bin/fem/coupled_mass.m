function m1=coupled_mass(m1,m2)
%  intégration sur le maillage 2 de N1'*N2

global DATA

m1.mc=sparse([],[],[],m1.ndof,m2.ndof);

% ELEMENTARY MASS MATRICES
ne=size(m2.elem,2);
for i=1:ne
    if any(m1.modif==i)
        [m1.elem(i).mc,rep]=mcelem(m1,m2.elem(i));
        m1.mc(rep,m2.elem(i).rep)=m1.mc(rep,m2.elem(i).rep)+m1.elem(i).mc;
    else
        m1.elem(i).mc=melem(m2.elem(i));
        m1.mc(m1.elem(i).rep,m1.elem(i).rep)=m1.mc(m1.elem(i).rep,m1.elem(i).rep)+m1.elem(i).mc;
    end
end

% GLOBAL MASS MATRIX

for i=1:ne
	
end
