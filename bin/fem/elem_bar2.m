function [e]=elem_bar2()
%  J.C. Passieux (2010)

e.typel='bar2';

e.node_posref=[-1 ; 1 ]; 	% local position of the nodes of the element

% linear shape function
e.N     = @(xi)  [(1-xi)/2 (1+xi)/2 ];         
e.dN_xi = @(xi) [-1 1];

end