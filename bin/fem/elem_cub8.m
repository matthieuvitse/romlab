function [e]=elem_cub8()
%  J.C. Passieux (2010)

e.typel='cub8';         % modified MV

e.node_posref=[	-1 -1 -1; 1 -1 -1; 1 1 -1; -1 1 -1
	     	-1 -1  1; 1 -1  1; 1 1  1; -1 1  1];	% local position of the nodes of the element

% bilinear shape function
e.N=inline('1/8 * [(1-x).*(1-y).*(1-z) (1+x).*(1-y).*(1-z) (1+x).*(1+y).*(1-z) (1-x).*(1+y).*(1-z) (1-x).*(1-y).*(1+z) (1+x).*(1-y).*(1+z) (1+x).*(1+y).*(1+z) (1-x).*(1+y).*(1+z)]','x','y','z');
e.dN_xi=inline('[ -(1-y).*(1-z)   (1-y).*(1-z)   (1+y).*(1-z)   -(1+y).*(1-z)  -(1-y).*(1+z)   (1-y).*(1+z)   (1+y).*(1+z)   -(1+y).*(1+z) ]./8', 'x', 'y', 'z');
e.dN_eta=inline('[ -(1-x).*(1-z)  -(1+x).*(1-z)   (1+x).*(1-z)    (1-x).*(1-z)  -(1-x).*(1+z)  -(1+x).*(1+z)   (1+x).*(1+z)    (1-x).*(1+z) ]./8', 'x', 'y', 'z');
e.dN_zeta=inline('[ -(1-x).*(1-y)  -(1+x).*(1-y)  -(1+x).*(1+y)   -(1-x).*(1+y)   (1-x).*(1-y)   (1+x).*(1-y)   (1+x).*(1+y)    (1-x).*(1+y) ]./8', 'x', 'y', 'z');
end