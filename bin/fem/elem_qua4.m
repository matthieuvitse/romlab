function [e]=elem_qua4()
%  J.C. Passieux (2010)

e.typel='qua4';

e.node_posref=[-1 -1 ; 1 -1 ; 1 1 ; -1 1];	% local position of the nodes of the element

% bilinear shape function
e.N=inline('0.25 * [(1-xi).*(1-eta) (1+xi).*(1-eta) (1+xi).*(1+eta) (1-xi).*(1+eta)]', 'xi', 'eta');
e.dN_xi=inline('0.25 * [ -(1-eta)  1-eta     1+eta    -(1+eta) ]', 'xi', 'eta');
e.dN_eta=inline('0.25 * [ -(1-xi)   -(1+xi)   1+xi     1-xi     ]', 'xi', 'eta');
