function [e]=elem_tet4()
%  J.C. Passieux (2010)

e.typel='tet4';

e.node_posref=[0 0 0; 1 0 0; 0 1 0 ; 0 0 1]; 	% local position of the nodes of the element

% linear shape function
e.N = inline('[(1-x-y-z) x y z]', 'x', 'y', 'z');
e.dN_xi = inline('[-1 1 0 0]', 'x', 'y', 'z');
e.dN_eta = inline('[-1 0 1 0]', 'x', 'y', 'z');
e.dN_zeta = inline('[-1 0 0 1]', 'x', 'y', 'z');

end