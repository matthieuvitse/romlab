function [e]=elem_tri3()
%  J.C. Passieux (2010)

e.typel='tri3';

e.node_posref=[0 0 ; 1 0 ; 0 1]; 	% local position of the nodes of the element

% linear shape function
e.N = inline('[(1-xi-eta) xi eta]', 'xi', 'eta');
e.dN_xi = inline('[-1 1 0]', 'xi', 'eta');
e.dN_eta = inline('[-1 0 1]', 'xi', 'eta');

end