function [e]=elem_tri6()
%  A. Courard (2012)

e.typel='tri6';

e.node_posref=[0 0 ; 1 0 ; 0 1 ; 1/2 0 ; 1/2 1/2 ; 0 1/2]; 	% local position of the element nodes 

% quadratic shape function
e.N = inline('[(1-xi-eta)*(1-2*xi-2*eta) xi*(2*xi-1) eta*(2*eta-1) 4*xi*(1-xi-eta) 4*xi*eta 4*eta*(1-xi-eta)]', 'xi', 'eta');
e.dN_xi = inline('[(-3+4*xi+4*eta) (-1+4*xi) 0 4*(1-2*xi-eta)  4*eta -4*eta]', 'xi', 'eta');
e.dN_eta = inline('[(-3+4*xi+4*eta) 0 (-1+4*eta) -4*xi 4*xi 4*(1-xi-2*eta)]', 'xi', 'eta');
