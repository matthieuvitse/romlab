function [x, w] = gaussleg_tet(n)
%  J.C. Passieux (2010)

switch n
case 1,
    x=[1 1 1]/4;
    w=1/6;
case 2,
    a=(5-sqrt(5))/20;
    b=(5+3*sqrt(5))/20;
    x=[a a a ; a a b ; a b a ; b a a];
    w=[1 1 1 1]/24;
case 3,
    a=1/4;
    b=1/6;
    c=1/2;
    x=[a a a ; b b b ; b b c ; b c b ; c b b];
    w=[-2/15 [1 1 1 1]*3/40];
case {4,5},
    a=1/4;
    x=[a a a];
    b=(7+sqrt(15))/34;
    c=(13+3*sqrt(15))/34;
    x=[x ; b b b ; b b c ; b c b ; c b b];
    b=(7-sqrt(15))/34;
    c=(13-3*sqrt(15))/34;
    x=[x ; b b b ; b b c ; b c b ; c b b];
    d=(5-sqrt(15))/34;
    e=(5+sqrt(15))/20;
    x=[x ; d d e ; d e d ; e d d ; d e e ; e d e ; e e d];
    w=[112/5670 [1 1 1 1]*(2665+14*sqrt(15))/226800 [1 1 1 1]*(2665-14*sqrt(15))/226800 [1 1 1 1 1 1]*5/567];
otherwise,
	disp('WARNING: Maximum number of Gauss points for tetrahedra reached (11)');
end

end