function [gp] = gp_bar2(e,n)

%Number of Gauss points
if nargin==1 
    n=1; 
end

[vx,vp]=gaussleg(n);
npg=max(size(vp));
for j=1:npg
	pg(j).pos=vx(j,:);
	pg(j).pd=vp(j);
end

for i = 1:npg
	xi  = pg(i).pos(1);
	pg(i).N = e.N(xi);
   	pg(i).dN_xi = e.dN_xi(xi);
end
gp=pg;

end