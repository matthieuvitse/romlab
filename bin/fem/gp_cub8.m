function [gp]=gp_cub8(e,n)
%  J.C. Passieux (2010)

if nargin < 2
    n=2;
end

[vx,vp]=gaussleg(n);

for i=1:n
    for j=1:n
        for k=1:n
            c=n*n*(k-1)+n*(i-1)+j;
            gp(c).pos=[vx(k) vx(j) vx(i)];
            gp(c).pd=vp(k)*vp(i)*vp(j);
        end
    end
end


if 1 == 0
    fontsize  = 18;
    linewidth = 2;
    set(0,'DefaultAxesFontSize',fontsize)
    set(0,'DefaultLineLineWidth',linewidth)
    figure;
    plot3([-1 1],[-1 -1],[-1 -1]);
    hold on
    plot3([1 1],[-1 1],[-1 -1]);
    plot3([1 -1],[1 1],[-1 -1]);
    plot3([-1 -1],[1 -1],[-1 -1]);
    plot3([-1 1],[-1 -1],[1 1]);
    plot3([1 1],[-1 1],[1 1]);
    plot3([1 -1],[1 1],[1 1]);
    plot3([-1 -1],[1 -1],[1 1]);
    plot3([1 1],[1 1],[-1 1]);
    plot3([-1 -1],[-1 -1],[1 -1]);
    plot3([1 1],[-1 -1],[1 -1]);
    plot3([-1 -1],[-1 -1],[1 -1]);
    plot3([-1 -1],[1 1],[1 -1]);
    
    for i=1:(n*n*n)
        plot3(gp(i).pos(1), gp(i).pos(2) ,gp(i).pos(3), '+' );
        text(gp(i).pos(1), gp(i).pos(2) ,gp(i).pos(3),[num2str(i),' [',num2str(gp(i).pos(1)),' , ', num2str(gp(i).pos(2)),' , ' ,num2str(gp(i).pos(3)),']']);
    end
    axis([-1 1 -1 1 -1 1]);
    axis equal
    
    A = @(X,Y) {[gp(X).pos(1) gp(Y).pos(1)],[gp(X).pos(2) gp(Y).pos(2)],[gp(X).pos(3) gp(Y).pos(3)]};
    B = A(1,2);
    plot3(B{1},B{2},B{3},'--');
    B = A(1,3);
    plot3(B{1},B{2},B{3},'--');
    B = A(1,5);
    plot3(B{1},B{2},B{3},'--');
    B = A(2,6);
    plot3(B{1},B{2},B{3},'--');
    B = A(2,4);
    plot3(B{1},B{2},B{3},'--');
    B = A(3,7);
    plot3(B{1},B{2},B{3},'--');
    B = A(3,4);
    plot3(B{1},B{2},B{3},'--');
    B = A(4,8);
    plot3(B{1},B{2},B{3},'--');
    B = A(5,6);
    plot3(B{1},B{2},B{3},'--');
    B = A(5,7);
    plot3(B{1},B{2},B{3},'--');
    B = A(6,8);
    plot3(B{1},B{2},B{3},'--');
    B = A(7,8);
    plot3(B{1},B{2},B{3},'--');
    
    foo;keyboard
end


for i = 1:(n*n*n)
    xi  = gp(i).pos(1);
    eta = gp(i).pos(2);
    zeta = gp(i).pos(3);
    gp(i).N = e.N(xi,eta,zeta);
    gp(i).dN_xi = e.dN_xi(xi,eta,zeta);
    gp(i).dN_eta = e.dN_eta(xi,eta,zeta);
    gp(i).dN_zeta = e.dN_zeta(xi,eta,zeta);
end

end
