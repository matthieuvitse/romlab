function [gp]=gp_qua4(e,n)
%  J.C. Passieux (2010)

if nargin < 2
    n=2;
end

[vx,vp]=gaussleg(n);

for i=1:n
    for j=1:n
        c=n*(i-1)+j;
        pg(c).pos=[vx(j) vx(i)];
        pg(c).pd=vp(i)*vp(j);
    end
end

% plot([-1 1],[-1 -1]);
% hold on
% plot([1 1],[-1 1]);
% plot([1 -1],[1 1]);
% plot([-1 -1],[1 -1]);
% for i=1:(n*n)
%     plot(pg(i).pos(1), pg(i).pos(2), '+' );
% end
% axis([-1 2 -1 2]);
% axis equal
% keyboard

for i = 1:(n*n)
    xi  = pg(i).pos(1);
    eta = pg(i).pos(2);
    pg(i).N = e.N(xi,eta);
    pg(i).dN_xi = e.dN_xi(xi,eta);
    pg(i).dN_eta = e.dN_eta(xi,eta);
end

gp=pg;
