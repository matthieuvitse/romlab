function [gp]=gp_tet4(e,n)
%  J.C. Passieux (2010)

if nargin < 2  
    n=2; 
end

[vx,vp]=gaussleg_tet(n);
npg=numel(vp);

for j=1:npg
	pg(j).pos=vx(j,:);
	pg(j).pd=vp(j);
end
% 
% plot3([0 1],[0 0],[0 0]);
% hold on
% plot3([0 1],[1 0],[0 0]);
% plot3([0 0],[0 1],[0 0]);
% plot3([0 0],[0 0],[0 1]);
% plot3([1 0],[0 0],[0 1]);
% plot3([0 0],[1 0],[0 1]);
% 
% for i=1:npg
%     plot3(pg(i).pos(1), pg(i).pos(2) ,pg(i).pos(3), '+' );
%     text(pg(i).pos(1), pg(i).pos(2) ,pg(i).pos(3),num2str(i));
% end
% axis equal
% keyboard


for i = 1:npg
	xi  = pg(i).pos(1);
	eta = pg(i).pos(2);
    zeta = pg(i).pos(3);
	pg(i).N = e.N(xi,eta,zeta);
   	pg(i).dN_xi = e.dN_xi(xi,eta,zeta);
   	pg(i).dN_eta = e.dN_eta(xi,eta,zeta);
    pg(i).dN_zeta = e.dN_zeta(xi,eta,zeta);
end
gp=pg;
