function [gp]=gp_tri3(e,n)
%  J.C. Passieux (2010)

%Number of Gauss points
if nargin < 2 
    n=1; 
end

[vx,vp]=gaussleg_tri(n);
npg=max(size(vp));

for j=1:npg
	pg(j).pos=vx(j,:);
	pg(j).pd=vp(j);
end

% plot([0 1],[0 0]);
% hold on
% plot([1 0],[0 1]);
% plot([0 0],[1 0]);
% for i=1:npg
%     plot(pg(i).pos(1), pg(i).pos(2), '+' );
% end
% axis([-1 2 -1 2]);
% axis equal
% keyboard

for i = 1:npg
	xi  = pg(i).pos(1);
	eta = pg(i).pos(2);
	pg(i).N = e.N(xi,eta);
   	pg(i).dN_xi = e.dN_xi(xi,eta);
   	pg(i).dN_eta = e.dN_eta(xi,eta);
end
gp=pg;
end
