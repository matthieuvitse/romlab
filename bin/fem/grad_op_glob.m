function [B,detjac] = grad_op_glob(DATA,m,type)

% if strcmp(type,'FE3D')
%     comp = 6;
%     dim = 3;
% elseif strcmp(type,'FE2D')
%     comp = 3;
%     dim = 2;
% elseif strcmp(type,'TRUSS')
%     comp = 1;
%     if DATA.dim == 3
%         dim = 3;
%     elseif DATA.dim == 2
%         dim = 2;
%     end
% end
comp = m.comp;
dim  = m.dim;

nelem = numel(m.elem);

I = [];  
J = [0];  
A = [];    
 
detjac = [];
for i_elem = 1 : nelem
    ngp = size(m.elem(i_elem).gp,2);
    
    for gp = 1 : ngp
        
        [Bloc,detjaci] = compute_gradient(dim,m.elem(i_elem).gp(gp),m.elem(i_elem),type,1);
        
        Bloc2 = reshape(Bloc', 1,(size(Bloc,1)*size(Bloc,2)));
        A = [A,Bloc2];
        if strcmp(type,'FE3D') || strcmp(type,'FE2D')
            I = [I,repmat(m.elem(i_elem).dof,1,comp)];
        elseif strcmp(type,'TRUSS')
            I = [I,repmat(m.elem(i_elem).dof,1,comp)];
        end
        J3 = repmat((J(end)+1) : (J(end)+comp),dim * size(m.elem(i_elem).node_num,1),1);
        J = [J, J3(:)'];
                    
        detjac = [detjac, repmat(detjaci,1,comp)];        
    end
end
J = J(2:end);

B = sparse(J,I,A);

end