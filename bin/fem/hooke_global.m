function [H,var_E_g] = hooke_global(in_out,DATA,m,varargin)

% computes the global hooke / h1 matrices
% rq: we're inside a i_model loop (meaning DATA and m are in fact
% DATA.model(i_model) and m.model(i_model)

ngp_tot = m.ngp_tot;
nelem = numel(m.elem);

I = speye(ngp_tot);

% if strcmp(DATA.type,'TRUSS')
%     comp = 1;
% elseif strcmp(DATA.type,'FE2D')
%     comp = 3;
% elseif strcmp(DATA.type,'FE3D')
%     comp = 6;
% end
comp = m.comp;

switch varargin{1}
    case 'hooke'
        K = DATA.material.hooke;
    case 'invhooke'
        K = DATA.material.flex;
    case 'h1'
        K = DATA.h1;
    case 'DDR'
        K = DATA.ddr;
end

H = kron(I,K);

if strcmp(DATA.behavior.type,'damage')  && (in_out.var_young) && (strcmp(varargin{1},'hooke') || strcmp(varargin{1},'invhooke') )
    warning off backtrace
    warning('variability on Young modulus per element implemented')
    warning on backtrace
    perc    = in_out.var_young_val;
    
    I = [];
    J = [];
    A = [];
    n_gp = 0;
    if ~isfield(m,'var_E_g')
        var_E_g = zeros(1,ngp_tot);
    else
        var_E_g = m.var_E_g;
    end
    
    for i_elem =  1 : nelem
        nb_gp = size(m.elem(i_elem).gp,2);
        for i_gp = 1 : nb_gp
            n_gp = n_gp+1;
            dof_gp = [((i_elem-1)*nb_gp*comp + (i_gp-1) * comp + 1) : ((i_elem - 1)*comp*nb_gp) + (i_gp) * comp];
            
            I = [I , repmat(dof_gp,1,comp)];
            J1 = repmat(dof_gp,comp,1);
            J2 = J1(:)';
            J = [J , J2 ];
            if strcmp(varargin{1},'hooke')
                val_var = rand;
                sign_var = sign(val_var - 0.5); 
                var_E = (1 + perc * sign_var * val_var/100);
                var_hooke = var_E * K;
                var_E_g(n_gp) = var_E;
            elseif strcmp(varargin{1},'invhooke')
                var_E = var_E_g(n_gp);
                var_hooke = 1/var_E * K;
            end
            Bloc = reshape(var_hooke', 1,(size(var_hooke,1)*size(var_hooke,2)));
            A = [A , Bloc];
        end
    end
    H = sparse(J,I,A);
else
    var_E_g = [];
end

end