function II = integ_full(DATA,m)

% if strcmp(DATA.type,'FE3D')
%     comp = 6;       % number of elements in the strain vector
%     S = 1;
% elseif strcmp(DATA.type,'FE2D') %%%%%%% A VOIR mais je pense que c'est �a %%%%%
%     comp = 3;
%     S = 1;
% elseif strcmp(DATA.type,'TRUSS')
%     comp = 1;
%     S = DATA.section;
% end
S    = DATA.section;
comp = m.comp;

ngp_tot = m.ngp_tot;
ngp_pd = m.ngp_pd;

pds = spdiags(ngp_pd',0,size(ngp_pd,2),size(ngp_pd,2));       % matrix containing the weigths

jacs = spdiags(m.detjac',0,size(ngp_pd,2),size(ngp_pd,2));    % matrix containing the jacobians of the elementary transformations

I = speye(ngp_tot);
J = speye(comp);

II = S .* kron(I,J) .*pds .*abs(jacs);

end