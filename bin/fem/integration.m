function m=integration(DATA,m)
%  J.C. Passieux (2010)

disp('ROMlab: Building integration operators...');

m.ndofgp     = 0;
m.ngp_tot    = 0;
ngp_tot_full = 0;
cdof         = 0;

for i_model = 1 : numel(DATA.model)

    tic
       
    mqua4=1;
    mtri3=1;
    mtet4=1;
    mcub8=1;                                     % mqua-> mcub  MV
    mtri6=1;
    mbar2=1;
    
    ngp_pd = [];
        
%     if strcmp(DATA.model(i_model).type, 'FE3D')
%         comp = 6;
%     elseif strcmp(DATA.model(i_model).type, 'FE2D')
%         comp = 3;
%     elseif strcmp(DATA.model(i_model).type, 'TRUSS')
%         comp = 1;
%     end
    comp = m.model(i_model).comp;
    
    cdof2 = 0;
    for i=1:numel(m.model(i_model).elem)
        
        if strcmp(m.model(i_model).elem(i).typel,'qua4')
            if mqua4
                [qua4_pg]=gp_qua4(m.model(i_model).elem(i));
                mqua4=0;
            end
            m.model(i_model).elem(i).gp=qua4_pg;
            
        elseif strcmp(m.model(i_model).elem(i).typel,'tri3')
            if mtri3
                [tri3_pg]=gp_tri3(m.model(i_model).elem(i));
                mtri3=0;
            end
            m.model(i_model).elem(i).gp=tri3_pg;
        elseif strcmp(m.model(i_model).elem(i).typel,'tet4')
            if mtet4
                [tet4_pg]=gp_tet4(m.model(i_model).elem(i));
                mtet4=0;
            end
            m.model(i_model).elem(i).gp=tet4_pg;
        elseif strcmp(m.model(i_model).elem(i).typel,'cub8')
            if mcub8                            
                [cub8_pg]=gp_cub8(m.model(i_model).elem(i)); 
                mcub8=0;                         
            end                                  
            m.model(i_model).elem(i).gp=cub8_pg;    
        elseif strcmp(m.model(i_model).elem(i).typel,'tri6')
            if mtri6
                [tri6_pg]=gp_tri6(m.model(i_model).elem(i));
                mtri6=0;
            end
            m.model(i_model).elem(i).gp=tri6_pg;
        elseif strcmp(m.model(i_model).elem(i).typel,'bar2')
            if mbar2
                [bar2_pg]=gp_bar2(m.model(i_model).elem(i));
                mbar2=0;
            end
            m.model(i_model).elem(i).gp=bar2_pg;
        end
                
        for j = 1 : size(m.model(i_model).elem(i).gp,2)
            for k = 1 : comp
                ngp_pd(end+1) = m.model(i_model).elem(i).gp(j).pd;
            end
        end
        
        % numbering the gauss points
        m.model(i_model).elem(i).dofgp        = cdof2 + [1:numel(m.model(i_model).elem(i).gp)];
        m.model(i_model).elem(i).dofgp_corres = cdof  + [1:numel(m.model(i_model).elem(i).gp)];
        
        cdof2 = cdof2 + numel(m.model(i_model).elem(i).gp);
        cdof  = cdof + numel(m.model(i_model).elem(i).gp);
        
    end
    
    m.model(i_model).ngp_tot = size([m.model(i_model).elem(:).gp],2);
    m.model(i_model).ngp_pd  = ngp_pd;
    m.model(i_model).ndofgp  = m.model(i_model).ngp_tot * comp;
    m.model(i_model).dofgp   = [(ngp_tot_full+1) : (ngp_tot_full + m.model(i_model).ndofgp)];

    ngp_tot_full = m.model(i_model).dofgp(end);
        
    disp(['    ... integration for mesh ',num2str(i_model),'... ' num2str(toc,2) ' s']);
    disp(['    ... number of integration points for mesh ',num2str(i_model),'... ' num2str(m.model(i_model).ngp_tot)]);
    
    m.ndofgp  = m.ndofgp  + m.model(i_model).ndofgp ; 
    m.ngp_tot = m.ngp_tot + m.model(i_model).ngp_tot;
end

end
