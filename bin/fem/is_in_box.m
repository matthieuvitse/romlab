function r=is_in_box(p,ld,e,type)
%  J.C. Passieux (2010)

if strcmp(class(ld),'double')
    b=ld;
elseif strcmp(class(ld),'struct')
    x=p(1); y=p(2);
    b=ld.box(x,y);
end

if nargin <4 || strcmp(type,'Default')
    
    % reordering
    for i=1:size(b,1)
        if(b(i,2)<b(i,1))
            tmp=b(i,1);
            b(i,1)=b(i,2);
            b(i,2)=tmp;
        end
    end
    
    % tolerance
    if nargin < 3
        if (norm(b(:,1)-b(:,2))==0)
            e = 1e-8*norm(max(abs(b(:))));
        else
            e = 1e-6*norm(b(:,1)-b(:,2));
        end
    end
    
    % test
    r=0;
    if min((b(:,1)-e)<p') && min(p'<(b(:,2)+e))
        r=1;
    end
    
elseif strcmp(type,'Edge')
  
    r=0;
    
    if (x + e)>min(b(1,:)) && (x - e)<max(b(1,:))
        if (y + e)>min(b(2,:)) && (y - e)<max(b(2,:))
            r=1;
        end
    end
    
end

end


