function [r]=is_in_edge(pos,f,l)

x=pos(1); y=pos(2);

r=0;

tol=1e-5;

if x>(-l/2-tol) && x<(l/2 + tol)
    if y>(f(x) - tol) && y<(f(x) + tol)
        r=1;
    end
end

end