function kel=kelem(DATA,e,operator)
%  J.C. Passieux (2010)

% switch nargin
%     case 3
%         matrix=eval(operator);
%     otherwise
%         matrix='DATA.hooke';
% end

matrix = operator;
ndof=size(e.dof,2);
kel = zeros(ndof,ndof);

%Matrice de rigidit� de l'�l�ment

for i = 1:size(e.gp,2)
    [B, det_jac]=compute_gradient(DATA.dim,e.gp(i),e,DATA.model.type,1);
    kel = kel + (B' * matrix * B * abs(det_jac) * e.gp(i).pd);
end


end