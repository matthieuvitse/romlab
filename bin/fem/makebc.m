function m=makebc(DATA,m)
tic
disp('ROMlab: Setting boundary conditions...');
% All informations concerning parameters
m.params(1).name = 'M';
m.params(1).ndof = m.ndof ;
m.params(1).mesh = {'m.elem' 'm.node'};

sizedofs = m.params(1).ndof;

for i = 2 : length(DATA.params)
    m.params(i).name = DATA.params(i).name;
    m.params(i).ndof = length(DATA.params(i).mesh);
    m.params(i).mesh = DATA.params(i).mesh;
    m.params(i).type = DATA.params(i).type;
    m.params(i).unit = DATA.params(i).unit;
    if strcmp(m.params(i).type,'loading') || strcmp(m.params(i).type,'time')
        sizedofs = [ sizedofs m.params(i).ndof ];
    end
end

m.dofud=[];
for i_model = 1 : numel(m.model)
    m.model(i_model).dofud = [];
    m.model(i_model).dofuu = [];
    m.model(i_model).dofud_corres = [];
    sizedofs2 = [m.model(i_model).ndof,sizedofs(2:end)];
    m.model(i_model).fu = sptensor(sizedofs2);
end

m.dofvd = [];
m.fu    = sptensor(sizedofs);
m.ud    = sptensor(sizedofs);
m.vd    = sptensor(sizedofs);

m.elem_in_box = [];   % elements in boxes where damage will be imposed to zero

for k=1:size(DATA.load,2)
    switch lower(DATA.load(k).type)
        case {'displ','depl'}
            m=makebc_disp(m,DATA.load(k));
        case {'velocity'}                       % not implemented for multi-model
            m=makebc_velo(m,DATA.load(k));
        case {'dnorm'}                          % not implemented for multi-model
            m=makebc_dnorm(m,DATA.load(k));
        case {'stres','stress'}
            m=makebc_stress(DATA,m,DATA.load(k));
        case {'force'}                      
            m=makebc_force(m,DATA.load(k));
        case {'sym'}
            m=makebc_sym(m,DATA.load(k));
        case {'contact'}
            m=makebc_contact2(m,DATA.load(k));
        otherwise
            error([DATA.load(k).type,' : unknown loading type.']);
    end
    % check if damage can occur to create elastic boxes where prescribed
    % dofs will be imposed
    if isfield(DATA.load(k),'dam_box')
        if ~isempty(DATA.load(k).dam_box)
            m = makebc_damage(DATA,m,DATA.load(k));
        end
    end
end

% delete multiple prescribed dofs and build global mapping
allbutM = '';
for i_M = 2 : length(m.params)
    if strcmp(m.params(i_M).type,'loading') || strcmp(m.params(i_M).type,'time')
        allbutM = [ allbutM ',:' ];
    end
end

for i_model = 1 : numel(m.model)
    [m.model(i_model).dofud,ia,~] = unique(m.model(i_model).dofud);
    m.model(i_model).dofud_corres = m.model(i_model).dofud_corres(ia);
    corres = sort([ m.model(i_model).dofuu m.model(i_model).dofud; m.model(i_model).dofuu_corres m.model(i_model).dofud_corres ]')';
    m.model(i_model).dof_corres = corres(2,:);
    if ~isempty(m.model(i_model).dofud_corres) && ~isempty(allbutM)
        eval(strcat('m.ud(m.model(i_model).dofud_corres',allbutM,') = sptensor(m.model(i_model).ud(m.model(i_model).dofud',allbutM,'));'));
    elseif ~isempty(m.model(i_model).dofud_corres) && isempty(allbutM)
        m.ud=zeros(sizedofs,1);
        eval(strcat('m.ud(m.model(i_model).dofud_corres) = (m.model(i_model).ud(m.model(i_model).dofud));'));
    end
end

m.dofud        = unique([m.model(:).dofud_corres]);
prescribed_dof = unique([m.dofud m.dofvd]);
m.dofuu        = 1:m.ndof;
m.dofuu(prescribed_dof) = [];

disp(['    ... boundary conditions set... ',num2str(toc),' s']);

end
