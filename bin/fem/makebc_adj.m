function m=makebc_adj(m)
%  J.C. Passieux (2011)

global DATA_adj

if isfield(DATA_adj,'timeline');
    nt=numel(DATA_adj.timeline);
else
    nt=1;
end

m.repud=[];
m.repvd=[];
m.fbc=zeros(m.ndof,nt);
m.ubc=zeros(m.ndof,nt);
m.vbc=zeros(m.ndof,nt);
for k=1:size(DATA_adj.load,2)
    switch lower(DATA_adj.load(k).type)
        case {'displ','depl'}
            m=makebc_disp(m,DATA_adj.load(k));
        case {'velocity'}
            m=makebc_velo(m,DATA_adj.load(k));
        case {'dnorm','sym'}
            m=makebc_dnorm(m,DATA_adj.load(k));
        case {'stres','stress'}
            m=makebc_stress(m,DATA_adj.load(k));
        case {'force'}
            m=makebc_force(m,DATA_adj.load(k));
        otherwise
            error([DATA_adj.load(k).type,' : unknown loading type.']);
    end
end

prescribed_dof=[m.repud m.repvd];
prescribed_dof=unique(prescribed_dof);
m.repsubdof=1:m.ndof;
m.repsubdof(prescribed_dof)=[];
% keyboard
m.rep_eff=setdiff(m.rep_eff,prescribed_dof);
