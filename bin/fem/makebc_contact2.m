function m=makebc_contact2(m,ld)


for i_model = 1 : numel(m.model)
    
    ndim = length(m.model(i_model).node(1).pos);
    tgt = 1 : ndim;
    tgt(ld.normal) = [];
    contact(i_model).dofpos = [];
    contact(i_model).node=[];
    contact(i_model).C = spalloc(m.model(i_model).ndof,m.model(i_model).ndof,1);
    
    for i=1:numel(m.model(i_model).elem)
        
        nn=0;
        nd=[];
        contacti(i_model).dofpos = [];
        contacti(i_model).node=[];
        
        for jj=1:numel(m.model(i_model).elem(i).node_num)
            j=m.model(i_model).elem(i).node_num(jj);
            if(is_in_box(m.model(i_model).node(j).pos,ld,10^-5,'Edge'))
                nn=nn+1;
                nd=[nd j];
                contacti(i_model).dofpos = [contacti(i_model).dofpos ; m.model(i_model).node(j).dof m.model(i_model).node(j).pos];
                contacti(i_model).node = [contacti(i_model).node ; j];
                
                if nn==2
                    break;
                end
                
            end
        end
        
        if nn==2
            contact(i_model).dofpos = [contact(i_model).dofpos ; contacti(i_model).dofpos];
            contact(i_model).node = [contact(i_model).node ; contacti(i_model).node];
            Length=norm(m.model(i_model).node(nd(1)).pos - m.model(i_model).node(nd(2)).pos);
            
            dofi = [];
            for i_fbc=1:nn
                dofi=[dofi m.model(i_model).node(nd(i_fbc)).dof];
            end
            %%%% WORKS ONLY IN 2D WITH P1 ELEMENTS FOR THE BOUNDARY %%%%
            contact(i_model).C(dofi,dofi) = contact(i_model).C(dofi,dofi) + Length/6*[2 0 1 0 ; 0 2 0 1 ; 1 0 2 0 ; 0 1 0 2 ];
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        end
    end
    
    if ~isempty(contact(i_model).dofpos)
        [contact(i_model).dofpos,aux] = unique(contact(i_model).dofpos,'rows');
        contact(i_model).node = contact(i_model).node(aux);
        for i_tgt = tgt
            [contact(i_model).dofpos,aux] = sortrows(contact(i_model).dofpos,(i_tgt+ndim));
            contact(i_model).node=contact(i_model).node(aux);
        end
    else
        warning('No contact zone in this model');
        foo;keyboard;
    end
    
end

gt=abs(contact(1).dofpos(:,tgt + ndim) - contact(2).dofpos(:,tgt + ndim));

J= contact(2).dofpos(:,ld.normal + ndim) - contact(1).dofpos(:,ld.normal + ndim);

if max(gt)<10^-5;
    for i_model = 1 : numel(m.model)
        m.model(i_model).dofc =  reshape(contact(i_model).dofpos(:,1:ndim)',numel(contact(i_model).dofpos(:,1:ndim)),1)';
        R = sparse(eye(m.model(i_model).ndof));
        N = sparse(eye(length(m.model(i_model).dofc)));
        Pi = sparse(eye(length(m.model(i_model).dofc)));
        m.model(i_model).C = contact(i_model).C;
        m.model(i_model).R = R(m.model(i_model).dofc,:);
        m.model(i_model).N = N(ld.normal:2:end,:);
        m.model(i_model).Pi = Pi(tgt:2:end,:);  
        m.model(i_model).J=J;
        m.model(i_model).J=J;
        m.model(i_model).cn=contact(i_model).node;
    end
else
    warning('Contact zones must have coincident meshes');
    foo;keyboard;
end


end