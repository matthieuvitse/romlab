function m = makebc_damage(DATA,m,ld)
% define damage box in which damage will be equal to zero (elastic boxes)
for i_model = 1 : numel(m.model)
    for i=1:size(m.model(i_model).elem,2)
        nn=0;
        nd=[];
        tot_node_elem = numel(m.model(i_model).elem(i).node_num);
        for jj=1:numel(m.model(i_model).elem(i).node_num)
            j=m.model(i_model).elem(i).node_num(jj);
            if(is_in_box(m.model(i_model).node(j).pos,ld.dam_box))
                nn=nn+1;
                nd=[nd j];
                %m.elem_in_box = [m.elem_in_box,i];   % at least one node of the element are in the box, then elem is in the box
                %break
            end
        end
        if nn == tot_node_elem   % all the nodes of the elements are in the box, then elem is in the box
            m.elem_in_box = [m.elem_in_box,i];
        end 
    end    
end

end