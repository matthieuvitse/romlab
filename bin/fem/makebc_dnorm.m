function m=makebc_dnorm(m,ld)
%  J.C. Passieux (2010)

v=ld.box(:,2)-ld.box(:,1);
v=v/norm(v);
comp=find(v==0);
if numel(comp)~=1
    error('TODO: Normal displacement only for section parallel to the axis');
end

for i=1:size(m.node,2)
    if(is_in_box(m.node(i).pos,ld.box))
	if (isempty(find(m.repsubdof==m.node(i).rep(comp))))
	        m.ubc(m.node(i).rep(comp))=ld.val(comp);
		m.repud=[m.repud m.node(i).rep(comp)];
	end
    end
end
