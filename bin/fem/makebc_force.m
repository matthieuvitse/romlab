function m=makebc_force(m,ld)

for i_model = 1 : numel(m.model)
    for j=1:numel(m.model(i_model).node)
        if(is_in_box(m.model(i_model).node(j).pos,ld.box))
            dof=m.model(i_model).node(j).dof;
            m.model(i_model).fu([dof],:)=m.model(1).fu([dof],:)+sptensor(ld.val);
        end
    end
end

end
