function m=makebc_pressure(m,ld)
%  A. Courard (2014)

global DATA


% hold on
y5=DATA.y5;
yA=DATA.yA;
x5=DATA.x5;

for i=1:size(m.elem,2)
    nn=0;
    nd=[];
    for jj=1:numel(m.elem(i).node_num)
        j=m.elem(i).node_num(jj);
        if(is_in_box(m.node(j).pos,ld.box))
            nn=nn+1;
            nd=[nd j];
        else vert=j;
        end
    end
    
   
    if nn>(DATA.dim-1)&&(m.node(vert).pos(1)<ld.box(1,1))
        switch nn
            case 2,
                length=norm(m.node(nd(1)).pos-m.node(nd(2)).pos);
%                 plot(m.node(nd(1)).pos(1),m.node(nd(1)).pos(2),'o');
%                 plot(m.node(nd(2)).pos(1),m.node(nd(2)).pos(2),'o');

            case 3,
                if strcmp(m.elem(i).typel,'tri6')
                    length=norm(m.node(nd(3)).pos-m.node(nd(1)).pos);
                    
                elseif strcmp(m.elem(i).typel,'tri3')
                    disp(['elem ' num2str(i) ', nodes ',num2str(nd)]);
                    error('Flat element'); 
                    
                else
                    vec1=m.node(nd(2)).pos-m.node(nd(1)).pos;
                    vec2=m.node(nd(3)).pos-m.node(nd(1)).pos;
                    length=norm(cross(vec1,vec2))/2;
                end
                
            case 4,
                vec1=m.node(nd(2)).pos-m.node(nd(1)).pos;
                vec2=m.node(nd(3)).pos-m.node(nd(1)).pos;
                length=norm(cross(vec1,vec2))/2;
                vec1=m.node(nd(3)).pos-m.node(nd(1)).pos;
                vec2=m.node(nd(4)).pos-m.node(nd(1)).pos;
                length=length+norm(cross(vec1,vec2))/2;
            otherwise,
                disp(['elem ' num2str(i) ', nodes ',num2str(nd)]);
                error('unknown skin element!');
        end
        
        if strcmp(m.elem(i).typel,'tri6')
            rep1=m.node(nd(1)).rep;
            rep2=m.node(nd(2)).rep;
            rep3=m.node(nd(3)).rep;
            m.fbc(rep1)=m.fbc(rep1)+length*ld.val/2;
            m.fbc(rep2)=m.fbc(rep2)+length*ld.val/2;
            m.fbc(rep3)=m.fbc(rep3)+length*ld.val;
        else
            for i=1:nn
                repi=m.node(nd(i)).rep;
                m.fbc(repi)=m.fbc(repi)+length*ld.val/nn;
            end
        end
    end
end
