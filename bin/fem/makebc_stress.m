function m=makebc_stress(DATA,m,ld)

allbutM = '';
for i = 2 : length(m.params)
    allbutM = [ allbutM ',:' ];
end

for i_model = 1 : numel(m.model)
    
    doff = [];
    doff_cor = [];
    
    for i=1:size(m.model(i_model).elem,2)
        nn=0;
        nd=[];
        for jj=1:numel(m.model(i_model).elem(i).node_num)
            j=m.model(i_model).elem(i).node_num(jj);
            if(is_in_box(m.model(i_model).node(j).pos,ld.box))
                nn=nn+1;
                nd=[nd j];
            end
        end
        
        if nn>(DATA.dim-1)
            
            switch nn
                case 2,
                    Length=norm(m.model(i_model).node(nd(1)).pos - m.model(i_model).node(nd(2)).pos);
                case 3,
                    vec1=m.model(i_model).node(nd(2)).pos - m.model(i_model).node(nd(1)).pos;
                    vec2=m.model(i_model).node(nd(3)).pos - m.model(i_model).node(nd(1)).pos;
                    if DATA.dim == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                    end
                    Length=norm(cross(vec1,vec2))/2;
                case 4,
                    vec1=m.model(i_model).node(nd(2)).pos - m.model(i_model).node(nd(1)).pos;
                    vec2=m.model(i_model).node(nd(3)).pos - m.model(i_model).node(nd(1)).pos;
                    if DATA.dim == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                    end
                    Length=norm(cross(vec1,vec2))/2;
                    vec1=m.model(i_model).node(nd(3)).pos - m.model(i_model).node(nd(1)).pos;
                    vec2=m.model(i_model).node(nd(4)).pos - m.model(i_model).node(nd(1)).pos;
                    if length(vec1) == 2
                       vec1(3) = 0;
                       vec2(3) = 0;
                    end
                    Length=Length+norm(cross(vec1,vec2))/2;
                otherwise,
                    disp(['elem ' num2str(i) ', nodes ',num2str(nd)]);
                    error('unknown skin element!');
            end
            for i_fbc=1:nn
                dofi_corres=m.model(i_model).node(nd(i_fbc)).dof_corres;
                dofi=m.model(i_model).node(nd(i_fbc)).dof;
                eval(strcat('m.model(i_model).fu(dofi',allbutM,')=m.model(i_model).fu(dofi',allbutM,')+Length*sptensor(ld.val)/nn;'));
                doff = [doff dofi];
                doff_cor = [doff_cor dofi_corres];
            end
        end
    end
    
    if ~isempty(unique(doff_cor))
        eval(strcat('m.fu(unique(doff_cor)',allbutM,')=m.fu(unique(doff_cor)',allbutM,')+ m.model(i_model).fu(unique(doff)',allbutM,');'));
    end
end

end