function m=makebc_sym(m,ld)

% allbutM = '';
% for i = 2 : length(m.params)
%     allbutM = [ allbutM ',:' ];
% end

allbutM = '';
for i_M = 2 : length(m.params)
    if strcmp(m.params(i_M).type,'loading') || strcmp(m.params(i_M).type,'time')
        allbutM = [ allbutM ',:' ];
    end
end

dir = ld.normal;

for i_model = 1 : numel(m.model)

    m.model(i_model).dofuu=sort([m.model(i_model).node(:).dof],'ascend');
    m.model(i_model).dofuu_corres=sort([m.model(i_model).node(:).dof_corres],'ascend');
     
    for i=1:size(m.model(i_model).node,2)
        if(is_in_box(m.model(i_model).node(i).pos,ld.box))
            if numel(m.model(i_model).node(i).dof)>0
                eval(strcat('m.model(i_model).ud(m.model(i_model).node(i).dof(dir)',allbutM,')=sptensor(ld.val);'));
                m.model(i_model).dofud=[m.model(i_model).dofud m.model(i_model).node(i).dof(dir)];
                m.model(i_model).dofud_corres = [m.model(i_model).dofud_corres m.model(i_model).node(i).dof_corres(dir)];
            end
        end
    end
    m.model(i_model).dofuu([m.model(i_model).dofud]) = [];    
    m.model(i_model).dofuu_corres([m.model(i_model).dofud]) = [];
    
end

end