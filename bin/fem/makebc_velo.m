function m=makebc_velo(m,ld)

for i=1:size(m.node,2)
    if m.node(i).num && is_in_box(m.node(i).pos,ld.box)
        if numel(m.node(i).rep)>0
            m.vbc(m.node(i).rep,:)=ld.val;
            m.repvd=[m.repvd m.node(i).rep];
        end
    end
end
end
