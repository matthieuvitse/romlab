function m=mass(DATA,m,arg)
%  J.C. Passieux (2010)
dim = DATA.dim;
if arg
disp('ROMlab: Assembling mass and l2 operators... ');

m.l2 = spalloc(m.ndof,m.ndof,1);
m.m  = spalloc(m.ndof,m.ndof,1);

for i_model = 1 : numel(DATA.model)

%     if strcmp(DATA.model(i_model).type,'FE3D')
%         if strcmp(m.model(i_model).elem(1).typel,'cub8')
%             num_dof_elem = 24;         % add that in the loop, with check on element type
%         else
%             foo;keyboard;
%         end
%     elseif strcmp(DATA.model(i_model).type,'FE2D')
%         if strcmp(m.model(i_model).elem(1).typel,'qua4')
%             num_dof_elem = 8;
%         elseif strcmp(m.model(i_model).elem(1).typel,'tri3')
%             num_dof_elem = 6;
%         else
%             foo;keyboard
%         end
%     elseif strcmp(DATA.model(i_model).type,'TRUSS')
%         if strcmp(m.model(i_model).elem(1).typel,'bar2')
%             if DATA.dim == 3
%             num_dof_elem = 6;
%             elseif DATA.dim == 2
%                 num_dof_elem = 4;
%             end
%         else
%             foo;keyboard
%         end
%     end
    num_dof_elem = m.model(i_model).dof_node * numel(m.model(i_model).elem(1).node_num);
    
    
    dof_corres = [m.model(i_model).node(:).dof_corres];
    
    I = [];
    J = [];
    A = [];
    
    % ELEMENTARY MASS MATRICES
    ne=size(m.model(i_model).elem,2);
    for i_elem=1:ne
        Bloc = melem(DATA.model(i_model),m.model(i_model).elem(i_elem),dim);
        A = [A, reshape(Bloc, 1,(size(Bloc,1)*size(Bloc,2)))];
        I = [I,repmat(m.model(i_model).elem(i_elem).dof,1,num_dof_elem)];
        
        J3 = repmat(m.model(i_model).elem(i_elem).dof,num_dof_elem,1);
        J = [J, J3(:)'];
    end
     
    B = sparse(J,I,A);
    m.model(i_model).m = B;
    
    disp(['    ... mass matrix ',num2str(i_model),' assembled...']);
    
    m.model(i_model).l2 = m.model(i_model).m / DATA.model(i_model).material.density;
    disp(['    ... l2 matrix ',num2str(i_model),' assembled...']);
    
    m.m(dof_corres,dof_corres)  = m.m(dof_corres,dof_corres)  + m.model(i_model).m;
    m.l2(dof_corres,dof_corres) = m.l2(dof_corres,dof_corres) + m.model(i_model).l2;
end

disp([ '    ... operators assembled... ', num2str(toc,2) ' s']);


else
    warning off backtrace
    warning('Computation of mass and L2 matrices skipped')
    warning on backtrace

    m.m  = [];
    m.l2 = [];
end

end
