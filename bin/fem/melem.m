function mel=melem(DATA,e,dim)
%  J.C. Passieux (2010)

density=DATA.material.density;

ndof=size(e.dof,2);
mel = zeros(ndof,ndof);

if strcmp(DATA.type,'BEAM3D')
    foo;keyboard
    for i = 1:size(e.gp,2)
        N = e.gp(i).N;
        [~, det_jac]=compute_gradient(dim,e.gp(i),e,DATA.type,0);
        mel = mel + (N' * N * abs(det_jac) * e.gp(i).pd);
    end
elseif strcmp(DATA.type,'FE2D')
    for i = 1:size(e.gp,2)
        N=[ reshape([e.gp(i).N ; 0*e.gp(i).N],1,2*max(size(e.gp(i).N))) ;
            reshape([0*e.gp(i).N ; e.gp(i).N],1,2*max(size(e.gp(i).N))) ];
        [~, det_jac]=compute_gradient(dim,e.gp(i),e,DATA.type,0);
        mel = mel + (N' * N * abs(det_jac) * e.gp(i).pd);
    end
elseif strcmp(DATA.type,'FE3D')
    for i = 1:size(e.gp,2)
        a1=[e.gp(i).N ; zeros(2,numel(e.gp(i).N)) ];
        N=a1(:)';
        a1=[zeros(1,numel(e.gp(i).N)) ; e.gp(i).N ; zeros(1,numel(e.gp(i).N)) ];
        N=[N ; a1(:)'];
        a1=[zeros(2,numel(e.gp(i).N)) ; e.gp(i).N ];
        N=[N ; a1(:)'];
        [~, det_jac]=compute_gradient(dim,e.gp(i),e,DATA.type,0);
        mel = mel + (N' * N * abs(det_jac) * e.gp(i).pd);
    end
end
mel=density*mel;
end