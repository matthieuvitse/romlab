function u=FULL_sol(U,G)


nmod=size(U,1);

    %Initialisation
u=outprod(U(1,:)',G(1,:)');


for k=2:nmod
   
    u=u+outprod(U(k,:)',G(k,:)');
            
end


end