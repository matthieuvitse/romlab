function F=RHS_G(mpara)

Finter=sparse(1,mpara.ndof);


for k=1:numel(mpara.elem)
    
    e=mpara.elem(k);
    m = zeros(1,size(mpara.elem(k).node_pos,1));
    
    for l = 1:size(e.pg,2)
        N=e.pg(l).N;
        
        [~, det_jac]=compute_gradient(e.pg(l),e,2);
        
        m = m + (N * abs(det_jac) * e.pg(l).pd);
    end

%     keyboard
    Finter(:,e.rep)=Finter(:,e.rep)+m;
    
end

F=(Finter)';

end