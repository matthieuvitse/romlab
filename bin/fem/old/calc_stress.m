function [eps,sig,e]=calc_stress(DATA,e)
%  J.C. Passieux (2010)

hooke_matrix=DATA.hooke;

eps=0;
for i = 1:size(e.gp,2)
	B=compute_gradient(e.gp(i),e,DATA.dim,1);
	eps = eps+ B * e.u;
end

eps=eps/size(e.gp,2);

sig=hooke_matrix*eps;

end