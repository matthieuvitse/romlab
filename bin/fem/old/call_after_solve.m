function m=call_after_solve(DATA,m,u)
%  J.C. Passieux (2010)

dim=DATA.dim;
tridim=dim==3;

for j=1:numel(m.node)
    m.node(j).u=u(m.node(j).dof);
end
for ii=1:numel(m.elem)
    m.elem(ii).u=u(m.elem(ii).dof);
end
if(DATA.plot_sigeps)
    for ii=1:numel(m.elem)
        [eps,sig]=calc_stress(DATA,m.elem(ii));
        correceps=[eye(dim) zeros(dim,2*dim-3) ; zeros(2*dim-3,dim)  1/sqrt(2)*eye(2*dim-3)];
        correcsig=[eye(dim) zeros(dim,2*dim-3) ; zeros(2*dim-3,dim)  1/sqrt(2)*eye(2*dim-3)];
        if dim==3
            % this line because Paraview's tensor ordering is XX YY ZZ XY YZ XZ
            correceps(5:6,5:6)=correceps(5:6,6:-1:5);                          %[0 1 ; 1 0]
            correcsig(5:6,5:6)=correcsig(5:6,6:-1:5);                          %[0 1 ; 1 0]
        end
        m.elem(ii).eps=correceps*eps;
        m.elem(ii).sig=correcsig*sig;
    end
end
if(DATA.plot_vonmises)
    for ii=1:numel(m.elem)
        sigtens=zeros(3);
        sigtens(1:2,1:2)=[m.elem(ii).sig(1) m.elem(ii).sig(3)
            m.elem(ii).sig(3) m.elem(ii).sig(2)];
        if tridim
            sigtens(2,1)= m.elem(ii).sig(4);
            sigtens(1,2)= m.elem(ii).sig(4);
            sigtens(3,:)=[1/sqrt(2) * m.elem(ii).sig(5)   1/sqrt(2) *m.elem(ii).sig(6)   m.elem(ii).sig(3) ];
            sigtens(:,3)=[1/sqrt(2) * m.elem(ii).sig(5) ; 1/sqrt(2) *m.elem(ii).sig(6) ; m.elem(ii).sig(3) ];
        end
        m.elem(ii).principal_stress=sort(eig(sigtens),'descend');
    end
    
end
end
