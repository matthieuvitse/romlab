function m=call_after_solve_PGD_E(m,u,E)
%  J.C. Passieux (2010)

global DATA
tic
dim=DATA.dim;
tridim=dim==3;

for j=1:numel(m.node)
	m.node(j).u=u(m.node(j).rep);
end
for ii=1:numel(m.elem)
	m.elem(ii).u=u(m.elem(ii).rep);
end
if(DATA.plot_sigeps)
	for ii=1:numel(m.elem)
		[eps,sig]=calc_stress_PGD_E(m.elem(ii),E);
		correc=[eye(dim) zeros(dim,2*dim-3) ; zeros(2*dim-3,dim)  0.5*eye(2*dim-3)];
        if dim==3
           % this line because Paraview's tensor ordering is XX YY ZZ XY YZ XZ
           correc(5:6,5:6)=[0 1 ; 1 0]/2; 
        end
		m.elem(ii).eps=correc*eps;
		m.elem(ii).sig=correc*sig;	
	end
end
if(DATA.plot_vonmises)
    for ii=1:numel(m.elem)
	sigtens=zeros(3);
        sigtens(1:2,1:2)=[m.elem(ii).sig(1) m.elem(ii).sig(3)
        m.elem(ii).sig(3) m.elem(ii).sig(2)];
        if tridim
            sigtens(2,1)= m.elem(ii).sig(4);
            sigtens(1,2)= m.elem(ii).sig(4);
            sigtens(3,:)=[m.elem(ii).sig(5)   m.elem(ii).sig(6)   m.elem(ii).sig(3) ];
            sigtens(:,3)=[m.elem(ii).sig(5) ; m.elem(ii).sig(6) ; m.elem(ii).sig(3) ];
        end
%        [vals,vecs]=eig(sigtens);
        m.elem(ii).principal_stress=sort(eig(sigtens),'descend');
	end
    
end
disp(['ROMlab: Post-processing... ' num2str(toc,2) ' s']);
