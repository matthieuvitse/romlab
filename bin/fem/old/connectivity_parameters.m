function m=connectivity_parameters(m)
%  J.C. Passieux (2010)

global DATA
tic

ne=numel(m.elem);
cdof=0;
for i=1:ne
    for jj=1:size(m.elem(i).node_num,1)
        j=m.elem(i).node_num(jj);
        if(m.node(j).done==0)
            m.node(j).rep=cdof+1;
            cdof=cdof+1;
            m.node(j).done=1;
        end
        m.elem(i).rep=[m.elem(i).rep m.node(j).rep];
    end
end
m.ndof=cdof;

disp(['ROMlab: Connectivity... ' num2str(toc,2) ' s']);
