function [Beta,J]=jacobien(i)
%  A. COURARD (2013)

global DATA

y12=DATA.y12;
yA=DATA.yA;
xA=DATA.xA;
x5=DATA.x5;
x54=DATA.x54;


% %Comatrice du jacobien
% DATA.comjac_F{1}=inline('[beta+yA/y12*(1-beta) 0; 0 1]','alpha','beta','y12','yA','xA','x5','x54');
% DATA.comjac_F{2}=inline('[beta 0; 0 1]','alpha','beta','y12','yA','xA','x5','x54');
% DATA.comjac_F{3}=inline('[1 0; 0 1]','alpha','beta','y12','yA','xA','x5','x54');
% DATA.comjac_F{4}=inline('[1 0; 0 beta]','alpha','beta','y12','yA','xA','x5','x54');
% DATA.comjac_F{5}=inline('[1 0; 0 alpha]*[1 0; 0 (xA-x5+beta*(x54-xA))/(x54-x5)]','alpha','beta','y12','yA','xA','x5','x54'); 
% DATA.comjac_F{6}=inline('beta*[1 0; 0 1]','alpha','beta','y12','yA','xA','x5','x54');



switch(i)
    case 1
        %Sous-domaine 1
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)beta+yA/y12*(1-beta);
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)1};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(beta+yA/y12*(1-beta));
        
    case 2
        %Sous-domaine 2
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)beta;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)1};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(beta);
        
    case 3
        %Sous-domaine 3
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)1;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)1};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(1);
        
    case 4
        %Sous-domaine 4
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)1;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)beta};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(beta);
        
    case 5
        %Sous-domaine 5
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)1;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)alpha*(xA-x5+beta*(x54-xA))/(x54-x5)};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(alpha*(xA-x5+beta*(x54-xA))/(x54-x5));
        
    case 6
        %Sous-domaine 6
        Beta={@(alpha,beta,y12,yA,xA,x5,x54)beta;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)0;
            @(alpha,beta,y12,yA,xA,x5,x54)beta};
        
        J=@(alpha,beta,y12,yA,xA,x5,x54)abs(beta^2);
          
end


end














