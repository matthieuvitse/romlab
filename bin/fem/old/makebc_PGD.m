function m=makebc_PGD(m)
%  J.C. Passieux (2011)

global DATA

if isfield(DATA,'timeline');
    nt=numel(DATA.timeline);
else
    nt=1;
end

m.repud=[];
m.repvd=[];
m.fbc=zeros(m.ndof,nt);
m.ubc=zeros(m.ndof,nt);
m.vbc=zeros(m.ndof,nt);
for k=1:size(DATA.load,2)
    switch lower(DATA.load(k).type)
        case {'displ','depl'}
            m=makebc_disp(m,DATA.load(k));
        case {'velocity'}
            m=makebc_velo(m,DATA.load(k));
        case {'dnorm','sym'}
            m=makebc_dnorm(m,DATA.load(k));
        case {'stres','stress'}
            m=makebc_stress_PGD(m,DATA.load(k));
        case {'force'}
            m=makebc_force(m,DATA.load(k));
        otherwise
            error([DATA.load(k).type,' : unknown loading type.']);
    end
end

prescribed_dof=[m.repud m.repvd];
prescribed_dof=unique(prescribed_dof);
m.repsubdof=1:m.ndof;
m.repsubdof(prescribed_dof)=[];
