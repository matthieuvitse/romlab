function m=makebc_dispx(m,ld)

allbutM = '';
for i = 2 : length(m.params)
    allbutM = [ allbutM ',:' ];
end

for i_model = 1 : numel(m.model)

    m.model(i_model).repsubdof=sort([m.model(i_model).node(:).rep],'ascend');
    m.model(i_model).repsubdof_corres=sort([m.model(i_model).node(:).rep_corres],'ascend');
        
    for i=1:size(m.model(i_model).node,2)
        if(is_in_box(m.model(i_model).node(i).pos,ld.box))
            if numel(m.model(i_model).node(i).rep)>0
                eval(strcat('m.model(i_model).ubc(m.model(i_model).node(i).rep(1)',allbutM,')=sptensor(ld.val);'));
                eval(strcat('m.ubc(m.model(i_model).node(i).rep_corres(1)',allbutM,')=sptensor(ld.val);'));
                m.model(i_model).repud=[m.model(i_model).repud m.model(i_model).node(i).rep(1)];
                m.model(i_model).repud_corres = [m.model(i_model).repud_corres m.model(i_model).node(i).rep_corres(1)];
            end
        end
    end
       
    m.model(i_model).repsubdof([m.model(i_model).repud]) = [];    
    m.model(i_model).repsubdof_corres([m.model(i_model).repud]) = [];
    
end

end