function m=makebc_stress_time(m,ld,k)
%  J.C. Passieux (2010)

global DATA

for i=1:size(m.elem,2)
    nn=0;
    nd=[];
    for jj=1:numel(m.elem(i).node_num)
        j=m.elem(i).node_num(jj);
        if(is_in_box(m.node(j).pos,ld.box))
            nn=nn+1;
            nd=[nd j];
        end
    end

    if nn>(DATA.dim-1)
        switch nn
            case 2,
                length=norm(m.node(nd(1)).pos-m.node(nd(2)).pos);
            case 3,
                vec1=m.node(nd(2)).pos-m.node(nd(1)).pos;
                vec2=m.node(nd(3)).pos-m.node(nd(1)).pos;
                length=norm(cross(vec1,vec2))/2;
            case 4,
                vec1=m.node(nd(2)).pos-m.node(nd(1)).pos;
                vec2=m.node(nd(3)).pos-m.node(nd(1)).pos;
                length=norm(cross(vec1,vec2))/2;
                vec1=m.node(nd(3)).pos-m.node(nd(1)).pos;
                vec2=m.node(nd(4)).pos-m.node(nd(1)).pos;
                length=length+norm(cross(vec1,vec2))/2;
            otherwise,
                disp(['elem ' num2str(i) ', nodes ',num2str(nd)]);
                error('unknown skin element!');
        end
        for i=1:nn
            repi=m.node(nd(i)).rep;
            m.fbc(repi)=m.fbc(repi)+length*[DATA.loadtimex(k) DATA.loadtimey(k)]'/nn;
        end
    end
end
