function m=makebc_symy(m,ld)
%  M. Vitse - 21/10/15

allbutM = '';
for i = 2 : length(m.params)
    allbutM = [ allbutM ',:' ];
end

for i=1:size(m.node,2)
    if(is_in_box(m.node(i).pos,ld.box))
        if numel(m.node(i).rep)>0
            temp = sptensor(ld.val);
            %eval(strcat('m.ubc(m.node(i).rep(2)',allbutM,')=sptensor(ld.val);'));
            eval(strcat('m.ubc(m.node(i).rep(2)',allbutM,')=temp(2',allbutM,');'));
            m.repud=[m.repud m.node(i).rep(2)];
        end
    end
end
end
