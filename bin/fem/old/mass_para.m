function m=mass_para(m)


Mg=sparse(m.ndof,m.ndof);

for k=1:numel(m.elem)
    
    e=m.elem(k);
    ndof=size(e.rep,2);
    mass = zeros(ndof,ndof);
    
    for r = 1:size(e.pg,2)
        N=e.pg(r).N;
        [~, det_jac]=compute_gradient(e.pg(r),e,2);
        
        mass = mass + (N' * N * abs(det_jac) * e.pg(r).pd);
    end

    
    Mg(e.rep,e.rep)=Mg(e.rep,e.rep)+mass;
    
end

m.m=Mg;
end














