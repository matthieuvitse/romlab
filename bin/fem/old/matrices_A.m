function Ag=matrices_A(i,j,mpara,Beta,J)

global DATA


y12=DATA.y12;
yA=DATA.yA;
xA=DATA.xA;
x5=DATA.x5;
x54=DATA.x54;


Ag=sparse(1,mpara.ndof);


for k=1:numel(mpara.elem)
    
    e=mpara.elem(k);
    ndof=size(e.rep,2);
    m = zeros(1,ndof);
    
    for r = 1:size(e.pg,2)
        N=e.pg(r).N;
        e.pg(r).real_pos=N*e.node_pos;
        [~, det_jac]=compute_gradient(e.pg(r),e,2);
        
        a1=1/J(e.pg(r).real_pos(1),e.pg(r).real_pos(2),y12,yA,xA,x5,x54);
        a2=Beta{i}(e.pg(r).real_pos(1),e.pg(r).real_pos(2),y12,yA,xA,x5,x54);
        a3=Beta{j}(e.pg(r).real_pos(1),e.pg(r).real_pos(2),y12,yA,xA,x5,x54);
      

        m = m + a1*a2*a3*( N * abs(det_jac) * e.pg(r).pd);
    end

    
    Ag(1,e.rep)=Ag(1,e.rep)+m;
    
end

end














