function M=matrices_K(e)

global DATA
hooke_matrix=DATA.hooke;

n=e.node_pos;

M=cell(4,4);

for r=1:4
    for l=r:4
        M{r,l}=sparse(2*size(n,1),2*size(n,1));
    end
end

for i=1:size(e.pg,2)
    
    [B, det_jac]=compute_gradient(e.pg(i),e,DATA.dim);
    
    if size(B,1)==3
        
        Bx{1}=zeros(3,2*size(n,1));
        for j=1:size(n,1)
            Bx{1}(:,(2*j-1):2*j)=[B(1,2*j-1)  0.; 0.  0 ; 0  B(1,2*j-1)];
        end

        Bx{2}=zeros(3,2*size(n,1));
        for j=1:size(n,1)
            Bx{2}(:,(2*j-1):2*j)=[B(2,2*j)  0; 0.  0 ; 0  B(2,2*j)];
        end

        Bx{3}=zeros(3,2*size(n,1));
        for j=1:size(n,1)
            Bx{3}(:,(2*j-1):2*j)=[0  0.; 0.  B(1,2*j-1) ; B(1,2*j-1)  0];
        end
    
        Bx{4}=zeros(3,2*size(n,1));
        for j=1:size(n,1)
            Bx{4}(:,(2*j-1):2*j)=[0  0.; 0.  B(2,2*j) ; B(2,2*j)  0];
        end

        for r=1:4
            for l=r:4
                M{r,l}=M{r,l}+(Bx{r}' * hooke_matrix * Bx{l} * abs(det_jac) * e.pg(i).pd);

                if r~=l
                   M{l,r}=M{r,l}'; 
                end
            end
        end
        
    else
        
        %Cas axialsymétrique
        Bx{1}=zeros(4,2*size(n,1));
        for j=1:size(n,1)
            Bx{1}(:,(2*j-1):2*j)=[B(1,2*j-1)  0.; B(3,2*j-1)  0 ; 0  B(1,2*j-1)];
        end

        Bx{2}=zeros(4,2*size(n,1));
        for j=1:size(n,1)
            Bx{2}(:,(2*j-1):2*j)=[B(2,2*j)  0; 0.  0 ; 0  B(2,2*j)];
        end

        Bx{3}=zeros(4,2*size(n,1));
        for j=1:size(n,1)
            Bx{3}(:,(2*j-1):2*j)=[0  0.; 0.  B(1,2*j-1) ; B(1,2*j-1)  0];
        end
    
        Bx{4}=zeros(4,2*size(n,1));
        for j=1:size(n,1)
            Bx{4}(:,(2*j-1):2*j)=[0  0.; 0.  B(2,2*j) ; B(2,2*j)  0];
        end

        for r=1:4
            for l=r:4
                M{r,l}=M{r,l}+2*pi*(Bx{r}' * hooke_matrix * Bx{l} * abs(det_jac) * e.pg(i).pd*rayon);

                if r~=l
                   M{l,r}=M{r,l}'; 
                end
            end
        end
    
    end
end


end