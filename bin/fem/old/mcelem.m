function [mel,rep]=mcelem(m1,e2)
%  J.C. Passieux (2010)

global DATA
density=DATA.density;

mel=zeros(6,6);

% i=indice;

% elements_parcourus=[];


maillage=class_mesh;
nb_elem=0;

noeuds=[];
nb_maillages=0;


for ind=1:numel(m1.modif)
    i=m1.modif(ind);
    
    intersection=ConvexIntersect(m1.elem(i),e2);
    if any(intersection)
        nb_maillages=nb_maillages+1;
%          figure
%         hold on
% %          intersection
% %         m1.elem(i).node_pos
% %         e2.node_pos
%         line([m1.elem(i).node_pos(1,1) m1.elem(i).node_pos(2,1)],[m1.elem(i).node_pos(1,2) m1.elem(i).node_pos(2,2)]);
%         line([m1.elem(i).node_pos(2,1) m1.elem(i).node_pos(3,1)],[m1.elem(i).node_pos(2,2) m1.elem(i).node_pos(3,2)]);
%         line([m1.elem(i).node_pos(3,1) m1.elem(i).node_pos(1,1)],[m1.elem(i).node_pos(3,2) m1.elem(i).node_pos(1,2)]);
%     
%         line([e2.node_pos(1,1) e2.node_pos(2,1)],[e2.node_pos(1,2) e2.node_pos(2,2)],'Color','red');
%         line([e2.node_pos(2,1) e2.node_pos(3,1)],[e2.node_pos(2,2) e2.node_pos(3,2)],'Color','red');
%         line([e2.node_pos(3,1) e2.node_pos(1,1)],[e2.node_pos(3,2) e2.node_pos(1,2)],'Color','red');
%     
%         for j=1:size(intersection,1)
%             plot(intersection(j,1),intersection(j,2),'o')
%         end
        
        
        
        %Nombre de nouveaux noeuds
        nn=0;
        node_num=[];

        for j=1:size(intersection,1)
            test=1;
            for jj=1:size(noeuds,1)
               if noeuds(jj,:)~=intersection(j,:)
                   test=test*1;
               else 
                   test=test*0;
                   node_num=[node_num jj];
               end
            end
            if test
                noeuds=[noeuds;intersection(j,:)];
                node_num=[node_num size(noeuds,1)];
                nn=nn+1;
            end
        end
        
        
        %Maillage de l'intersection
        submesh{nb_maillages}=class_mesh;
        submesh{nb_maillages}.elem_associe=i;
        tri=delaunay(intersection(:,1),intersection(:,2));
        
        
        ne=size(tri,1);
        
        
        m.elem(nb_elem+1:nb_elem+ne)=class_elem;
        submesh{nb_maillages}.elem(1:ne)=class_elem;
        
        nn=size(noeuds,1);
        maillage.node(1:nn)=class_node;
        submesh{nb_maillages}.node(1:size(intersection,1))=class_node;
        
        for j=1:nn
           maillage.node(j).pos=noeuds(j,:);
        end
        
        for j=1:size(intersection,1)
           submesh{nb_maillages}.node(j).pos=intersection(j,:); 
        end
        
        etri3=elem_tri3;
        
        for j=1:ne
            maillage.elem(nb_elem+j).num=nb_elem+j;
            submesh{nb_maillages}.elem(j).num=j;
            maillage.elem(nb_elem+j).typel=etri3.typel;
            submesh{nb_maillages}.elem(j).typel=etri3.typel;
            maillage.elem(nb_elem+j).node_posref=etri3.node_posref;
            submesh{nb_maillages}.elem(j).node_posref=etri3.node_posref;
            maillage.elem(nb_elem+j).N=etri3.N;
            submesh{nb_maillages}.elem(j).N=etri3.N;
            maillage.elem(nb_elem+j).dN_xi=etri3.dN_xi;
            submesh{nb_maillages}.elem(j).dN_xi=etri3.dN_xi;
            maillage.elem(nb_elem+j).dN_eta=etri3.dN_eta;
            submesh{nb_maillages}.elem(j).dN_eta=etri3.dN_eta;
            maillage.elem(nb_elem+j).node_num=node_num(tri(j,:))';
            submesh{nb_maillages}.elem(j).node_num=tri(j,:)';
            
            for jj=1:3
                maillage.elem(nb_elem+j).node_pos(jj,:)=maillage.node(maillage.elem(nb_elem+j).node_num(jj)).pos;
                submesh{nb_maillages}.elem(j).node_pos(jj,:)=maillage.node(maillage.elem(nb_elem+j).node_num(jj)).pos;
            end
        end
%         
%         figure 
%         hold on
%         for i=nb_elem+1:numel(maillage.elem)
%     
%             line([maillage.elem(i).node_pos(1,1) maillage.elem(i).node_pos(2,1)],[maillage.elem(i).node_pos(1,2) maillage.elem(i).node_pos(2,2)],'Marker','.','Color','red');
%             line([maillage.elem(i).node_pos(2,1) maillage.elem(i).node_pos(3,1)],[maillage.elem(i).node_pos(2,2) maillage.elem(i).node_pos(3,2)],'Marker','.','Color','red');
%             line([maillage.elem(i).node_pos(3,1) maillage.elem(i).node_pos(1,1)],[maillage.elem(i).node_pos(3,2) maillage.elem(i).node_pos(1,2)],'Marker','.','Color','red');
%     
%         end
%         
%         
       
        
        
        nb_elem=nb_elem+ne;
        
    end
end

% figure 
%         hold on
%         for i=1:numel(maillage.elem)
%     
%             line([maillage.elem(i).node_pos(1,1) maillage.elem(i).node_pos(2,1)],[maillage.elem(i).node_pos(1,2) maillage.elem(i).node_pos(2,2)],'Marker','.','Color','red');
%             line([maillage.elem(i).node_pos(2,1) maillage.elem(i).node_pos(3,1)],[maillage.elem(i).node_pos(2,2) maillage.elem(i).node_pos(3,2)],'Marker','.','Color','red');
%             line([maillage.elem(i).node_pos(3,1) maillage.elem(i).node_pos(1,1)],[maillage.elem(i).node_pos(3,2) maillage.elem(i).node_pos(1,2)],'Marker','.','Color','red');
%     
%         end

 %Integration
maillage=integration(maillage);
maillage=connectivity(maillage);
%         keyboard

P_inter_1=sparse(maillage.ndof,length(m1.elem(submesh{j}.elem_associe).rep));
for j=1:nb_maillages
%     submesh{j}=connectivity(submesh{j});
%     submesh{j}=integration(submesh{j});

%     submesh{j}=mass(submesh{j});
%             keyboard
    submesh{j}=local_collocation(m1.elem(submesh{j}.elem_associe),submesh{j});
    P_subm_1=submesh{j}.Pc;
%     submesh{j}=local_collocation(e2,submesh{j});
%     P_inter_2=submesh{j}.Pc;
    keyboard
    for jj=1:numel(submesh{j}.elem)
        submesh{j}.elem(jj).rep=maillage.elem(j+jj).rep;
    end
    submesh{j}.ndof=numel(submesh{j}.node)*2;
%             size(P_inter_1)
%             size(P_inter_2)
%             size(submesh{j}.m)
%              keyboard
    P_inter_1=P_inter_1+P_subm_1';
end

maillage=mass(maillage);
maillage=local_collocation(e2,maillage);
P_inter_2=maillage.Pc;
mel=P_inter_1'*maillage.m*P_inter_2;
end


