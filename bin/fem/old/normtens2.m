function n=normtens2(E,i)

n=0;

for j=1:size(E,2)
    for r=1:size(E,3)
            n=n+E(i,j,r)^2;
    end
end

n=sqrt(n);
