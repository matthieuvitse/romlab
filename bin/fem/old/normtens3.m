function n=normtens3(E)

n=0;

for j=1:size(E,1)
    for r=1:size(E,2)
        for l=1:size(E,3)
            
            n=n+E(j,r,l)^2;
            
        end
    end
end

n=sqrt(n);