function n=normtens4(E)

n=0;

for i=1:size(E,1)
    for j=1:size(E,2)
        for r=1:size(E,3)
            for l=1:size(E,4)
                
                  n=n+E(i,j,r,l)^2;
                
            end
        end
    end
end

n=n^(1/2);