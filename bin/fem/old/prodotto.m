function prod=prodotto(U,V,K)

global DATA

alpha=DATA.alpha;
beta=DATA.beta;

B=cell(length(alpha),length(beta));

for r=1:length(alpha)
    for l=1:length(beta)
        B{r,l}=U(:,r,l)'*(K{r,l}\V(:,r,l)); 
    end
end

prod=trapeze_mat(alpha,beta,B);

end