function [res,tf] = residual(U,G,Krig,f,Fpara,mpara,subdomain,i)

global DATA

nb_subd=DATA.subdomains;

nmod = i;

%Initialisation
res=outprod(Krig{1,1,1}*U(1,:)',subdomain(1).Mg{1,1}*G(1,:)');

for k=1:nmod
    for r=1:4
        for l=1:4
            for sub=1:nb_subd
                if k*r*l*sub ~= 1
                    res=res+outprod(Krig{l,r,sub}*U(k,:)',subdomain(sub).Mg{r,l}*G(k,:)');
                end
            end
        end
    end
end

%RHS
tf=outprod(f,mpara.m*Fpara);

res=tf-res;



end