function m=scal_prod(m)
%  J.C. Passieux (2010)
tic

% ELEMENTARY STIFFNESS MATRICES
ne=size(m.elem,2);
for i=1:ne
	m.elem(i).scal=scalelem(m.elem(i));
end

% GLOBAL STIFFNESS MATRIX
m.scal=sparse([],[],[],m.ndof,m.ndof);

for i=1:ne
	m.scal(m.elem(i).rep,m.elem(i).rep)=m.scal(m.elem(i).rep,m.elem(i).rep)+m.elem(i).scal;
end

disp(['ROMlab: Assembling stiffness operator... ' num2str(toc,2) ' s']);
