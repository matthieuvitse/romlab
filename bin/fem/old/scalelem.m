function scal=scalelem(e)
%  J.C. Passieux (2010)

global DATA


ndof=size(e.rep,2);
scal = zeros(ndof,ndof);


%keyboard


for i = 1:size(e.pg,2)
    N=shape_matrix(e.pg(i),e,DATA.dim);
    [~, det_jac]=compute_gradient(e.pg(i),e,DATA.dim);
		scal = scal + (N' * N * abs(det_jac) * e.pg(i).pd);
end

% abs(det_jac)