function m=stiffness_passieux(DATA,m)
%  J.C. Passieux (2010)
tic

disp(['ROMlab: Assembling stiffness operator... [OLD VERSION]']);

% ELEMENTARY STIFFNESS MATRICES
ne=size(m.elem,2);

% GLOBAL STIFFNESS AND L2 MATRIX
m.k1=sparse([],[],[],m.ndof,m.ndof);
m.h11=sparse([],[],[],m.ndof,m.ndof);

for i=1:ne
    
    %%%% RUN %%%%
    % Hooke operator \int defor U * K * defor V
    m.elem(i).k=kelem(DATA,m.elem(i),'DATA.hooke');
    m.k1(m.elem(i).rep,m.elem(i).rep)=m.k1(m.elem(i).rep,m.elem(i).rep)+m.elem(i).k;
    % H1 operator \int defor U * defor V
    elem(i).k=kelem(DATA,m.elem(i),'DATA.h1');
    m.h11(m.elem(i).rep,m.elem(i).rep)=m.h11(m.elem(i).rep,m.elem(i).rep)+elem(i).k;
end

disp([ num2str(toc,2) ' s']);
