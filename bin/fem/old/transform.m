function sol=transform(u,alpha,beta)

global DATA



sol=zeros(length(u),1);

jacobien_transform_domaine(alpha,beta)

for j=1:length(u)/2
    
   if (u(j+1)>= DATA.yA)
        if (u(j)>= DATA.x54)
            sol(j:j+1)=DATA.jac_F{5}*u(j:j+1)+[(1-alpha)*DATA.x54*(DATA.xA-DATA.x5+beta*(DATA.x54-DATA.xA))/(DATA.x54-DATA.x5)+(1-beta)*DATA.x5*(DATA.x54-DATA.xA)/(DATA.x54-DATA.x5);0];
                
                
        elseif (u(j)>= DATA.xA)
            sol(j:j+1)=DATA.jac_F{4}*u(j:j+1)+[(1-beta)*DATA.xA;0];
        else sol(j:j+1)=u(j:j+1);
        end


    elseif (u(j)<= DATA.xA)
        if (u(j+1)<= DATA.y12)
            sol(j:j+1)=DATA.jac_F{1}*u(j:j+1);

        else sol(j:j+1)=DATA.jac_F{2}*u(j:j+1)+[0;(1-beta)*DATA.yA];
        end


   else sol(j:j+1)=DATA.jac_F{6}*u(j:j+1)+[(1-beta)*DATA.xA;(1-beta)*DATA.yA];
   end

end
    
end