function [fx,gx,fy,gy]=transformation(sub)
    
    switch(sub)
        
        case 1
          
            fx=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gx=@(alpha,beta,y12,yA,xA,x5,x54)0;
            fy=@(alpha,beta,y12,yA,xA,x5,x54)beta+yA/y12*(1-beta);
            gy=@(alpha,beta,y12,yA,xA,x5,x54)0;
           
        case 2
            
            fx=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gx=@(alpha,beta,y12,yA,xA,x5,x54)0;
            fy=@(alpha,beta,y12,yA,xA,x5,x54)beta;
            gy=@(alpha,beta,y12,yA,xA,x5,x54)(1-beta)*yA;
            
        case 3
            
            fx=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gx=@(alpha,beta,y12,yA,xA,x5,x54)0;
            fy=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gy=@(alpha,beta,y12,yA,xA,x5,x54)0;
            
        case 4
            
            fx=@(alpha,beta,y12,yA,xA,x5,x54)beta;
            gx=@(alpha,beta,y12,yA,xA,x5,x54)(1-beta)*xA;
            fy=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gy=@(alpha,beta,y12,yA,xA,x5,x54)0;
            
        case 5
            
            fx=@(alpha,beta,y12,yA,xA,x5,x54)alpha*(xA-x5+beta*(x54-xA))/(x54-x5);
            gx=@(alpha,beta,y12,yA,xA,x5,x54)(1-alpha)*x54*(xA-x5+beta*(x54-xA))/(x54-x5)+(1-beta)*x5*(x54-xA)/(x54-x5);
            fy=@(alpha,beta,y12,yA,xA,x5,x54)1;
            gy=@(alpha,beta,y12,yA,xA,x5,x54)0;
            
        case 6
        
            fx=@(alpha,beta,y12,yA,xA,x5,x54)beta;
            gx=@(alpha,beta,y12,yA,xA,x5,x54)xA*(1-beta);
            fy=@(alpha,beta,y12,yA,xA,x5,x54)beta;
            gy=@(alpha,beta,y12,yA,xA,x5,x54)yA*(1-beta);
        
        
    end
end