function A=trapeze_mat(x,y,K)


B=cell(1,size(K,1));

hx=x(2)-x(1);
hy=y(2)-y(1);

% for i=1:length(x)
%     B{i}=zeros(size(K{1,1}));
%     
%     for j=1:length(y)-1
%         B{i}=B{i}+(K{i,j}+K{i,j+1})*(y(j+1)-y(j))/2;
%     end
%     
% end
% 
% for i=1:length(x)-1
%     A=A+(B{i}+B{i+1})*(x(i+1)-x(i))/2;
% end

% disp('intégration beta')
for i=1:length(x)
    B{i}=sparse(size(K{1,1},1),size(K{1,1},2));
    
    for j=2:length(y)-1
        B{i}=B{i}+K{i,j};
    end
    
    B{i}=B{i}+(K{i,1}+K{i,length(y)})/2;
    B{i}=B{i}*hy;
    
end


A=sparse(size(K{1,1},1),size(K{1,1},2));
% disp('intégration alpha')
for i=2:length(x)-1
    A=A+B{i};
end

A=A+(B{1}+B{length(x)})/2;
A=A*hx;

end









