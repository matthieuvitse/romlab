function A=trapeze_mat2(x,y,K)

%Pas
hx=x(2)-x(1);
hy=y(2)-y(1);

nl=size(K{1,1},1);
nc=size(K{1,1},2);

% %Opérateur d'intégration selon x
% INT_gauche=repmat(2*eye(nl),1,length(x));
% ng=size(INT_gauche,2);
% INT_gauche(1:nl,1:nl)=eye(nl);
% INT_gauche(1:nl,ng-nl+1:ng)=eye(nl);
% 
% %Opérateur d'intégration selon y
% INT_droite=repmat(2*eye(nc),length(y),1);
% nd=size(INT_droite,1);
% INT_droite(1:nc,1:nc)=eye(nc);
% INT_droite(nd-nc+1:nd,1:nc)=eye(nc);

B=zeros(nl*length(x),nc);
% 
% for i=1:length(x)
%     
%     for j=2:length(y)-1
%         B((i-1)*nl+1:i*nl,1:nc)=B((i-1)*nl+1:i*nl,1:nc)+2*K{i,j};
%     end
%     
%     B((i-1)*nl+1:i*nl,1:nc)=B((i-1)*nl+1:i*nl,1:nc)+K{i,1}+K{i,length(y)};
%     
% end



A=hx/2*hy/2*INT_gauche*K*INT_droite;


end