function r=which_subdomain(e)

global DATA

y12=DATA.y12;
yA=DATA.yA;
xA=DATA.xA;
x5=DATA.x5;
x54=DATA.x54;


% Position de l'�l�ment
    if (any(e.node_pos(:,1)> x54))
        r=5;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'o','Color','red');
%         end

    elseif (any(e.node_pos(:,1)> xA))&&(any(e.node_pos(:,2)< yA))
        r=6;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'o','Color','blue');
%         end
        
     elseif (any(e.node_pos(:,1)> xA))
        r=4;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'x','Color','red');
%         end    
        

    elseif (any(e.node_pos(:,2)> yA))
        r=3;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'o','Color','green');
%         end

    elseif (any(e.node_pos(:,2)> y12))
        r=2;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'o','Color','black');
%         end
        
    else
        r=1;
        
%         for i=1:3
%             plot(e.node_pos(i,1),e.node_pos(i,2),'o','Color','yellow');
%         end
        
    end

end