function flag=point_in_triangle(P,A,B,C)

flag=0;


%%%Triangle
    h1=norm(A-B);
    h2=norm(C-B);
    h3=norm(A-C);

    L=sort([h1,h2,h3]);
    a=L(3);
    b=L(2);
    c=L(1);
    
    %Aire de l'�l�ment(formule de Kahan)
    Aire=1/4*sqrt((a+b+c)*(c-a+b)*(c+a-b)*(a+b-c));

%%%Triangle 1
    h1=norm(A-B);
    h2=norm(P-B);
    h3=norm(A-P);
    
    L=sort([h1,h2,h3]);
    a=L(3);
    b=L(2);
    c=L(1);

    %Aire de l'�l�ment(formule de Kahan)
    Aire1=1/4*sqrt((a+b+c)*(c-a+b)*(c+a-b)*(a+b-c));

%%%Triangle 2
    h1=norm(A-P);
    h2=norm(C-P);
    h3=norm(A-C);
    
    L=sort([h1,h2,h3]);
    a=L(3);
    b=L(2);
    c=L(1);

    %Aire de l'�l�ment(formule de Kahan)
    Aire2=1/4*sqrt((a+b+c)*(c-a+b)*(c+a-b)*(a+b-c));

%%%Triangle 3
    h1=norm(B-P);
    h2=norm(C-P);
    h3=norm(B-C);
    
    L=sort([h1,h2,h3]);
    a=L(3);
    b=L(2);
    c=L(1);

    %Aire de l'�l�ment(formule de Kahan)
    Aire3=1/4*sqrt((a+b+c)*(c-a+b)*(c+a-b)*(a+b-c));

    
    if abs(Aire-(Aire1+Aire2+Aire3))/Aire<0.01
        flag=1;
    end

end