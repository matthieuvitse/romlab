function s=prod_scal_matrix(M,N)

s=trace(M'*N);

end