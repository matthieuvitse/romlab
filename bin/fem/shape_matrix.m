function NN=shape_matrix(pg,e,dim)
%  J.C. Passieux (2010)

N=pg.N;
n=e.node_pos;
NN=zeros(dim,dim*size(n,1));
for j=1:size(n,1)
    NN(:,(dim*j-2):dim*j)= eye(dim)*N(j);
end

%     ATTENTION : modif pour essayer 3D avec MV.
%
%     NN=zeros(2,2*size(n,1));
%     for j=1:size(n,1)
%         NN(:,(2*j-1):2*j)=[N(j)  0.; 0.  N(j)];
%     end
