function m=stiffness(in_out,DATA,m)

disp('ROMlab: Assembling stiffness operators... ');
tic
m.k            = spalloc(m.ndof,m.ndof,1);
m.h1           = spalloc(m.ndof,m.ndof,1);
m.I_g          = spalloc(m.ndofgp,m.ndofgp,1);
m.B_g          = spalloc(m.ndofgp,m.ndof,1);
m.hooke_g      = spalloc(m.ndofgp,m.ndofgp,1);
m.hooke_inv_g  = spalloc(m.ndofgp,m.ndofgp,1);

for i_model = 1 : numel(DATA.model)
    
    m.model(i_model).detjac =[];
    m.model(i_model).var_E_g = [];
    m.model(i_model).k_inv = [];
    m.model(i_model).h1 = [];
    m.model(i_model).B_g = [];
    m.model(i_model).hooke_g = [];
    m.model(i_model).hooke_inv_g = [];
    m.model(i_model).I_g = [];
    
%     model_i = hooke_model(in_out,DATA,m.model(i_model),i_model);
    
%     m.model(i_model) = model_i;
    
        dnof_loc            = m.model(i_model).ndof;
        m.model(i_model).k  = spalloc(dnof_loc,dnof_loc, 1);
    
        [B_g,detjac] = grad_op_glob(DATA,m.model(i_model),DATA.model(i_model).type);
    
        m.model(i_model).detjac  = detjac;
        [hooke_g,var_E_g]        = hooke_global(in_out,DATA.model(i_model),m.model(i_model),'hooke');
        m.model(i_model).var_E_g = var_E_g;
        [hooke_inv_g,~]          = hooke_global(in_out,DATA.model(i_model),m.model(i_model),'invhooke');
        I_g                      = integ_full(DATA.model(i_model),m.model(i_model));
    
        m.model(i_model).k =  B_g' * I_g *  hooke_g * B_g;                % assembled stiffness operator
        m.model(i_model).k_inv = B_g' * I_g *  hooke_inv_g * B_g;
        disp(['    ... stiffness matrix ',num2str(i_model),' assembled...']);
    
        % LU decomposition of k
        % [m.k_l,m.k_u] = lu(m.k);
    
        [H_1,~] = hooke_global(in_out,DATA.model(i_model),m.model(i_model),'h1');
    
        m.model(i_model).h1 = B_g' * I_g * H_1 * B_g;     % assembled h1 integration operator
        disp(['    ... h1 operator ',num2str(i_model),' assembled...']);
    
        m.model(i_model).B_g          = B_g;
        m.model(i_model).hooke_g      = hooke_g;
        m.model(i_model).hooke_inv_g  = hooke_inv_g;
        m.model(i_model).I_g          = I_g;
    %
    % we assemble the big coupled matrix
    dof         = [m.model(i_model).node(:).dof];
    dof_corres  = [m.model(i_model).node(:).dof_corres];
    dofgp_model = m.model(i_model).dofgp;
    dof_node = m.model(i_model).dof_node;
    
    if i_model == 1
        m.B_g(dofgp_model,sort(dof_corres,'ascend')) = m.B_g(dofgp_model,sort(dof_corres,'ascend')) + m.model(i_model).B_g;
    else
        for iii = 1 : numel(m.model(i_model).node)
            if  ~isempty(m.model(i_model).node(iii).dof)
                dof2(iii,1) = m.model(i_model).node(iii).num;
                dof2(iii,2:(dof_node+1)) = m.model(i_model).node(iii).dof;
            end
        end
        [~,toto] = sort(dof2(:,2),'ascend');
        dof_corres2 = [m.model(i_model).node(toto).dof_corres];
        
        m.B_g(dofgp_model,dof_corres2) = m.B_g(dofgp_model,dof_corres2) + m.model(i_model).B_g;
    end
    m.k(dof_corres,dof_corres)      = m.k(dof_corres,dof_corres) + m.model(i_model).k(dof,dof);
    m.h1(dof_corres,dof_corres)     = m.h1(dof_corres,dof_corres) +  m.model(i_model).h1(dof,dof);
    m.I_g(dofgp_model,dofgp_model)  = m.I_g(dofgp_model,dofgp_model) + m.model(i_model).I_g;
    m.hooke_g(dofgp_model,dofgp_model)     = m.hooke_g(dofgp_model,dofgp_model) + m.model(i_model).hooke_g;
    m.hooke_inv_g(dofgp_model,dofgp_model) = m.hooke_inv_g(dofgp_model,dofgp_model) + m.model(i_model).hooke_inv_g;
    
end




disp([ '    ... operators assembled... ', num2str(toc,2) ' s']);

end
