function DATA = update_hooke(DATA,scal)
%  J.C. Passieux (2010)

if nargin==1
    scal=1;
end

for i_model = 1 : numel(DATA.model)
    E       =  DATA.model(i_model).material.E;
    nu      =  DATA.model(i_model).material.nu;
    lambda  =  DATA.model(i_model).material.lambda;
    mu      =  DATA.model(i_model).material.mu;
    
    if strcmp(DATA.model(i_model).type,'TRUSS')
        %hooke      = zeros(6);
        hooke = E;
        %flex       = zeros(6);
        flex  = inv(E);
        
        %h1 = eye(6);
        h1 = 1;
        
        DATA.model(i_model).material.hooke = hooke.*scal;
        DATA.model(i_model).material.flex  = flex;
        DATA.model(i_model).h1 = h1;
        
    elseif strcmp(DATA.model(i_model).type,'FE2D')
        
        if(DATA.plst)
            % plane strain
            hooke = E / ((1-2*nu)*(1+nu)) * [ 1-nu  nu    0
                nu    1-nu  0
                0       0             (1-2*nu)];
        else
            % plane stress
            hooke = E / (1-(nu^2)) * [ 1        nu  0
                nu  1        0
                0        0        (1-nu)];
        end
        
        h1 = eye(3);
        
        DATA.model(i_model).material.hooke = hooke.*scal;
        DATA.model(i_model).material.flex  = inv(hooke);
        DATA.model(i_model).h1 = h1;
        
    elseif strcmp(DATA.model(i_model).type,'FE3D')
        
        % premultiplication by sqrt(2) ignored for shear terms.
        %
        % theory: 	eps = [epsxx epsyy epszz sqrt(2).epsxy sqrt(2).epsxz sqrt(2).epsyz]';
        %  		    sig = [sigxx sigyy sigzz sqrt(2).sigxy sqrt(2).sigxz sqrt(2).sigyz]';
        %    %
        % so that tr(sig.eps) is ok.
        
        hooke = zeros(6);
        hooke(1:3,1:3) = 2*mu*eye(3) + lambda*ones(3);
        hooke(4:6,4:6) = 2*mu*eye(3);
        
        h1 = eye(6);
        
        DATA.model(i_model).material.hooke = hooke.*scal;
        DATA.model(i_model).material.flex  = inv(hooke);
        DATA.model(i_model).h1 = h1;
        
    end
    
end

end