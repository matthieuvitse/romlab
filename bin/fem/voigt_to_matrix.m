function [ m ] = voigt_to_matrix(DATA,m)
% MV
% conversion matrix <-> Voigt vector notations

if strcmp(DATA.type,'FE3D')
    
    %voigt to matrix
    
    %  /eps_xx\
    % | eps_xy |         /    eps_xx    \
    % | eps_xz |        |     eps_yy     |
    % | eps_xy |        |     eps_zz     |
    % | eps_yy | = OP * | sqrt(2)*eps_xy |
    % | eps_yz |        | sqrt(2)*eps_xz |
    % | eps_xy |         \sqrt(2)*eps_yz/
    % | eps_yz |
    %  \eps_zz/
    
    OP=sparse(9,6);
    OP(1,1)=1;        OP(9,3)=1;        OP(5,2)=1;
    OP(2,4)=1/sqrt(2);OP(3,5)=1/sqrt(2);OP(4,4)=1/sqrt(2);
    OP(6,6)=1/sqrt(2);OP(7,5)=1/sqrt(2);OP(8,6)=1/sqrt(2);
    
    % matrix to voigt
    
    %                                /eps_xx\
    %  /    eps_xx    \             | eps_xy |         
    % |     eps_yy     |            | eps_xz |        
    % |     eps_zz     |            | eps_xy |        
    % | sqrt(2)*eps_xy | = OP_inv * | eps_yy |  
    % | sqrt(2)*eps_xz |            | eps_yz |        
    %  \sqrt(2)*eps_yz/             | eps_xy |        
    %                               | eps_yz |
    %                                \eps_zz/
    
    OP_inv = sparse(6,9);
    OP_inv(1,1) = 1;       OP_inv(2,5) = 1 ;       OP_inv(3,9) = 1;
    OP_inv(4,4) = sqrt(2); OP_inv(5,3) = sqrt(2) ; OP_inv(6,6) = sqrt(2) ;
    
elseif strcmp(DATA.type,'FE2D')
    
    %voigt to matrix
    
    %  /eps_xx\          /    eps_xx    \
    % | eps_xy | = OP * |     eps_yy     |  
    % | eps_xy |         \sqrt(2)*eps_xy/
    %  \eps_yy/  
  
    OP = sparse(4,3);
    OP(1,1) = 1;            OP(2,3) = 1/sqrt(2);
    OP(3,3) = 1/sqrt(2);    OP(4,2) = 1;
    
    %  /    eps_xx    \               /eps_xx\ 
    % |     eps_yy     |  = OP_inv * | eps_xy |
    %  \sqrt(2)*eps_xy/              | eps_xy |        
    %                                   \eps_yy/ 
    
    OP_inv = sparse(3,4);
    OP_inv(1,1) = 1;        OP_inv(3,2) = sqrt(2);
    OP_inv(2,4) = 1;
    
elseif strcmp(DATA.type,'TRUSS')
    OP = 1;
    OP_inv = 1;
end

m.OP = OP;
m.OP_inv = OP_inv;

end

