function [hat_dg,hat_eps,hat_sigma,YYmax] = local_stage_parallel_element(in_out,DATA_g,DATA,m_g,m,s_linear,k_param)
% M. Vitse

warning('has to be changed in order to work')
foo;keyboard

if DATA_g.dim == 3
    comp = 6;            % number of components in the elementary eps/sig matrices
end

% substepping
nT = m_g.params(2).ndof;
lT = m_g.params(2).mesh;
nstep = 1;
if isfield(DATA,'substepping')
    if DATA.substepping.on
        nstep = DATA.substepping.step;
        [u_loc,v_loc] = meshgrid(1 : 1/nstep : nT , 1 : 1 : comp);
        
    end
    
end
lT_step = lT(1) : 1/nstep : lT(end) ;

% material parameters
hooke = DATA.material.hooke;
F_Y   = DATA.behavior.F_Y;         % function for damage variable d
Ad    = DATA.material.Ad;
Y0    = DATA.material.Y0;

if ~isfield(m,'OP') ||  ~isfield(m,'OP_inv')
    m = voigt_to_matrix(DATA,m);
end

YYmax   = 0;
Hup_loc = m.H_loc;
eps     = s_linear.eps;
sigma   = s_linear.sigma;

for i_elem = 1 : numel(m.elem)
    
    if ~ismember(i_elem,m_g.elem_in_box)
        
        npg_i = numel(m.elem(i_elem).gp);
        
        if 1 == 1 % trying different methods for treating the local stage
            
            for gp = 1 : npg_i
                
                if in_out.unilateral
                    eps_f_max   = 1e-6*ones(comp,1);
                    hat_sigma_f = zeros(comp,1);
                    hat_eps_f   = zeros(comp,1);
                end
                
                hat_Z = zeros(m.ngp_tot,nT);
                gp_current_time = (i_elem-1) * npg_i + gp;                % current Gauss Point
                dof_eps_gp = ((i_elem-1) * npg_i * comp + (gp-1) * comp + 1) : ((i_elem-1) * npg_i* comp + gp * comp);  % eps values at gauss point gp_current
                
                eps_loc_temp.U{1}  = eps.U{1}(dof_eps_gp,:);
                eps_loc            = ktensor(eps.lambda,eps_loc_temp.U{1},eps.U{2},eps.U{3}(k_param,:));
                eps_loc_full       = double(full(eps_loc));
                
                sigma_loc_temp.U{1}  = sigma.U{1}(dof_eps_gp,:);
                sigma_loc            = ktensor(sigma.lambda,sigma_loc_temp.U{1},sigma.U{2},sigma.U{3}(k_param,:));
                sigma_loc_full       = double(full(sigma_loc));
                
                
                for j_time_step = 1 : length(lT_step)
                    
                    eps_loc_full_int    = interp2(eps_loc_full,u_loc,v_loc);
                    sigma_loc_full_int  = interp2(sigma_loc_full,u_loc,v_loc);
                    
                    vec1           = eps_loc_full_int(:,j_time_step);
                    vec2           = m.OP * vec1;
                    mat1           = reshape(vec2,3,3);
                    
                    [v_mat,D_mat]  = eig(mat1);
                    D_mat_pos      = 0.5 * ( D_mat+abs(D_mat) );
                    hat_eps_pos    = v_mat * D_mat_pos * v_mat';
                    
                    % computation of threshold
                    hat_eps_pos_vec1 = reshape(hat_eps_pos,9,1);
                    hat_eps_pos_vec = m.OP_inv * hat_eps_pos_vec1;
                    
                    YY = 1/2 * hat_eps_pos_vec' * hooke * hat_eps_pos_vec ;
                    if j_time_step == 1
                        fd = YY - (Y0);
                    else
                        fd = YY - (Y0 + hat_Z(gp_current_time, j_time_step-1));
                    end
                    
                    % computation of damage
                    if fd > 0
                        hat_d(gp_current_time,j_time_step) = F_Y(YY,Y0,Ad);
                        hat_Z(gp_current_time,j_time_step) = YY - Y0;
                    elseif j_time_step == 1
                        hat_d(gp_current_time,j_time_step) = 0;
                        hat_Z(gp_current_time,j_time_step) = 0;
                    else
                        hat_d(gp_current_time,j_time_step) = hat_d(gp_current_time,j_time_step-1);
                        hat_Z(gp_current_time,j_time_step) = hat_Z(gp_current_time,j_time_step-1);
                    end
                    
                    if hat_d(gp_current_time,j_time_step) > 1
                        warning('damage greater than 1 !')
                        foo;keyboard
                    end
                    if hat_d(gp_current_time,j_time_step) < 0
                        warning('damage is negative!')
                        foo;keyboard
                    end
                    
                    % regularization of damage variable
                    if j_time_step == 1
                        hat_d_prev_time = [];
                    else
                        hat_d_prev_time = hat_d(gp_current_time,j_time_step-1);
                    end
                    [hat_d_reg(gp_current_time,j_time_step)] = regularization(in_out,DATA,hat_d(gp_current_time,j_time_step),hat_d_prev_time,j_time_step,lT_step);
                    
                    hat_dg_step(dof_eps_gp, j_time_step)= repmat(hat_d_reg(gp_current_time,j_time_step),6,1);
                    hooke_d_loc                   = (1 - hat_d_reg(gp_current_time,j_time_step)) * hooke ;
                    
                    % computation of hat_eps and hat_sigma using the local search direction
                    alpha_n                   = Hup_loc * sigma_loc_full_int(:,j_time_step) + eps_loc_full_int(:,j_time_step);           % WE CAN IMPROVE THAT
                    A                         = Hup_loc * hooke_d_loc + eye(size(hooke_d_loc));
                    hat_eps_step(dof_eps_gp, j_time_step)    = A \ alpha_n;
                    hat_sigma_step(dof_eps_gp, j_time_step)  = hooke_d_loc * hat_eps_step(dof_eps_gp, j_time_step) ;
                    
                    % unilateral effect
                    if in_out.unilateral
                        delta_eps_f = hat_dg_step(dof_eps_gp, j_time_step) .* hat_eps_step(dof_eps_gp, j_time_step) - hat_eps_f;
                        [hat_sigma_f,eps_f_max,hat_eps_f] = unilateral(in_out,DATA,hat_dg_step(dof_eps_gp, j_time_step),hat_eps_step(dof_eps_gp, j_time_step),eps_f_max,hat_sigma_f,delta_eps_f);
                        hat_sigma_step(dof_eps_gp, j_time_step) = hat_sigma_step(dof_eps_gp, j_time_step) + hat_sigma_f;
                    end
                    
                    if YY > YYmax
                        YYmax = YY;
                    end
                end
                
                hat_eps(dof_eps_gp,:)   = hat_eps_step(dof_eps_gp,1:nstep:end);
                hat_sigma(dof_eps_gp,:) = hat_sigma_step(dof_eps_gp,1:nstep:end);
                hat_dg(dof_eps_gp,:)    = hat_dg_step(dof_eps_gp,1:nstep:end);
                
            end
            
        else
            % vectorialized resolution on the gauss points... analytic
            % computation of the eigenvalues and eigenvectors, ...
            foo;keyboard
            eye_i = eye(npg_i);
            OP_elem = kron(eye_i,m_g.OP);
            
            warning('To be continued...')
            foo; keyboard;
        end
        
    else
        
        npg_i = numel(m.elem(i_elem).gp);
        dof_eps = ((i_elem-1) * npg_i * comp  + 1) : ((i_elem-1) * npg_i* comp + npg_i * comp);
        
        eps_loc_temp.U{1}  = eps.U{1}(dof_eps,:);
        eps_loc = double(full(ktensor(eps.lambda,eps_loc_temp.U{1},eps.U{2},eps.U{3}(k_param,:))));
        hat_eps(dof_eps,1:nT) = eps_loc;
        
        sigma_loc_temp.U{1}  = sigma.U{1}(dof_eps,:);
        sigma_loc            = double(full(ktensor(sigma.lambda,sigma_loc_temp.U{1},sigma.U{2},sigma.U{3}(k_param,:) )));
        hat_sigma(dof_eps,1:nT) = sigma_loc;
        
        hat_dg(dof_eps,1:nT) = zeros(numel(dof_eps),nT);
        
    end
    
end


end