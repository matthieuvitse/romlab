function indic = compute_L2_indicator(m,U1,U2)
% Compute the l2norm of U1,U2 and ||U2||/||U1||

L2U1 = 0; % L2 norm of U1
% keyboard
    for L = 1:size(U1.lambda,1)
        a=1;
        for i = 1 : ndims(U1)
            a = a * U1.U{i}(:,L)'*m.params(i).integ2*U1.U{i}(:,L);
        end
        L2U1 = L2U1 + a;
    end
L2U1 =  sqrt(L2U1);
    

L2U2 = 0; % L2 norm U2
    for L = 1:size(U2.lambda,1)
        a=1;
        for i = 1 : ndims(U2)
            a = a * U2.U{i}(:,L)'*m.params(i).integ2*U2.U{i}(:,L);
        end
        L2U2 = L2U2 + a;
    end
L2U2 =  sqrt(L2U2);

% keyboard
indic = L2U2/L2U1; % Variation indicator

end