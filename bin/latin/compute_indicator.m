function indic = compute_indicator(m,s1,s2,part_val)

%[~,~,I_g,~,hooke_g,~] = computed_coupled_operators(m);
part = 1;
hooke_g = m.hooke_g;
I_g = m.I_g;
I_t = m.params(2).integ1;

if part
    if numel(m.params)==2
        ne1 = 1;
        ne2 = 1;
    elseif numel(m.params)==3
        ne1 = part_val;
        ne2 = 1;
    elseif numel(m.params)==4
        ne1 = part_val; % the indicator is computed for one set of parameters
        ne2 = part_val;
    end
    
    eps01 = double(full(s1.eps));
    eps02 = double(full(s2.eps));
    
    eps1 = eps01(:,:,ne1,ne2);
    eps2 = eps02(:,:,ne1,ne2);
    clear eps01 eps02

    indic = sqrt(I_t * ( dot (eps2 - eps1 , I_g * hooke_g * (eps2 - eps1) )' ) ...
        / ( I_t * ( dot (1/2 * (eps2 + eps1) , I_g * hooke_g * 1/2 * (eps2 + eps1) )' ) ) );
    
else
    if numel(m.params)==2
        ne1 = 1;
        ne2 = 1;
        eps1 = double(full(s1.eps));
        eps2 = double(full(s2.eps));
        
	indic = sqrt(I_t * ( dot (eps2 - eps1 , I_g * hooke_g * (eps2  - eps1 ) )' ) ...
            / ( I_t * ( dot (1/2 * (eps2 + eps1 ) , I_g * hooke_g * 1/2 * (eps2 + eps1 ))' ) ) );
        
    elseif numel(m.params)==3
        ne1 = m.params(3).ndof;
        ne2 = 1;
        indic1 = 0;
        for k_param = 2 : ne1
            A= tensor(s1.eps);
            eps11 = double(A(:,:,k_param));
            eps12 = double(A(:,:,k_param-1));
            eps21 = double(full(s2.eps(:,:,k_param)));
            eps22 = double(full(s2.eps(:,:,k_param-1)));
            indic1 = indic1 + 1/2 * (m.params(3).mesh(k_param) - m.params(3).mesh(k_param-1) ) * ( I_t * ( dot (eps21 - eps11 , I_g * hooke_g * (eps21 - eps11) )' ) / ( I_t * ( dot (1/2 * (eps21 + eps11) , I_g * hooke_g * 1/2 * (eps21 + eps11))' ) )  ...
                +  I_t * ( dot (eps22 - eps12 , I_g * hooke_g * (eps22 - eps12) )' ) / ( I_t * ( dot (1/2 * (eps22 + eps12) , I_g * hooke_g * 1/2 * (eps22 + eps12))' ) )  );
        end
        indic = sqrt(indic1);
        
    elseif numel(m.params)==4
        ne1 = m.params(3).ndof; % the indicator is computed for one set of parameters
        ne2 = m.params(4).ndof;
        
        indic1 = 0;
        for j_param = 1 : ne1
            indic1(j_param) = 0;
            for k_param = 2 : ne2
                A= tensor(s1.eps);
                eps11 = double(A(:,:,j_param,k_param));
                eps12 = double(A(:,:,j_param,k_param-1));
                eps21 = double(full(s2.eps(:,:,j_param,k_param)));
                eps22 = double(full(s2.eps(:,:,j_param,k_param-1)));
                indic1(j_param) = indic1(j_param) + 1/2 * (m.params(4).mesh(k_param) - m.params(4).mesh(k_param-1) ) * ( I_t * ( dot (eps21 - eps11 , I_g * hooke_g * (eps21 - eps11) )' ) / ( I_t * ( dot (1/2 * (eps21 + eps11) , I_g * hooke_g * 1/2 * (eps21 + eps11) )' ) )  ...
                    +  I_t * ( dot (eps22 - eps12 , I_g * hooke_g * (eps22 - eps12) )' ) / ( I_t * ( dot (1/2 * (eps22 + eps12) , I_g * hooke_g * 1/2 * (eps22 + eps12 ))' ) ) ) ;
            end
        end
        
        indic2 = 0;
        for i_param = 2 : ne1
            indic2 = indic2 + 1/2*(m.params(3).mesh(i_param) - m.params(3).mesh(i_param-1)) * (indic1(i_param) + indic1(i_param-1));
        end
        indic = sqrt(indic2);
    end
    
end

end
