function [m] = compute_operators_latin(in_out,DATA,m)

tic
disp(['    ... computing LATIN operators... ']);

% search directions
for i_model = 1 : numel(m.model)
    H_loc         = 1 .* DATA.model(i_model).material.flex;
    H_loc_inv     = inv(H_loc);
    Hup_inv       = search_direction(m.model(i_model),H_loc_inv,'regular');
    Hdo_inv_model = Hup_inv;
    
    m.model(i_model).H_loc      = H_loc;
    m.model(i_model).H_loc_inv  = H_loc_inv;
    m.model(i_model).Hdo_inv    = Hdo_inv_model;
    
end
Hdo_inv      = sparse(m.ndofgp,m.ndofgp);
dofgp_model1 = [1 : m.model(1).ndofgp];
Hdo_inv(dofgp_model1,dofgp_model1) = m.model(1).Hdo_inv;

if numel(m.model) > 1
    dofgp_model2 = [(m.model(1).ndofgp+1) : (m.model(1).ndofgp + m.model(2).ndofgp)];
    Hdo_inv(dofgp_model2,dofgp_model2) = m.model(2).Hdo_inv;
end

m.Hdo_inv = Hdo_inv;

disp(['        ... LATIN operators computed... ',num2str(toc,2),' s']);

end