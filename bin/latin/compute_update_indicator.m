function indic = compute_update_indicator(m,e1,s_local,s_update)

e2 = compute_indicator(m,s_update,s_local,3);

indic = abs((e1 - e2)/e1);

end
