function [s_linear] = linear_increm(m,NIter,s_linear,Hdo_f,Hdo_fuu,s_local)

warning('to fix')
%keyboard



if length(m.params) < 3
    ne = 1;
else
    ne = m.ndofparams(3);
end
if length(m.params) == 4 && strcmp( m.params(4).target,'young')
    ne2 = m.ndofparams(4);
else
    ne2 = 1;
end 

param_redis = m.param_redis;

ddlu = m.dofuu;
ddld = m.dofud;
%dU_nopgd = tenzeros(m.ndofparams);
dU_nopgd = zeros(m.ndofparams);

U_latin = s_linear.U;
hat_eps = s_local.eps;
hat_sigma = s_local.sigma;
hat_dg = s_local.d;

B_g = m.B_g;
I_g = m.I_g;
Hdo_inv = m.Hdo_inv;

% Null ktensors
%%%% CAUTION: HOMGENEOUS BC IN SPACE ASSUMED %%%%

% Computation of Qn and K
%Hdo_f = B_g' * I_g * Hdo_inv * B_g;
%Hdo_fuu = Hdo_f(ddlu,ddlu);

hat_eps_f = full(hat_eps);

if numel(m.params)== 4
     for i = 1 : m.params(4).ndof
         Q1(:,:,:,i) = double(m.fu);
     end
     Q1 = sptensor(Q1);
else
    Q1 = m.fu;
end
Q2 = - ttm( (full(hat_sigma - ttm(hat_eps_f,Hdo_inv,1))) , B_g' * I_g , 1);
Q3 = - ttm(full(U_latin) , Hdo_f , 1);
    
Q = Q1 + Q2 + Q3;

Qf = double(Q);
udf = double(m.ud);

for i_p = 1 : ne
    for j_p = 1 : ne2
        dU_nopgd(ddlu,:,i_p,j_p) = Hdo_fuu \ Qf(ddlu,:,i_p,j_p);
        dU_nopgd(ddld,:,i_p,j_p) = zeros(size(udf(ddld,:,i_p)));
    end    
end

R = 1;
delta_U_latin{NIter}     = cp_als(tensor(dU_nopgd),1,'printitn',m.printitn_cp_als);
[delta_U_latin{NIter},R] = check_approx(m,dU_nopgd,delta_U_latin{NIter},R);           % USELESS FOR CA0 MODES
disp(['        ... done. Rank of solution: R = ',num2str(R)']);

U_latin = U_latin +  redistribute(m.relax * delta_U_latin{NIter},param_redis);

eps     = ttm(U_latin, B_g , 1);
sigma   = hat_sigma + ttm( (full(eps) - hat_eps_f) ,Hdo_inv,1);
sigma   = redistribute(cp_als(sigma,1,'printitn',m.printitn_cp_als),param_redis);

s_linear = sol_class(U_latin,eps,sigma,hat_dg,'linear');

end