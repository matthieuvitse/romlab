function [s_linear,nb_pgd_modes] = linear_pgd(in_out,DATA,m,nb_pgd_modes,s_linear,Hdo_f,s_local,indic_latin)
% (hybrid) Galerkin PGD algorithm for time-space-parameter problems
warning off backtrace

param_redis = m.param_redis;

s_old       = s_linear;
U_latin     = s_old.U;
hat_eps     = s_local.eps;
hat_eps_f   = full(hat_eps);
hat_sigma   = s_local.sigma;
hat_dg      = s_local.d;

%[~,~,I_g,B_g,~,~] = computed_coupled_operators(m);

B_g      = m.B_g;
I_g      = m.I_g;
Hdo_inv  = m.Hdo_inv;

% Computation of Qn and K
if numel(m.params)== 4
     for i = 1 : m.params(4).ndof
         if ~strcmp(DATA.params(3).type,'loading')
             for j = 1 : m.params(3).ndof
                Q1(:,:,j,i) = double(m.fu);
             end
         else
             Q1(:,:,:,i) = double(m.fu);
         end
     end
     Q1 = sptensor(Q1);
elseif numel(m.params)==3 && ~strcmp(DATA.params(3).type,'loading')
    for i = 1 : m.params(3).ndof
         Q1(:,:,i) = double(m.fu);
     end
else
    Q1 = m.fu;
end

Q2 = - ttm( (full(hat_sigma - ttm(hat_eps_f,Hdo_inv,1))) , B_g' * I_g , 1);

% update of the time functions
crit_update = 0;
if ~isequal(m.rank_init,size(U_latin.U{1},2)) && in_out.orthog && in_out.update_pgd
    disp('                ... update of the existing basis...')
    Q3 = - ttm(full(U_latin) , Hdo_f , 1);
    Q  = Q1 + Q2 + Q3;
    
    [U_latin]     = update_pgd(in_out,DATA,m,U_latin,Q,Hdo_f);
    
    eps           = ttm(U_latin, B_g , 1);
    sigma_full    = full(hat_sigma) + ttm( (full(eps) - hat_eps_f) ,Hdo_inv,1);
    sigma         = redistribute(cp_als(sigma_full,3,'printitn',m.printitn_cp_als,'tol',m.tol_cp_als),param_redis);

    s_update      = sol_class(U_latin,eps,sigma,hat_dg,'linear');
    
    % criterion for skipping 
    indic_old = indic_latin(end);
    crit_update = compute_update_indicator(m,indic_old,s_local,s_update);
    disp(['                    ...  update indicator: ',num2str(crit_update)]);
    % warning('implement criterion on update stage')
    disp('                ... existing basis updated...')
end

if crit_update < in_out.crit_update
    % generation of a new PGD mode
    for i = 1 : 1   
        Q3 = - ttm(full(U_latin) , Hdo_f , 1);
        Q  = Q1 + Q2 + Q3;
        nb_pgd_modes_old = nb_pgd_modes;
        [delta_U_latin,U_latin,nb_pgd_modes] = new_pgd_mode(in_out,m,nb_pgd_modes,U_latin,Hdo_f,Q);
        
        if ~isequal(nb_pgd_modes_old,nb_pgd_modes)
            U_latin = U_latin + delta_U_latin;
        end
        disp('            ... new mode added...');
    end
    
    % storage
    eps      = ttm(U_latin, B_g , 1);
    sigma    = full(hat_sigma) + ttm( (full(eps) - hat_eps_f) ,Hdo_inv,1);  
    sigma    = redistribute(cp_als(sigma,3,'printitn',1,'tol',m.tol_cp_als),param_redis);
    s_linear = sol_class(U_latin,eps,sigma,[],'linear');

else
    warning('update criterion OK - computation of a new mode skipped')
    s_linear = s_update;
end

disp(['            ...done. Size of the reduced basis: ',num2str(size(s_linear.U{1},2)),' modes.']);

warning on backtrace    

end
