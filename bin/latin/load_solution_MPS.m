function [m,s,R] = load_solution_MPS(i,d,m)

path(path,'data/MPS/')
rank_trunc = i.rank_trunc_MPS;
%file = i.sol_init_MPS;
file = 'sol_init.mat';

disp('need to store initial rank of the solution')
foo;keyboard

B_g = m.B_g;
I_g = m.I_g;
Hdo_inv = m.Hdo_inv;
hooke_g = m.hooke_g;

Hdo_f = B_g' * I_g * Hdo_inv * B_g;

sol_0 = load(file);

U_load = sol_0.s_linear.U;
m.rank_init = sol_0.rank_init;

if rank_trunc == 0
    U_sol_0 = U_load;
else
    U_sol_0 = extract(U_load,1:rank_trunc);
end

% Computation of Qn and K
if numel(m.params)== 4
    for i = 1 : m.params(4).ndof
        Q1(:,:,:,i) = double(m.fu);
    end
    Q1 = sptensor(Q1);
else
    Q1 = m.fu;
end
Q3 = - ttm(full(U_sol_0) , Hdo_f , 1);

Q = Q1 + Q3;

[U_update]    = update_pgd(i,d,m,U_sol_0,Q,Hdo_f);

keyboard
eps           = ttm(U_update, B_g , 1);
%sigma_full    = full(hat_sigma) + ttm( (full(eps) - hat_eps_f) ,Hdo_inv,1);
%sigma         = redistribute(cp_als(sigma_full,1,'printitn',m.printitn_cp_als),param_redis);
sigma         = ttm(eps, hooke_g , 1);
s             = sol_class(U_update,eps,sigma,[],'linear');
R             = size(U_update.U{1},2);

end