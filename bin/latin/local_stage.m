function [s_local,hooke_dg] = local_stage(in_out,DATA,m,s_linear,iter)

allbutM     = '';
allbutM_dof = '';
mparams = m.params;
for i = 2 : numel(m.params)
    allbutM     = [ allbutM ',:' ];
    allbutM_dof = [allbutM_dof ',' num2str(mparams(i).ndof)];
end

if numel(mparams) < 3
    ne = 1;
else
    ne = mparams(3).ndof;
end

% init of the hat tensors
eval(strcat('hat_eps   = tenzeros([m.ndofgp',allbutM_dof,']);'));
eval(strcat('hat_sigma = tenzeros([m.ndofgp',allbutM_dof,']);'));
eval(strcat('hat_dg    = tenzeros([m.ndofgp',allbutM_dof,']);'));

if in_out.update_sd
    hooke_dg = sptensor([m.ndofgp m.ndofgp 1]);
else
    hooke_dg = [ ];
end

for i_model = 1 : numel(m.model)
    
    eval(strcat('dof_',num2str(i_model),'=m.model(i_model).dofgp;'));
    
   if in_out.update_sd && 1 == 1
       hooke_dg_t = sptensor([m.model(i_model).ndofgp m.model(i_model).ndofgp ne]);
   elseif in_out.update_sd && 0 == 1
       hooke_dg_t = sptensor([m.model(i_model).ndofgp m.model(i_model).ndofgp 1]);
   else
       hooke_dg_t = [];
   end
    
    m_i   = m.model(i_model);
    dat_i = DATA.model(i_model);

    if strcmp(dat_i.behavior.type,'damage')
        
        % parfor loop on the parameter values
        %for i_local = 1 : ne
        parfor (i_local = 1 : ne , in_out.arg_parallel)
            if numel(mparams) == 2
                [hat_dg_par,hat_eps_par,hat_sigma_par,hooke_dg_par] = local_stage_parallel_2(in_out,DATA,dat_i,m,m_i,s_linear,i_local);
                hat_dg_t(:,:,i_local)      = hat_dg_par;
                hat_eps_t(:,:,i_local)     = hat_eps_par;
                hat_sigma_t(:,:,i_local)   = hat_sigma_par;
                hooke_dg_t(:,:,i_local)    = hooke_dg_par;
            elseif numel(mparams) == 3
                [hat_dg_par,hat_eps_par,hat_sigma_par,hooke_dg_par] = local_stage_parallel_3(in_out,DATA,dat_i,m,m_i,s_linear,i_local);
                hat_dg_t(:,:,i_local)      = hat_dg_par;
                hat_eps_t(:,:,i_local)     = hat_eps_par;
                hat_sigma_t(:,:,i_local)   = hat_sigma_par;
                hooke_dg_t(:,:,i_local)    = hooke_dg_par;
                %disp(['            ... local stage for parameter ',m.params(3).name,' = ',num2str(i_local),' computed...']);
            elseif numel(mparams) == 4
                [hat_dg_par,hat_eps_par,hat_sigma_par,hooke_dg_par] = local_stage_parallel_4_V2(in_out,DATA,dat_i,m,m_i,s_linear,i_local);
		warning('this is a V2')
                hat_dg_t4(:,:,i_local,:)      = hat_dg_par;
                hat_eps_t4(:,:,i_local,:)     = hat_eps_par;
                hat_sigma_t4(:,:,i_local,:)   = hat_sigma_par;
                hooke_dg_t(:,:,i_local)       = hooke_dg_par;
                %disp(['            ... local stage for parameter ',m.params(3).name,' = ',num2str(i_local),' computed...']);
            end
        end
        
        if numel(mparams) == 4
            hat_dg_t    = hat_dg_t4;
            hat_eps_t   = hat_eps_t4;
            hat_sigma_t = hat_sigma_t4;
        end
        
    elseif strcmp(dat_i.behavior.type,'elastic')
        
        hat_eps_t = ktensor;
        hat_eps_t.lambda = s_linear.eps.lambda;
        eval(strcat('hat_eps_t.u{1} = s_linear.eps.u{1}(dof_',num2str(i_model),',:);'));
         hat_sigma_t = ktensor;
         hat_sigma_t.lambda = s_linear.sigma.lambda;
         eval(strcat('hat_sigma_t.u{1} = s_linear.sigma.u{1}(dof_',num2str(i_model),',:);'));
         for i_p = 2 : numel(mparams)
             hat_eps_t.u{i_p}   = s_linear.eps.u{i_p};
             hat_sigma_t.u{i_p} = s_linear.sigma.u{i_p};
         end
         
        eval(strcat('hat_dg_t = sptensor([numel(dof_',num2str(i_model),'),',allbutM_dof(2:end),']);'));
        if in_out.update_sd
            for i_param = 1 : ne
                eval(strcat('hooke_dg_t(:,:,i_param) = sptensor(m.hooke_g(dof_',num2str(i_model),',dof_',num2str(i_model),'));'));
            end
        end
    
    elseif strcmp(dat_i.behavior.type,'platicity')
        warning('not -yet- implemented')
        foo;keyboard
        
    elseif strcmp(dat_i.behavior.type,'viscoplaticity')
        warning('not -yet- implemented')
        foo;keyboard
        
    end
        
    % integrate operator over param3
    if ne>1 && in_out.update_sd 
        hook = sptensor([m.model(i_model).ndofgp m.model(i_model).ndofgp 1]);
        hook(:,:,1) = hooke_dg_t(:,:,round(m.params(3).ndof/2));
        clear hooke_dg_t
        hooke_dg_t = sptensor([m.model(i_model).ndofgp m.model(i_model).ndofgp 1]);
        hooke_dg_t = hook;
    end
    
    eval(strcat('hat_eps(dof_',num2str(i_model)',allbutM,')   = tensor(hat_eps_t);'));
    eval(strcat('hat_sigma(dof_',num2str(i_model)',allbutM,') = tensor(hat_sigma_t);'));
    %eval(strcat('hat_sigma(dof_',num2str(i_model)',allbutM,') = s_linear.sigma(dof_',num2str(i_model)',allbutM,');'));
    eval(strcat('hat_dg(dof_',num2str(i_model)',allbutM,')    = tensor(hat_dg_t);'));
    
    if in_out.update_sd
        eval(strcat('hooke_dg(dof_',num2str(i_model),',dof_',num2str(i_model),',1) = hooke_dg_t(:,:,1);'));
    end
 
end

s_local = sol_class([],hat_eps,hat_sigma,hat_dg,'local');

if in_out.debug == 1
    localdir = 'debug';
    export_vtk(in_out,DATA,m,s_local,1,localdir,iter);
end

end
