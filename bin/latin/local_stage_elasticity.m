function [s_local] = local_stage_elasticity(~,m,s_linear,Hup)
% global resolution for linear elasticity problems

ne = m.params(3).ndof;
sigma = s_linear.sigma;
eps = s_linear.eps;
hooke_g = m.hooke_g;

alpha_n = double(full(ttm(sigma,Hup,1))) + double(full(eps));
A = Hup * hooke_g + speye(size(hooke_g));

parfor i_local = 1 : ne
    a_d_ee = alpha_n(:,:,i_local);
    h_e_ee = A\a_d_ee;
    hat_eps_full(:,:,i_local) = h_e_ee;
end

hat_eps = redistribute(cp_als(tensor(hat_eps_full),1,'printitn',m.printitn_cp_als),3);
hat_sigma = hat_eps;
hat_sigma.u{1} = hooke_g * hat_sigma.u{1} ;
hat_sigma = redistribute(hat_sigma,3);

s_local = sol_class([],hat_eps,hat_sigma,[],'local');

end
