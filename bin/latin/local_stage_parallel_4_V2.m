function [hat_dg,hat_eps,hat_sigma,hooke_dg] = local_stage_parallel_4_V2(in_out,DATA_g,DATA,m_g,m,s_linear,k_param)
% M. Vitse

if DATA_g.dim == 3
    comp = 6;            % number of components in the elementary eps/sig matrices
end

params = m_g.params;
nT = params(2).ndof;
lT = params(2).mesh;
nE = params(4).ndof;
lE = params(4).mesh;
if strcmp(params(3).type,'loading')
    if strcmp(params(4).type,'young');
        alpha_4 = params(4).mesh;
        beta_4  = ones(size(params(4).mesh));
        gamma_4 = ones(size(params(4).mesh));
    elseif strcmp(params(4).type,'init_thres');
        alpha_4 = ones(size(params(4).mesh));
        beta_4  = params(4).mesh;
        gamma_4 = ones(size(params(4).mesh));
    elseif strcmp(params(4).type,'post_peak');
        alpha_4 = ones(size(params(4).mesh));
        beta_4  = ones(size(params(4).mesh));
        gamma_4 = params(4).mesh;
    end
elseif strcmp(params(3).type,'init_thres')
    if strcmp(params(4).type,'young');
        alpha_4 = params(4).mesh;
        beta_4  = params(3).mesh(k_param);
        gamma_4 = ones(size(params(4).mesh));
%     elseif strcmp(params(4).type,'init_thres');
%         alpha_4 = ones(size(params(4).mesh));
%         beta_4  = params(4).mesh;
%         gamma_4 = ones(size(params(4).mesh));
    elseif strcmp(params(4).type,'post_peak');
        alpha_4 = ones(size(params(4).mesh));
        beta_4  = params(3).mesh(k_param);
        gamma_4 = params(4).mesh;
    end
end

% substepping
nstep = 1;
if isfield(DATA,'substepping')
    if DATA.substepping.on && ~isequal(DATA.substepping.step,1)
        nstep = DATA.substepping.step;
        toto1 = 1 : 1 : comp;
        toto2 = 1 : 1 : nT;
        toto3 = 1 : nT/(nT*nstep+1) : nT;
        if ~isequal(toto3(end),nT)
            toto3 = [toto3, nT];
        end
        toto4 = 1 : comp/(comp+1) : comp;
        [u_loc,v_loc] = meshgrid(toto3 , toto4);
        [u_0,v_0]     = meshgrid(toto2 , toto1);
    end
    
end
lT_step = lT(1) : 1/nstep : lT(end) ;
nT_step =  numel(lT_step);

% material parameters
hooke_i = DATA.material.hooke;
F_Y     = DATA.behavior.F_Y;         % function for damage variable d
Ad_i    = DATA.material.Ad;
Y0_i    = DATA.material.Y0;

if ~isfield(m,'OP') ||  ~isfield(m,'OP_inv')
    m = voigt_to_matrix(DATA,m);
end

Hup_loc = m.H_loc;
eps     = s_linear.eps;
sigma   = s_linear.sigma;

% preallocation
hat_eps     = zeros(m.ndofgp,nT,nE);
hat_sigma   = zeros(m.ndofgp,nT,nE);
hat_dg      = zeros(m.ndofgp,nT,nE);

if in_out.update_sd
    hooke_dg = sptensor([m.ndofgp m.ndofgp]);
else
    hooke_dg = [ ];
end
j_hooke = 0;
var_hooke = m.var_E_g;

for i_elem = 1 : numel(m.elem)
    
    npg_i = numel(m.elem(i_elem).gp);
    
    if ~ismember(i_elem,m_g.elem_in_box) 
        
        for gp = 1 : npg_i
            j_hooke = j_hooke +1 ;
            dof_eps_gp       = ((i_elem-1) * npg_i * comp + (gp-1) * comp + 1) : ((i_elem-1) * npg_i* comp + gp * comp);  % eps values at gauss point gp_current
            
            hooke_ij = var_hooke(j_hooke) * hooke_i;
            
            for k_param4 = 1 : nE
                
                hooke = alpha_4(k_param4) * hooke_ij;
                Y0    = beta_4  * Y0_i;
                Ad    = gamma_4(k_param4) * Ad_i;
                
                if in_out.unilateral
                    eps_f_max   = 1e-6*ones(comp,1);
                    hat_sigma_f = zeros(comp,1);
                    hat_eps_f   = zeros(comp,1);
                end
                
                hat_Z            = zeros(1,nT);
                hat_d            = zeros(1,nT);
                hat_d_reg        = zeros(1,nT);
                
                hat_dg_step    = zeros(comp,nT_step);
                hat_eps_step   = zeros(comp,nT_step);
                hat_sigma_step = zeros(comp,nT_step);
                
                eps_loc_temp.U{1}  = eps.U{1}(dof_eps_gp,:);
                eps_loc            = ktensor(eps.lambda,eps_loc_temp.U{1},eps.U{2},eps.U{3}(k_param,:),eps.U{4}(k_param4,:));
                eps_loc_full       = double(full(eps_loc));
                
                sigma_loc_temp.U{1}  = sigma.U{1}(dof_eps_gp,:);
                sigma_loc            = ktensor(sigma.lambda,sigma_loc_temp.U{1},sigma.U{2},sigma.U{3}(k_param,:),sigma.U{4}(k_param4,:));
                sigma_loc_full       = double(full(sigma_loc));
                
                hooke_dg4 = sptensor([comp comp nE]);
                
                for j_time_step = 1 : length(lT_step)
                    
                    % interpolation of operators - if substepping
                    if isfield(DATA,'substepping')
                        if DATA.substepping.on && ~isequal(DATA.substepping.step,1)
                            eps_loc_full_int    = qinterp2(u_0,v_0,eps_loc_full,u_loc,v_loc,1);     % much faster than built-in interp2 function
                            sigma_loc_full_int  = qinterp2(u_0,v_0,sigma_loc_full,u_loc,v_loc,1);
                        else
                            eps_loc_full_int   = eps_loc_full;
                            sigma_loc_full_int = sigma_loc_full;
                        end
                    else
                        eps_loc_full_int   = eps_loc_full;
                        sigma_loc_full_int = sigma_loc_full;
                    end
                    
                    vec1           = eps_loc_full_int(:,j_time_step);
                    vec2           = m.OP * vec1;
                    mat1           = reshape(vec2,3,3);
                    
                    [v_mat,D_mat]  = eig(mat1);
                    D_mat_pos      = 0.5 * ( D_mat+abs(D_mat) );
                    hat_eps_pos    = v_mat * D_mat_pos * v_mat';
                    
                    % computation of threshold
                    hat_eps_pos_vec1 = reshape(hat_eps_pos,9,1);
                    hat_eps_pos_vec  = m.OP_inv * hat_eps_pos_vec1;
                    
                    YY = 1/2 * hat_eps_pos_vec' * hooke * hat_eps_pos_vec ;
                    if j_time_step == 1
                        fd = YY - (Y0);
                    else
                        fd = YY - (Y0 + hat_Z(1, j_time_step-1));
                    end
                    
                    % computation of damage
                    if fd > 0
                        hat_d(1,j_time_step) = F_Y(YY,Y0,Ad);
                        hat_Z(1,j_time_step) = YY - Y0;
                    elseif j_time_step == 1
                        hat_d(1,j_time_step) = 0;
                        hat_Z(1,j_time_step) = 0;
                    else
                        hat_d(1,j_time_step) = hat_d(1,j_time_step-1);
                        hat_Z(1,j_time_step) = hat_Z(1,j_time_step-1);
                    end
                    
                    if hat_d(1,j_time_step) > 1
                        warning('damage greater than 1 !')
                        foo;keyboard
                    end
                    if hat_d(1,j_time_step) < 0
                        warning('damage is negative!')
                        foo;keyboard
                    end
                    
                    % regularization of damage variable
                    if j_time_step == 1
                        hat_d_prev_time = [];
                    else
                        hat_d_prev_time = hat_d(1,j_time_step-1);
                    end
                    [hat_d_reg(1,j_time_step)] = regularization(in_out,DATA,hat_d(1,j_time_step),hat_d_prev_time,j_time_step,lT_step);
                    
                    hat_dg_step(:, j_time_step)   = repmat(hat_d_reg(1,j_time_step),6,1);
                    hooke_d_loc                   = (1 - hat_d_reg(:,j_time_step)) * hooke ;
                    
                    % computation of hat_eps and hat_sigma using the local search direction
                    alpha_n                   = Hup_loc * sigma_loc_full_int(:,j_time_step) + eps_loc_full_int(:,j_time_step);           % WE CAN IMPROVE THAT
                    A                         = Hup_loc * hooke_d_loc + eye(size(hooke_d_loc));
                    hat_eps_step(:, j_time_step)    = A \ alpha_n;
                    hat_sigma_step(:, j_time_step)  = hooke_d_loc * hat_eps_step(:, j_time_step) ;
                    
                    % unilateral effect
                    if in_out.unilateral
                        delta_eps_f = hat_dg_step(:, j_time_step) .* hat_eps_step(:, j_time_step) - hat_eps_f;
                        [hat_sigma_f,eps_f_max,hat_eps_f] = unilateral(in_out,DATA,hat_dg_step(:, j_time_step),hat_eps_step(:, j_time_step),eps_f_max,hat_sigma_f,delta_eps_f,hooke);
                        hat_sigma_step(:, j_time_step) = hat_sigma_step(:, j_time_step) + hat_sigma_f;
                    end
                    
                end
                
                hat_eps(dof_eps_gp,:,k_param4)   = hat_eps_step(:,1:nstep:end);
                hat_sigma(dof_eps_gp,:,k_param4) = hat_sigma_step(:,1:nstep:end);
                hat_dg(dof_eps_gp,:,k_param4)    = hat_dg_step(:,1:nstep:end);
                
                if in_out.update_sd
                    ones_t = ones(1,numel(lT)-1);
                    d_cur = hat_dg(dof_eps_gp(1),:,k_param4);
                    hooke_dg4(:,:,k_param4) = (1/2 * (lT(2:end) - lT(1:end-1)) * ( (ones_t - d_cur(2:end) ) + (ones_t - d_cur(1:end-1)) )') * sptensor(hooke);
                end
            end
            if in_out.update_sd
                hooke_dg(dof_eps_gp,dof_eps_gp) = sptensor(ttv( full( hooke_dg4(:,:,2:end) + hooke_dg4(:,:,1:end-1)),(1/2 * (lE(2:end) - lE(1:end-1)))',3 ));
            end
        end
        
    else
        j_hooke = j_hooke +npg_i ;
        %hooke_ij = var_hooke(j_hooke) * hooke_i;
        dof_eps = ((i_elem-1) * npg_i * comp  + 1) : ((i_elem-1) * npg_i* comp + npg_i * comp);

        hooke_dg4 = sptensor([comp comp nE]);

        for k_param4 = 1 : nE
            hooke = k_param4 * m.hooke_g(dof_eps,dof_eps);
            eps_loc_temp.U{1}             = eps.U{1}(dof_eps,:);
            eps_loc                       = double(full(ktensor(eps.lambda,eps_loc_temp.U{1},eps.U{2},eps.U{3}(k_param,:),eps.U{4}(k_param4,:)  )));
            hat_eps(dof_eps,:,k_param4)   = eps_loc;
            
            sigma_loc_temp.U{1}           = sigma.U{1}(dof_eps,:);
            sigma_loc                     = double(full(ktensor(sigma.lambda,sigma_loc_temp.U{1},sigma.U{2},sigma.U{3}(k_param,:),sigma.U{4}(k_param4,:) )));
            hat_sigma(dof_eps,:,k_param4) = sigma_loc;
            
            hat_dg(dof_eps,:,k_param4)    = zeros(numel(dof_eps),nT);
            
            if in_out.update_sd
                hooke_dg4(:,:,k_param4) = sptensor(hooke);
            end
        end
        if in_out.update_sd
            hooke_dg(dof_eps,dof_eps) = sptensor(ttv( full( hooke_dg4(:,:,2:end) + hooke_dg4(:,:,1:end-1)),(1/2 * (lE(2:end) - lE(1:end-1)))',3 ));
        end
    end
    
end


end
