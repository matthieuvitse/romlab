function Hup = search_direction(m,H,varargin)

ngp_tot = m.ngp_tot;

if nargin == 3 
    switch varargin{1}
        case 'regular'
            J   = H;
            I   = speye(ngp_tot);
            Hup = kron(I,J);
            disp(['        ... search direction *',varargin{1},'* has been computed...'])
        case 'tangent'
            
            foo;keyboard
            J   = H;  
            I   = speye(ngp_tot);
            Hup = kron(I,J);
            disp(['        ... search direction *',varargin{1},'* has been computed...'])
        case 'update'
            Hup = H;
            disp(['            ... search direction updated...'])
    end 
else 
    J   = H;
    I   = speye(ngp_tot);
    Hup = kron(I,J);
    disp(['        ... search direction *',varargin{1},'* has been computed...'])
end

%disp(['        ... search direction *',varargin{1},'* has been computed...'])

end