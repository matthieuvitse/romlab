function [class] = sol_class(U,eps,sigma,d,stage)

class.U = U;
class.eps = eps;
class.sigma = sigma;
class.d = d;
class.stage = stage;
class.F = [];
end