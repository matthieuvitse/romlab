function [m,s_contact] = solver_raphael(in_out,DATA,m)
% Initialization of the process
disp('    ... resolution of the contact problem...')
tic

% loading of all the stuff

% model 1
ddl1 = m.model(1).dof_corres;
ddl1u = m.model(1).dofuu;
ddl1d = m.model(1).dofud;
ddl1c = m.model(1).dofc;
B1_g = m.model(1).B_g;
hooke1_g = m.model(1).hooke_g;
R1 = m.model(1).R;
C1 = m.model(1).C;
k1 = m.model(1).k;
u1bcd = double(m.model(1).ud(ddl1d,:,:));
f1bcu = double(m.model(1).fu(ddl1u,:,:));

% model 2
ddl2 = m.model(2).dof_corres;
ddl2u = m.model(2).dofuu;
ddl2d = m.model(2).dofud;
ddl2c = m.model(2).dofc;
B2_g = m.model(2).B_g;
hooke2_g = m.model(2).hooke_g;
R2 = m.model(2).R;
C2 = m.model(2).C;
k2 = m.model(2).k;
u2bcd = double(m.model(2).ud(ddl2d,:,:));
f2bcu = double(m.model(2).fu(ddl2u,:,:));

% preparation of all the operators needed for the LATIN algorithm

% search direction parameter

Lc=0.04;

kappa = 0.5*(DATA.model(1).material.E + DATA.model(2).material.E)/Lc;

H1=k1 + kappa*C1;

H2=k2 + kappa*C2;

J=m.model(1).J;

N=m.model(1).N;

Pi=m.model(2).Pi;

n_ddlc=length(ddl1c);

n_time=m.ndofparams(2);

n_param=m.ndofparams(3);

phi1 = zeros(n_ddlc,n_time,n_param);

w1 = zeros(n_ddlc,n_time,n_param);

phi2 = zeros(n_ddlc,n_time,n_param);

w2 = zeros(n_ddlc,n_time,n_param);

U1=zeros(m.model(1).ndof,n_time,n_param);

U2=zeros(m.model(2).ndof,n_time,n_param);

dw1=zeros(n_ddlc,n_time,n_param);

dw2=zeros(n_ddlc,n_time,n_param);

w1c = zeros(n_ddlc,n_time,n_param);

w2c = zeros(n_ddlc,n_time,n_param);

% decoupled resolution

mi=0.9; % friction coefficient

n_it=500; % maximum number of iterations (LATIN)

it=[];
Er=[];

for l=1:size(DATA.load,2)
    if strcmp(DATA.load(l).type,'contact');
        break
    end
end

ndim=DATA.dim;

ld=DATA.load(l);
nr=ld.normal;
tgt = 1 : ndim;
tgt(ld.normal) = [];

for i = 1:n_it;
    
    for i_init = 1 : n_param
        
        if i_init>1
            
            U1(:,:,i_init)=U1(:,:,1);
            U2(:,:,i_init)=U2(:,:,1);
            phi1(:,:,i_init)=phi1(:,:,1);
            phi2(:,:,i_init)=phi2(:,:,1);
            
        else
            
            for time=2:n_time;
                
                phi1n=N*phi1(:,time,i_init);
                phi1t=Pi*phi1(:,time,i_init);
                phi2n=N*phi2(:,time,i_init);
                phi2t=Pi*phi2(:,time,i_init);
                
                dw1n=N*dw1(:,time,i_init);
                dw1t=Pi*dw1(:,time,i_init);
                dw2n=N*dw2(:,time,i_init);
                dw2t=Pi*dw2(:,time,i_init);
                
                cn=0.5*((phi1n - phi2n) + kappa*(dw2n - dw1n) + kappa*(N*(w2c(:,time-1,i_init) - w1c(:,time-1,i_init)) + J));
                ct=0.5*((phi1t - phi2t) + kappa*(dw2t - dw1t));
                
                for j=1:n_ddlc/2
                    
                    if cn(j)<=0
                        
                        phi1nc(j,1)=cn(j);
                        phi2nc(j,1)=-cn(j);
                        
                        dw1nc(j,1)=dw1n(j) + (phi1nc(j) - phi1n(j))/kappa;
                        dw2nc(j,1)=dw2n(j) + (phi2nc(j) - phi2n(j))/kappa;
                        
                        g=mi*abs(phi1nc(j));
                        
                        if abs(ct(j))<g
                            
                            phi1tc(j,1)=ct(j);
                            phi2tc(j,1)=-ct(j);
                            
                            dw1tc(j,1)=dw1t(j) + (phi1tc(j) - phi1t(j))/kappa;
                            dw2tc(j,1)=dw2t(j) + (phi2tc(j) - phi2t(j))/kappa;
                            
                        elseif abs(ct(j))>=g
                            
                            phi1tc(j,1)=g*sign(ct(j));
                            phi2tc(j,1)=-phi1tc(j,1);
                            
                            dw1tc(j,1)=dw1t(j) + (phi1tc(j) - phi1t(j))/kappa;
                            dw2tc(j,1)=dw2t(j) + (phi2tc(j) - phi2t(j))/kappa;
                            
                        end
                        
                    end
                    
                    if cn(j)>0;
                        
                        phi1nc(j,1)=0;
                        phi2nc(j,1)=0;
                        phi1tc(j,1)=0;
                        phi2tc(j,1)=0;
                        
                        dw1nc(j,1)=dw1n(j) - phi1n(j)/kappa;
                        dw2nc(j,1)=dw2n(j) - phi2n(j)/kappa;
                        dw1tc(j,1)=dw1t(j) - phi1t(j)/kappa;
                        dw2tc(j,1)=dw2t(j) - phi2t(j)/kappa;
                        
                    end
                    
                end
                
                dw1c(nr:2:n_ddlc,1)=dw1nc;
                dw1c(tgt:2:n_ddlc,1)=dw1tc;
                
                w1c(:,time,i_init)=w1c(:,time-1,i_init) + dw1c;
                
                phi1c(nr:2:n_ddlc,1)=phi1nc;
                phi1c(tgt:2:n_ddlc,1)=phi1tc;
                
                alpha1=phi1c + kappa*dw1c;
                
                b1=C1*(R1')*alpha1 + kappa*C1*U1(:,time-1,i_init);
                b1u=b1(ddl1u);
               
                U1(ddl1u,time,i_init)=H1(ddl1u,ddl1u)\((f1bcu(:,time,i_init)+b1u) - H1(ddl1u,ddl1d)*u1bcd(:,time,i_init));
                U1(ddl1d,time,i_init)=u1bcd(:,time);
                
                w1(:,time,i_init)=R1*U1(:,time,i_init);
                
                dw1(:,time,i_init)=w1(:,time,i_init) - w1(:,time-1,i_init);
                
                phi1(:,time,i_init)=phi1c + kappa*(dw1c - dw1(:,time,i_init));
                
                dw2c(nr:2:n_ddlc,1)=dw2nc;
                dw2c(tgt:2:n_ddlc,1)=dw2tc;
                
                w2c(:,time,i_init)=w2c(:,time-1,i_init) + dw2c;
                
                phi2c(nr:2:n_ddlc,1)=phi2nc;
                phi2c(tgt:2:n_ddlc,1)=phi2tc;
                
                alpha2=phi2c + kappa*dw2c;
                
                b2=C2*(R2')*alpha2 + kappa*C2*U2(:,time-1,i_init);
                b2u=b2(ddl2u);
                
                U2(ddl2u,time,i_init)=H2(ddl2u,ddl2u)\((f2bcu(:,time,i_init)+b2u) - H2(ddl2u,ddl2d)*u2bcd(:,time,i_init));
                U2(ddl2d,time,i_init)=u2bcd(:,time);
                
                w2(:,time,i_init)=R2*U2(:,time,i_init);
                
                dw2(:,time,i_init)=w2(:,time,i_init) - w2(:,time-1,i_init);
                
                phi2(:,time,i_init)=phi2c + kappa*(dw2c - dw2(:,time,i_init));
                
            end
            
        end
        
    end
    
    err = (norm(w1(:,:,1) - w1c(:,:,1),'fro') + norm(w2(:,:,1) - w2c(:,:,1),'fro'))/ ...
        (norm(w1(:,:,1) + w1c(:,:,1),'fro')/2 + norm(w2(:,:,1) + w2c(:,:,1),'fro')/2);
    
   
    it=[it i];
    Er=[Er err];
    
    if err<10^-4
        break
    end
   
end

loglog(it,Er,'o-')
xlabel('iteration')
ylabel('error')

if i==n_it
    disp(' ')
    disp(['convergence not achieved,','error = ',num2str(err)]);
    disp(' ')
end

phi1t=phi1(tgt:2:end,:,:);
phi1n=phi1(ld.normal:2:end,:,:);

pos1=[m.model(1).node(m.model(1).cn).pos]';

x1=pos1(1:2:end-1,:);
y1=pos1(2:2:end,:);

m.model(1).xc=x1;
m.model(1).yc=y1;
m.model(1).phin=phi1n;
m.model(1).phit=phi1t;

phi2t=phi2(tgt:2:end-1,:,:);
phi2n=phi2(ld.normal:2:end,:,:);

pos2=[m.model(2).node(m.model(2).cn).pos]';

x2=pos2(1:2:end-1,:);
y2=pos2(2:2:end,:);

m.model(2).xc=x2;
m.model(2).yc=y2;
m.model(2).phin=phi2n;
m.model(2).phit=phi2t;


% storage
U1 = tensor(U1);
U2 = tensor(U2);
U_n = tensor;
U_n(ddl1,:,:) = U1;
U_n(ddl2,:,:) = U2;

% computation and storage of the globalized (but local) eps and sigma

% eps1 = ttm(U1,B1_g,1);
% sigma1 = ttm(eps1,hooke1_g,1);
% eps2 = ttm(U2,B2_g,1);
% sigma2 = ttm(eps2,hooke2_g,1);

% ... the following must work in the future
% eps_init(pg1,:,:) = eps1;
% eps_init(pg2,:,:) = eps2;
% sigma_init(pg1,:,:) = sigma1;
% sigma_init(pg2,:,:) = sigma2;
% ... but waiting for that the following does the job

B_g = m.B_g;
hooke_g = m.hooke_g;
eps_n = ttm(U_n,B_g,1);
sigma_n = ttm(eps_n,hooke_g,1);
d_n = tensor(size(eps_n)); % no damage

% final storage for post-processing
s_contact = sol_class(U_n,eps_n,sigma_n,d_n,'init');

a=toc;
disp(['    ... initialization done... ',num2str(a),' s'])

end