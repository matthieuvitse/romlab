function colors=Choose_color1(n)
colors=zeros(n,3);
colors(1:7,:) = get(gca,'colororder');
colors(8,:) = [1.0, 0.5, 0.0];
   if n>8
       for k= 9:n
         colors(k,:) =  colors(mod(k,8)+1,:);
       end
   end
end
    