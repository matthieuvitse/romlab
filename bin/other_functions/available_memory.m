function [mem_avail] = available_memory()
% gets the available RAM on machine


command1 = ['cat /proc/meminfo > mem1.txt'];
command2 = ['sed -n 2p mem1.txt > mem2.txt'];
command3 = ['sed ','"s/[^0-9]*//g"',' mem2.txt > mem3.txt'];
system(command1);
system(command2);
system(command3);
fileID = fopen('mem3.txt','r');
formatSpec = '%f';
mem_avail  = (fscanf(fileID,formatSpec))/1024;

command_clean = ['rm mem1.txt mem2.txt mem3.txt'];
system(command_clean);

end
