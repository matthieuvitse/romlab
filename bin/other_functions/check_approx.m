function [U_out, R] = check_approx(m,U,U_tensor,R)
% checks that the boundary conditions are satisfied during the CP
% decomposition. Increases the rank of the approximation if not.
warning off backtrace

ddld        = m.dofud;
param_redis = m.param_redis;
A           = double(U_tensor);
U_f         = double(U);
err_approx  = (U_f(ddld,:,:) - A(ddld,:,:));

while max((err_approx(:))) > 1e-10 && R < 5
    warning('    boundary conditions are not satisfied during the CP decomposition')
    warning('         -> increasing the rank of decomposition...')
    R          = R + 1;
    U_tensor   = redistribute(cp_als(tensor(U),R,'printitn',m.printitn_cp_als,'tol',m.tol_cp_als),param_redis);
    A          = double(U_tensor);
    err_approx = (U_f(ddld,:,:) - A(ddld,:,:));
end

if R == 5
    disp(' ');
    warning(['CP decomposition not converged - BC error = ',num2str(max(err_approx(:)))]);
end
U_out = U_tensor;

vec1 = [1:numel(m.params)];
vec2 = vec1(vec1~=2); 

% normalization of the initial basis
for i_r = 1 : R
    for i_param = 1 : numel(vec2)
        ind_p                 = vec2(i_param);
        dU(i_param,i_r)       = U_out.U{ind_p}(:,i_r)' * m.params(ind_p).integ2 * U_out.U{ind_p}(:,i_r); 
        U_out.U{ind_p}(:,i_r) = U_out.U{ind_p}(:,i_r) / sqrt(dU(i_param,i_r));
    end  
end
for i_r = 1 : R
    U_out.U{2}(:,i_r) = U_out.U{2}(:,i_r) * prod( sqrt(dU(:,i_r)) );
end


warning on backtrace

end