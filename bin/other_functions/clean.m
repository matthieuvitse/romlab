function [] = clean(varargin)
% specify file to clean. 
% if none, 'tmp/' is emptied
warning off backtrace

if isempty(varargin)
    warning('ALL files in tmp/ will be deleted')
    disp(' ')
    disp('    ... press F5 to continue...')
    keyboard
    disp(' ')
    system('rm -rfv data/tmp/*');
    disp(' ')
    disp('    ... files deleted')
else
    path = varargin{1};
    if ~exist(path)
       warning('path does not exist')
       foo;keyboard
    else
        disp(['deleting files in ',path,' ...'])
        a = system(['rm -rf ',path,'*']);
        if a == 0
        disp('    ... files deleted')
        else
            foo;keyboard
        end
    end
end

warning on backtrace

end