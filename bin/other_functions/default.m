function i = default()

% INPUT parameters
i.name = 'default';    % 'smart'     % name of the file to load
i.test = 'cas_test';
i.test = '';
i.solver = 'DIRECT';
                              % sets up debug options like profile, diary, export along iterations, dbstop if warning or errors, ...
i.debug = 1;     

i.m.pgd =1 ;
i.max_iter = 40;
i.indic_target = 1e-7;
i.relax = 1;

i.regularization = 0;

i.cluster = 0;

% OUTPUT parameters
i.plot_mesh=0;
i.plot_sol=1;
i.plot_sigeps=0;
i.plot_vonmises=0;
i.exp_pxdmf = 1;                               % pxdmf export for separated fields
i.exp_pv = 1;                                  % standard paraview export
i.exp_pv_name = 'output';

end