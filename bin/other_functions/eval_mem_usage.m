function [] = eval_mem_usage(in_out,DATA,m,flag)

warning off backtrace

switch flag
    case {0,1}           % Linux
        mem_avail = available_memory();
        %case 1          % OSX
        
    case 2               % windows
        mem_avail = 8e3;
        warning('not implemented... check the "memory" function --only available on Windows')
end

disp('ROMlab: Evaluating the memory required...')

switch in_out.solver
    
    case 'LATIN'
        
        dof_gp = m.ndofgp;
        n_p    = [];
        for i_params = 2 : length(m.params)
            n_p(i_params-1) = numel(m.params(i_params).mesh);
        end
        prod_p    = prod(n_p);
        sum_p     = sum(n_p);
        precision = 64;
        fac       = 5;
        maj       = (100 +fac)/100;
        
        %------------------------------------------------------------------------%
        % size local stage
        n_fields_loc   = 3;   % eps, sig, d
        if isfield(DATA.model(1),'substepping')
            substep = DATA.model(1).substepping.step;
        else
            substep = 1;
        end
        mem_eval_local = substep * maj * dof_gp * prod_p * n_fields_loc * precision / 8 / 1048576;
        disp(['    ... estimated max size of local stage   : ',num2str(mem_eval_local,4),' MB (maj ',num2str(fac),'%)'])
        
        %------------------------------------------------------------------------%
        
        % size global components
        iter         = in_out.max_iter;
        dof          = m.ndof;
        n_fields_lin = 2;                % sigma, epsilon
        
        %size eps and sigma
        mem_eval_sigeps = maj * iter * (dof_gp + sum_p) * precision / 8 / 1048576;
        %size u
        mem_eval_u      = maj * iter * (dof + sum_p) * precision / 8 / 1048576;
        % size of d:
        mem_eval_d      = maj * dof_gp * prod_p * 1 * precision / 8 / 1048576;
        
        % size linear stage
        mem_eval_linear = mem_eval_d + mem_eval_u + n_fields_lin * mem_eval_sigeps ;
        disp(['    ... estimated max size of linear stage  : ',num2str(mem_eval_linear,4),' MB (maj ',num2str(fac),'%)'])
        
        %------------------------------------------------------------------------%
        
        % size of m
        size_m          = whos('m');
        mem_eval_m      = maj * size_m.bytes * 1.5 / 1048576;
        size_search_dir =  0; %maj * dof_gp^2 * precision / 8 / 1048576;
        disp(['    ... estimated max size of m             : ',num2str(mem_eval_m + size_search_dir,4),' MB (maj ',num2str(fac),'%)'])
        
        % total size
        mem_eval_tot = mem_eval_local + mem_eval_linear + mem_eval_m;
        disp(['        ==>> Total estimated max size   : ',num2str(mem_eval_tot,4),' MB or ',num2str((mem_eval_tot/mem_avail)*100,2),'% of the available memory (maj ',num2str(fac),'%)'])
        
    case 'PGD'
        warning('not implemented yet')
        mem_eval_tot = 0;
    case 'DIRECT'
        warning('not implemented yet')
        mem_eval_tot = 0;
    otherwise
        warning('not implemented yet')
        mem_eval_tot = 0;
end

%  warning if mem_eval_tot > criteria
if mem_eval_tot > mem_avail
    warning('expeted necessary memory may be greater than available memory')
end

warning on backtrace

end