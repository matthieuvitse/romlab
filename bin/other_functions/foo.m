function foo
    % MV - displays the path towards where it's used. Useful using multiple
    % keyboard commands
    
    d=dbstack;
    loc = [{d.file}',{d.name}',{d.line}'];
    space = [];
    for i = 1:size(loc,1) -1
        disp([space,loc(end-i +1,:)]);
        space = [space,'|-->'];
    end
end