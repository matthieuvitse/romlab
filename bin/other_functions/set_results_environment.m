function [in_out] = set_results_environment(in_out)

format shortg
c = clock;

name = in_out.name;

% create folders for storing solution
in_out.out_dir1 = strcat('data/tmp/',name,'_',in_out.test,'/');
out_file        = strcat('result_',num2str(c(1)),'_',sprintf('%02d',c(2)),'_',num2str(c(3)));
%in_out.out_dir  = strcat(in_out.out_dir1,out_file,'/');
in_out.out_dir2 = strcat(in_out.out_dir1,out_file,'_',sprintf('%04d',0),'/');

if ~exist(in_out.out_dir1,'dir')
    mkdir(in_out.out_dir1);
end
if ~exist(in_out.out_dir2,'dir')
    mkdir(in_out.out_dir2);
    in_out.out_dir = in_out.out_dir2;
else
    u = ['find ',in_out.out_dir1,' -maxdepth ',num2str(1),' -name ',strcat('"',out_file,'*','"'),' -type ','d ','| ','wc ','-l'];
    [~,tata2]=unix(u);
    in_out.out_dir = strcat(in_out.out_dir1,'result_',num2str(c(1)),'_',sprintf('%02d',c(2)),'_',num2str(c(3)),'_',sprintf('%04d',(str2double(tata2))),'/');
    mkdir(in_out.out_dir);
end

% create log file
diary(strcat(in_out.out_dir2,'log'));

% copy the data.m and input_esbroufe.m files to the tmp/ directory 
cp_command = ['cp data/data/',name,'/',name,'.m ',in_out.out_dir];
cp1 = unix(cp_command); 
if cp1 ~= 0 
    disp('File was not copied succesfuly. Please check path');
end
cp_command2 = ['cp input_ROMlab.m ',in_out.out_dir];
cp2 = unix(cp_command2);
if cp2 ~= 0 
    disp('File was not copied succesfuly. Please check path');
end

end
