% some useful functions

clear all;
clc;
close all;
paths();


print('-depsc')


%% preampble: execute prior to generating curves
fontsize  = 20;
linewidth = 4;
set(0,'DefaultAxesFontSize',fontsize)
set(0,'DefaultLineLineWidth',linewidth)
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesTickLabelInterpreter','latex');
set(0,'DefaultLegendInterpreter','latex');
scrsz = get(0,'ScreenSize');


%%
% store the loading condition in a file
%     this is useful to adapt the warp in paraview
toto = DATA.load(3).val(:,:,end)';
pos1 = 1/2 * (DATA.load(3).box(1,2) + DATA.load(3).box(1,1));
pos2 = 1/2 * (DATA.load(4).box(1,2) + DATA.load(4).box(1,1));
fileID = fopen('loading.txt','w');
fprintf(fileID,'%d\n',pos1);
fprintf(fileID,'%d\n',pos2);
fprintf(fileID,'%d\n',toto);
fclose(fileID);
%%
cd Documents/SINAPS/beton/3D/ROMlab/
paths
[in_out,DATA,m,s] = load_big_file('/svutmp/yquem/vitse/manuscrit_these/cas_test_1_traction/modul_amp/tension_376599/beam_reinforced_tension_3_tension_3_param/result_2016_07_13_0000/result_2016_7_13.mat');

UU      = ktensor(s.U.lambda,s.U.u);
%epseps  = ktensor(s.eps.lambda, s.eps.u);
%sigsig  = ktensor(s.sigma.lambda, s.sigma.u);
epseps  = [];
sigsig  = [];

dam = tensor(s.d);
if isempty(s.d)
    in_out.plot_damage = 0;
end
s_exp = sol_class(UU,epseps,sigsig,dam,'linear');

if in_out.exp_pv==1 && 0 == 1
    export_vtk(in_out,DATA,m,s,1,in_out.exp_pv_name,[]);
end
in_out.out_dir  = '/u/vitse/Documents/toto/';
in_out.out_dir1 = in_out.out_dir;
in_out.out_dir2 = in_out.out_dir;
if in_out.exp_pxdmf == 1 && 1 == 0
    [output] = export_pxdmf(in_out,DATA,m,s);
end

%%
%s = s_local;
s = s_exp;

ne1 = numel(DATA.params(3).mesh);
nn = 1;
col = hsv(floor(ne1/nn));

figure;
grid on
hold on
j= 0;
for i = 1:nn:ne1
    j = j+1;
    plot(DATA.load(2).val(:,:,i),'color',col(j,:))
    leg{j} = num2str(i);
end
legend(leg')
xlabel('t')
ylabel('ud(t,\theta)')

%% TENSION

% sigma_xx - eps_xx curve (from csv files imported from paraview)
figure('Position',[1 1 scrsz(3)/2 scrsz(4)/2]);plot(eps_xx,sig_xx);grid on


F = plot_FU_tension(in_out,DATA,m,s);


figure;hold on
for order = 1 : 10
    F = plot_FU_tension(in_out,DATA,m,s,order);
    plot(DATA.load(2).val(:,:,3),F(1,:,3,:));
    leg{order} = num2str(order);
end

title('F(ud)')
xlabel('ud(t,theta_i)')
ylabel('F')
legend(leg')
grid on












ne1 = numel(DATA.params(3).mesh);
nn = 1;
col = hsv(floor(ne1/nn));

figure;
hold on
j = 0;
% several FU curves on the same graph
for i = 1 : ne1
    if ~mod(i,nn)
        j = j + 1 ;
        %plot(DATA.load(2).val(:,:,i),F(1,:,i,:),'color',col(j,:));
        plot(DATA.load(2).val(:,:,i),F(1,:,i,:),'-b');
        leg{j} = num2str(i);
    end
end
title('F(ud)')
xlabel('ud(t,theta_i)')
ylabel('F')
legend(leg')
grid on

% several FU curves on the same graph (difference with the previous)
figure;
j = 0;
ylabel('F(ud)')
for i = 1 :nn: ne1
    grid on
    j = j+1;
    ax2 = subplot(1,6,j);
    plot(DATA.load(2).val(:,:,i),F(1,:,i,:),'color',col(j,:))
    xlabel(['ud(t,\mu=',num2str(i),')'])
    clear ax2
end
grid on

% several FU curves on different graphs
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on;hold on;
plot(DATA.load(2).val(:,:,1),F(1,:,1,:));
plot(DATA.load(2).val(:,:,floor(ne1/2)),F(1,:,floor(ne1/2),:));
plot(DATA.load(2).val(:,:,end),F(1,:,end,:));
legend('$\mu_1 = 1$','$\mu_1 = 12$','$\mu_1 = 24$')

% loading
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on;hold on;
plot(DATA.load(2).val(:,:,1));
plot(DATA.load(2).val(:,:,floor(ne1/2)));
plot(DATA.load(2).val(:,:,end));
legend('$\mu_1 = 1$','$\mu_1 = 12$','$\mu_1 = 24$')


% time / params functions
j = 0;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on;hold on;
for i = 1 :nn: size(s.U{2},2)
    grid on
    j = j+1;
    leg{j} = num2str(i);
    plot(s.U{2}(:,i))
end
legend(leg);

%csv save -- mesh
exp11(:,1) = DATA.params(2).mesh';
exp11(:,2) = DATA.load(2).val(1,:,1);
csvwrite('tension_load_1.csv',exp11);

exp12(:,1) = m.params(2).mesh';
exp12(:,2) = DATA.load(2).val(1,:,12);
csvwrite('tension_load_12.csv',exp12);

exp13(:,1) = m.params(2).mesh';
exp13(:,2) = DATA.load(2).val(1,:,24);
csvwrite('tension_load_24.csv',exp13);

%csv save -- Fu
clear exp31
exp31(:,1) = DATA.load(2).val(1,:,1);
exp31(:,2) = F(1,:,1,:);
csvwrite('tension_FU_1.csv',exp31);

clear exp32
exp32(:,1) = DATA.load(2).val(1,:,12);
exp32(:,2) = F(1,:,12,:);
csvwrite('tension_FU_12.csv',exp32);

clear exp33
exp33(:,1) = DATA.load(2).val(1,:,24);
exp33(:,2) = F(1,:,24,:);
csvwrite('tension_FU_24.csv',exp33);

%csv save -- time func
for i_modes = 1:size(s.U{2},2)
    clear exp21
    exp21(:,1) = DATA.params(2).mesh';
    exp21(:,2) = s.U{2}(:,i_modes)';
    name = ['tension_time_mode_',num2str(i_modes),'.csv'];
    csvwrite(name,exp21);
end

%csv save -- param1 func
for i_modes = 1:size(s.U{3},2)
    clear exp31
    exp31(:,1) = DATA.params(3).mesh';
    exp31(:,2) = s.U{3}(:,i_modes)';
    name = ['tension_param_mode_',num2str(i_modes),'.csv'];
    csvwrite(name,exp31);
end

%% BENDING

ne1 = numel(DATA.params(3).mesh);
nn = 1;
col = hsv(floor(ne1/nn));

% load
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on
hold on
j= 0;
for i = 1:nn:ne1
    j = j+1;
    plot(DATA.load(3).val(:,:,i))
    leg{j} = num2str(i);
end
legend(leg')
xlabel('t')
ylabel('ud(t,\theta)')

% 3 values of the load
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on;hold on;
plot(DATA.load(3).val(:,:,1));
plot(DATA.load(3).val(:,:,floor(ne1/2)));
plot(DATA.load(3).val(:,:,end));
legend('$\mu_1 = 1$','$\mu_1 = 6$','$\mu_1 = 12$')


s = s_exp;
[F,U] = plot_FU_bending(in_out,DATA,m,s,1576);
warning('sign of F to be determined ? ');
figure;plot(U(1,:,4,1),-F(1,:,4,1))
grid on
xlabel('maximum deflection')
ylabel('F')
title('F(max deflection)')



figure
ne1 = numel(DATA.params(3).mesh);
nn = 1;
col = hsv(floor(ne1/nn));
j = 0;
hold on
for i = 1 : ne1
    if ~mod(i,nn)
        j
        j = j + 1 ;
        plot(U(:,:,i,1),F(1,:,i,1));
        leg{j} = num2str(i);
    end
end
title('F(ud)')
xlabel('ud(t,theta_i)')
ylabel('F')
legend(leg')
grid on








clear exp21
exp21(:,1) = DATA.params(2).mesh';
exp21(:,2) = DATA.load(3).val(:,:,1);
csvwrite('bending_load_1.csv',exp21);

clear exp21
exp21(:,1) = DATA.params(2).mesh';
exp21(:,2) = DATA.load(3).val(:,:,6);
csvwrite('bending_load_6.csv',exp21);

clear exp21
exp21(:,1) = DATA.params(2).mesh';
exp21(:,2) = DATA.load(3).val(:,:,12);
csvwrite('bending_load_12.csv',exp21);

%csv save -- time func
for i_modes = 1:size(s.U{2},2)
    clear exp21
    exp21(:,1) = DATA.params(2).mesh';
    exp21(:,2) = s.U{2}(:,i_modes)';
    name = ['bending_time_mode_',num2str(i_modes),'.csv'];
    csvwrite(name,exp21);
end

%csv save -- param1 func
for i_modes = 1:size(s.U{3},2)
    clear exp31
    exp31(:,1) = DATA.params(3).mesh';
    exp31(:,2) = s.U{3}(:,i_modes)';
    name = ['bending_param1_mode_',num2str(i_modes),'.csv'];
    csvwrite(name,exp31);
end

%csv save -- param2 func
for i_modes = 1:size(s.U{4},2)
    clear exp31
    exp31(:,1) = DATA.params(4).mesh';
    exp31(:,2) = s.U{4}(:,i_modes)';
    name = ['bending_param2_mode_',num2str(i_modes),'.csv'];
    csvwrite(name,exp31);
end

% computes the norm of the time functions (eg norm of the modes)
for i = 1 : size(s.U{1},2)
    norm_modes(i) = sqrt(s.U{2}(:,i)' * m.params(2).integ2 * s.U{2}(:,i));
end
matA(:,1) = [1:numel(norm_modes)];
matA(:,2) = norm_modes;
csvwrite('norm_modes.csv',matA);



% compute surface responses of the max defection

max_deflection = open_csv('/usrtmp/vitse/These/cas_test_2_flexion/bending_376635/beam_reinforced_flexture_4_flexture_4_param/result_2016_07_15_0000/csv1');
max_deflection = open_csv('/u/vitse/Dropbox/TAFF/Redaction/VM');
% plot the (t,varUd) surface for a given value of paramE
figure;surf(max_deflection(:,:,1))

% plot the (t,varE) surface for a given value of paramUd
D = permute(max_deflection,[1 3 2]);
figure;surf(D(:,:,1))

% plot the (varE,varUd) surface for a given timestep (t=90s)
D = permute(max_deflection,[2 3 1]);
figure;surf(D(:,:,90))





%modes
% time
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{2}(:,1));grid on;figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{2}(:,3));grid on
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{2}(:,5));grid on;figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{2}(:,7));grid on

% param1
ax2 = [0 12 -0.1 0.6];
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{3}(:,1),'r');axis(ax2);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{3}(:,3),'r');axis(ax2);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{3}(:,5),'r');axis(ax2);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{3}(:,7),'r');axis(ax2);grid on;

% param2
colvec = [21/256 , 152 / 256 , 87/256];
ax3 = [ 0 10 -4 4 ];
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{4}(:,1),'Color',colvec);axis(ax3);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{4}(:,3),'Color',colvec);axis(ax3);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{4}(:,4),'Color',colvec);axis(ax3);grid on;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);plot(s.U{4}(:,7),'Color',colvec);axis(ax3);grid on;


% time / params functions
j = 0;
figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
grid on;hold on;
for i = 1 :nn: size(s.U{2},2)
    grid on
    j = j+1;
    leg{j} = num2str(i);
    plot(s.U{4}(:,i))
end
legend(leg);

%%
% computes the norm of the time functions (eg norm of the modes)
for i = 1 : size(s.U{1},2)
    norm_modes(i) = sqrt(s.U{2}(:,i)' * m.params(2).integ2 * s.U{2}(:,i));
end

figure('Position',[1 1 scrsz(3)/2 scrsz(3)/2]);
semilogy(norm_modes)
%title('L2 norm of the modes')
%xlabel('Iterations')
%ylabel('orm')
grid on

