function [] = store_quantities_iter(in_out,DATA,m,s_local,s_linear,iter)

% We save the results at each iteration for post-process and debugging
%    a "output_data" class is created, storing different quantities of
%    interest, and stored as results_iter_N.mat
output_data.DATA = DATA;
output_data.m = m;
output_data.s_local = s_local;
output_data.s_linear = s_linear;

iterdir = 'iter/';
if (exist(strcat(in_out.out_dir,iterdir),'dir') == 0)
    mkdir(strcat(in_out.out_dir,iterdir));
end

name_iter = strcat(in_out.out_dir,iterdir,'results_iter_',num2str(iter),'.mat');
save(name_iter,'output_data');

end