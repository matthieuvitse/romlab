function [vec_sum] = sum_v(vec)
% sum of the 3 first components of a vector

vec_sum = vec(1) + vec(2) + vec(3);

end