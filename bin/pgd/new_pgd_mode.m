function [dU,U_latin,nb_pgd_modes] = new_pgd_mode(in_out,m,nb_pgd_modes,U_latin,Hdo_f,Q)
% computation of new pgd mode(s)
%%%% CAUTION: HOMGENEOUS BC IN SPACE ASSUMED %%%%

% initialization
nb_pgd_modes = nb_pgd_modes +1;

param_redis = m.param_redis;

ddlu  = m.dofuu;
ddld  = m.dofud;
relax = m.relax;
switch in_out.solver
    case 'LATIN'
        if in_out.update_sd
            Hdo_fuu = Hdo_f(ddlu,ddlu);
        else
            L_H = m.HHdo_inv_L;
            U_H = m.HHdo_inv_U;
        end
    case 'PGD'
        L_H = m.k_L;
        U_H = m.k_U;
end

fooU = cell(1,length(m.params));
for i = 1 : length(m.params)
    fooU{i} = zeros(m.params(i).ndof,1);
end
dU = ktensor(fooU);

if nb_pgd_modes == 1
    for i = 2 : length(m.params)
        dU.U{i} = ones(size(m.params(i).mesh))';
    end
else
    for i = 2 : length(m.params)
        dU.U{i} = U_latin.U{i}(:,nb_pgd_modes-1);
    end
end

% generation of a new PGD mode
ptfixe = 1;
du2old = 1;
stagn  = 1;

% fixed point
while (stagn > 1e-3) && (ptfixe <=5)
    ptfixe = ptfixe + 1;
    
    % computation of the space function
    du2  = 1;
    dudf = Q;
    for i = 2 : length(m.params)
        du2  = du2 * dU.U{i}'*m.params(i).integ2*dU.U{i};
        dudf = ttv(dudf,m.params(i).integ2*dU.U{i},2);
    end
    DFu = double(1/du2*dudf);
    
    if ~in_out.update_sd
        dU1 = L_H \ DFu(ddlu,1);
        dU.U{1}(ddlu,1) = U_H \ dU1;
    else
        dU.U{1}(ddlu,1) = Hdo_fuu\DFu(ddlu,1);
    end
    
    dU.U{1}(ddld,1) = zeros(length(ddld),1);
    
    % computation of the time-parameters functions
    dU2  = dU.U{1}'*Hdo_f*dU.U{1};
    dUdf = ttv(Q,dU.U{1},1);
    r    = 1/dU2 * dUdf;
    
    if numel(m.params) == 2
        du = ktensor(1,double(r));
    else
        du = cp_als(r,1,'printitn',m.printitn_cp_als);                %parafac decomposition to get the different functions
    end
    for i = 2 : length(m.params)
        dU.U{i} = du.U{i-1};
    end
    
    % stagnation criterion
    stagn  = sqrt(abs(du2-du2old)/du2old);
    du2old = du2;
     
    if in_out.debug
        disp(['            ... criterion at fixed point iteration: ',num2str(ptfixe-1),' : ',num2str(stagn)]);
    end
end

% relaxation of the new mode
dU = redistribute(relax * dU,param_redis);

% (ortho) normalization of the new mode
if in_out.orthog && ~isequal(m.rank_init,size(U_latin.U{1},2))
    [dU,U_latin,nb_pgd_modes] = orthog_pgd(m,dU,U_latin,nb_pgd_modes);
else
    vec1 = [1:numel(m.params)];   %%%
    vec2 = vec1(vec1~=2);         %%%
    
    for i_param = 1 : numel(vec2)
        ind_p=vec2(i_param);
        dUU(i_param) = dU.U{ind_p}' * m.params(ind_p).integ2 * dU.U{ind_p};       %%%
        dU.U{ind_p} = dU.U{ind_p} / sqrt(dUU(i_param));
    end

    dU.U{2} = dU.U{2} * prod(sqrt(dUU));
end

end
