function [dU,U_latin,nb_pgd_modes] = orthog_pgd(m,dU,U_latin,nb_pgd_modes,varargin)
warning off backtrace
disp('                ... orthonormalization of the new mode with respect to the existing basis...')

dU_new = dU;

vec1 = [1:numel(m.params)];
vec2 = vec1(vec1~=2);

if nargin == 5
    I_M = varargin{1};
else
    I_M = m.params(1).integ2;
end
I_t = m.params(2).integ2;

rank_init = m.rank_init;
size_basis = size(U_latin.U{1},2);

vec_ten = ktensor(ones(size(U_latin.U{2},2),1),U_latin.U{2});
if numel(m.params) > 2
    for i_p = 3 : numel(m.params)
        vec_ten.U{i_p-1} = U_latin.U{i_p};
    end
end

for i = (rank_init+1) : size_basis
    
    vec_ten2 = ktensor(1, U_latin.U{2}(:,i));
    vec_dU = ktensor(1, dU.U{2});
    if numel(m.params) > 2
        for i_p = 3 : numel(m.params)
            vec_ten2.U{i_p-1} = U_latin.U{i_p}(:,i);
            vec_dU.U{i_p-1} = dU.U{i_p};
        end
    end
    dU_proj_i   = U_latin.U{1}(:,i)' * I_M * dU.U{1};
    toto_i = tensor(vec_ten2) + dU_proj_i .* tensor(vec_dU);
    
    dU_new.U{1}       = dU_new.U{1} - dU_proj_i * U_latin.U{1}(:,i);
    
    if numel(m.params) > 2
        dU_tp             = redistribute(cp_als(tensor(toto_i),1,'printitn',m.printitn_cp_als),1);   % redistribution on time functions
        for i_p = 3 : numel(m.params)
            dU_tp_norm(i_p-2) = sqrt(dU_tp.U{i_p-1}' * m.params(i_p).integ2 * dU_tp.U{i_p-1});
            U_latin.U{i_p}(:,i) = dU_tp.U{i_p-1} / dU_tp_norm(i_p-2);
        end
    else
        dU_tp = ktensor(1,double(toto_i));
        dU_tp_norm = 1;
    end
    
    U_latin.U{2}(:,i) = dU_tp.U{1} * prod(dU_tp_norm);
    
end

% normalizing new mode
for i_param = 1 : numel(vec2)
    dU_norm(i_param) = dU_new.U{vec2(i_param)}' * m.params(vec2(i_param)).integ2 * dU_new.U{vec2(i_param)};
end

for i_param = 1 : numel(vec2)
    ind_p=vec2(i_param);
    dU_new.U{ind_p} = dU_new.U{ind_p} / sqrt(dU_norm(i_param));
end
dU_new.U{2} = dU_new.U{2} * prod(sqrt(dU_norm));


dU2 = dU_new.U{2}' * I_t * dU_new.U{2};

if sqrt(dU2) > 1e-8
    dU = dU_new;
else
    nb_pgd_modes = nb_pgd_modes - 1;
    warning('mode skipped')
    
    dU = [];
end

warning on backtrace
disp('                ... new pgd mode orthonormalized...')

end