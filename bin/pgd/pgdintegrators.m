function m=pgdintegrators(m,arg)
tic
if arg
    disp(['ROMlab: Assembling PGD operators... ']);
    
    m.param_redis = 2;
    ne = length(m.params);
    
    % special treatment for space
    m.params(1).integ2 = m.l2;
    
    % for all parameters but space
    for i = 2:ne
        nt = m.params(i).ndof;
        lt = m.params(i).mesh;
        h = lt(2:end) - lt(1:end-1);
        m.params(i).integ1 = sparse(1,nt);
        m.params(i).integ2 = sparse(nt,nt);
        Y1 = (1./2.) * [ 1 1 ];
        Y2 = (1./6.) * [2 1;1 2];
        
        for j = 1 : nt-1
            m.params(i).integ1(1,j:j+1) = m.params(i).integ1(1,j:j+1) + h(j)*Y1;
            m.params(i).integ2(j:j+1,j:j+1) = m.params(i).integ2(j:j+1,j:j+1) + h(j)*Y2;
        end
    end
    
    disp([ '    ... operators assembled... ',num2str(toc,2) ' s']);
else
    warning('Computation of mass and L2 matrices skipped')
end

end