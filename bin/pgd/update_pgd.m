function [U] = update_pgd(in_out,DATA,m,U_old,Q,Hdo)
% updates the time basis prior to generating a new PGD mode

U = U_old;

rank_init  = m.rank_init;
size_basis = size(U_old.U{1},2);

size_basis_to_update = size_basis - (rank_init);

K_update = zeros(size_basis_to_update,size_basis_to_update);
Qj       = zeros(size_basis_to_update,numel(m.params(2).mesh));

for j_mode = 1 : size_basis_to_update
     
    indj = rank_init + j_mode;
    U1j  = U_old.U{1}(:,indj);
    Qmode = Q;
    if numel(m.params) > 2
        for i_p = 3 : numel(m.params)
            I_j = m.params(i_p).integ2;
            U3j{i_p-2}  = U_old.U{i_p}(:,indj);
            Qmode       = ttv(Qmode,I_j*U3j{i_p-2},3);
        end
    else
        U3j  = {1};
    end
        
    % Computation of Qj  
    Qj(j_mode,:) = ttv(Qmode,U1j,1);
    
    for i_mode = 1 : size_basis_to_update
        indi = rank_init + i_mode;
        U1i  = U_old.U{1}(:,indi);
        if numel(m.params) >2
            for j_p = 3 : numel(m.params)
                I_j = m.params(j_p).integ2;
                U3i{j_p-2} = U_old.U{j_p}(:,indi);
                fac3(j_p-2)  = (U3j{j_p-2}' * I_j * U3i{j_p-2});
            end
        else
            U3j  = {1};
            fac3 = 1;
        end
        
        % Assembly of the K_update operator
        K_update(j_mode,i_mode) = prod(fac3) * (U1j' * Hdo * U1i);
    end
end

U2i_correc = K_update \ Qj;

for i_mode = (rank_init+1) : size_basis
    U.U{2}(:,i_mode) = U.U{2}(:,i_mode) + U2i_correc(i_mode-rank_init,:)';
end

end