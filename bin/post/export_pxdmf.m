function [output] = export_pxdmf(in_out,DATA,m,s_linear)

param_redis = numel(m.params);

disp('ROMlab: Exporting .pxdmf file...')
tic
U_pxdmf = s_linear.U;                   % ordered as DDL-numbering
Uvisu   = s_linear.U;                   % reordered as node-numbering
F_visu   = s_linear.F;
F_pxdmf = s_linear.F;
sig_visu= s_linear.sigma;
if in_out.plot_damage
    hat_d_visu = redistribute(cp_als(tensor(s_linear.d),size(s_linear.U{1},2),'printitn',m.printitn_cp_als,'tol',m.tol_cp_als),param_redis);   % NITER ?
else
    hat_d_visu = [];
end

i_corres = 0;
for i_model = 1 : numel(m.model)
    for i = 1 : length(m.model(i_model).node)
        i_corres = i_corres + 1;
        a(i_corres,:) = m.model(i_model).node(i).dof_corres;
        if strcmp(m.model(i_model).elem(1).typel,'tri3') == 1 || strcmp(m.model(i_model).elem(1).typel,'qua4') == 1
            Uvisu.U{1}(2*i_corres-1:2*i_corres,:) = U_pxdmf.U{1}(a(i_corres,:),:);  
        elseif strcmp(m.model(i_model).elem(1).typel,'bar2') == 1
            if DATA.dim == 2
                Uvisu.U{1}(2*i_corres-1:2*i_corres,:) = U_pxdmf.U{1}(a(i_corres,:),:);
            elseif DATA.dim == 3
                Uvisu.U{1}(3*i_corres-2:3*i_corres,:) = U_pxdmf.U{1}(a(i_corres,:),:);
            end
        elseif strcmp(m.model(i_model).elem(1).typel,'cub8') == 1 
            Uvisu.U{1}(3*i_corres-2:3*i_corres,:) = U_pxdmf.U{1}(a(i_corres,:),:); 
            if in_out.reac_nodale
                F_visu.U{1}(3*i_corres-2:3*i_corres,:) = F_pxdmf.U{1}(a(i_corres,:),:); 
            end
        end
    end
end

output = output_pxdmf(in_out,DATA,m,Uvisu,U_pxdmf,hat_d_visu,sig_visu,F_visu);  % U visu ordered as nodes_list; U_pxdmf ordered as DDL_list

disp(['    ... pxdmf export done... ',num2str(toc,2) ' s'])

end