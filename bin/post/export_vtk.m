function [m] = export_vtk(in_out,DATA,m,s,flag,pref,iter)
% exports strain, stress and pricipal stree fields to VTK (=> Paraview)

if flag == 1
    disp('ROMlab: Exporting .vtu files...')
    tic
    U = tensor(s.U);
    eps = tensor(s.eps);
    sig = tensor(s.sigma);
    d = tensor(s.d);

    [m,U_vis,eps_vis,sig_vis,sig_ps_vis,d_vis,sig_vm_vis,var_E_vis]=viz_fields(in_out,DATA,m,U,eps,sig,d);
    clear eps sig d U
    
    plot_sigeps = in_out.plot_sigeps;
    plot_damage = in_out.plot_damage;
    plot_sig_vm = in_out.plot_sig_vm;
    plot_sig_ps = in_out.plot_sig_ps;
    plot_var_E  = in_out.plot_var_E;
    
    if strcmp (m.params(2).name,'t');
        nT = length(m.params(2).mesh);    
    else
        nT = 1;
    end
    
    ne = [1 , 1];
    if numel(m.params) > 2
        ne(1:(numel(m.params)-2)) = [m.params(3:end).ndof];
    end
    
    for i_model = 1 : numel(m.model)
        
        if in_out.debug && strcmp(s.stage,'local')
            out_folder = strcat(in_out.out_dir,'local_iter_',num2str(iter),'/',pref,'_model',num2str(i_model),'/');
        else
            out_folder = strcat(in_out.out_dir,pref,'_model',num2str(i_model),'/');
        end
        if ~exist(out_folder,'dir')
            mkdir(out_folder);
        end
        
        dofelem  = m.model(i_model).elem_dof_list;
        dofelem2 = m.model(i_model).elem_list;
        
        if plot_var_E 
            var_E_g = var_E_vis;
        else
            var_E_g = [];
        end
        
        if nT == 1;
            Uvi     = [];
            Evi     = [];
            Svi     = [];
            Dvi     = [];
            Spsvi   = [];
            Svmvi   = [];
            for j_plot = 1:1;%ne(1) : ne(1)
                for k_plot = 1:1;%ne(2) : ne(2)
                switch s.stage
                    case 'local'
                        Uvi = [];
                        if plot_sigeps
                            Evi = eps_vis(dofelem,1,j_plot,k_plot);
                            Svi = sig_vis(dofelem,1,j_plot,k_plot);
                        else
                            Evi = [];
                            Svi = [];
                        end
                        if plot_damage
                            Dvi = d_vis(dofelem2,1,j_plot,k_plot);
                        else
                            Dvi = [];
                        end
                        if plot_sig_ps
                            Spsvi = sig_ps_vis(:,1,j_plot,k_plot);
                        else
                            Spsvi = [];
                        end
                        if plot_sig_vm
                            Svmvi = sig_vm_vis(:,1,j_plot,k_plot);
                        else
                            Svmvi = [];
                        end
                            
                        
                    case 'linear'
                        Uvi = U_vis(:,1,j_plot,k_plot);
                        if plot_sigeps
                            
                            Evi = eps_vis(dofelem,1,j_plot,k_plot);
                            Svi = sig_vis(dofelem,1,j_plot,k_plot);
                        else
                            Evi = [];
                            Svi = [];
                        end
                        
                        if plot_damage && ~strcmp(in_out.solver,'DIRECT')
                            Dvi = d_vis(dofelem2,1,j_plot,k_plot);
                        else
                            Dvi = [];
                        end
                        if plot_sig_ps
                            Spsvi = sig_ps_vis(:,1,j_plot,k_plot);
                        else
                            Spsvi = [];
                        end
                        if plot_sig_vm
                            Svmvi = sig_vm_vis(:,1,j_plot,k_plot);
                        else
                            Svmvi = [];
                        end
                    case 'contact'
                        Dvi = [];
                        Uvi = U_vis(:,1,j_plot,k_plot);
                        if plot_sigeps
                            Evi = eps_vis(dofelem,1,j_plot,k_plot);
                            Svi = sig_vis(dofelem,1,j_plot,k_plot);
                        else
                            Evi = [];
                            Svi = [];
                        end
                        if plot_sig_ps
                            Spsvi = sig_ps_vis(:,1,j_plot,k_plot);
                        else
                            Spsvi = [];
                        end
                        if plot_sig_vm
                            Svmvi = sig_vm_vis(:,1,j_plot,k_plot);
                        else
                            Svmvi = [];
                        end
                    otherwise
                        warning('this case is not taken into account...')
                        foo;keyboard;
                end
                
                vtk_sol2(in_out,DATA,m.model(i_model),s,1,j_plot,k_plot,Uvi,Evi,Svi,Spsvi,Dvi,Svmvi,var_E_g,out_folder);
                end 
            end
        else  % Several timesteps
             for (i_plot = 1 : nT)
%             parfor (i_plot = 1 : nT,  in_out.arg_parallel)
                Uvi     = [];
                Evi     = [];
                Svi     = [];
                Dvi     = [];
                Spsvi   = [];
                Svmvi   = [];
                for j_plot = 1:1;%ne(1) : ne(1)
                    for k_plot = 1:1;%ne(2) : ne(2)
                    switch s.stage
                        case 'local'
                            Uvi = [];
                            if plot_sigeps
                                Evi = eps_vis(dofelem,i_plot,j_plot,k_plot);
                                Svi = sig_vis(dofelem,i_plot,j_plot,k_plot);
                            else
                                Evi = [];
                                Svi = [];
                            end
                            if plot_damage
                                Dvi = d_vis(dofelem2,i_plot,j_plot,k_plot);
                            else
                                Dvi = [];
                            end
                            if plot_sig_ps
                                Spsvi = sig_ps_vis(:,i_plot,j_plot,k_plot);
                            else
                                Spsvi = [];
                            end
                            if plot_sig_vm
                                Svmvi = sig_vm_vis(:,i_plot,j_plot,k_plot);
                            else
                                Svmvi = [];
                            end


                        case 'linear'
                            Uvi = U_vis(:,i_plot,j_plot,k_plot);
                            if plot_sigeps

                                Evi = eps_vis(dofelem,i_plot,j_plot,k_plot);
                                Svi = sig_vis(dofelem,i_plot,j_plot,k_plot);
                            else
                                Evi = [];
                                Svi = [];
                            end

                            if plot_damage && ~strcmp(in_out.solver,'DIRECT')
                                Dvi = d_vis(dofelem2,i_plot,j_plot,k_plot);
                            else
                                Dvi = [];
                            end
                            if plot_sig_ps
                                Spsvi = sig_ps_vis(:,i_plot,j_plot,k_plot);
                            else
                                Spsvi = [];
                            end
                            if plot_sig_vm
                                Svmvi = sig_vm_vis(:,i_plot,j_plot,k_plot);
                            else
                                Svmvi = [];
                            end
                        case 'contact'
                            Dvi = [];
                            Uvi = U_vis(:,i_plot,j_plot,k_plot);
                            if plot_sigeps
                                Evi = eps_vis(dofelem,i_plot,j_plot,k_plot);
                                Svi = sig_vis(dofelem,i_plot,j_plot,k_plot);
                            else
                                Evi = [];
                                Svi = [];
                            end
                            if plot_sig_ps
                                Spsvi = sig_ps_vis(:,i_plot,j_plot,k_plot);
                            else
                                Spsvi = [];
                            end
                            if plot_sig_vm
                                Svmvi = sig_vm_vis(:,i_plot,j_plot,k_plot);
                            else
                                Svmvi = [];
                            end
                        otherwise
                            warning('this case is not taken into account...')
                            foo;keyboard;
                    end

                    vtk_sol2(in_out,DATA,m.model(i_model),s,i_plot,j_plot,k_plot,Uvi,Evi,Svi,Spsvi,Dvi,Svmvi,var_E_g,out_folder);
                    end 
                end
            end
        end
        
        if in_out.debug && strcmp(s.stage,'local')
            pvd_file(strcat(in_out.out_dir,'local_iter_',num2str(iter),'/'),strcat(pref,'_model',num2str(i_model),'/'),strcat('output_',num2str(i_model)),'output',ne,nT);
        else
            pvd_file(in_out.out_dir,strcat(pref,'_model',num2str(i_model),'/'),strcat('output_',num2str(i_model)),'output',ne,nT);
        end
    end
    disp(['    ... vtu export done... ' num2str(toc,2) ' s'])
end

end
