function VTKImage(u,v,Xi,Yi,num)

if nargin<3
    num=1;
end
tic
sizeim=size(u);

u=u';
v=v';

filename=strcat('tmp/image_1_',num2str(num),'.vtr');
fmesh = fopen(filename,'w');
fprintf(fmesh,'<?xml version="1" ?>\n');

fprintf(fmesh,'<VTKFile type="RectilinearGrid" version="0.1" byte_order="LittleEndian">\n');
fprintf(fmesh,'\t <RectilinearGrid WholeExtent="0 %u 0 %u 0 0">\n',sizeim(1)-1,sizeim(2)-1);
fprintf(fmesh,'\t\t <Piece Extent="0 %u 0 %u 0 0">\n',sizeim(1)-1,sizeim(2)-1);
fprintf(fmesh,'\t\t\t <PointData> \n');

fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="u" NumberOfComponents="1" format="ascii"> \n');
fprintf(fmesh,'%e \n',u(:));
fprintf(fmesh,'\t\t\t\t </DataArray>\n');

fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="v" NumberOfComponents="1" format="ascii"> \n');
fprintf(fmesh,'%e \n',v(:));
fprintf(fmesh,'\t\t\t\t </DataArray>\n');

fprintf(fmesh,'\t\t\t </PointData> \n');
fprintf(fmesh,'\t\t\t <Coordinates> \n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="X_COORDINATES" NumberOfComponents="1" format="ascii" >\n');
fprintf(fmesh,'%f \n',Xi);
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="X_COORDINATES" NumberOfComponents="1" format="ascii" >\n');
fprintf(fmesh,'%f \n',Yi);
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="X_COORDINATES" NumberOfComponents="1" format="ascii" >\n');
fprintf(fmesh,'0 \n');
fprintf(fmesh,'\t\t\t\t </DataArray>\n');

fprintf(fmesh,'\t\t\t </Coordinates> \n');

fprintf(fmesh,'\t\t </Piece> \n');
fprintf(fmesh,'\t </RectilinearGrid> \n');
fprintf(fmesh,'</VTKFile>  \n');

disp(['VTK LevelSet File... ' num2str(toc,2) ' s']);
end