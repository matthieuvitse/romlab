function sol_new=mesh_change(sol,m1,m2)

rep=m2.repsubdof;
U=m2.u;
U(rep)=sol;
Unew=zeros(length(m2.u),1);

for ii=1:numel(m1.elem)
   
	e=m1.elem(ii);
    listx=m2.elem(ii).rep(1:2:length(m1.elem(ii).rep));
    listy=m2.elem(ii).rep(2:2:length(m1.elem(ii).rep));
    
    Xref=m1.elem(ii).node_pos(:,1);
    Yref=m1.elem(ii).node_pos(:,2);
    
    X=m2.elem(ii).node_pos(:,1);
    Y=m2.elem(ii).node_pos(:,2);

%     denom=(Xref(1)-Xref(3))*(Yref(2)-Yref(3))-(Xref(2)-Xref(3))*(Yref(1)-Yref(3));
% 
%     xi=inline('[(x-Xref(3))*(Yref(2)-Yref(3))-(Xref(2)-Xref(3))*(y-Yref(3))]','x','y','Xref','Yref');
% 
%     eta=inline('[(Xref(1)-Xref(3))*(y-Yref(3))-(x-Xref(3))*(Yref(1)-Yref(3))]','x','y','Xref','Yref');
%     
%     xi=1/denom*xi(U(listx),U(listy),Xref,Yref);
%     eta=1/denom*eta(U(listx),U(listy),Xref,Yref);
%     
%     U(listx)=X(1)*xi+X(2)*eta+X(3)*(1-xi-eta);
%     U(listy)=Y(1)*xi+Y(2)*eta+Y(3)*(1-xi-eta);
    
    
%     keyboard


M=[Xref(1) Yref(1) 1;
   Xref(2) Yref(2) 1;
   Xref(3) Yref(3) 1];

if det(M)==0
    keyboard
end
    
a=M\[1;0;0];
b=M\[0;1;0];
c=M\[0;0;1];

f=inline('[coef(1)*x+coef(2)*y+coef(3)]','x','y','coef');


%     listx=m2.elem(ii).rep(1:2:length(u));
%     listy=m2.elem(ii).rep(2:2:length(u));
%     keyboard
%     f(sol(listx),sol(listy),a)
    Unew(listx)=X(1)*f(X,Y,a)+X(2)*f(X,Y,b)+X(3)*f(X,Y,c);
    Unew(listy)=Y(1)*f(U(listx),U(listy),a)+Y(2)*f(U(listx),U(listy),b)+Y(3)*f(U(listx),U(listy),c);
    
%     Unew(listx)=X(1)*f(U(listx),U(listy),a)+X(2)*f(U(listx),U(listy),b)+X(3)*f(U(listx),U(listy),c);
%     Unew(listy)=Y(1)*f(U(listx),U(listy),a)+Y(2)*f(U(listx),U(listy),b)+Y(3)*f(U(listx),U(listy),c);
    
    
    
    
end

sol_new=Unew(rep);

end