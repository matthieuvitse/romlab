function sol_new=mesh_transfert(sol,e2,e1,pos1,pos2)

sol_new=zeros(length(sol),1);
    
listx=1:2:length(sol);
listy=2:2:length(sol);
    
    Xref=e1.node_pos(:,1);
    Yref=e1.node_pos(:,2);
    
    X=e2.node_pos(:,1);
    Y=e2.node_pos(:,2);

    denom=(Xref(1)-Xref(3))*(Yref(2)-Yref(3))-(Xref(2)-Xref(3))*(Yref(1)-Yref(3));

    xi=inline('[(x-Xref(3))*(Yref(2)-Yref(3))-(Xref(2)-Xref(3))*(y-Yref(3))]','x','y','Xref','Yref');

    eta=inline('[(Xref(1)-Xref(3))*(y-Yref(3))-(x-Xref(3))*(Yref(1)-Yref(3))]','x','y','Xref','Yref');
    
    xi=1/denom*xi(sol(listx)+pos1(1),sol(listy)+pos1(2),Xref,Yref);
    eta=1/denom*eta(sol(listx)+pos1(1),sol(listy)+pos1(2),Xref,Yref);
%     
%     xi
%     eta
%     listx
%     listy
    
    sol_new(listx)=X(1)*xi+X(2)*eta+X(3)*(1-xi-eta)-pos2(1);
    sol_new(listy)=Y(1)*xi+Y(2)*eta+Y(3)*(1-xi-eta)-pos2(2);
%     keyboard

% 
% M=[Xref(1) Yref(1) Xref(1)*Yref(1);
%    Xref(2) Yref(2) Xref(2)*Yref(2)
%    Xref(3) Yref(3) Xref(3)*Yref(3)];
% 
% if det(M)==0
%     keyboard
% end
%     
% a=M\[1;0;0];
% b=M\[0;1;0];
% c=M\[0;0;1];
% 
% f=inline('[coef(1)*x+coef(2)*y+coef(3)*x.*y]','x','y','coef');

% 
%     listx=m2.elem(ii).rep(1:2:length(u));
%     listy=m2.elem(ii).rep(2:2:length(u));
% %     keyboard
% %     f(sol(listx),sol(listy),a)
%     sol_new(listx)=X(1)*f(sol(listx),sol(listy),a)+X(2)*f(sol(listx),sol(listy),b)+X(3)*f(sol(listx),sol(listy),c);
%     sol_new(listy)=Y(1)*f(sol(listx),sol(listy),a)+Y(2)*f(sol(listx),sol(listy),b)+Y(3)*f(sol(listx),sol(listy),c);
    



end