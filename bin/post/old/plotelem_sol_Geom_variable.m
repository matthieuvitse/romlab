function [add,node]=plotelem_sol_Geom_variable(el,node,nn,alpha,beta)
%  J.C. Passieux (2010)

u=[];
n=[];
e=[];
sig=[];
eps=[];
for k=1:numel(el.node_num)
    in=el.node_num(k);
    if(node(in).pnum==0)
        nn=nn+1;
        node(in).pnum=nn;
        n=[n ; transform(node(in).pos',alpha,beta)];
        u=[u ; node(in).u];
        if numel(node(in).pos)==2
            n=[n ; 0];
            u=[u ; 0];
        end
    end
    e=[e ; node(in).pnum];
end

add.u=u;
add.node=n;
add.elem=e;
add.nnode=nn;
add.eps=el.eps;
add.sig=el.sig;
add.ps=el.principal_stress;
