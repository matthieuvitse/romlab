function []=vtk_sol_Geom_variable(m,part,time,alpha,beta)
%  J.C. Passieux (2010)

if nargin<3
	time=1;
end

global DATA
tic

if(DATA.plot_sol)

nelem=0;
nnode=0;
connectivity=[];
offsets=[];
types=[];
node=[];
u=[];
eps=[];
sig=[];
ps=[];
for i=1:size(m.elem,2)
	if(1 || m.elem(i).plot)
		if isempty(offsets)
			coffs=0;
		else
			coffs=offsets(size(offsets,2));
		end
		[add,m.node]=plotelem_sol_Geom_variable(m.elem(i),m.node,nnode,alpha,beta);
  		if(m.elem(i).typel=='qua4')
			offsets=[offsets coffs+4];
			types=[types 9];
		elseif(m.elem(i).typel=='tri3')
			offsets=[offsets coffs+3];
			types=[types 5];
       elseif(m.elem(i).typel=='tri6')
			offsets=[offsets coffs+6];
			types=[types 22];
        elseif strcmp(m.elem(i).typel,'tet4')
            offsets=[offsets coffs+4];
			types=[types 10];
        elseif strcmp(m.elem(i).typel,'qua8')   
            offsets=[offsets coffs+8];
			types=[types 12];
		end
		connectivity=[connectivity ; add.elem];
		nelem=nelem+1;		
		eps=[eps ; add.eps];
		sig=[sig ; add.sig];
        ps=[ps ; add.ps];
		u=[u ; add.u];
		node=[node ; add.node];
  		nnode=add.nnode;
	end
end


filename=strcat('tmp/sol_',num2str(part),'_',num2str(time),'.vtu');
fmesh = fopen(filename,'w');
fprintf(fmesh,'<?xml version="1" ?>\n');

fprintf(fmesh,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
fprintf(fmesh,'\t <UnstructuredGrid>\n');
fprintf(fmesh,'\t\t <Piece NumberOfPoints="%u" NumberOfCells="%u">\n',nnode,nelem);

% POINT DATA

fprintf(fmesh,'\t\t\t <PointData scalars="scalar"> \n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="displ" NumberOfComponents="3" format="ascii">\n');
fprintf(fmesh,'%e \n',u);
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t </PointData> \n');

% CELL DATA

fprintf(fmesh,'\t\t\t <CellData> \n');
if(DATA.plot_sigeps)
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="epsilon" NumberOfComponents="%u" format="ascii">\n',3*(DATA.dim-1));
    fprintf(fmesh,'%e \n',eps);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="sigma" NumberOfComponents="%u" format="ascii">\n',3*(DATA.dim-1));
    fprintf(fmesh,'%e \n',sig);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n'); 
    if(DATA.plot_vonmises)
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="pstress" NumberOfComponents="3" format="ascii">\n');
        fprintf(fmesh,'%e \n',ps);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    end
end
fprintf(fmesh,'\t\t\t </CellData> \n');

% POINTS

fprintf(fmesh,'\t\t\t <Points>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" NumberOfComponents="3" format="ascii">\n');
fprintf(fmesh,'%f \n',node');
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t </Points>\n');

% CELLS
fprintf(fmesh,'\t\t\t <Cells>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="connectivity" format="ascii">\n');
fprintf(fmesh,'%u \n',connectivity-1');
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="offsets" format="ascii">\n');
fprintf(fmesh,'%u \n',offsets);
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t\t <DataArray type="UInt8" Name="types" format="ascii">\n');
fprintf(fmesh,'%u \n',types);
fprintf(fmesh,'\t\t\t\t </DataArray>\n');
fprintf(fmesh,'\t\t\t </Cells>\n');

% END TVK FILE

fprintf(fmesh,'\t\t </Piece>\n');
fprintf(fmesh,'\t </UnstructuredGrid> \n');
fprintf(fmesh,'</VTKFile> \n');

fclose(fmesh);
end

disp(['ROMlab: Output solution file written... ' num2str(toc,2) ' s']);
