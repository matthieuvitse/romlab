function [ output ] = output_pxdmf(in_out,DATA,m,U,Ugrad,dam,sig_vis,F_vis)
% numbering: U ordered as nodes_list; U_grad ordered as DDL_list
dof_corres = 0;
for i_model = 1 : 1 %numel(DATA.model)
    name = in_out.exp_pv_name;
    filename1 = strcat(name,'_',num2str(i_model),'.pxdmf');
    filename2 = strcat(name,'_',num2str(i_model),'.xdmf');
    model = m.model(i_model);
    dof_corres = dof_corres(end) + 1 : (dof_corres(end) + model.ndof);
    U_model = U.u{1}(dof_corres,:);    
    
    nodes = cell(numel(m.params),2);
    
    if strcmp(DATA.model(i_model).type,'FE2D') 
        UX = U_model(1:2:end,:);
        UY = U_model(2:2:end,:);
        UZ = zeros(size(UY));
%        comp = 3;
%        nodes{1} = zeros(size(model.node,2),2);
    elseif strcmp(DATA.model(i_model).type,'TRUSS')
        if DATA.dim == 3
            UX = U_model(1:3:end,:);
            UY = U_model(2:3:end,:);
            UZ = U_model(3:3:end,:);
%           nodes{1} = zeros(size(model.node,2),3);
%           comp = 1;
        elseif DATA.dim == 2
            UX = U_model(1:2:end,:);
            UY = U_model(2:2:end,:);
            UZ = zeros(size(UY));
%           comp = 3;
        end
    elseif strcmp(DATA.model(i_model).type,'FE3D')
        UX = U_model(1:3:end,:);
        UY = U_model(2:3:end,:);
        UZ = U_model(3:3:end,:);
        if in_out.reac_nodale
           F_model = F_vis.u{1}(dof_corres,:);
           FX = F_model(1:3:end,:);
           FY = F_model(2:3:end,:);
           FZ = F_model(3:3:end,:);
        end
%        nodes{1} = zeros(size(model.node,2),3);
%        comp = 6;
    end
    comp     = m.model(i_model).comp;
    nodes{1} = zeros(size(model.node,2),3);    %%% ??????
    
    for i = 1 : size(model.node,2)
        nodes{1}(i,:) = model.node(i).pos;
    end
    
    for i_p2 = 2 : numel(m.params)
        nodes{i_p2} = [m.params(i_p2).mesh ; zeros(size(m.params(i_p2).mesh))]' ;
    end
    
    % BESOIN DE MODIFIER CETTE PARTIE DANS LE CAS OU ON A DIFFERENTS TYPES
    % D'ELEMENTS
    cells = cell(numel(m.params),1);
    
    switch model.elem(2).typel
        case 'cub8'
            type = 8;
        case 'bar2'
            type = 2;
        case 'tri3'
            type = 3;
        case 'qua4'
            type = 4;
        case 'tet4'
            type = 4;
        case 'tri6'
            type = 6;
        otherwise
            warning('not implemented');
            foo;keyboard
    end
    
    cellsss = zeros(size(model.elem,2),type);     % If quad elements
    
    for i = 1 : numel(model.elem)
        cellsss(i,:) = [model.elem(i).node_num];
    end
    
    cells{1} = [cells{1} ; cellsss];
    for i_p = 2 : numel(m.params)
        %cells{i_p} = (1:(size(nodes{i_p},1)))' ;
        cells{i_p} = (1:(size(nodes{i_p},1)-1))' ;
    end
    
    if in_out.reac_nodale
        names = cell(numel(m.params)+1,2);
    else
            names = cell(numel(m.params),2);
    end
    names{1,1} = {'X' 'Y' 'Z'};
    names{1,2} = {'m' 'm' 'm'};
    
    names{end,1} = {'X' 'Y' 'Z'};
    names{end,2} = {'N','N','N'};
    
    if numel(m.params) > 1
        for i_p = 2 : numel(m.params)
            names{i_p,1} = {m.params(i_p).name};
            names{i_p,2} = {m.params(i_p).unit};
        end
    end
    
    if in_out.reac_nodale
        nodes_fields = cell(numel(m.params),6);
    else
        nodes_fields = cell(numel(m.params),3);
    end
    
    nodes_fields_names{1} = 'displacementX';
    nodes_fields{1,1}     = UX';
    nodes_fields_names{2} = 'displacementY';
    nodes_fields{1,2}     = UY';
    nodes_fields_names{3} = 'displacementZ';
    nodes_fields{1,3}     = UZ';
    
    for i_p = 2 : numel(m.params)
        nodes_fields{i_p,1} = U.u{i_p}';
        nodes_fields{i_p,2} = U.u{i_p}';
        nodes_fields{i_p,3} = U.u{i_p}';
    end
    
    if in_out.reac_nodale
        nodes_fields_names{4} = 'FX';
        nodes_fields{1,4}     = FX';
        nodes_fields_names{5} = 'FY';
        nodes_fields{1,5}     = FY';
        nodes_fields_names{6} = 'FZ';
        nodes_fields{1,6}     = FZ';
        for i_p = 2 : numel(m.params)
            nodes_fields{i_p,4} = U.u{i_p}';
            nodes_fields{i_p,5} = U.u{i_p}';
            nodes_fields{i_p,6} = U.u{i_p}';
        end
    end
    
    if ~isfield(m,'Av')
        m = average_elem(DATA,m);
    end

    if i_model == 1 && strcmp(in_out.solver,'LATIN')
        %cell_fields = cell(numel(m.params),14);
        %cell_fields_names = cell(14,1);
        diag = [0,1];
      
        % export of the strain field
        gradU = m.B_g * Ugrad.u{1};
        eps1 = m.Av * gradU;
        eps = eps1(model.elem_dof_list,:);
        
        eps_xx = eps(1:comp:end,:);                % ICI ON EST REPASSE SUR LA NOTATION PARAVIEW (VIA m.AV) XX YY ZZ XY YZ XZ
        eps_yy = eps(2:comp:end,:);
        eps_zz = eps(3:comp:end,:);
        eps_xy = eps(4:comp:end,:);
        eps_yz = eps(5:comp:end,:);
        eps_xz = eps(6:comp:end,:);
        
        diag = [0,1];
        
        cell_fields_names{1} = 'eps_xx';
        cell_fields{1,1} = eps_xx';
        cell_fields_names{2} = 'eps_yy';
        cell_fields{1,2} = eps_yy';
        cell_fields_names{3} = 'eps_zz';
        cell_fields{1,3} = eps_zz';
        cell_fields_names{4} = 'eps_xy';
        cell_fields{1,4} = eps_xy';
        cell_fields_names{5} = 'eps_xz';
        cell_fields{1,5} = eps_xz';
        cell_fields_names{6} = 'eps_yz';
        cell_fields{1,6} = eps_yz';
        
        % export of the stress field
        sig1 = m.Av * sig_vis.U{1} ;
        sig = sig1(model.elem_dof_list,:);
        
        sig_xx = sig(1:comp:end,:);
        sig_yy = sig(2:comp:end,:);
        sig_zz = sig(3:comp:end,:);
        sig_xy = sig(4:comp:end,:);
        sig_yz = sig(5:comp:end,:);
        sig_xz = sig(6:comp:end,:);
        
        cell_fields_names{7} = 'sig_xx';
        cell_fields{1,7} = sig_xx';
        
        cell_fields_names{8} = 'sig_yy';
        cell_fields{1,8} = sig_yy';
        
        cell_fields_names{9} = 'sig_zz';
        cell_fields{1,9} = sig_zz';
        
        cell_fields_names{10} = 'sig_xy';
        cell_fields{1,10} = sig_xy';
        
        cell_fields_names{11} = 'sig_xz';
        cell_fields{1,11} = sig_xz';
        
        cell_fields_names{12} = 'sig_yz';
        cell_fields{1,12} = sig_yz';
        
        for i_p = 2 : numel(m.params)
            av_p = ones(size(U.u{i_p}));
            mat_p = 1/2 * spdiags(av_p,diag,size(U.u{i_p},1)-1,size(U.u{i_p},1) );
            for j_q = 1 : 6
                cell_fields{i_p,j_q} = (mat_p * Ugrad.u{i_p})';
            end
            for j_q = 7 : 12
                cell_fields{i_p,j_q} = (mat_p * sig_vis.U{i_p})';
            end
        end
        
        if strcmp(DATA.model(i_model).behavior.type,'damage') && in_out.plot_damage

            % average per element on fields 1
            Av_U11 = m.Av * dam.u{1};
            Av_U1  = Av_U11(model.elem_dof_list,:);
            % average per element on fields 2 and 3

            cell_fields_names{13} = 'damage';
            cell_fields{1,13} = (Av_U1(1:6:end,:))';
            
            for i_p = 2 : numel(m.params)
                av_p = ones(size(U.u{i_p}));
                mat_p = 1/2 * spdiags(av_p,diag,size(U.u{i_p},1)-1,size(U.u{i_p},1) );
                cell_fields{i_p,13} = (mat_p * dam.u{i_p})';
            end
            
        %else
            %cell_fields = {};
            %cell_fields_names = {};
        end
        
        if isfield(model,'var_E_g')
            if ~isempty(model.var_E_g)
                var_E_g = model.var_E_g;
                cell_fields_names{14} = 'E';
                Av2 = m.Av(1:6:6*numel(model.elem),1:6:48*numel(model.elem));
                Av_E = DATA.model(i_model).material.E .* Av2 * var_E_g' ;
                cell_fields{1,14} = (Av_E)';
                for i_p = 2 : numel(m.params)
                    av_p = ones(size(U.u{i_p}));
                    mat_p = 1/2 * spdiags(av_p,diag,size(U.u{i_p},1)-1,size(U.u{i_p},1) );
                    cell_fields{i_p,14} = (mat_p * ones(size(U.u{i_p},1),1))';
                end
            end
        end
        
    else
        cell_fields = {};
        cell_fields_names = {};
        
    end
        
    % export of the pxdmf files
    filename = [in_out.out_dir filename1];
    output   = writepxdmf(filename, nodes, cells, names ,nodes_fields, cell_fields, nodes_fields_names, cell_fields_names,'from1',1,'verbose',0);
    
    % export of the .xdmf files
    filename3 = [in_out.out_dir filename2];
    command   = ['cp ',filename,' ',filename3];
    system(command);
end

end
