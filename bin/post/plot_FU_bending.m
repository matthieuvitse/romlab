function [F,U] = plot_FU_bending( in_out,DATA,m,s,id)

nT = m.params(2).ndof;
if numel(m.params) > 2
    ne1 = m.params(3).ndof;
    if numel(m.params) > 3
        ne2 = m.params(4).ndof;
    else
        ne2 = 1;
    end
else
    ne1 = 1;
    ne2 = 1;
end

L11 = DATA.model(1).L(1);
L21 = DATA.model(1).L(2);
L31 = DATA.model(1).L(3);

%box_F = [DATA.load(3).box(1,:);DATA.load(3).box(2,:) ;(L31 - L31/DATA.model(1).Ng(3)) L31];
box_F = [DATA.load(1).box(1,:);DATA.load(1).box(2,:) ;(0 ) (0 + L31/DATA.model(1).Ng(3))];
warning off backtrace
warning('the box one is chosen to compute the effort as the box 3 induces big discontinuities in the stress field... ')
warning on backtrace
%box_U = [(L11/2 - L11 / DATA.model(1).Ng(1)) (L11/2 + L11 / DATA.model(1).Ng(1)) ; 0.0 L21 ; 0.0 0.0];
box_U = [(L11/2)  (L11/2 ) ; 0.0 L21 ; 0.0 0.0];
elem_in_box_F = [];
elem_in_box_U = [];

ijk = 0;
for i_model = 1 : 1
    
    for i=1:size(m.model(i_model).elem,2)
        ijk = ijk+1;
        nn1=0;
        nn2=0;
        nd=[];
        tot_node_elem = numel(m.model(i_model).elem(i).node_num);
        for jj=1:numel(m.model(i_model).elem(i).node_num)
            j=m.model(i_model).elem(i).node_num(jj);
            if(is_in_box(m.model(i_model).node(j).pos,box_F))
                nn1=nn1+1;
                %nd=[nd j];
                %elem_in_box = [elem_in_box,ijk];   % at least one node of the element are in the box, then elem is in the box
                %break
            elseif (is_in_box(m.model(i_model).node(j).pos,box_U))
                %nn2=nn2+1;
                elem_in_box_U = [elem_in_box_U,ijk];   % at least one node of the element are in the box, then elem is in the box
                break
            end
        end
        if nn1 == tot_node_elem   % all the nodes of the elements are in the box, then elem is in the box
            elem_in_box_F = [elem_in_box_F,ijk];
        %elseif nn2 == tot_node_elem
        %    elem_in_box_U = [elem_in_box_U,ijk];
        end
    end
end


if ~isfield(m,'Av')
    m = average_elem(DATA,m);
end
sig = s.sigma;
sig_vis = double(ttm(sig,m.Av,1));
sig_vis_zz = zeros(m.elem,nT,ne1,ne2);

for i_model = 1 : 1
    if strcmp(DATA.model(i_model).type,'FE3D')
        comp = 6;
        dS = (L21 / DATA.model(1).Ng(2)) * (L11 / DATA.model(1).Ng(1));
    elseif strcmp(DATA.model(i_model).type,'TRUSS')
        comp = 1;
        dS2 = (L11 / DATA.model(1).Ng(1));
    end
    sig_vis_zz(m.model(i_model).elem_list,:,:,:) = sig_vis(m.model(i_model).elem_dof_list(3):comp:m.model(i_model).elem_dof_list(end),:,:,:);
end


for i_time = 1 : nT
    for i_param1 = 1 : ne1
        for i_param2 = 1 : ne2
            F(1,i_time,i_param1,i_param2) =  dS * sum(sig_vis_zz(elem_in_box_F,i_time,i_param1,i_param2),1);
            %F(:,i_time,i_param1,i_param2) = dS * sig_vis(elem_in_box,i_time,i_param1,i_param2);
        end
    end
end

uu = double(s.U);
U = uu(id*3,:,:,:);

U = -U;
F = -F;


end