function [ F ] = plot_FU_tension( in_out,DATA,m,s,order)
% example: F=plot_FU_tension(in_out,DATA,m,s_linear);

nT = m.params(2).ndof;
if numel(m.params) > 2
    ne1 = m.params(3).ndof;
    if numel(m.params) > 3
        ne2 = m.params(4).ndof;
    else
        ne2 = 1;
    end
else
    ne1 = 1;
    ne2 = 1;
end

L11 = DATA.model(1).L(1);
L21 = DATA.model(1).L(2);
L31 = DATA.model(1).L(3);

box_FU = [(L11 - 2/3 * L11 / DATA.model(1).Ng(1)) L11 ; 0.0 L21 ; 0.0 L31];
elem_in_box = [];
ijk = 0;
for i_model = 1 : numel(m.model)
    
    for i=1:size(m.model(i_model).elem,2)
        ijk = ijk+1;
        for jj=1:numel(m.model(i_model).elem(i).node_num)
            j=m.model(i_model).elem(i).node_num(jj);
            if(is_in_box(m.model(i_model).node(j).pos,box_FU))
                %nn=nn+1;
                %nd=[nd j];
                elem_in_box = [elem_in_box,ijk];   % at least one node of the element are in the box, then elem is in the box
                break
            end
        end
        %if nn == tot_node_elem   % all the nodes of the elements are in the box, then elem is in the box
        %    elem_in_box = [elem_in_box,ijk];
        %end
    end
end

if ~isfield(m,'Av')
    m = average_elem(DATA,m);
end

% sig = s.sigma;
% sig_vis = double(ttm(sig,m.Av,1));
% sig_vis_xx = zeros(m.elem,nT,ne1,ne2);

sig1{1} = s.sigma{1}(:,1:order);
sig1{2} = s.sigma{2}(:,1:order);
sig1{3} = s.sigma{3}(:,1:order);

sig = ktensor(sig1);
sig_vis = double(ttm(sig,m.Av,1));
sig_vis_xx = zeros(m.elem,nT,ne1,ne2);

for i_model = 1 : numel(m.model)
    if strcmp(DATA.model(i_model).type,'FE3D')
        comp = 6;
        dS = (L21 / DATA.model(1).Ng(2)) * (L31 / DATA.model(1).Ng(3));
    elseif strcmp(DATA.model(i_model).type,'TRUSS')
        comp = 1;
        dS2 = (L11 / DATA.model(1).Ng(1));
    end
    sig_vis_xx(m.model(i_model).elem_list,:,:,:) = sig_vis(m.model(i_model).elem_dof_list(1):comp:m.model(i_model).elem_dof_list(end),:,:,:);
end

for i_time = 1 : nT
    for i_param1 = 1 : ne1
        for i_param2 = 1 : ne2
            F(1,i_time,i_param1,i_param2) =  dS * sum(sig_vis_xx(elem_in_box,i_time,i_param1,i_param2),1);
            %F(:,i_time,i_param1,i_param2) = dS * sig_vis(elem_in_box,i_time,i_param1,i_param2);
        end
    end
end

end

