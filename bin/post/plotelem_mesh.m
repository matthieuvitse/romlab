function [add,node]=plotelem_mesh(el,node,nn)
%  J.C. Passieux (2010)

n=[];
e=[];
nnum=[];

for k=1:size(el.node_num,1)
    in=el.node_num(k);
    if(node(in).pnum==0)
        nn=nn+1;
        node(in).pnum=nn;
        n=[n ; node(in).pos'];
        if numel(node(in).pos)==2
            n=[n ; 0];
        end
        nnum=[nnum node(in).num];
    end
    e=[e ; node(in).pnum];
end

add.node=n;
add.elem=e;
add.nnode=nn;
add.nnum=nnum;
