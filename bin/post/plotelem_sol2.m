function [add,node]=plotelem_sol2(s,el,node,nn,U)

u=[];
n=[];
e=[];

for k=1:numel(el.node_num)
    in=el.node_num(k);
    if(node(in).pnum==0)
        nn=nn+1;
        node(in).pnum=nn;
        n=[n ; node(in).pos'];
        if ~strcmp(s.stage,'local')
            u=[u ; U(node(in).dof_corres)];
        end
        if numel(node(in).pos)==2
            n=[n ; 0];
            if ~strcmp(s.stage,'local')
                u=[u ; 0];
            end
        end
    end
    e=[e ; node(in).pnum];
end

add.u=u;
add.node=n;
add.elem=e;
add.nnode=nn;

end