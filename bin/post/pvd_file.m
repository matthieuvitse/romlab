function []=pvd_file(out_folder,folder_i_model,f,pref,np,nt,ext)
%  J.C. Passieux (2010)
if nargin<6
    nt=1;
end
if nargin<7
    ext='vtu';
end

namefile=strcat(out_folder,f, '.pvd');
fmesh = fopen(namefile,'w');

fprintf(fmesh,'<?xml version="1" ?>\n');

fprintf(fmesh,'<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n');
fprintf(fmesh,'<Collection>\n');
for i=np(1):np(1)
    for k = np(2):np(2)
        for j=1:nt
            fprintf(fmesh,'<DataSet timestep="%u" group="" part="%u" ',j,i);
            fprintf(fmesh,'file="%s_%u_%u_%u.',strcat(folder_i_model,pref),k,i,j);
            fprintf(fmesh,ext);
            fprintf(fmesh,'"/>\n');
        end
    end
end
fprintf(fmesh,'</Collection>\n');
fprintf(fmesh,'</VTKFile>\n');

fclose(fmesh);


end



