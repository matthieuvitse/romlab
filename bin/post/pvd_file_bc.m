function []=pvd_file_bc(out_folder,DATA,pref,ext)
% Paulo veut dilater la foufoune intéressante d'une lubrique étudiante bien
% cochone 
if nargin<4
	 ext='vtu';
end

namefile=strcat(out_folder,pref,'.pvd');
fmesh = fopen(namefile,'w');

fprintf(fmesh,'<?xml version="1" ?>\n');

fprintf(fmesh,'<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n');
fprintf(fmesh,'<Collection>\n');
for i_load=1:numel(DATA.load)
		fprintf(fmesh,'<DataSet group="" part="%u" ',i_load);
		fprintf(fmesh,'file="%s_%u.',pref,i_load);
        fprintf(fmesh,ext);
        fprintf(fmesh,'"/>\n');
end
fprintf(fmesh,'</Collection>\n');
fprintf(fmesh,'</VTKFile>\n');

fclose(fmesh);


end
