function [m,U_vis,eps_vis,sig_vis,sig_ps_vis,d_vis,sig_vm_vis,var_E_vis]=viz_fields(in_out,DATA,m,U,eps,sig,d)

% MV : process the stress/strain fields stored at each element for
% visualization of the fields during the iterations

% adapted from J.C. Passieux's Esbroufe's *call_after_solve* function

tic

ne(1) = 1;
ne(2) = 1;
if numel(m.params) == 4
    ne = [m.params(3:end).ndof];
elseif numel(m.params) == 3
    ne(1) = m.params(3).ndof;
end
% construction of the full operator
% elementary matrices

eps_vis     = [];
sig_vis     = [];
sig_ps_vis  = [];    %principal stress
sig_vm_vis  = [];    %Von Mises stress
var_E_vis   = [];

if(in_out.plot_sigeps)
    if ~isfield(m,'Av')
        m = average_elem(DATA,m);
    end
    eps_vis = double(ttm(eps,m.Av,1));   % or Av * double(eps), but won't work for tensors
    sig_vis = double(ttm(sig,m.Av,1));
end

if(in_out.plot_damage) && ~strcmp(in_out.solver,'DIRECT')
    if ~isfield(m,'Av')
        m = average_elem(DATA,m);
    end
    d_vis2=ttm(d,m.Av,1);
    d_temp = double(d_vis2);
    d_vis(m.model(1).elem_list,:,:,:) = d_temp(1:6:(numel(m.model(1).elem)*6),:,:,:);
    if numel(m.model) > 1
        d_vis(m.model(2).elem_list,:,:,:) = d_temp((numel(m.model(1).elem)*6+1):1:end,:,:,:);
    end
else
    d_vis = sptensor([m.elem,m.params(2:end).ndof]);
end

if(in_out.plot_var_E) && ~strcmp(in_out.solver,'DIRECT')
    warning off backtrace
    warning('this is not the general implementation')
    warning on backtrace
    if ~isfield(m,'Av')
        m = average_elem(DATA,m);
    end
    Av2 = m.Av(1:6:6*numel(m.model(1).elem),1:6:48*numel(m.model(1).elem));
    var_E_vis = DATA.model(1).material.E * Av2 * m.model(1).var_E_g';
end

compt_ps = 0;
compt_vm = 0;
for i_model = 1 : numel(m.model)
%     if strcmp(DATA.model(i_model).type,'FE3D')
%         comp    = 6;
%         num_dof = 3;
%     elseif  strcmp(DATA.model(i_model).type,'FE2D')
%         comp    = 3;
%         num_dof = 2;
%     elseif  strcmp(DATA.model(i_model).type,'TRUSS')
%         comp    = 1;
%         num_dof = 1;
%     end
%     
    comp = m.model(i_model).comp;
    num_dof = m.model(i_model).dof_node;
    
    if(in_out.plot_sig_ps)
        % computation of the operator enabling the voigt -> matrix transition
        
        if ~isfield(m.model(i_model),'OP')
            m_i = voigt_to_matrix(DATA.model(i_model),m.model(i_model));
        end
        
        nelem = numel(m.model(i_model).elem);
        
        for i_elem_ps = 1 : nelem
            compt_ps = compt_ps +1;
            for j = 1 : m.params(2).ndof
                for k_p1 = 1 : ne(1)
                    for k_p2 = 1 : ne(2)
                        sig_vec = m_i.OP * double(sig_vis( (compt_ps-1)*comp + 1 : compt_ps * comp , j , k_p1 , k_p2));
                        sig_mat = reshape(sig_vec,num_dof,num_dof);
                        sig_ps_vis((compt_ps-1)*num_dof + 1 : compt_ps * num_dof , j , k_p1 , k_p2)  = sort(eig(sig_mat),'descend');
                    end
                end
            end
        end
        sig_ps_vis = double(sig_ps_vis);
        
    else
        sig_ps_vis = sptensor([num_dof*m.elem,m.params(2:end).ndof]);
    end
    
    if (in_out.plot_sig_vm)
        nelem = numel(m.model(i_model).elem);
        
        for i_elem = 1 : nelem
            compt_vm = compt_vm +1;
            for j = 1 : m.params(2).ndof
                for k_p1 = 1 : ne(1)
                    for k_p2 = 1 : ne(2)
                        sig_elem = sig_vis((i_elem-1)*comp + 1 : (i_elem)*comp ,j,k_p1,k_p2);
                        if comp == 3
                            sig_vm_vis(compt_vm , j , k_p1,k_p2) = sqrt((sig_elem(1))^2 + (sig_elem(2))^2  - sig_elem(1) * sig_elem(2) ...
                                + 3 * sig_elem(3)^2 );
                        elseif comp == 6
                            sig_vm_vis(compt_vm , j , k_p1,k_p2) = sqrt( 1/2 * ( (sig_elem(1)-sig_elem(2))^2 + (sig_elem(2)-sig_elem(3))^2 + (sig_elem(3)-sig_elem(1))^2 ...
                                + 6 * ( sig_elem(4)^2 + sig_elem(5)^2 + sig_elem(6)^2) ) );
                        end
                    end
                end
            end
        end
    else
        sig_vm_vis = sptensor([m.elem,m.params(2:end).ndof]);
    end
end

if size(U) == 0
    U_vis = [];
else
    U_vis = double(U);
end

disp(['    ... post-processing... ' num2str(toc,2) ' s']);

end
