function []=vtk_bc(in_out,DATA)

disp(['    ... exporting boundary conditions... ']);

if ~exist(strcat(in_out.out_dir,'bc','/'),'dir')
    mkdir(strcat(in_out.out_dir,'bc','/'));
end

path_file = [in_out.out_dir,'bc','/','bc','_'];

for i_load = 1 : numel(DATA.load)
    
    offset=0;
    
    nonzeros = find(DATA.load(i_load).box(:,1)-DATA.load(i_load).box(:,2));
    
    switch size(nonzeros,1)
        case 0
            warning('        ... not implemented...')
        case 1
            warning('        ... not implemented...')
            
        case 2
            O = DATA.load(i_load).box(:,1) ;
            L = DATA.load(i_load).box(:,2) ;
            Ng = [1 1];
            [X,Y]=meshgrid(linspace(O(nonzeros(1)),L(nonzeros(1)),(Ng(1)+1)), ...
                linspace(O(nonzeros(2)),L(nonzeros(2)),(Ng(2)+1)));
            
            offset_mesh = .01*ones(size(X(:),1),1);
            
            if strcmp(DATA.load(i_load).type,'sym')
                
                switch DATA.load(i_load).normal
                    case 1
                        n=[O(1)*ones(size(X(:),1),1)  X(:) Y(:)];
                    case 2
                        n=[X(:) O(2)*ones(size(X(:),1),1) Y(:)];
                    case 3
                        n=[X(:) Y(:) O(3)*ones(size(X(:),1),1)];
                end
            elseif strcmp(DATA.load(i_load).type,'displ')
                n=[X(:) Y(:)];
                
            end
            nx=Ng(1);
            ny=Ng(2);
            e=[];
            for i=1:nx
                for j=1:ny
                    p1=(i-1)*(ny+1)+j;
                    p4=(i-1)*(ny+1)+j+1;
                    p2=(i-1)*(ny+1)+j+ny+1;
                    p3=(i-1)*(ny+1)+j+ny+2;
                    e=[e; p1 p2 p3 p4];
                end
            end
            
            t=3*ones(size(e,1),1);
            
        case 3         
            O = DATA.load(i_load).box(:,1);
            L = DATA.load(i_load).box(:,2);
            Ng = [1 1 1];
            
            [Y,Z,X]=meshgrid(linspace(O(2),L(2),(Ng(2)+1)), ...
                linspace(O(3),L(3),(Ng(3)+1)), ...
                linspace(O(1),L(1),(Ng(1)+1)));
            n=[X(:) Y(:) Z(:)];
            
            nx=Ng(1);
            ny=Ng(2);
            nz=Ng(3);
            e=[];
            for i=1:nx
                for j=1:ny
                    for k=1:nz
                        p1=(i-1)*(ny+1)*(nz+1)+(j-1)*(nz+1)+k;
                        p2=(i-1)*(ny+1)*(nz+1)+(j-1)*(nz+1)+k+1;
                        p3=(i-1)*(ny+1)*(nz+1)+  j  *(nz+1)+k;
                        p4=(i-1)*(ny+1)*(nz+1)+  j  *(nz+1)+k+1;
                        p5=  i  *(ny+1)*(nz+1)+(j-1)*(nz+1)+k;
                        p6=  i  *(ny+1)*(nz+1)+(j-1)*(nz+1)+k+1;
                        p7=  i  *(ny+1)*(nz+1)+  j  *(nz+1)+k;
                        p8=  i  *(ny+1)*(nz+1)+  j  *(nz+1)+k+1;
                        e=[e; p1 p5 p6 p2 p3 p7 p8 p4];
                    end
                end
            end
            
            t=5*ones(size(e,1),1);
    end
            
    ne=size(e,1);
    nn=size(n,1);
    bc = class_mesh;
    bc.node(1:nn)=class_node;
    bc.elem(1:ne)=class_elem;
    
    for i_node = 1 : nn
        bc.node(i_node).pos = n(i_node,:);
        bc.node(i_node).num=i;
    end
    
    nelem=0;
    nnode=0;
    connectivity=[];
    offsets=[];
    types=[];
    node=[];
    numelem=[];
    numnode=[];
    
    for i_elem = 1 : ne
        bc.elem(i_elem).num=i_elem;
        ei_list=e(i_elem,:);
        ei_list(find(ei_list==0))=[];
        switch t(i_elem)
            case 5, % QUAD 8 (3D)
                bc.elem(i_elem).typel='cub8';
            case 3,	% QUAD 4 (2D)
                bc.elem(i_elem).typel='qua4';
        end
        pnodei=n(ei_list,:);
        bc.elem(i_elem).node_pos=pnodei;
        bc.elem(i_elem).posc=sum(pnodei,1)/numel(ei_list);
        bc.elem(i_elem).node_num=ei_list';
        
        if isempty(offsets)
            coffs=0;
        else
            coffs=offsets(size(offsets,2));
        end
        [add,bc.node]=plotelem_mesh(bc.elem(i_elem),bc.node,nnode);
        if strcmp(bc.elem(i_elem).typel,'bar2')
            offsets=[offsets coffs+2];
            types=[types 3];
        elseif strcmp(bc.elem(i_elem).typel,'qua4')
            offsets=[offsets coffs+4];
            types=[types 9];
        elseif strcmp(bc.elem(i_elem).typel,'tri3')
            offsets=[offsets coffs+3];
            types=[types 5];
        elseif strcmp(bc.elem(i_elem).typel,'tri6')
            offsets=[offsets coffs+6];
            types=[types 22];
        elseif strcmp(bc.elem(i_elem).typel,'tet4')
            offsets=[offsets coffs+4];
            types=[types 10];
        elseif strcmp(bc.elem(i_elem).typel,'cub8')
            offsets=[offsets coffs+8];
            types=[types 12];
        end
        connectivity=[connectivity ; add.elem];
        nelem=nelem+1;
        numelem=[numelem bc.elem(i).num];
        node=[node ; add.node];
        nnode=add.nnode;
        numnode=[numnode add.nnum];
        
    end
    
    filename=strcat(path_file,num2str(i_load),'.vtu');
    fmesh = fopen(filename,'w');
    
    fprintf(fmesh,'<?xml version="1" ?>\n');
    
    fprintf(fmesh,['<VTKFile type="UnstructuredGrid" version="0.1" byte_order="','">\n']);
    
    fprintf(fmesh,'\t <UnstructuredGrid>\n');
    fprintf(fmesh,'\t\t <Piece NumberOfPoints="%u" NumberOfCells="%u">\n',nnode,nelem);
    
    % POINT DATA
    
    fprintf(fmesh,'\t\t\t <PointData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="ascii">\n');
    fprintf(fmesh,'%u \n',numnode);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </PointData> \n');
    
    % CELL DATA
    
    fprintf(fmesh,'\t\t\t <CellData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="ascii" >\n');
    fprintf(fmesh,'%u \n',numelem);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </CellData> \n');
    
    % POINTS
    
    fprintf(fmesh,'\t\t\t <Points>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" NumberOfComponents="3" format="ascii" >\n');
    fprintf(fmesh,'%f \n',node');
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Points>\n');
    
    % CELLS
    fprintf(fmesh,'\t\t\t <Cells>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="connectivity" format="ascii">\n');
    fprintf(fmesh,'%u \n',connectivity-1);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="offsets" format="ascii">\n');
    fprintf(fmesh,'%u \n',offsets);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int8" Name="types" format="ascii">\n');
    fprintf(fmesh,'%u \n',types);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Cells>\n');
    
    % END VTK FILE
    
    fprintf(fmesh,'\t\t </Piece>\n');
    fprintf(fmesh,'\t </UnstructuredGrid> \n');
    
    fprintf(fmesh,'</VTKFile> \n');
    fclose(fmesh);

end

pvd_file_bc(strcat(in_out.out_dir,'bc/'),DATA,'bc');
disp(['    ... boundary condition export done... ',num2str(toc,2) ' s']);

end