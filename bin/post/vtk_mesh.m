function []=vtk_mesh(in_out,m,num)
disp(['    ... exporting mesh ',num2str(num)]);
binary_output=0;

if ~exist(strcat(in_out.out_dir,'mesh','/'),'dir')
    mkdir(strcat(in_out.out_dir,'mesh','/'));
end

path_file = [in_out.out_dir,'mesh','/','mesh','_'];

tic

nelem=0;
nnode=0;
connectivity=[];
offsets=[];
types=[];
node=[];
numelem=[];
numnode=[];

for i=1:size(m.elem,2)
    if(m.elem(i).plot)
        if isempty(offsets)
            coffs=0;
        else
            coffs=offsets(size(offsets,2));
        end
        [add,m.node]=plotelem_mesh(m.elem(i),m.node,nnode);
        if strcmp(m.elem(i).typel,'bar2')
            offsets=[offsets coffs+2];
            types=[types 3];
        elseif strcmp(m.elem(i).typel,'qua4')
            offsets=[offsets coffs+4];
            types=[types 9];
        elseif strcmp(m.elem(i).typel,'tri3')
            offsets=[offsets coffs+3];
            types=[types 5];
        elseif strcmp(m.elem(i).typel,'tri6')
            offsets=[offsets coffs+6];
            types=[types 22];
        elseif strcmp(m.elem(i).typel,'tet4')
            offsets=[offsets coffs+4];
            types=[types 10];
        elseif strcmp(m.elem(i).typel,'cub8')
            offsets=[offsets coffs+8];
            types=[types 12];
        end
        connectivity=[connectivity ; add.elem];
        nelem=nelem+1;
        numelem=[numelem m.elem(i).num];
        node=[node ; add.node];
        nnode=add.nnode;
        numnode=[numnode add.nnum];
    end
end

offset=0;

filename=strcat(path_file,num2str(num),'.vtu');
fmesh = fopen(filename,'w');

fprintf(fmesh,'<?xml version="1" ?>\n');

fprintf(fmesh,['<VTKFile type="UnstructuredGrid" version="0.1" byte_order="','">\n']);

fprintf(fmesh,'\t <UnstructuredGrid>\n');
fprintf(fmesh,'\t\t <Piece NumberOfPoints="%u" NumberOfCells="%u">\n',nnode,nelem);

if binary_output
    
    % POINT DATA
    fprintf(fmesh,'\t\t\t <PointData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="appended" offset="%u" />\n',offset);
    offset=offset+4+4*numel(numnode);
    fprintf(fmesh,'\t\t\t </PointData> \n');
    
    % CELL DATA
    
    fprintf(fmesh,'\t\t\t <CellData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="appended" offset="%u" />\n',offset);
    offset=offset+4+4*numel(numelem);
    fprintf(fmesh,'\t\t\t </CellData> \n');
    
    % POINTS
    
    fprintf(fmesh,'\t\t\t <Points>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" NumberOfComponents="3" format="appended" offset="%u" />\n',offset);
    offset=offset+4+4*numel(node);
    fprintf(fmesh,'\t\t\t </Points>\n');
    
    % CELLS
    fprintf(fmesh,'\t\t\t <Cells>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="connectivity" format="appended" offset="%u" />\n',offset);
    offset=offset+4+4*numel(connectivity);
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="offsets" format="appended" offset="%u" />\n',offset);
    offset=offset+4+4*numel(offsets);
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int8" Name="types" format="appended" offset="%u" />\n',offset);
    fprintf(fmesh,'\t\t\t </Cells>\n');
    
    % APPENDED DATA
    fprintf(fmesh,'\t\t </Piece>\n');
    fprintf(fmesh,'\t </UnstructuredGrid> \n');
    fprintf(fmesh,'\t <AppendedData encoding="raw"> \n _');
    
    % NUM NODES
    fwrite(fmesh,4*numel(numnode),'uint32');
    fwrite(fmesh,numnode,'uint32');
    
    % NUM ELEMS
    fwrite(fmesh,4*numel(numelem),'uint32');
    fwrite(fmesh,numelem,'uint32');
    
    % NODES
    fwrite(fmesh,4*numel(node),'uint32');
    fwrite(fmesh,node','float32');
    
    % ELEMS
    fwrite(fmesh,4*numel(connectivity),'uint32');
    fwrite(fmesh,connectivity-1,'int32');
    fwrite(fmesh,4*numel(offsets),'uint32');
    fwrite(fmesh,offsets,'int32');
    fwrite(fmesh,numel(types),'uint32');
    fwrite(fmesh,types,'int8');
    
    fprintf(fmesh,'\n%s\n','</AppendedData>');
else
    
    % POINT DATA
    fprintf(fmesh,'\t\t\t <PointData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="ascii">\n');
    fprintf(fmesh,'%u \n',numnode);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </PointData> \n');
    
    % CELL DATA
    fprintf(fmesh,'\t\t\t <CellData scalars="scalar"> \n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="num" NumberOfComponents="1" format="ascii" >\n');
    fprintf(fmesh,'%u \n',numelem);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </CellData> \n');
    
    % POINTS
    fprintf(fmesh,'\t\t\t <Points>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" NumberOfComponents="3" format="ascii" >\n');
    fprintf(fmesh,'%f \n',node');
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Points>\n');
    
    % CELLS
    fprintf(fmesh,'\t\t\t <Cells>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="connectivity" format="ascii">\n');
    fprintf(fmesh,'%u \n',connectivity-1);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="offsets" format="ascii">\n');
    fprintf(fmesh,'%u \n',offsets);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int8" Name="types" format="ascii">\n');
    fprintf(fmesh,'%u \n',types);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Cells>\n');
    
    % END VTK FILE
    fprintf(fmesh,'\t\t </Piece>\n');
    fprintf(fmesh,'\t </UnstructuredGrid> \n');
end
fprintf(fmesh,'</VTKFile> \n');
fclose(fmesh);
disp(['    ... mesh ',num2str(num),' export done... ',num2str(toc,2) ' s']);


end


