function []=vtk_sol2(in_out,DATA,m,s,time,part1,part2,U,eps,sig,sig_ps,d,sig_vm,var_E_g,out_folder)
%  J.C. Passieux (2010)

% MV : function modified from original version so that it only exports
% stress and strain fields (usually used at local stage) stored at each
% elements.

if nargin<3
    time=1;
end

if(in_out.plot_sol)
    
    nelem=0;
    nnode=0;
    connectivity=[];
    offsets=[];
    types=[];
    node=[];
    u=[];
    ps = sig_ps;    % principal stress
    vm = sig_vm;    % Von Mises stress
    
    for i=1:size(m.elem,2)
        if(m.elem(i).plot)
            if isempty(offsets)
                coffs=0;
            else
                coffs=offsets(size(offsets,2));
            end
            [add,m.node]=plotelem_sol2(s,m.elem(i),m.node,nnode,U);
            
            if strcmp(m.elem(i).typel,'bar2')
                offsets=[offsets coffs+2];
                types=[types 3];
%                 if DATA.dim == 3
%                     num_ddl = 3;
%                 elseif DATA.dim == 2
%                   num_ddl= 2;
%                 end
%                 comp = 1;
                num_ps = 1;
            elseif strcmp(m.elem(i).typel,'qua4')
                offsets=[offsets coffs+4];
                types=[types 9];
%                 num_ddl = 3;
%                 comp = 3;
                num_ps = 2;
            elseif strcmp(m.elem(i).typel,'tri3')
                offsets=[offsets coffs+3];
                types=[types 5];
%                 num_ddl = 2;
%                 comp = 3;
                 num_ps = 2;
            elseif strcmp(m.elem(i).typel,'tri6')
                offsets=[offsets coffs+6];
                types=[types 22];
%                 num_ddl = 3;
%                 comp = 3;
                num_ps = 2;
            elseif strcmp(m.elem(i).typel,'tet4')
                offsets=[offsets coffs+4];
                types=[types 10];
%                 num_ddl = 3;
%                 comp = 6;
                num_ps = 3;
            elseif strcmp(m.elem(i).typel,'cub8')
                offsets=[offsets coffs+8];
                types=[types 12];
%                 num_ddl = 3;
%                 comp = 6;
                num_ps = 3;
            else
                foo;keyboard
            end
            
            num_ddl = m.dof_node;
            comp = m.comp;
            
            connectivity=[connectivity ; add.elem];
            nelem=nelem+1;
            u = [u; add.u];
            node=[node ; add.node];
            nnode=add.nnode;
        end
    end
    
    filename = [out_folder,'output_'];
    
    filename=strcat(filename,num2str(part2),'_',num2str(part1),'_',num2str(time),'.vtu');
    fmesh = fopen(filename,'w');
    fprintf(fmesh,'<?xml version="1" ?>\n');
    
    fprintf(fmesh,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
    fprintf(fmesh,'\t <UnstructuredGrid>\n');
    fprintf(fmesh,'\t\t <Piece NumberOfPoints="%u" NumberOfCells="%u">\n',nnode,nelem);
    
    % POINT DATA
    if ~strcmp(s.stage,'local')
        fprintf(fmesh,'\t\t\t <PointData scalars="scalar"> \n');
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="displ" NumberOfComponents="%u" format="ascii">\n',num_ddl);
        fprintf(fmesh,'%e \n',u);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
        fprintf(fmesh,'\t\t\t </PointData> \n');
    end
    
    % CELL DATA
    fprintf(fmesh,'\t\t\t <CellData> \n');
    if(in_out.plot_sigeps)
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="epsilon" NumberOfComponents="%u" format="ascii">\n',comp);
        fprintf(fmesh,'%e \n',eps);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="sigma" NumberOfComponents="%u" format="ascii">\n',comp);
        fprintf(fmesh,'%e \n',sig);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
        
        if(in_out.plot_sig_ps)
            fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="pstress" NumberOfComponents="%u" format="ascii">\n',num_ps);
            fprintf(fmesh,'%e \n',ps);
            fprintf(fmesh,'\t\t\t\t </DataArray>\n');
        end
        
        if(in_out.plot_sig_vm)
            fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="vmstress" NumberOfComponents="%u" format="ascii">\n',1);
            fprintf(fmesh,'%e \n',vm);
            fprintf(fmesh,'\t\t\t\t </DataArray>\n');
        end    
    end
    
    if(in_out.plot_damage) && ~strcmp(in_out.solver,'DIRECT')
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="damage" NumberOfComponents="1" format="ascii">\n');
        fprintf(fmesh,'%e \n',d);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    end
    
    if(in_out.plot_var_E) && ~strcmp(in_out.solver,'DIRECT') && ~isempty(var_E_g)
        fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" Name="var_E" NumberOfComponents="1" format="ascii">\n');
        fprintf(fmesh,'%e \n',var_E_g);
        fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    end
    
    fprintf(fmesh,'\t\t\t </CellData> \n');
    
    % POINTS
    fprintf(fmesh,'\t\t\t <Points>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Float32" NumberOfComponents="3" format="ascii">\n');
    fprintf(fmesh,'%f \n',node');
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Points>\n');
    
    % CELLS
    fprintf(fmesh,'\t\t\t <Cells>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="connectivity" format="ascii">\n');
    fprintf(fmesh,'%u \n',connectivity-1');
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="Int32" Name="offsets" format="ascii">\n');
    fprintf(fmesh,'%u \n',offsets);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t\t <DataArray type="UInt8" Name="types" format="ascii">\n');
    fprintf(fmesh,'%u \n',types);
    fprintf(fmesh,'\t\t\t\t </DataArray>\n');
    fprintf(fmesh,'\t\t\t </Cells>\n');
    
    % END TVK FILE
    
    fprintf(fmesh,'\t\t </Piece>\n');
    fprintf(fmesh,'\t </UnstructuredGrid> \n');
    fprintf(fmesh,'</VTKFile> \n');
    
    fclose(fmesh);
end

end
