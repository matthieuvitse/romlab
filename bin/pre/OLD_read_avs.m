function [n,e,t] = read_avs( FileName )
%  J.C. Passieux (2010)
e=[];
n=[];
pfin = fopen( FileName );
if pfin > 0
    while ~feof(pfin)
        line = fgetl(pfin);
        line=str2num(line);
        nnode=line(1);
        nelem=line(2);
        for i=1:nnode
            line = fgetl(pfin);
            line=str2num(line);
            n=[n ; line(2:4)];      % 2:3 -> 2:4 (3D)   MV
        end
        for i=1:nelem
            line = fgetl(pfin);
            line = strrep(line,'line','');
            line = strrep(line,'tri','');
            line = strrep(line,'quad','');
            line = strrep(line,'hex','');   % ADDED MV
            line=str2num(line);
            line([1:2])=[];
            if size(line,2) == 2
                e = [e ; line 0 0 ];
                t(i) = 1;
            elseif(size(line,2)==3)             % Cf read_mesh MV in "remarks"
                e=[ e ; line 0];
                t(i)=2;
            elseif(size(line,2)==4)
                e=[ e ; line];
                t(i)=3;
            elseif(size(line,2)==8)         % ADDED MV
                e = [e ; line];             % ADDED MV
                t(i) = 5;                   % ADDED MV
            end
        end
    end
    fclose(pfin);
end

end
