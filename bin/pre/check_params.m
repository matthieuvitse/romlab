function [in_out] = check_params(in_out,DATA)

warning off backtrace

% disable update when params>2
if numel(DATA.params) > 2 && in_out.update_sd  && 1 == 0
    in_out.update_sd = 0;
    warning('update of the search direction not implemented for params>2 - disabled')
end

if ~strcmp(in_out.solver,'LATIN')
    in_out.update_sd = 0;
    warning('update of the search direction set to 0')
end

% Check enough parameters are entered (at least 2)
if numel(DATA.params) < 2
    warning('not enough parameters entered. See data file')
    keyboard
end

if strcmp(~in_out.solver,'LATIN')
    in_out.relax = 1;
end

% Check if damage model is entered 
test_dam = zeros(numel(DATA.model),1);
for i_model = 1 : numel(DATA.model)
    if strcmp(DATA.model(i_model).behavior.type,'damage')
        test_dam(i_model) = 1;
    end
end

if ~isempty(find(test_dam,1))
    in_out.dam = 1;
else
    in_out.dam = 0;
end

if ~in_out.dam
    if in_out.plot_damage == 1
        in_out.plot_damage = 0;
        disp(' ')
        warning('plot_damage changed from 1 to 0');
    end
    if in_out.plot_var_E
        in_out.plot_var_E = 0;
    end
    if in_out.regularization > 0
        in_out.regularization = 0;
        disp(' ')
        warning('regularization set to 0');
    end
elseif in_out.dam
    if in_out.plot_damage == 0
        in_out.plot_damage = 1;
        disp(' ')
        warning('plot_damage set to 1');
    end
    if in_out.regularization < 0
        in_out.regularization = 1;
        disp(' ')
        warning('default regularization (damage-dalay) set to 1');
    end
end

% Check if PGD is required
if ~in_out.pgd
    in_out.orthog = 0;
    in_out.update_pgd = 0;
    in_out.exp_pxdmf = 0;
    disp(' ')
    warning('no PGD - orthogonalization set to 0')
    warning('no PGD - PXDMF export set to 0');
    warning('no PGD - update stage set to 0');
end
    
% Check visualization stuff
if (in_out.plot_sig_vm || in_out.plot_sig_ps) && ~in_out.plot_sigeps
    in_out.plot_sigeps = 1;
    disp(' ')
    warning('plot_sigeps set to 1');
end
warning on backtrace
end