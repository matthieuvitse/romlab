function DATA = init_data(DATA)
%  J.C. Passieux (2010)

for i_model = 1 : numel(DATA.model)
    
    %Lame's coefficients
    DATAmat = DATA.model(i_model).material;
    DATAmat.mu     =  DATAmat.E / (2*(1 + DATAmat.nu));
    DATAmat.lambda =  DATAmat.nu * DATAmat.E / ...
        ((1+DATAmat.nu)*(1-2*DATAmat.nu));
    DATA.model(i_model).material = DATAmat;
end

% CALC HOOKE MATRIX
DATA = update_hooke(DATA);

end
