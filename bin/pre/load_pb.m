function DATA= load_pb(pb)
%  J.C. Passieux (2010)
tic

dir=['data/data/' pb ];
path(path,dir);
DATA = eval(pb);

disp(['ROMlab: Test loaded... ' num2str(toc,2) ' s']);
