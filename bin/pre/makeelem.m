function [e] = makeelem(DATA)

n = DATA.Ng;

if strcmp(DATA.type,'FE3D')
	nx=n(1);
	ny=n(2);
	nz=n(3);
    e=[];
    for i=1:nx
        for j=1:ny
            for k=1:nz
                p1=(i-1)*(ny+1)*(nz+1)+(j-1)*(nz+1)+k;
                p2=(i-1)*(ny+1)*(nz+1)+(j-1)*(nz+1)+k+1;
                p3=(i-1)*(ny+1)*(nz+1)+  j  *(nz+1)+k;
                p4=(i-1)*(ny+1)*(nz+1)+  j  *(nz+1)+k+1;
                p5=  i  *(ny+1)*(nz+1)+(j-1)*(nz+1)+k;
                p6=  i  *(ny+1)*(nz+1)+(j-1)*(nz+1)+k+1;
                p7=  i  *(ny+1)*(nz+1)+  j  *(nz+1)+k;
                p8=  i  *(ny+1)*(nz+1)+  j  *(nz+1)+k+1;
                e=[e; p1 p5 p6 p2 p3 p7 p8 p4];
            end
        end
    end
elseif strcmp(DATA.type,'FE2D')
	nx=n(1);
	ny=n(2);
    e=[];
    for i=1:nx
        for j=1:ny
            p1=(i-1)*(ny+1)+j;
            p4=(i-1)*(ny+1)+j+1;
            p2=(i-1)*(ny+1)+j+ny+1;
            p3=(i-1)*(ny+1)+j+ny+2;
            e=[e; p1 p2 p3 p4];
        end
    end
end
	
