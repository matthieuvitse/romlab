function [m,nonmatch] = map_nodes(DATA,m,m1,m2)


% if DATA.dim == 2 && m1 == m2
%     num = 2;
%     n1 = reshape([m.model(m1).node(:).pos],num,numel(m.model(m1).node))';
%     n2 = reshape([m.model(m2).node(:).pos],num,numel(m.model(m2).node))';
% elseif DATA.dim == 2 && m1 ~= m2
%     num = 2;
%     n1 = reshape([m.model(m1).node(:).pos],num,numel(m.model(m1).node))';
%     A = [m.model(m2).node(:).pos];
%     A(3:3:end) = [];
%     keyboard
%     n2 = reshape(A,num,numel(m.model(m2).node))';
% elseif DATA.dim == 3
    num = 3;
    n1 = reshape([m.model(m1).node(:).pos],num,numel(m.model(m1).node))';
    n2 = reshape([m.model(m2).node(:).pos],num,numel(m.model(m2).node))';
% end



eps = 1e12;

[~,nodes12] = ismember(round(eps*n2)/eps,round(eps*n1)/eps,'rows');
nonmatch = find(nodes12==0);

if ~isempty(nonmatch) && strcmp(DATA.assembly,'reinforcement')
    warning('meshes do not match perfectly!')  
end

for i = 1 : numel(nodes12)
    m.model(m2).node(i).corres_model1 = nodes12(i);
end

m.model(m2).to_master = nodes12;     % match of model 1 on model 2;

% vectorisation de cette boucle ? m.model(1).node(:).corres_model1 = B

% + checker si on veut des voisins (et pas noeuds exacts)

end