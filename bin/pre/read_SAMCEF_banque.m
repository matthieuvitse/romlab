function m=read_SAMCEF_banque(DATA)
%  A. COURARD (2014)

tic

	idx = strfind(DATA.banque, '.');
	ext=DATA.banque((idx+1):size(DATA.banque,2));
	if min(ext=='dat')
		% USING AVS MESHES with extension *.AVS
		[n,e,t] = read_SAMCEF_mesh( [DATA.banque] );
    else
		error('Invalid data file');
	end



% NODES AND ELEMENT GENERATION
ne=size(e,1);
nn=size(n,1);

m=class_mesh;

%NODES
m.node(1:nn)=class_node;
for i=1:nn
    m.node(i).pos=n(i,:);
    m.node(i).num=i;
end


sizeh=0;
%ELEMENTS

if DATA.dim==3
etet4=elem_tet4;
equa8=elem_qua8;
else
equa4=elem_qua4;
etri3=elem_tri3;
etri6=elem_tri6;
end


m.elem(1:ne)=class_elem;
for i=1:ne
	m.elem(i).num=i;
	ei_list=e(i,:);
	ei_list(find(ei_list==0))=[];
	switch t(i)
	case 5, % QUAD 8 (3D)
		m.elem(i).typel=equa8.typel;
		m.elem(i).node_posref=equa8.node_posref;
		m.elem(i).N=equa8.N;
		m.elem(i).dN_xi=equa8.dN_xi;
		m.elem(i).dN_eta=equa8.dN_eta;
        m.elem(i).dN_zeta=equa8.dN_zeta;
	case 4,	% TETRA 4 (3D)
		m.elem(i).typel=etet4.typel;
		m.elem(i).node_posref=etet4.node_posref;
		m.elem(i).N=etet4.N;
		m.elem(i).dN_xi=etet4.dN_xi;
		m.elem(i).dN_eta=etet4.dN_eta;
   		m.elem(i).dN_zeta=etet4.dN_zeta;
    case 3,	% QUAD 4 (2D)
		m.elem(i).typel=equa4.typel;
		m.elem(i).node_posref=equa4.node_posref;
		m.elem(i).N=equa4.N;
		m.elem(i).dN_xi=equa4.dN_xi;
		m.elem(i).dN_eta=equa4.dN_eta;
    case 2, % TRI 3 (2D)
		m.elem(i).typel=etri3.typel;
		m.elem(i).node_posref=etri3.node_posref;
		m.elem(i).N=etri3.N;
		m.elem(i).dN_xi=etri3.dN_xi;
		m.elem(i).dN_eta=etri3.dN_eta;
    case 9, % TRI 6 (2D)
    m.elem(i).typel=etri6.typel;
    m.elem(i).node_posref=etri6.node_posref;
    m.elem(i).N=etri6.N;
    m.elem(i).dN_xi=etri6.dN_xi;
    m.elem(i).dN_eta=etri6.dN_eta;
    otherwise, 
        keyboard
        error('Type of element not recognised');
	end
	pnodei=n(ei_list,:);
	m.elem(i).node_pos=pnodei;
	m.elem(i).posc=sum(pnodei,1)/numel(ei_list);
	m.elem(i).node_num=ei_list';
	dist=pnodei-(ones(size(pnodei))*diag(m.elem(i).posc));
	dist=sum(dist.^2,2);
	m.elem(i).h=2*sum(dist)/numel(ei_list);
	sizeh=sizeh+m.elem(i).h;
end
m.h=sizeh/ne;

disp(['ROMlab: Mesh Loaded... ' num2str(toc,2) ' s']);
