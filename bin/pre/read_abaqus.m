function [n,e,type] = read_abaqus(FileName,dim)


pfin = fopen( FileName );
if pfin > 0
    while pfin>0
        line = fgetl(pfin);
        if strcmp(line,'*Node')
            break;
        end
    end
    
    % NODES
    while pfin>0
        line = fgetl(pfin);
        if ~strcmp(line(1),'*')
            line=str2num(line);
            n(line(1),1:dim)=line(2:dim+1);
        else
            break;
        end
    end
    
    % ELEMENTS
    while pfin>0  % elem of type i
        if strcmp(line(1:8),'*Element')
            els=[];
            typel=line(16:end);
            while pfin>0  % jth elem of type i
                line = fgetl(pfin);
                if ~strcmp(line(1),'*')
                    line=str2num(line);
                    e(line(1),:)=line(2:end);
                    els=[els line(1)];
                else
                    break;
                end
            end
            switch typel
                case 'C3D4'
                    type(els)=4*ones(size(els));
                case {'C3D8','C3D8R'}
                    type(els)=5*ones(size(els));
                otherwise
                    error([typel,': unknown Abaqus element type.']);
            end
            
        else
            break;
        end
    end  
  	fclose(pfin);
end
