function [n,e,type] = read_gmsh_tri6(FileName,dim)

if dim==2
    elts=[9 10];
    maxnd=6;
elseif dim==3
    elts=[4 5];
    maxnd=8;
end

pfin = fopen( FileName );
if pfin > 0
	line = fgetl(pfin);
	line = fgetl(pfin);
	line = fgetl(pfin);
	line = fgetl(pfin);
	line = fgetl(pfin);
	line=str2num(line);
	nnode=line(1);
    n=zeros(nnode,dim);
	for i=1:nnode
		line = fgetl(pfin);
		line=str2num(line);
		n(i,1:dim)=line(2:dim+1);
	end
	line = fgetl(pfin);
	line = fgetl(pfin);
	line = fgetl(pfin);
	line=str2num(line);
	nelem=line(1);
    e=zeros(nelem,maxnd);
    nel=1;
	for i=1:nelem
        line = fgetl(pfin);
		line = str2num(line);
        if max(line(2)==elts)
            if dim==3
                rep=6:numel(line);
            else
                rep=6:numel(line);
            end
            e(nel,1:numel(rep))=line(rep);
            type(nel)=line(2);
            nel=nel+1;
		end
    end
    e=e(1:(nel-1),:);
	fclose(pfin);
end