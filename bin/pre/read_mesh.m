function m=read_mesh(DATA)

disp('ROMlab: Loading mesh(es)...');
for i_model = 1 : numel(DATA.model)
    tic
    DATA_model_i = DATA.model(i_model);
    % MESH GENERATION
    if ~isfield(DATA.model(i_model),'mesh')||(isempty(DATA.model(i_model).mesh))
        
        if  strcmp(DATA_model_i.type,'TRUSS')
            warning('not implemented')
            foo;keyboard;
        elseif strcmp(DATA_model_i.type,'FE2D')
            % STD QUA4 HOME MADE MESHER
            [X,Y]=meshgrid(linspace(DATA_model_i.O(1),DATA_model_i.O(1) + DATA_model_i.L(1),(DATA_model_i.Ng(1)+1)), ...
                linspace(DATA_model_i.O(2),+DATA_model_i.O(2) + DATA_model_i.L(2),(DATA_model_i.Ng(2)+1)));
            n=[X(:) Y(:)];
            e=makeelem(DATA_model_i);
            t=3*ones(size(e,1),1);
        elseif strcmp(DATA_model_i.type,'FE3D')
            % STD QUA8 HOME MADE MESHER
            [Y,Z,X]=meshgrid(linspace(DATA_model_i.O(2),DATA_model_i.O(2) + DATA_model_i.L(2),(DATA_model_i.Ng(2)+1)), ...
                linspace(DATA_model_i.O(3),DATA_model_i.O(3) + DATA_model_i.L(3),(DATA_model_i.Ng(3)+1)), ...
                linspace(DATA_model_i.O(1),DATA_model_i.O(1) + DATA_model_i.L(1),(DATA_model_i.Ng(1)+1)));
            n=[X(:) Y(:) Z(:)];
            e=makeelem(DATA_model_i);
            t=5*ones(size(e,1),1);
            % disp('WARNING');
            %         ebis=[];
            %         for i=1:size(e,1)
            %             ei_list=e(i,:);
            %             pnodei=n(ei_list,:);
            %             posc=sum(pnodei,1)/numel(ei_list);
            %             if ~is_in_box(posc,[0 200 ; 2.5 37.5 ; 2.5 37.5])
            %                 ebis=[ebis ; ei_list];
            %             end
            %         end
            %         e=ebis;
        else
            error('DATA.dim undefined or different from {2;3}!');
        end
    else
        idx = strfind(DATA_model_i.mesh, '.');
        ext=DATA_model_i.mesh((idx+1):size(DATA_model_i.mesh,2));
        if min(ext=='avs')
            % USING AVS MESHES with extension *.AVS
            [n,e,t] = read_avs( [DATA_model_i.mesh] );                % [nodes, elements, type of elements]
        elseif min(ext=='msh')
            % USING GMSH MESHES with extension *.MSH
            if strcmp(DATA_model_i.type,'BAR')
                dim_msh = 2;
            elseif strcmp(DATA_model_i.type,'TRUSS')
                dim_msh = 3;
            elseif strcmp(DATA_model_i.type,'FE2D')
                dim_msh = 2;
            end
            [n,e,t] = read_gmsh( [DATA_model_i.mesh] ,dim_msh);
        elseif min(ext=='inp')
            % USING ABAQUS MESHES with extension *.INP
            [n,e,t] = read_abaqus( [DATA_model_i.mesh] ,DATA.dim);
        else
            disp('??? The possible mesh types are AVS (*.avs), GMSH (*.msh) or ABAQUS (*.inp');
            disp('??? You can also input directly the mesh in ./pre/read_mesh.m ');
            disp('??? - n: position of each nodes [x1, y1 ; x2, y2...]');
            disp('??? - e: the nodes of each elements [n1e1 n2e1 n3e1 n4e1... ]');
            disp('??? provided for instance by the library Mesh2d.');
            error('unknown mesh type!');
        end
    end
    
    %remarks :
    % - t is the flag of the element
    %        t = 1: BAR2
    %        t = 2: TRI3
    %        t = 3: QUAD4
    %        t = 4: TETRA4
    %        t = 5: QUAD8
    % - n is the matrix of the position of the nodes
    % - e is the matrix of the nodes associated to the elements
    
    % NODES AND ELEMENT GENERATION
    ne=size(e,1);
    nn=size(n,1);
    
    m_model_i=class_mesh;
    
    %NODES
    m_model_i.node(1:nn)=class_node;
    
    for i=1:nn
        m_model_i.node(i).pos=n(i,:);
        m_model_i.node(i).num=i;
    end
    
    sizeh=0;
    %ELEMENTS
    if strcmp(DATA.model(i_model).type,'FE3D')
        etet4=elem_tet4;
        equa8=elem_cub8;
        comp = 6;
        dim  = 3;
        dof_node = 3;
        section = 1;
    elseif strcmp(DATA.model(i_model).type,'FE2D')
        equa4=elem_qua4;
        etri3=elem_tri3;
        comp = 3;
        dim  = 2;
        dof_node = 2;
        section = 1;
    elseif strcmp(DATA.model(i_model).type,'TRUSS')
        ebar2=elem_bar2;
        comp = 1;
         if DATA.dim == 3
            dim = 3;
            dof_node = 3;     %% ??
        elseif DATA.dim == 2
            dim = 2;
            dof_node = 2;     %% ??
         end
        section = DATA.model(i_model).section;
    end
    
    m_model_i.elem(1:ne)=class_elem;
    for i=1:ne
        m_model_i.elem(i).num=i;
        ei_list=e(i,:);
        ei_list(find(ei_list==0))=[];
        switch t(i)
            case 5, % QUAD 8 (3D)
                m_model_i.elem(i).typel=equa8.typel;
                m_model_i.elem(i).node_posref=equa8.node_posref;
                m_model_i.elem(i).N=equa8.N;
                m_model_i.elem(i).dN_xi=equa8.dN_xi;
                m_model_i.elem(i).dN_eta=equa8.dN_eta;
                m_model_i.elem(i).dN_zeta=equa8.dN_zeta;
            case 4,	% TETRA 4 (3D)
                m_model_i.elem(i).typel=etet4.typel;
                m_model_i.elem(i).node_posref=etet4.node_posref;
                m_model_i.elem(i).N=etet4.N;
                m_model_i.elem(i).dN_xi=etet4.dN_xi;
                m_model_i.elem(i).dN_eta=etet4.dN_eta;
                m_model_i.elem(i).dN_zeta=etet4.dN_zeta;
            case 3,	% QUAD 4 (2D)
                m_model_i.elem(i).typel=equa4.typel;
                m_model_i.elem(i).node_posref=equa4.node_posref;
                m_model_i.elem(i).N=equa4.N;
                m_model_i.elem(i).dN_xi=equa4.dN_xi;
                m_model_i.elem(i).dN_eta=equa4.dN_eta;
            case 2, % TRI 3 (2D)
                m_model_i.elem(i).typel=etri3.typel;
                m_model_i.elem(i).node_posref=etri3.node_posref;
                m_model_i.elem(i).N=etri3.N;
                m_model_i.elem(i).dN_xi=etri3.dN_xi;
                m_model_i.elem(i).dN_eta=etri3.dN_eta;
            case 1, % BAR 2 (1D)
                m_model_i.elem(i).typel=ebar2.typel;
                m_model_i.elem(i).node_posref=ebar2.node_posref;
                m_model_i.elem(i).N=ebar2.N;
                m_model_i.elem(i).dN_xi=ebar2.dN_xi;
            otherwise,
                keyboard
                error('Type of element not recognized');
        end
        pnodei=n(ei_list,:);
        m_model_i.elem(i).node_pos=pnodei;
        m_model_i.elem(i).posc=sum(pnodei,1)/numel(ei_list);
        m_model_i.elem(i).node_num=ei_list';
        dist=pnodei-(ones(size(pnodei))*diag(m_model_i.elem(i).posc));
        dist=sum(dist.^2,2);
        m_model_i.elem(i).h=2*sum(dist)/numel(ei_list);
        sizeh=sizeh+m_model_i.elem(i).h;
        
        for j=1:length(m_model_i.elem(i).node_num)
            m_model_i.node(m_model_i.elem(i).node_num(j)).elem=[m_model_i.node(m_model_i.elem(i).node_num(j)).elem, i, j];  % elements ou est pr�sent ce noeud
        end
    end
    m_model_i.h        = sizeh/ne; 
    m_model_i.comp     = comp;
    m_model_i.dim      = dim;
    m_model_i.dof_node = dof_node;
    m_model_i.section  = section;
    
    m.model(i_model) = m_model_i;
  
    disp(['    ... spatial mesh ',num2str(i_model),' loaded... ' num2str(toc,2) ' s']);

end
