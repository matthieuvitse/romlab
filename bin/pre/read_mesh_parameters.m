function mpara=read_mesh_parameters(DATA)
%  J.C. Passieux (2010)

tic
alpha=DATA.alpha;
beta=DATA.beta;

% MESH GENERATION
% STD QUA4 HOME MADE MESHER
[X,Y]=meshgrid(linspace(alpha(1),alpha(length(alpha)),(length(alpha))),linspace(beta(1),beta((length(beta))),(length(beta))));
n=[X(:) Y(:)];
e=makeelem([length(alpha)-1,length(beta)-1]);
t=3*ones(size(e,1),1);


% NODES AND ELEMENT GENERATION
ne=size(e,1);
nn=size(n,1);

mpara=class_mesh;

%NODES
mpara.node(1:nn)=class_node;
for i=1:nn
    mpara.node(i).pos=n(i,:);
    mpara.node(i).num=i;
end


sizeh=0;
%ELEMENTS
equa4=elem_qua4;


mpara.elem(1:ne)=class_elem;
for i=1:ne
	mpara.elem(i).num=i;
	ei_list=e(i,:);
	ei_list(find(ei_list==0))=[];
	switch t(i)
	
    case 3,	% QUAD 4 (2D)
		mpara.elem(i).typel=equa4.typel;
		mpara.elem(i).node_posref=equa4.node_posref;
		mpara.elem(i).N=equa4.N;
		mpara.elem(i).dN_xi=equa4.dN_xi;
		mpara.elem(i).dN_eta=equa4.dN_eta;
    
    otherwise, 
        keyboard
        error('Type of element not recognised');
	end
	pnodei=n(ei_list,:);
	mpara.elem(i).node_pos=pnodei;
	mpara.elem(i).posc=sum(pnodei,1)/numel(ei_list);
	mpara.elem(i).node_num=ei_list';
	dist=pnodei-(ones(size(pnodei))*diag(mpara.elem(i).posc));
	dist=sum(dist.^2,2);
	mpara.elem(i).h=2*sum(dist)/numel(ei_list);
	sizeh=sizeh+mpara.elem(i).h;
    
    %�l�ments o� ce noeud est pr�sent
    for j=1:length(mpara.elem(i).node_num)
    	mpara.node(mpara.elem(i).node_num(j)).elem=[mpara.node(mpara.elem(i).node_num(j)).elem, i, j];  
    end
end
mpara.h=sizeh/ne;

disp(['ROMlab: Mesh Loaded... ' num2str(toc,2) ' s']);
