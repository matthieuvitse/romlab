function [m,s_local,s_linear]=solver_LATIN(in_out,DATA,m)

disp('ROMlab: Solving nonlinear problem using LATIN...');
tic

max_iter     = in_out.max_iter;
indic_target = in_out.indic_target;
m.relax      = in_out.relax;

%[k,~,I_g,B_g,~,~] = computed_coupled_operators(m);

% global operators grad, int, hooke
I_g = m.I_g;
B_g = m.B_g;
ddlu=m.dofuu;

if in_out.debug == 1
    m.printitn_cp_als = 1;
else
    m.printitn_cp_als = 0;
end
m.tol_cp_als = 1e-3;

ndofparams = zeros(1,numel(m.params));
for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
end
m.ndofparams = ndofparams;
if length(m.params) < 3
    m.ndofparams(3) = 1;
end

k = m.k;
% compute conditioning number of k
disp(['    ... condition number = ' num2str(condest(k(ddlu,ddlu)))])

% compute LATIN operators
[m] = compute_operators_latin(in_out,DATA,m);
Hdo_inv = m.Hdo_inv;

% initialization of the method
% ------------------------------------- %
if in_out.MPS
    % multiparametric strategy - we load and trucate and update an
    % precomputed ROB
    [m,s_init,R0] = load_solution_MPS(in_out,DATA,m);
else
    [m,s_init] = init_solution(in_out,DATA,m);
end
s_linear       = s_init;
s_linear.stage = 'linear';

localdir = 'init';
m = export_vtk(in_out,DATA,m,s_linear,0,localdir);

iter            = 0;
nb_pgd_modes    = 0;

indic_latin0     = [1];
indic_latin      = [1];
indic_latin2     = [1];
%l2_ind          = [1];

while indic_latin(end) > indic_target  && iter < max_iter
    
    %U_old = s_linear.U;
    tstart = tic;
    iter = iter + 1;
    disp(' ');
    disp(['    ... LATIN iteration: ',num2str(iter)]);
    
    % local stage %
    % ------------------------------------- %
    disp('        ... local stage...')
    
    clear s_local
    warning off backtrace
    warning('local variables cleared')
    warning on backtrace
    
    if 1 == 1
        [s_local,hooke_dg] = local_stage(in_out,DATA,m,s_linear,iter);
    else
        s_local = local_stage_elasticity(DATA,m,s_linear,Hup);
    end
    
    % update of the search direction and rigidity operator ? %
    % ------------------------------------- %
    % reconstruction of the damaged hooke_g operator ?
    if iter == 1 && ~in_out.update_sd
        HHdo_inv    = B_g' * I_g * Hdo_inv * B_g;
        HHdo_invuu  = HHdo_inv(ddlu,ddlu);
        % LU decomposition of H
        [m.HHdo_inv_L,m.HHdo_inv_U] = lu(HHdo_invuu);
    elseif in_out.update_sd
        disp('        ... updating search direction...')
        Hdo_inv     = search_direction(m,hooke_dg,'update');
        U           = ttm(Hdo_inv,B_g' * I_g ,1);
        HHdo_inv    = double(ttm(U,B_g',2));
        HHdo_invuu  = HHdo_inv(ddlu,ddlu);
    end
    
    % linear stage %
    % ------------------------------------- %
    disp('        ... linear stage...')
    
    % resolution of K * Delta_U_latin = Q
    if in_out.pgd == 1
        [s_linear,nb_pgd_modes] = linear_pgd(in_out,DATA,m,nb_pgd_modes,s_linear,HHdo_inv,s_local,indic_latin);
    else
        [s_linear] = linear_increm(m,iter,s_linear,HHdo_inv,HHdo_invuu,s_local);
    end
    
    % Error estimators
    % ------------------------------------- %
%     s_local = rmfield(s_local,'sigma');
%     warning off backtrace
%     warning('variable hat_sigma cleared');
%     warning on backtrace
    
%     indic_latin0(end+1) = compute_indicator(m,s_linear,s_local,1);
%     disp(['        ... error indicator......... ',num2str(indic_latin0(end))]);
    indic_latin(end+1) = compute_indicator(m,s_linear,s_local,3);
    disp(['        ... error indicator......... ',num2str(indic_latin(end))]);
%     indic_latin2(end+1) = compute_indicator(m,s_linear,s_local,5);
%     disp(['        ... error indicator......... ',num2str(indic_latin2(end))]);
    
    %l2_ind(end+1) = compute_L2_indicator(m,(U_old),s_linear.U );
    %disp(['        ... L2 indicator......... ',num2str(l2_ind(end))]);
    
    if in_out.debug == 1
        store_quantities_iter(in_out,DATA,m,s_local,s_linear,iter);
    end
    
%     diary(strcat(in_out.out_dir,'log'));        % update log
    t_elapsed = toc(tstart);
    disp(['        --> time for iteration ',num2str(iter),'... ',num2str(t_elapsed,2),' s']);
end

disp(['    ... resolution done... ',num2str(toc,2),' s']);

% ------------------------------------------------------------------------
% export of the output
%     Rq : quite heavy for now as everything is saved.
format shortg
c = clock;
name_out = strcat(in_out.out_dir,'result_',num2str(c(1)),'_',num2str(c(2)),'_',num2str(c(3)),'.mat');
disp('ROMlab: Saving solution... ');
save(name_out,'in_out','DATA','m','s_local','s_linear','-v7.3');
%
%
if 1 == 1
    name_damage = strcat(in_out.out_dir,'damage.mat');
    damage = s_local.d;
    save(name_damage,'damage','-v7.3');
    name_varE = strcat(in_out.out_dir,'var_E_g.mat');
    var_E = m.model(1).var_E_g;
    save(name_varE,'var_E','-v7.3');
end
%
%
save(strcat(in_out.out_dir,'indicator.mat'),'indic_latin');
disp('    ... solution saved');
% ------------------------------------------------------------------------
m.u = s_linear.U;
figure;semilogy(indic_latin0,'b');hold on;semilogy(indic_latin,'g'); semilogy(indic_latin2,'r') ;legend('error indicator1','error indicator5','error indicator7' )
% ------------------------------------------------------------------------
% export PXDMF

if in_out.exp_pxdmf == 1
    if 1 == 0
        disp('        ... HOSVD decomposition of sigma...')
        t_decomp = tic;
        if size(s_linear.U{1}) > 10
            dec = 10;
        else
            dec = size(s_linear.U{1},2);
        end
        sigma    = redistribute(cp_als(tensor(s_linear.sigma),dec,'printitn',1,'tol',m.tol_cp_als),m.param_redis);
        %m.printitn_cp_als
        s_linear.sigma = sigma;
        t_elapsed_decomp = toc(t_decomp);
        disp(['        ... HOSVD of sigma done... ',num2str(t_elapsed_decomp),' s'])
    end
    if in_out.plot_damage
        s_linear.d = s_local.d;
    end
    in_out.reac_nodale=1;
    if in_out.reac_nodale == 1 
        K= m.model(1).k;
        F=s_linear.U;
        F.u{1} = K*F.u{1};
        s_linear.F = F;
       % [output] = export_pxdmf(in_out,DATA,m,s_lin2);
    end
    [output] = export_pxdmf(in_out,DATA,m,s_linear);
end

 
 
 
% ------------------------------------------------------------------------
% vtu export of the displacement, strain, stresses, VM stress and damage
if in_out.exp_pv==1
    s_linear.d = s_local.d;
    m = export_vtk(in_out,DATA,m,s_linear,1,in_out.exp_pv_name,[]);
end
% ------------------------------------------------------------------------

end
