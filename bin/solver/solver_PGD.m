function [m,s]=solver_PGD(in_out,DATA,m)

warning off backtrace
disp('ROMlab: Solving problem using PGD...');
tic

max_iter      = in_out.max_iter;     % max number of PGD iterations
indic_target  = in_out.indic_target; % target indicator
m.relax       = in_out.relax;
param_redis = m.param_redis;

if in_out.debug == 1
    m.printitn_cp_als = 1;
else
    m.printitn_cp_als = 0;
end
m.tol_cp_als = 1e-3;

ndofparams = zeros(1,numel(m.params));
for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
end
m.ndofparams = ndofparams;
if length(m.params)<3
    m.ndofparams(3) = 1;
end

ddlu=m.dofuu;
disp(['    ... condition number = ' num2str(condest(m.k(ddlu,ddlu)))])

% global operators grad, int, hooke
hooke_g = m.hooke_g;
B_g = m.B_g;

[m.k_L,m.k_U] = lu(m.k(ddlu,ddlu));

% ------------------------------------- %
% initialization of the method
% ------------------------------------- %

%[m,s_init] = init_solution(in_out,DATA,m);
[m,s_init] = init_PGD_thesis(in_out,DATA,m);
s= s_init;

% Iterations
iter            = 0;
nb_pgd_modes    = 0;
indic_PGD     = [1];

while indic_PGD(end) > indic_target  && iter < max_iter
    
    iter = iter + 1;
    disp(' ');
    disp(['    ... PGD iteration: ',num2str(iter)]);
    
    s_old = s;
    U_pgd   = s.U;
    dg      = s.d;
    
    % Computation of F :
    F = m.fu;
    
    % Update of the time functions
    crit_update = 0;
    if ~isequal(m.rank_init,size(U_pgd.U{1},2)) && in_out.update_pgd
        disp('                ... update of the existing basis...')
        Q3 = - ttm(full(U_pgd) , m.k , 1);
        Q  = F  + Q3;
        [U_pgd] = update_pgd(in_out,DATA,m,U_pgd,Q,m.k);
        
        eps           = ttm(U_pgd, B_g , 1);
        sigma_full    = ttm(eps,hooke_g,1);%+ ttm( (full(eps) - eps_f) ,Hdo_inv,1);
        sigma         = redistribute(cp_als(sigma_full,1,'printitn',m.printitn_cp_als),param_redis);
        s_update      = sol_class(U_pgd,eps,sigma,dg,'linear');
        
        %         crit_update = compute_indicator(m,s_update,s_old,3);
        %         warning('implement criterion on update stage')
        %         disp(['                                    CRIT UPDATE : ',num2str(crit_update)]);
        %         disp('                ... existing basis updated...')
        % criterion for skipping
        indic_old = indic_PGD(end);
        crit_update = compute_update_indicator(m,indic_old,s_old,s_update);
        disp(['                    ...  update indicator: ',num2str(crit_update)]);
        % warning('implement criterion on update stage')
        disp('                ... existing basis updated...')
        %U_pgd = s_update.U;
        %s_old = s_update;
    end
    if crit_update < in_out.crit_update
        
        for i = 1 : 1
            % Generation of a new PGD mode
            Q3 = - ttm(full(U_pgd) , m.k , 1);
            Q  = F +  Q3;
            nb_pgd_modes_old = nb_pgd_modes;
            [delta_U,U_pgd,nb_pgd_modes] = new_pgd_mode(in_out,m,nb_pgd_modes,U_pgd,m.k,Q);
            if nb_pgd_modes_old ~= nb_pgd_modes
                U_pgd = U_pgd + delta_U;
                disp('            ... new mode added...');
            end
        end
        % storage
        eps      = ttm(U_pgd, B_g , 1);
        sigma    = ttm(eps,hooke_g,1);
        sigma    = redistribute(cp_als(sigma,1,'printitn',m.printitn_cp_als),param_redis);
        
        s = sol_class(U_pgd,eps,sigma,dg,'linear');
        
    else
        warning('update criterion OK - computation of a new mode skipped')
        s = s_update;
    end
    
    disp(['            ...done. Size of the reduced basis: ',num2str(size(s.U{1},2)),' modes.']);
    
    % Error and estimators
    % ------------------------------------- %
    indic_PGD(end+1) = compute_indicator(m,s,s_old,3);
    disp(['        ... error indicator......... ',num2str(indic_PGD(end))]);
    
    if in_out.debug == 1
        store_quantities_iter(in_out,DATA,m,s_local,s_linear,iter);
    end
    
    diary(strcat(in_out.out_dir,'log'));        % update log
    
end

disp(['    ... resolution done... ',num2str(toc,2),' s']);

% ------------------------------------------------------------------------
% export of the output
%     Rq : quite heavy for now as everything is saved.
format shortg
c = clock;
name_out = strcat(in_out.out_dir,'result_',num2str(c(1)),'_',num2str(c(2)),'_',num2str(c(3)),'.mat');
disp('ROMlab: Saving solution... ');
save(name_out,'-v7.3');
disp('    ... solution saved');
% ------------------------------------------------------------------------
m.u = s.U;
figure;semilogy(indic_PGD,'b');legend('error indicator')
% ------------------------------------------------------------------------
% export PXDMF
if in_out.exp_pxdmf == 1
    [output] = export_pxdmf(in_out,DATA,m,s);
end
% ------------------------------------------------------------------------
% vtu export of the displacement, strain, stresses, VM stress and damage
if in_out.exp_pv==1
    m = export_vtk(in_out,DATA,m,s,1,in_out.exp_pv_name,[]);
end
% ------------------------------------------------------------------------
warning on backtrace
end
