function [m, s]  = solver_PGD_var_sep(in_out,DATA,m)

disp('esb3d: Solving problem using PGD...');
total = tic;

Uref = m.Uref; % caution: for a particular value of parameters
ddlu=m.dofuu;
ddld=m.dofud;
particular = DATA.particular;

tiroir           = in_out.tiroir;
ngroup        = in_out.ngroup;
max_iter      = in_out.Giter ;     % max number of PGD iteration
indic_target  = in_out.indic_target; % max value of the error indic
rez_target = in_out.rez_target;

lparams = sprintf('%.0f,' , particular(1,:));
lparams = lparams(1:end-1);% strip final comma

A = m.k; % /!\ only one model
b = m.fu;
for i = 1 : length(A)
    A(i).k = A(i).k(ddlu,ddlu);
end
b.U{1} = b.U{1}(ddlu,:);


% Conditionner H
meanparams = [];
for i = 2 : length(m.params)
    a = abs(mean(m.params(i).mesh) - m.params(i).mesh);
    Imean = find(a==min(a));
    meanparams = [ meanparams Imean ];
end

H = pgdevaloperator(A,meanparams);
% Hinv = inv(H);

for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
    pgdnull{i} = zeros(ndofparams(i),1);
    pgdones{i} = ones(ndofparams(i),1);
end

% Null ktensors
pgdnull{1} = pgdnull{1}(ddlu,:);
pgdones{1} = pgdones{1}(ddlu,:);

if tiroir ==1
    % Group construction : 
    nparam = length(m.params)-1;
    sizegroup =  (length(m.params)-1)/ngroup;
    Alpha = zeros(ngroup,sizegroup); % Alpha : param of each group
    for ng = 1:ngroup  % Beta : params which are not in the group
        Beta{ng} = [1:nparam];
    end
    for group = 1:ngroup  
        Alpha(group,:) = [((group-1)*sizegroup +1):group*sizegroup];
        Beta{group}(((group-1)*sizegroup +1):group*sizegroup) = [];
        Beta{group} = fliplr(Beta{group}); % reverse Beta to use the ttv fonction during the integration
    end
    for i = 1:sizegroup % to init groups of params in fixed point iteration
        pgdgroup{i} = ones(ndofparams(i+1),1);
    end
end

% global operators grad
B_g = m.B_g;

% Reference : norm of b :
    int = ones(size(b.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(b{par}'*m.params(par).integ2*b{par});
    end
    int = int.*(b{1}'*(H\b{1}));
    ref = sqrt(sum(sum(int)));
    
%%%% CAUTION: HOMGENEOUS BC IN SPACE ASSUMED %%%%
%%%% IF NOT: USE Ud TO COMPUTE INITIAL SOLUTION %%%%

IntXblinit= ktensor(pgdones);
U_old = ktensor(pgdnull);
U_old.U{1}(ddlu) = U_old.U{1};
U_old.U{1}(ddld) = zeros(length(ddld),1);

% Iterations : adging PGD modes
iter = 1;
rez = 1;
Sol = ktensor(pgdnull); % Sol init null

while (iter <= max_iter) && (rez >= rez_target)
%     keyboard
disp(['   PGD iteration ',num2str( iter),]);
stagn = 1;
ptfixe = 1;
X2old = 1;

    % Computing product A*Sol : 
    
    ASol = ktensor(pgdnull); 

    a = Sol; % init a, will be completly rewritten
    for k = 1 : length(A)
             a.U{1} = A(k).k*Sol.U{1};
        for l = 2 : ndims(Sol)
            for Ls=1:size(Sol.lambda,1)
                a.U{l}(:,Ls) = Sol.U{l}(:,Ls).*A(k).u{l-1};
            end
        end
        ASol = ASol +  a;
    end
    % New second member for PGD iteration :
     ASol{length(m.params)} = - ASol{length(m.params)} ;
     b0 = b+ASol;


%%%%%%%%%%%%%
%%%% Fixed point %%%
%%%%%%%%%%%%%

    X = ktensor(pgdones); % ones : initialisation
    
    while (stagn > 1e-3) && (ptfixe <=15) 
            

%%%%%% Fonctions of space %%%%%%%%%%
            X2 = X.U{1}'*H*X.U{1};
            intub = b0; % Integer of b0*X.u on param space
            for i = 2 : ndims(X)
                X2 = X2 * X.U{i}'*m.params(i).integ2*X.U{i};
                intub = ttv(intub,m.params(i).integ2*X.U{i},2);
            end
            intub = sum(double(intub),2);

            intA = zeros(size(A(1).k));% Integer of A*X.u^� on param space, sum for each A.k
                for k = 1 : length(A)
                    a = 1;
                    for l = 2 : ndims(X)
                        a = a * X.U{l}'*m.params(l).integ2*(A(k).u{l-1}.* X.U{l});
                    end
                    intA = intA + a * A(k).k; 
                end

            % space solution :
            X.U{1} = intA\intub; 
            % Normalization :
            X.U{1} = X.U{1}/sqrt(X.U{1}'*H*X.U{1}); 
            X.U{length(m.params)} = X.U{length(m.params)} *sqrt(X.U{1}'*H*X.U{1});
            

        if tiroir ~= 1
                %%%%%  COMPUTE FUNCTIONS OF PARAMETERS %%%
            for par = 2 : length(m.params) 
                
                IntXb = b0;
                Den = zeros(m.params(par).ndof,size(A,2));
                
                % Compute Num : int b0*X.u
                    for i = 2 : ndims(b0)
                        fin = ndims(b0)-i+2;
                        if fin ~= par
                            IntXb = ttv(IntXb,m.params(fin).integ2*X.U{fin},fin);
                        end
                    end
                    IntXb = redistribute(IntXb,2);
                    Num = ttv(IntXb,X.U{1},1);%ttv(IntXb,m.params(1).integ2*X.U{1},1);

               SNum = sum(double(Num),2);

                % Compute Den : int X*A*X
                for l = 1:size(A,2)
                    IntXAX = 1;
                    for i = 2 : ndims(b0)
                        if i ~= par
                           IntXAX = IntXAX* ((X.U{i}.*A(l).u{i-1})'*m.params(i).integ2*X.U{i});
                        end
                    end
                        Den(:,l) = (X{1}'*A(l).k*X{1}) * IntXAX *A(l).u{par-1};

                end  
%                 SNum = sum(full(Num),2); % numerator
                 SDen = sum(Den,2);% denominator
                 X.U{par} = SNum./SDen;

                 % Normalisation : for all but the last param
                 if par ~= length(m.params)
                     X.U{length(m.params)}= X.U{length(m.params)}*sqrt(X.U{par}'*m.params(par).integ2*X.U{par});
                     X.U{par} = X.U{par}/ sqrt(X.U{par}'*m.params(par).integ2*X.U{par});
                 end

            end
            
       else % tiroir == 1
%%%%%  COMPUTE FUNCTIONS OF groups of PARAMETERS G %%%
         for group =1:ngroup % loop on the groups
                
                IntXb = b0; 
                
                % Compute Num : int b0*X.u

                IntBeta{size(Beta{group},2)}=[];
                ii=0;
                for i = Beta{group}
                    ii=ii+1;
                    IntBeta{ii} = m.params(i+1).integ2*X.U{i+1};
                end
                IntXb= ttv(IntXb,IntBeta,Beta{group}+1);

                IntXb = ttv(IntXb,X.U{1},1);
                IntXb  = redistribute(IntXb ,1);
                Num = double(IntXb);
                
%                 % Compute Num : int b0*X.u
%                 for l = 1:size(b0.lambda,1) 
%                     IntXbl = IntXblinit;
%                     IntXbl.U{1} = IntXb.U{1}(:,l);
%                     for j = 2:ndims(b0)
%                         IntXbl.U{j} = IntXb.U{j}(:,l);
%                     end
% 
%                     for i = beta1 : beta2
%                         fin = beta2-i+beta1;
%                             IntXbl = ttv(IntXbl,m.params(fin).integ2*X.U{fin},fin);
%                     end
%                     IntXbl = ttv(IntXbl,X.U{1},1);
%                     IntXbl  = redistribute(IntXbl ,1);
%                     if l == 1
%                         Num =  full(IntXbl);
%                     else
%                         Num  = Num + full(IntXbl) ; 
%                     end
%                 end
                               

%                 % Compute Den : int X*A*X
%                 for l = 1:size(A,2)
%                     IntXAX = 1;
%                     for i = beta1 : beta2
%                            IntXAX = IntXAX* ((X.U{i}.*A(l).u{i-1})'*m.params(i).integ2*X.U{i});
%                     end
%                     IntXAX = IntXAX*(X.U{1}'*A(l).k*X.U{1});
%                     Dtensor = ktensor(pgdgroup);
%                     alpha = 0;
%                     for dt = alpha1:alpha2
%                         alpha = alpha+1;
%                         Dtensor.U{alpha} = A(l).u{dt-1};
%                     end
%                     Dtensor = redistribute(IntXAX*Dtensor ,1);
%                     if l == 1
%                         Den = full(Dtensor);
%                     else
%                         Den = Den + full(Dtensor);
%                     end
% 
%                 end  
                
                % Compute Den : int X*A*X
                
                for La = 1:size(A,2)
                    
                    Au = cell(A(La).u){2};
                    AuBeta = Au(Beta{group});
                    gcell = cell(X){2}((Beta{group}+1));
                    Augi = cellfun(@(a,b) a.*b ,gcell,AuBeta,'UniformOutput',false);
                    Augi = ktensor(Augi);
                    IntXAX   = ttv(Augi,IntBeta);                 
                    
                    IntXAX = IntXAX*(X.U{1}'*A(La).k*X.U{1});

                    Dtensor = ktensor(IntXAX,Au(Alpha(group,:)));
                    if La == 1
                        Den = full(Dtensor);
                    else
                        Den = Den + full(Dtensor);
                    end

                end  

                 G = Num./Den;
                 G = cp_als(G,1,'printitn',0);
                 G.U{1} = G.lambda *  G.U{1};
                 alpha = 0;

                 if any ((length(m.params)-1)==Alpha(group,:))% /!\ Start with the last param, 
                     % not to overwite on the normalization of the other params of the group 
                     X.U{length(m.params)} = G.U{ndims(G)};                     
                 end
                 
%                  if alpha2 == length(m.params) % /!\ Start with the last param, 
%                      % not to overwite on the normalization of the other params of the group 
%                      X.U{alpha2} = G.U{ndims(G)};                     
%                      alpha2 = alpha2-1;
%                  end
                 
%                  for dt = alpha1:alpha2
%                      alpha = alpha+1;
%                      X.U{dt} = G.U{alpha};
%                      if dt ~=  length(m.params)% Normalisation : for all but the last param
%                          X.U{length(m.params)} =  X.U{length(m.params)}*sqrt(X.U{dt}'*m.params(dt).integ2*X.U{dt});
%                          X.U{dt} =  X.U{dt}/sqrt(X.U{dt}'*m.params(dt).integ2*X.U{dt});
%                      end
%                  end
%                  
                 for dt = Alpha(group,:)
                     if dt ~=  (length(m.params)-1)% For all but the last param
                     alpha = alpha+1;
                     X.U{dt+1} = G.U{alpha};
                     % normatlisation
                         X.U{length(m.params)} =  X.U{length(m.params)}*sqrt(X.U{dt+1}'*m.params(dt+1).integ2*X.U{dt+1});
                         X.U{dt+1} =  X.U{dt+1}/sqrt(X.U{dt+1}'*m.params(dt+1).integ2*X.U{dt+1});
                     end
                 end

        end    
        end 
        
        stagn  = sqrt(abs(X2-X2old)/X2old);
        disp(['    Stagnation for fixed point iteration ',num2str( ptfixe ),' : ',num2str(stagn)]);
        X2old = X2;
        ptfixe = ptfixe+1;
    end
    Sol = Sol + X;
    % Adding BC
    U_tot = Sol;
    for it = 1:size(Sol.lambda,1)
         U_tot.U{1}(ddlu,it) =Sol.U{1}(:,it);
         U_tot.U{1}(ddld,it) = zeros(length(ddld),1);
    end
    X_tot = X;
    X_tot.U{1}(ddlu,1) = X.U{1};
    X_tot.U{1}(ddld,1) = zeros(length(ddld),1);
    
    if iter == 1
        X1 = X_tot;
    end
    
    R0 = redistribute(b0,2);
    int = ones(size(R0.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(R0{par}'*m.params(par).integ2*R0{par});
    end
%     int = int.*(R0{1}'*Hinv*R0{1});
    int = int.*(R0{1}'*(H\R0{1}));
    rez = sqrt(sum(sum(int)))/ref;
    Norm_Residual(iter) = rez;

    PGD_estim(iter) = compute_L2_indicator(m,X1,X_tot);
    
    % Error with respect to the reference for a group of parameters :
    command = strcat('U_tot(:,',lparams,');');
    U_part= eval(command);

    Error_Part(iter) = (U_part(ddlu)-Uref{1}(ddlu))'*H*(U_part(ddlu)-Uref{1}(ddlu)) ...
        / (Uref{1}(ddlu)'*H*Uref{1}(ddlu));

    iter = iter + 1;
end

disp(['    ... resolution done... ',num2str(toc(total),2),' s']);

     
%%%%%%%%%%%%%%%%%%%
%   POST PROCESS  %
%%%%%%%%%%%%%%%%%%%

figure;semilogy(PGD_estim,'b');legend('error indicator')
figure;semilogy(Error_Part,'b');legend('error part')

    
    Norm_Residual(1) = 1;   
figure;semilogy(Norm_Residual,'b');legend('norm of the residual')

eps      = ttm(U_tot, B_g , 1); 
s = sol_class(U_tot,eps,[],[],'linear');

% Save everything :
graph.PGD_estim = PGD_estim';
graph.Error_Part = Error_Part';
graph.Norm_Residual = Norm_Residual';

store_quantities_iter(in_out,DATA,m,s,graph,1);

if in_out.exp_pxdmf == 1
    [output] = export_pxdmf(in_out,DATA,m,[],s);
end

end
