function [m,s_post]=solver_big_PGD(in_out,DATA,m)
% Solve the problem using a variant of the Extended PGD method

Uref = m.Uref{1}; % caution: for a particular value of parameters
ddlu=m.dofuu;
ddld=m.dofud;
particular = DATA.particular;


tiroir           = in_out.tiroir;
ngroup       = in_out.ngroup;
max_iter     = in_out.Giter; % Max number of global iteration
nkrylov       = in_out.nkrylov; % Critere pour le nb de modes
majKrylov    = in_out.majKrylov; % nomber of global iterations btw 2 computations of a new Krylov Basis
Rpgd          = in_out.Rpgd;       % =1, use of the pgd approximation of the residual at each iteration
modeperX  = in_out.modeperX;   % number of parametric functions for each spatial mode
rez_target = in_out.rez_target;


%%%%% Other parameters to test the algorithm :

maj = 15;           % Adding an initialisation at iteration maj
%%%
multiple_init = 0; % =1 -> test multiple initialisations
n_init    = 5;       % if multiple_init = 1, number of initialisations
precond = 1; % (different from 1 if multiple init)


A = m.k; % /!\ only one model2
b = m.fu;
for i = 1 : length(A)
    A(i).k = A(i).k(ddlu,ddlu);
end
b.U{1} = b.U{1}(ddlu,:);

lparams = sprintf('%.0f,' , particular(1,:));
lparams = lparams(1:end-1);% strip final comma

disp(['esb3d: Preconditioner computation'])
tic

% Different possible conditionners H
meanparams = [];
maxparams  = [];
minparams = [];

for i = 2 : length(m.params)
    a = abs(mean(m.params(i).mesh) - m.params(i).mesh);
    Imean = find(a==min(a));
    meanparams = [ meanparams Imean ];
    maxparams = [maxparams size(m.params(i).mesh,2)];
    minparams = [minparams 1];
end

H = pgdevaloperator(A,meanparams);
[Lh , Uh] = lu(H);
H0 = H;
Hplus = pgdevaloperator(A,maxparams);
Hmoins = pgdevaloperator(A,minparams);
% Hinv =inv(H);%eye(size(H));% inv(H);
% Hplusinv = inv(Hplus);

% Random parameters :
if multiple_init == 1 
Hrand = zeros(size(H,1),size(H,2),n_init );
for rp = 1:n_init 
    randparam = 1+round( rand(1,length(m.params)-1)*(size(m.params(i).mesh,2) -1));
    Hrand(:,:,rp) = pgdevaloperator(A,randparam);
end
end

disp(['esb3d: Condition Number = ' num2str(condest(H))])

disp(['     ... computed in ', num2str(toc), ' s'])

total = tic;
disp(['esb3d: PGD resolution... ']);


for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
    pgdnull{i} = zeros(ndofparams(i),1);
    pgdones{i} = ones(ndofparams(i),1);
end
T0_tot = ktensor(pgdnull);
pgdnull{1} = pgdnull{1}(ddlu,:);
pgdones{1} = pgdones{1}(ddlu,:);

% Null ktensors
%%%% CAUTION: HOMGENEOUS BC IN SPACE ASSUMED %%%%
%%%% IF NOT: USE Ud TO COMPUTE INITIAL SOLUTION %%%%
U = ktensor(pgdnull);
dU = ktensor(pgdnull);
Xi = ktensor(pgdones);
T0 = ktensor(pgdnull);
% Iterations
iter = 1;
err(iter) = 1;
indic(iter) = 1;
PGD_estim_mode = zeros(max_iter,nkrylov);
ind_tot = 1;
Norm_Residual = [];
   
        %%%% INITIALISATON %%%%
        
        if multiple_init ==1  % Test for multiple initialisation
            
            % First mode, H-1.b
            X0  = zeros(size(b.U{1},1),n_init +1);
            X0(:,1) = H\b.U{1};            
            
            Sol = b;
                Sol.U{1} = X0(:,1);
                X0ref = Sol;
                
                        % Normalisation of init sol
                        Sol.U{length(m.params)} = Sol.U{length(m.params)}*sqrt(Sol.U{1}'*H*Sol.U{1});
                        Sol.U{1} = Sol.U{1}/sqrt(Sol.U{1}'*H*Sol.U{1});
                        for par = 2 : length(m.params)-1
                                 Sol.U{length(m.params)}= Sol.U{length(m.params)}*sqrt(Sol.U{par}'*m.params(par).integ2*Sol.U{par});
                                 Sol.U{par} = Sol.U{par}/ sqrt(Sol.U{par}'*m.params(par).integ2*Sol.U{par});
                        end
                        
                    % Reference : norm of X0 :
                    int = ones(size(X0ref.lambda,1)); % Fast !
                    for par = 2:length(m.params)
                        int = int.*(X0ref {par}'*m.params(par).integ2*X0ref {par});
                    end
                    int = int.*(X0ref {1}'*H*X0ref {1});
                    ref = sqrt(sum(sum(int)));
                    
            % Other random initialisation vectors !!
            X0(:,1) = X0(:,1)/sqrt(X0(:,1)'*H*X0(:,1));
            for i =  2 : size(X0,2)
                X0(:,i) = Hrand(:,:,i-1)\b.U{1};
            end
                    X0_tot = b;
                    X0_tot.U{1}(ddlu,1) = X0(:,1);
                    X0_tot.U{1}(ddld,1) = zeros(length(ddld),1);
            % ortho des modes
            
                for i = 2 : size(X0,2)                   
                    xi_old = X0(:,i);
                    xi  = xi_old;
                    for j = 1 : i-1
                        xi = xi - (X0(:,j)'*H*xi_old)*X0(:,j);
                    end

                    X0(:,i) = xi/sqrt(xi'*H*xi);
         
                end
                
        
        else    % Single and simple initialisation
            
        X0 = b;
        X0.U{1} = H\b.U{1};

        % Normalisation of init sol
        X0.U{length(m.params)} = X0.U{length(m.params)}*sqrt(X0.U{1}'*H*X0.U{1});
        X0.U{1} = X0.U{1}/sqrt(X0.U{1}'*H*X0.U{1});
        for par = 2 : length(m.params)-1
                 X0.U{length(m.params)}= X0.U{length(m.params)}*sqrt(X0.U{par}'*m.params(par).integ2*X0.U{par});
                 X0.U{par} = X0.U{par}/ sqrt(X0.U{par}'*m.params(par).integ2*X0.U{par});
        end

        X0ref = X0;
        Sol = X0;
        X0_tot = X0;

        X0_tot.U{1}(ddlu,1) = X0.U{1};
        X0_tot.U{1}(ddld,1) = zeros(length(ddld),1);
        
        % Reference : norm of X0 :
        int = ones(size(X0ref.lambda,1)); % Fast !
        for par = 2:length(m.params)
            int = int.*(X0ref {par}'*m.params(par).integ2*X0ref {par});
        end
        int = int.*(X0ref {1}'*H*X0ref {1});
        ref = sqrt(sum(sum(int)));
    
    % First residual :
    for k = 1 : length(A)
        a = Sol;
        a.U{1} = A(k).k*Sol.U{1};
        for l = 2 : ndims(Sol)
            for Ls=1:size(Sol.lambda,1)
                a.U{l}(:,Ls) = Sol.U{l}(:,Ls).*A(k).u{l-1};
            end
        end
        if k ==1 
            ASol = a;
        else
        ASol = ASol +  a;
        end

    end
    
R0 = b-ASol;
R0 = redistribute(R0,2);   
    
    % Norm of the residual
    int = ones(size(R0.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(R0{par}'*m.params(par).integ2*R0{par});
    end
    int = int.*(R0{1}'*(H\R0{1}));
    Norm_R0 = sqrt(sum(sum(int)));
        end
    
T0 =  T0_tot;
T0.U{1} = zeros(300,1);


    it = 1;
    rez = 1;
while (it <= max_iter)   && (rez >= rez_target) %%%% loop on the order of the residual

    disp(' ')
    disp([ 'Iteration globale : ' num2str(it) ]);
    it = it + 1;
    
%%%%% Krylov basis : space fx %%%%%%%
    if mod(it+1,majKrylov)==0 && it ~= maj && it ~=2
        Xkrylov = zeros(size(b.U{1},1),nkrylov);
        
    tKrylov = tic;    
    disp(['    Krylov vector number ',num2str(1 )]);
    pgd1R1 = pgd1residue(A,Sol,b,H,m,in_out);   

    xi = pgd1R1.U{1};
    Xi.U{1} = xi/sqrt(xi'*H*xi);

    %   Orthonormalization with respect to the 2 previous modes
    if it >=3
        xi  = Xi.U{1} ;
        xi_old = xi;

        for j = 1 : 2
            xi = xi - (Sol.U{1}(:,end-(j-1)*modeperX)'*H*xi_old)*Sol.U{1}(:,end-(j-1)*modeperX);
        end
        Xi.U{1} = xi/sqrt(xi'*H*xi);
    end
        
    Xkrylov(:,1)=Xi.U{1};   
    disp(['    Krylov vector computed in ',num2str(toc(tKrylov)),' s'])


    for i = 2 : (nkrylov) % Parameters associated to the othe modes of the Krylov Basis
        tKrylov = tic;  
        disp(['    Krylov vector number ',num2str(i)]);
        pgd1Ri = pgd1residue(A,redistribute(-Xi,2),0,H,m,in_out);
        xi_old = pgd1Ri.U{1};
        xi  = xi_old;
        for j = 1 : i-1
            xi = xi - (Xkrylov(:,j)'*H*xi_old)*Xkrylov(:,j);
        end
        
        Xi.U{1} = xi/sqrt(xi'*H*xi);
        Xkrylov(:,i) = Xi.U{1};  
         disp(['    Krylov vector computed in ',num2str(toc(tKrylov)),' s'])
         
    end
        
    elseif it ==2
        if multiple_init ==1
            Xkrylov = X0;
        else
            Xkrylov = X0.U{1};
        end

        
        
    end
    
     %%% test if the K basis is degenerated :
%      disp('      Check ortho of the Krylov Basis    : ');
%             Xkrylov'*H*Xkrylov
            
%%%%%%%   Parameters functions :  %%%%%

    iter = 1;
    Sol_iter = T0_tot;
    while iter <= size(Xkrylov,2) % One set of fx g per spacial vector
        for gfunc = 1:modeperX 
        tIter = tic;
        disp([ '    Iteration param : ' num2str(iter) ]);

         X1 = Xkrylov(:,iter);
        %Product A*X :
        if Rpgd ~= 1
                     
        for k = 1 : length(A)
            a = Sol;
            a.U{1} = A(k).k*Sol.U{1};
            for l = 2 : ndims(Sol)
                for Ls=1:size(Sol.lambda,1)
                    a.U{l}(:,Ls) = Sol.U{l}(:,Ls).*A(k).u{l-1};
                end
            end
            if k ==1 
                ASol = a;
            else
            ASol = ASol +  a;
            end
            
         end

         R0 = b-ASol;
         R0 = redistribute(R0,2);
         gi = paramfunct(A,X1,R0,Lh,Uh,m,tiroir,ngroup,in_out);

        else  % (delated, see before 23_05_16) use of pgd1R1 insted of the complete residual

        end
         Xibar = ktensor(pgdones);

         Xibar.U{1}= X1;
         for i = 2:ndims(b)
              Xibar.U{i} = gi.U{i-1};
         end
          
         if iter ==1 &&  gfunc==1  
              Xtemp = Xibar;
         else
             Xtemp = Xtemp + Xibar;
         end
        
         Sol = Sol + Xibar;    
          
    X1_tot = Xibar;
    X1_tot.U{1}(ddlu,1) = Xibar.U{1};
    X1_tot.U{1}(ddld,1) = zeros(length(ddld),1);
    Sol_iter = Sol_iter + X1_tot;
    PGD_estim((precond-1)*ind_tot + (it-1)*nkrylov*modeperX  + (iter-1)*modeperX +gfunc) = compute_L2_indicator(m,X0_tot,X1_tot);  
    PGD_estim_mode(it-1,iter) = PGD_estim((it-1)*nkrylov + iter) ;
    
    if iter == nkrylov
    PGD_estim_av((it-1)) = compute_L2_indicator(m,X0_tot,Sol_iter);  
    end
    
    % Error with respect to the reference for a group of parameters :
    command = strcat('Sol(:,',lparams,');');
    U_part= eval(command);
    Error_Part((precond-1)*ind_tot + (it-1)*nkrylov*modeperX  + (iter-1)*modeperX +gfunc) = (U_part-Uref(ddlu))'*H*(U_part-Uref(ddlu)) ...
        / (Uref(ddlu)'*H*Uref(ddlu));
    
    % Error : norm of the residual
    tnorm = tic;
    int = ones(size(R0.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(R0{par}'*m.params(par).integ2*R0{par});
    end
    
    %%%%%%%%%%%%%%%%%%%% /!\ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    int = int.*(R0{1}'*(H\R0{1}));
%     int = int.*(R0{1}'*(R0{1}));
    rez = sqrt(sum(sum(int)))/ref;


    Norm_Residual(size(Norm_Residual,2)+1)=rez;
    
%     Norm_Residual((precond-1)*ind_tot + (it-1)*nkrylov*modeperX  + (iter-1)*modeperX +gfunc) = Norm_R;
    disp([ '    Norm_residual computed in ', num2str(toc(tnorm)), ' s' ]);
    disp([ '    Iteration param ' num2str(iter), ' computed in ', num2str(toc(tIter)), ' s', ]);
    disp([ '    Residual = ' num2str(rez) ]);
        end
    iter = iter + 1; 
    end  
    
%     %% Test : Update -> using the same basis to approximate the current residual
%     if it ==4 % 3 vecteurs
%          Sol_kept = extract(Sol,1:(size(Sol.lambda,1)-3*modeperX)); % Keeping all the solution except the last 3 modes
% 
%         % Current residual to approximate : 
%                 %Product A*X :
%    
%         for k = 1 : length(A)
%             a = Sol_kept;
%             a.U{1} = A(k).k*Sol_kept.U{1};
%             for l = 2 : ndims(Sol_kept)
%                 for Ls=1:size(Sol_kept.lambda,1)
%                     a.U{l}(:,Ls) = Sol_kept.U{l}(:,Ls).*A(k).u{l-1};
%                 end
%             end
%             if k ==1 
%                 ASol = a;
%             else
%             ASol = ASol +  a;
%             end
%             
%          end
% 
%          Rez_update = b-ASol;
%          Rez_update = redistribute(Rez_update,2);
%          
%         Rez_update = R0-Rez_update;
%         Rez_update = redistribute(Rez_update,2);
%         Rez_update = R0
%         % Current residual to approximate faster :
%         Sol_removed = extract(Sol,(size(Sol.lambda,1)-3*modeperX):size(Sol.lambda,1));
%         
%                 %Product A*X :
%    
% %         for k = 1 : length(A)
% %             a = Sol_removed;
% %             a.U{1} = A(k).k*Sol_removed.U{1};
% %             for l = 2 : ndims(Sol_removed)
% %                 for Ls=1:size(Sol_removed.lambda,1)
% %                     a.U{l}(:,Ls) = Sol_removed.U{l}(:,Ls).*A(k).u{l-1};
% %                 end
% %             end
% %             if k ==1 
% %                 ASol = a;
% %             else
% %             ASol = ASol +  a;
% %             end
% %             
% %         end    
% %          Rez_update = -ASol;
% %          Rez_update = redistribute(Rez_update,2);
%          
%          
%          iter = 1;
%          Sol_temp = T0;
%   while iter <= 3 % One set of fx g per spacial vector
%         for gfunc = 1:modeperX 
%         tIter = tic;
%         disp([ '    Iteration param : ' num2str(iter) ]);
% 
%          X1 = Sol.U{1}(:,(size(Sol.lambda,1)-(3-iter)*modeperX));
%          
%         %Product A*X :
%                      
%         for k = 1 : length(A)
%             a = Sol_temp;
%             a.U{1} = A(k).k*Sol_temp.U{1};
%             for l = 2 : ndims(Sol_temp)
%                 for Ls=1:size(Sol_temp.lambda,1)
%                     a.U{l}(:,Ls) = Sol_temp.U{l}(:,Ls).*A(k).u{l-1};
%                 end
%             end
%             if k ==1 
%                 ASol = a;
%             else
%             ASol = ASol +  a;
%             end
%             
%          end
% 
%          R0 = Rez_update-ASol;
%          R0 = redistribute(R0,2);
%          gi = paramfunct(A,X1,R0,Lh,Uh,m,tiroir,ngroup,in_out);
%          Xibar = ktensor(pgdones);
% 
%          Xibar.U{1}= X1;
%          for i = 2:ndims(b)
%               Xibar.U{i} = gi.U{i-1};
%          end
%           
%          if iter ==1 &&  gfunc==1  
%               Xtemp = Xibar;
%          else
%              Xtemp = Xtemp + Xibar;
%          end
%         
%          Sol_temp = Sol_temp + Xibar;    
%     
%     % Error : norm of the residual
%     tnorm = tic;
%     int = ones(size(R0.lambda,1)); % Fast !
%     for par = 2:length(m.params)
%         int = int.*(R0{par}'*m.params(par).integ2*R0{par});
%     end
%     int = int.*(R0{1}'*(H\R0{1}));
%     rez = sqrt(sum(sum(int)))/ref;
% 
%     Norm_Residual(size(Norm_Residual,2)+1)=rez;
%     
% %     Norm_Residual((precond-1)*ind_tot + (it-1)*nkrylov*modeperX  + (iter-1)*modeperX +gfunc) = Norm_R;
%     disp([ '    Norm_residual computed in ', num2str(toc(tnorm)), ' s' ]);
%     disp([ '    Iteration param ' num2str(iter), ' computed in ', num2str(toc(tIter)), ' s', ]);
%     disp([ '    Residual = ' num2str(rez) ]);
%         end
%     iter = iter + 1; 
%     end         
%     Sol = Sol + Sol_temp;   
%     Sol = redistribute(Sol,2);
%  
%                      
%     end
%  
end

ind_tot = (it-1)*nkrylov*modeperX  + (iter-1)*modeperX +gfunc ;


     Sol_tot = Sol;
     for it = 1:size(Sol.lambda,1)
         Sol_tot.U{1}(ddlu,it) =Sol.U{1}(:,it);
         Sol_tot.U{1}(ddld,it) = zeros(length(ddld),1);
     end
     Sol_com =  Sol_tot;     

 disp(['esb3d: BigPGD resolution done in ', num2str(toc(total),2),' s'])    
 
 
 
%%%%%%%%%%%%%%%%%%%
%   POST PROCESS  %
%%%%%%%%%%%%%%%%%%%


figure;semilogy(PGD_estim,'b');legend('norm mode')
figure;semilogy(PGD_estim_av,'b');legend('average error indicator')

figure('Name','Error Part','NumberTitle','off');
semilogy(Error_Part,'b');legend('Error with respect to reference')

LegendMode={};        % Legend
% Norme mode par iter
colors=Choose_color(size(PGD_estim_mode,1)  );
    for i = 1:size(PGD_estim_mode,1)    
    Ltemp=['Iteration ',num2str(i)];
    LegendMode=[LegendMode Ltemp ];
    end
figure('Name','Norm new mode','NumberTitle','off');
   for i = 1:size(PGD_estim_mode,1)  
       semilogy(PGD_estim_mode(i,:),'Color',colors(i,:))
       hold on
   end
legend(LegendMode,'Location','northeast')    


% Reference : norm of b :
    b=X0ref;
    int = ones(size(b.lambda,1)); % Fast !
    for par = 2:length(m.params)
        int = int.*(b{par}'*m.params(par).integ2*b{par});
    end
    int = int.*(b{1}'*H*b{1});
    ref = sqrt(sum(sum(int)));
    
figure;semilogy(Norm_Residual,'b');legend('norm of the residual')

% Print sol part and error map :

for part =1 : size(particular,1)
    
    lparams = sprintf('%.0f,' , particular(part,:));
    lparams = lparams(1:end-1);% strip final comma
    
    command = strcat('Sol_com(:,',lparams,');');
    Sol_part= eval(command);

    Upost = tensor(Sol_part);
    
    B_g          = m.B_g;
    hooke_g      = sparse(m.ndofgp,m.ndofgp);  % /!\ zeros : does not compute sigma
    eps_post = ttm(Upost,B_g,1);
    sigma_post = ttm(eps_post,hooke_g,1);
    d_init = [];
    s_post = sol_class(Upost,eps_post,sigma_post,d_init,'init');
    s_direct = s_post;
    s_direct.stage = 'linear';
    localdir = 'part';
    m = export_vtk(in_out,DATA,m,s_direct,1,[localdir, num2str(part)]);  
    
    
    Error = abs(Sol_part-full(m.Uref{part}));%/sqrt(m.Uref{part}(ddlu)'*H*m.Uref{part}(ddlu));
    Error = tensor(Error);
    eps_post = ttm(Error,B_g,1);
    sigma_post = ttm(eps_post ,hooke_g,1);    
    s_direct = sol_class(Error,eps_post,sigma_post,[],'init');
    s_direct.stage = 'linear';
    localdir = 'error';
    m = export_vtk(in_out,DATA,m,s_direct,1,[localdir, num2str(part)]);  
    
end
% Save everything :
graph.PGD_estim = PGD_estim';
graph.PGD_estim_av = PGD_estim_av';
graph.Error_Part = Error_Part';
graph.Norm_Residual = Norm_Residual';

s_post = sol_class(Sol_com,eps_post ,[],[],'linear');
store_quantities_iter(in_out,DATA,m,s_post,graph,1);


if in_out.exp_pxdmf == 1
    [output] = export_pxdmf(in_out,DATA,m,s_post);
end