function [m,s_contact] = solver_contact(in_out,DATA,m)

disp('ROMlab: Solving contact problem...');
tic
if in_out.debug == 1
    m.printitn_cp_als = 1;
else
    m.printitn_cp_als = 0;
end

ndofparams = zeros(1,numel(m.params));
for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
end
m.ndofparams = ndofparams;
if length(m.params)<3
    m.ndofparams(3) = 1;
end

ddlu=m.dofuu;
disp(['    ... condition number = ' num2str(condest(m.k(ddlu,ddlu)))])

% Solving using the LATIN method
% ------------------------------------- %
[m,s_init]  = solver_raphael(in_out,DATA,m);
s_contact = s_init;
s_contact.stage = 'contact';

disp(['    ... resolution... ']);
disp(['    ... resolution done... ',num2str(toc,2),' s']);

localdir = 'init';
m = export_vtk(in_out,DATA,m,s_contact,1,localdir);

end