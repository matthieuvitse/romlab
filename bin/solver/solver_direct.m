function [m,s_direct]=solver_direct(in_out,DATA,m)

disp('ROMlab: Direct resolution...')
% Lecture des donn�es n�cessaires � la r�solution
ddlu = m.dofuu;
disp(['    ... condition number = ' num2str(condest(m.k(ddlu,ddlu)))])

tic

for i = 1 : length(m.params)
    ndofparams(i) = m.params(i).ndof;
end
if length(m.params)<3
    ndofparams(3) = 1;
end

m.tol_cp_als = 1e-3;

U  = tenzeros(ndofparams);
ne = ndofparams(3);

% monolithic resolution
% ---------------------
ddlu    = m.dofuu;
ddld    = m.dofud;
B_g     = m.B_g;
hooke_g = m.hooke_g;
kuu     = m.k(ddlu,ddlu);
kud     = m.k(ddlu,ddld);



if numel(m.params) == 3
    udd     = double(m.ud(ddld,:,:));
    fuu     = double(m.fu(ddlu,:,:));
elseif numel(m.params) == 2
    udd     = double(m.ud(ddld,:));
    fuu     = double(m.fu(ddlu,:));
end

fuu     = (double(m.model(1).fu(ddlu,50)));

% PetscBinaryWrite(['ROMlab_',num2str(size(kuu,1)),'_vec.mat'],fuu);
% PetscBinaryWrite(['ROMlab_',num2str(size(kuu,1)),'.mat'],kuu);
% keyboard
% 

parfor (i_init = 1 : ne , in_out.arg_parallel)
    fu   = fuu(:,:,i_init);
    udd1 = udd(:,:,i_init);
    Uu(:,:,i_init) = kuu\(fu - kud*udd1);
    Ud(:,:,i_init) = udd(:,:,i_init);
end
U_mono(ddlu,:,:) = Uu;
U_mono(ddld,:,:) = Ud;

U_init     = tensor(U_mono);
eps_init   = ttm(U_init,B_g,1);
sigma_init = ttm(eps_init,hooke_g,1);
d_init     = [];
s_init     = sol_class(U_init,eps_init,sigma_init,d_init,'init');

%   PARAFAC of the initial solution 
if 1==0
    
    % CP decomposition using PARAFAC: rank R
    R = 3;
    Uvisu = zeros(size(U,1)*1.5,size(U,2),size(U,3));   % TRI3 ou QUA4
    for i = 1 : length(m.node)
        a(i,:) = m.node(i).rep;
        Uvisu(3*i-2:3*i-1,:,:) = U(a(i,:),:,:); % TRI3 ou QUA4
        %Uvisu(3*i-2:3*i,:,:) = U(a(i,:),:,:); % QUA8
    end
    Udirect = cp_als(tensor(Uvisu),R);
    for i = 1 : R
        Udirect.U{2}(:,i) = Udirect.U{2}(:,i)*Udirect.lambda(i);
    end
    Udirect.lambda = ones(R,1);
    output = output_pxdmf(DATA,Udirect,m,'soldirect');
    
end

%   POST PROCESS  
% ---------------------

s_direct        = s_init;
s_direct.stage  = 'linear';
localdir        = 'direct';
m = export_vtk(in_out,DATA,m,s_direct,1,localdir);

disp(['    ... resolution done... ',num2str(toc,2),' s']);

end