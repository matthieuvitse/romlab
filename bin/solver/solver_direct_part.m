function [m,s]=solver_direct_part(in_out,DATA,m)
% Compute and export a solution of the problem for a given set of
% parameters 

disp('esb3d: Computing a referece solution ... ');
tic
ddlu=m.dofuu;
ddld=m.dofud;

particular = DATA.particular; % Particular set of parameters

for part =1 : size(particular,1)
    K = pgdevaloperator(m.k,particular(part,:)); % Compute stiffness matrix  for the set of params
    disp(['esb3d: Condition Number = ' num2str(condest(K(ddlu,ddlu)))])

    lparams = sprintf('%.0f,' , particular(part,:));
    lparams = lparams(1:end-1);% strip final comma

        command = strcat('m.fu([',num2str(ddlu),'],',lparams,');');
        fu = eval(command);
        command = strcat('m.fu([',num2str(ddld),'],',lparams,');');
        fd= eval(command);
        command = strcat('m.ud([',num2str(ddld),'],',lparams,');');
        Ud= eval(command);

    disp(['esb3d: Direct resolution... ']);

    % FE resolution :

    U(ddlu,:) = K(ddlu,ddlu)\(double(fu) - K(ddlu,ddld)*Ud);
    U(ddld,:) = Ud;

    m.Uref{part} = U;


    disp(['    ... resolution done... ',num2str(toc,2),' s']);

    %%%%%%%%%%%%%%%%%%%
    %   POST PROCESS  %
    %%%%%%%%%%%%%%%%%%%
    
    Upost = tensor(U);
    B_g          = m.B_g;
    hooke_g      = sparse(m.ndofgp,m.ndofgp);  % /!\ zeros : does not compute sigma
    eps_post = ttm(Upost,B_g,1);
    sigma_post = ttm(eps_post,hooke_g,1);
    d_init = [];
    s_post = sol_class(Upost,eps_post,sigma_post,d_init,'init');

    s_direct = s_post;
    s_direct.stage = 'linear';
    localdir = 'direct';
    m = export_vtk(in_out,DATA,m,s_direct,1,[localdir, num2str(part)]);

s{part}=U;
end


if 1==0
    
    % Export for the mean value of parameter
    Upost = double(m.Uref);
    
    tic
    disp(['esb3d: Export for paraview... ']);
    n = progress('init','Processing...');
    t0 = toc;
    tm = t0;
    ne = m.params(2).ndof;
    pvd_file2('sol',1,ne);
    for i = 1:ne
%         if tm+1 < toc % refresh time every 1s only
%             tm = toc;
%             tt = ceil((toc-t0)*(ne-i)/i);
%             n = progress(i/ne,sprintf('Processing... (estimated time: %ds)',tt));
%         else
%             n = progress(i/ne,n);
%         end
        u=Upost(:,i);
        m=call_after_solve(DATA,m,u);
        vtk_sol3(DATA,m,1,i);
    end
    progress('close');
    disp([ num2str(toc,2) ' s']);
    
end

end