function [k,h1,I_g,B_g,hooke_g,hooke_inv_g] = computed_coupled_operators(m)
tic 

foo;
keyboard

k            = spalloc(m.ndof,m.ndof,1);
h1           = spalloc(m.ndof,m.ndof,1);
I_g          = spalloc(m.ndofgp,m.ndofgp,1);
B_g          = spalloc(m.ndofgp,m.ndof,1);
hooke_g      = spalloc(m.ndofgp,m.ndofgp,1);
hooke_inv_g  = spalloc(m.ndofgp,m.ndofgp,1);

for i_model = 1 : numel(m.model)
    % we assemble the big coupled matrix
    dof         = [m.model(i_model).node(:).dof];
    dof_corres  = [m.model(i_model).node(:).dof_corres];
    dofgp_model = m.model(i_model).dofgp;
    dof_node = m.model(i_model).dof_node;
    
    if i_model == 1
        B_g(dofgp_model,sort(dof_corres,'ascend')) = m.B_g(dofgp_model,sort(dof_corres,'ascend')) + m.model(i_model).B_g;
    else
        for iii = 1 : numel(m.model(i_model).node)
            if  ~isempty(m.model(i_model).node(iii).dof)
                dof2(iii,1) = m.model(i_model).node(iii).num;
                dof2(iii,2:(dof_node+1)) = m.model(i_model).node(iii).dof;
            end
        end
        [~,toto] = sort(dof2(:,2),'ascend');
        dof_corres2 = [m.model(i_model).node(toto).dof_corres];
        
        B_g(dofgp_model,dof_corres2) = m.B_g(dofgp_model,dof_corres2) + m.model(i_model).B_g;
    end
    k(dof_corres,dof_corres)      = m.k(dof_corres,dof_corres) + m.model(i_model).k(dof,dof);
    h1(dof_corres,dof_corres)     = m.h1(dof_corres,dof_corres) +  m.model(i_model).h1(dof,dof);
    I_g(dofgp_model,dofgp_model)  = m.I_g(dofgp_model,dofgp_model) + m.model(i_model).I_g;
    hooke_g(dofgp_model,dofgp_model)     = m.hooke_g(dofgp_model,dofgp_model) + m.model(i_model).hooke_g;
    hooke_inv_g(dofgp_model,dofgp_model) = m.hooke_inv_g(dofgp_model,dofgp_model) + m.model(i_model).hooke_inv_g;
end
toc 

end