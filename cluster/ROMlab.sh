#!/bin/bash
###PBS -q bigmem -l nodes=01:ppn=32,walltime=1000:00:00,vmem=250gb
###PBS -l nodes=01:ppn=3:TCP,walltime=100:00:00,vmem=50gb
#PBS -l nodes=01:ppn=12:TCP,walltime=100:00:00,vmem=125gb

dir=ROMlab
scriptname=ROMlab
jobdir=/u/vitse/$dir                      
inidir=/data1/vitse/resultats/$PBS_JOBID.$dir          ## where the results are stored
localdir=/usrtmp/vitse/$PBS_JOBID.$dir                 ## where the computation is carried out

mkdir -p $localdir                                     ## creates the folders if not already exist (-p)
mkdir -p $inidir
cp -r $jobdir $localdir
cp -r $jobdir $inidir
cat $PBS_NODEFILE > $inidir/nodelist_$PBS_JOBID

cd $localdir/$dir

/usr/local/MATLAB/R2014a/bin/matlab -nodisplay -r $scriptname  &> $inidir/out_$scriptname

chmod -R 755 $localdir
rsync -arv $localdir/* $inidir

rm -r $localdir

##./move_results.sh -v var=$jobdir var2=${var}    ## [MV] finish this thing....
