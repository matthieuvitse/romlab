%
%    TEST CASE FILE: ABAQUS READER 
%		(passieux 2011)
%

global DATA 
DATA.dim=3;

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='pipe.inp';

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=209e3;			% Young's Modulus
DATA.nu=0.3;			% Poisson ratio

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

DATA.load(1).type='sym';
DATA.load(1).box=[-0.228 0.0
                  -0.228 0.528
                  0.229 0.229];
DATA.load(1).val=[0;0;0];
 
DATA.load(2).type='sym';
DATA.load(2).box=[ 0.0   0.0
                  -0.228 0.528
                   0.0   0.229];
DATA.load(2).val=[0;0;0];


DATA.load(3).type='sym';
DATA.load(3).box=[-0.1 0
                  0.528 0.528
                  0.15  0.229];
DATA.load(3).val=[0 ; 0 ; 0];

DATA.load(4).type='stress';
DATA.load(4).box=[-0.228  0
                  -0.228  0.228
                   0.     0.];
DATA.load(4).val=[0 ; 0 ; -8.281e6];

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses