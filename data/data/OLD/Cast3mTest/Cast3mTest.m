%
%    TEST CASE FILE: Cast3m Reader 
%		(passieux 2009)
%

global DATA 
DATA.dim=2;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='tri_and_quad.avs';
% DATA.mesh='quad_nonreg.avs';
% DATA.mesh='tri.avs';

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e3;			% Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=1;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%


DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 0];
DATA.load(1).val=[0 ; 0];

DATA.load(2).type='stress';
DATA.load(2).box=[0 7 ; 16 16];
DATA.load(2).val=[0 ; 1000];


%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
