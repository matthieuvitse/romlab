%
%    TEST CASE FILE: GMSH READER 
%		(passieux 2010)
%

global DATA 
DATA.dim=2;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='coudeTRI.msh';	% 2d example (TRI3)
%DATA.mesh='coudeQUA.msh';	% 2d example (QUA4)

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e3;           % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=0;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

DATA.timeline=[0:0.5:20];
DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 0];
DATA.load(1).val=[zeros(1,length(DATA.timeline)) ; zeros(1,length(DATA.timeline))];

DATA.load(2).type='stress';
DATA.load(2).box=[18 18 ; 9 16];
fmax=1000;
loadtimey=fmax*sin(4*pi*DATA.timeline/DATA.timeline(end));
DATA.load(2).val=[zeros(1,length(DATA.timeline)) ; loadtimey];

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
