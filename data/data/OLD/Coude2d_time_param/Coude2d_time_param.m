%
%    TEST CASE FILE: GMSH READER 
%		(passieux 2010)
%

global DATA 
DATA.dim=2;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='coudeTRI.msh';	% 2d example (TRI3)
%DATA.mesh='coudeQUA.msh';	% 2d example (QUA4)

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e3;           % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=0;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:1:6];
DATA.params(3).mesh = [1:10];

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 0];
DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

DATA.load(2).type='stress';
DATA.load(2).box=[18 18 ; 9 16];
DATA.load(2).val=zeros(DATA.dim,n(2),n(3));
for k = 1 : n(3)
    loadtimey=1000*sin(DATA.params(3).mesh(k)*pi*DATA.params(2).mesh/DATA.params(2).mesh(end));
    DATA.load(2).val(2,:,k)=loadtimey;
end

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
