%
%    TEST CASE FILE: GMSH READER 
%		(passieux 2010)
%

global DATA 
DATA.dim=2;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

% DATA.mesh='coude_PGD_E.msh';	% 3d example (tet4)
DATA.mesh='coude_PGD_E_triangle.msh';

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=1;               % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=0;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%


DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 0];
DATA.load(1).val=[0 ; 0];

DATA.load(2).type='stress';
DATA.load(2).box=[18 18 ; 9 16];

DATA.load(2).val=[1000 ; 0];

DATA.Young=linspace(100e3,200e3,5);

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
