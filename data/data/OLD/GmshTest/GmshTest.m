%
%    TEST CASE FILE: GMSH READER 
%		(passieux 2010)
%

global DATA 
DATA.dim=3;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='piece.msh';	% 3d example (tet4)

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=100;             % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=1;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

DATA.load(1).type='sym';
DATA.load(1).box=[0 0.4; 0 0; 0 0.5];
DATA.load(1).val=[0 ; 0 ; 0];

DATA.load(2).type='stres';
DATA.load(2).box=[0 0.4 ; 0.7 0.7 ; 0 0.5];
DATA.load(2).val=[0 ;1; 0];

DATA.load(3).type='displ';
DATA.load(3).box=[0.4 0.4 ; 0 0; 0 0];
DATA.load(3).val=[0 ; 0 ; 0];

DATA.load(4).type='sym';
DATA.load(4).box=[-0.002 0.002 ; -0.002 0.002; 0 0];
DATA.load(4).val=[0 ; 0 ; 0];

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
