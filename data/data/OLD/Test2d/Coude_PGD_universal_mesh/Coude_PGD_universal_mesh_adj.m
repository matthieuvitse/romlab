%
%    TEST CASE FILE: GMSH READER 
%		(passieux 2010)
%

global DATA 
DATA.dim=2;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

DATA.mesh='Coude_PGD_universal_mesh4.msh';	% 3d example (tet4)

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e5;           % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=1;		% density
DATA.plst=0;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%


DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 0];
DATA.load(1).val=[0 ; 0];

DATA.load(2).type='force';
DATA.load(2).box=[18 ; 16];

DATA.load(2).val=[0 ; 1];

DATA.alpha=linspace(0.75,1,3);
DATA.beta=linspace(0.50,1,4);

%D�finition des r�gions
DATA.xA=7;
DATA.yA=9;
DATA.y12=3;
DATA.x54=13;
DATA.x5=18;
DATA.y5=16;

%Param�tre rigidit� partie molle
DATA.soft=1e-6;

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
