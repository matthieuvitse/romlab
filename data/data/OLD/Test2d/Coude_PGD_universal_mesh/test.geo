// Gmsh project created on Wed Jun 12 11:22:03 2013
Point(1) = {0, 0, 0, 1.0};
Point(2) = {1, 0, 0, 1.0};
Point(3) = {1, 1, 0, 1.0};
Point(4) = {0, 1, 0, 1.0};
Point(5) = {0.25, 0.5, 0, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 5};
Line(4) = {5, 3};
Line(5) = {4, 5};
Line(6) = {5, 1};
Line(7) = {1, 4};
Line(8) = {1, 2};
Line Loop(9) = {4, 2, 3};
Plane Surface(10) = {9};
Line Loop(11) = {4, -1, 5};
Plane Surface(12) = {11};
Line Loop(13) = {5, 6, 7};
Plane Surface(14) = {13};
Line Loop(15) = {6, 8, 3};
Plane Surface(16) = {15};
