%
%    TEST CASE FILE: 3d 
%		(passieux 2010)
%

global DATA 
DATA.dim=3;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

% Home made mesher for rectangular domains only...
DATA.L=[7 10 16];			% Size of the domain (mm)
DATA.Ng=[7 9 15];			% number of elements

% ...or use mesh readers

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e3;			    % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=1;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%


DATA.load(1).type='displ';
DATA.load(1).box=[0 7 ; 0 10 ; 0 0];
DATA.load(1).val=[0 ; 0 ; 0];

DATA.load(2).type='stress';
DATA.load(2).box=[0 7 ; 0 10 ; 16 16];
DATA.load(2).val=[0 ; 0 ; 1000];


%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
