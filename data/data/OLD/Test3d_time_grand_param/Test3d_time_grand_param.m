%
%    TEST CASE FILE: 3d
%		(passieux 2010)
%

global DATA
DATA.dim=3;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

% Home made mesher for rectangular domains only...
DATA.L=[4 4 4];			% Size of the domain (mm)
DATA.Ng=[4 4 4];			% number of elements

% ...or use mesh readers

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e3;			% Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=80;		% density
DATA.plst=1;			% 1: PLane STrain and 0: PLane STress

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';

% Material parameters
DATA.variablemat=1;
DATA.params(2).name = 'E1';
DATA.params(2).box = [0 2 ; 0 2 ; 0 2];
DATA.params(2).mesh = [-50:50];
DATA.params(3).name = 'E2';
DATA.params(3).box = [0 2 ; 2 4 ; 0 2];
DATA.params(3).mesh = [-50:50];
DATA.params(4).name = 'E3';
DATA.params(4).box = [2 4 ; 0 2 ; 0 2];
DATA.params(4).mesh = [-50:50];
DATA.params(5).name = 'E4';
DATA.params(5).box = [2 4 ; 2 4 ; 0 2];
DATA.params(5).mesh = [-50:50];
DATA.params(6).name = 'E5';
DATA.params(6).box = [0 2 ; 0 2 ; 2 4];
DATA.params(6).mesh = [-50:50];
DATA.params(7).name = 'E6';
DATA.params(7).box = [0 2 ; 2 4 ; 2 4];
DATA.params(7).mesh = [-50:50];
DATA.params(8).name = 'E7';
DATA.params(8).box = [2 4 ; 0 2 ; 2 4];
DATA.params(8).mesh = [-50:50];
DATA.params(9).name = 'E8';
DATA.params(9).box = [2 4 ; 2 4 ; 2 4];
DATA.params(9).mesh = [-50:50];

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

n(1) = DATA.dim;
for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[0 0 ; 0 4 ; 0 4];
DATA.load(1).val=[0 ; 0 ; 0];

DATA.load(2).type='stress';
DATA.load(2).box=[4 4 ; 0 4 ; 0 4];
DATA.load(2).val=sptensor(n);
DATA.load(2).val=[10000 ; 0; 0];

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
