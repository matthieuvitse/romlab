function DATA = Test3d_time_param()

%
%    TEST CASE FILE: 3d
%		(passieux 2010)
%


DATA.dim=3;		% Dimension 2d or 3d

%%%%%%%%
% MESH %
%%%%%%%%

L1 = 8;
L2 = 8;
L3 = 16;

% Home made mesher for rectangular domains only...
DATA.L=[L1 L2 L3];			% Size of the domain (mm)
%DATA.Ng=[3 3 3];			% number of elements
DATA.Ng=[4 4 8];
% ...or use mesh readers

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=36.5e3;			% Young's Modulus   MPa
DATA.nu=0.2;			% Poisson ratio
DATA.density=80 / 4;		% density   ?????
DATA.plst=1;			% 1: PLane STrain and 0: PLane STress
DATA.ft = 3.6;          % 10.0e6;       % limit in compression     MPa
DATA.Y0 = (DATA.ft^2)/(2*DATA.E); % initial threshold for damage activation  
DATA.Ad = 10 ;          %5e-1;

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%
    
DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:10];
DATA.params(3).mesh = [0:10];

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

sym = 1;  % 1 : symetries ; 0 : disp

if sym == 0
    DATA.load(1).type='displ';
    DATA.load(1).box=[0 L1 ; 0 L2 ; 0 0];
    DATA.load(1).val=zeros(DATA.dim,n(2),n(3));
else
    
    DATA.load(1).type='symx';
    DATA.load(1).box=[0 0 ; 0 L2 ; 0 L3];
    DATA.load(1).val=zeros(1,n(2),n(3));
    
    DATA.load(3).type='symy';
    DATA.load(3).box=[0 L1 ; L2 L2 ; 0 L3];
    DATA.load(3).val=zeros(1,n(2),n(3));
    
    DATA.load(4).type='symz';
    DATA.load(4).box=[0 L1 ; 0 L2 ; 0 0];
    DATA.load(4).val=zeros(1,n(2),n(3));
    
end

if 1 == 0
    DATA.load(2).type='stress';
    DATA.load(2).box=[0 L1 ; 0 L2 ; L3 L3];
    DATA.load(2).val=zeros(DATA.dim,n(2),n(3));
    for k = 1 : n(3)
        loadtimey= DATA.params(3).mesh(k) + 10000*sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end));
        DATA.load(2).val(3,:,k) = loadtimey;
    end
else
    if sym == 0
            DATA.load(2).type='displ';
            DATA.load(2).box=[0 L1 ; 0 L2 ; L3 L3];
            DATA.load(2).val=zeros(DATA.dim,n(2),n(3));
            for k = 1 : n(3)
                loadtimey= 0.0001 + DATA.params(2).mesh / 2e3; % 2 *  (.01 + DATA.params(3).mesh(k)/1000)*sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end));  % in mm
                DATA.load(2).val(3,:,k) = loadtimey;
            end
    else
        DATA.load(2).type='symz';
        DATA.load(2).box=[0 L1 ; 0 L2 ; L3 L3];
        DATA.load(2).val=zeros(1,n(2),n(3));
        for k = 1 : n(3)
            %loadtimey= 0.0001 + DATA.params(2).mesh / 2e3; % 2 *  (.01 + DATA.params(3).mesh(k)/1000)*sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end));  % in mm
            loadtimey= (0.0001 + DATA.params(3).mesh(k)/400) * sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end)); % in mm
            DATA.load(2).val(1,:,k) = loadtimey;
        end
    end

end
    
%%%%%%%%%%%%%%%%%%%%
% DAMAGED ELEMENTS %
%%%%%%%%%%%%%%%%%%%%

DATA.damage = 1;
DATA.dam_pg = 300;      % (num(elem)-1) * 8 + 4       WARNING !! NUMBERING FROM 0 !!!
DATA.dam_coef = .8;

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

DATA.plot_mesh=1;       % write mesh file
DATA.plot_sol=1;        % write solution file
DATA.plot_sigeps=1;     % compute stress and strain
DATA.plot_vonmises=1;   % compute principal stresses
DATA.plot_damage=1;
end

