%
%    TEST CASE FILE: 3d beam under impact 
%		(Odièvre 2009)
%

global DATA 
DATA.dim=3;		% Dimension 2d or 3d

%%%%%%%%%%%%
% GEOMETRY %
%%%%%%%%%%%%
DATA.L=[0.25 0.25 1];			% Size of the domain (mm)
DATA.Ng=[4 4 12];			% number of elements

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.E=200e9;             % Young's Modulus
DATA.nu=0.3;			% Poisson ratio
DATA.density=7800;		% density

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

DATA.timeline=[0:5e-6:375e-6];

DATA.load(1).type='displ';
DATA.load(1).box=[0 0.25
                  0 0.25
                  0 0];
DATA.load(1).val=[0 ; 0 ; 0]*DATA.timeline;

DATA.load(2).type='velocity';
DATA.load(2).box=[0 0.25
                  0 0.25;
                  1 1];
ld=0.5-cos(2*pi*DATA.timeline/(2*60e-6))/2;
ld(13:end)=1;
%ld(25:end)=0;
DATA.load(2).val=[0 ; 0 ; -1]*ld;


%%%%%%%%%%%%%%%%%%
% POSTPROCESSING %
%%%%%%%%%%%%%%%%%%

DATA.plot_mesh=0;
DATA.plot_sol=1;
DATA.plot_sigeps=0;
DATA.plot_vonmises=0;
