function DATA = contact_2D()

%
%    TEST CASE FILE: CONTACT  (Raphael's phd)
%		        (Vitse 2015)
%

DATA.dim        = 2;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'contact';   %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:10];
DATA.params(3).mesh = [0:5];

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%
% MESH %
%%%%%%%%

% Mesh refinement
Nh = 10;

% ------ %
% Mesh 1 %
% ------ %
L11 = 8;
L21 = 8;

X1 = 0;
Y1 = 0;

DATA.model(1).L=[L11 L21];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1];
DATA.model(1).Ng=[2 2]*Nh;

% ------ %
% Mesh 2 %
% ------ %

L12 = 8;
L22 = 8;

X2 = 0;
Y2 = L21 + 1;

% DATA.model(2).L=[L12 L22];			% Size of the domain (mm)
% DATA.model(2).O = [X2 Y2];
% DATA.model(2).Ng=[2 2]*Nh;

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.model(1).type              = 'FE2D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  205e9;                    % Young's Modulus
DATA.model(1).material.nu       =  0.3;                       % Poisson ratio
DATA.model(1).material.density  =  8800;                      % density
%DATA.model(1).material.ft       =  3.6e6;                    % limit in compression MPa
DATA.model(1).mesh = 'carre.msh';

DATA.model(2).type              = 'FE2D';
DATA.model(2).behavior.type     = 'elastic';
DATA.model(2).material.E        =  205e9;                     % Young's Modulus
DATA.model(2).material.nu       =  0.3;                       % Poisson ratio
DATA.model(2).material.density  =  8800;                      % density
%DATA.model(2).material.ft       =  3.6e6;                    % limit in compression MPa
DATA.model(2).mesh = 'rond.msh';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DAMAGE LAW AND PARAMETERS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% [none]

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%

DATA.load(1).type='displ';
DATA.load(1).box=[0 L11 ; 0 0];
DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

DATA.load(2).type='displ';
DATA.load(2).box=[0 L12 ; (Y2+L22/2) (Y2+L22/2)];
DATA.load(2).val=zeros(DATA.dim,n(2),n(3));

DATA.load(3).type='stress';
DATA.load(3).box=[0 L12 ;  Y2-.5 Y2+.5];
for k = 1 : n(3)
    loadtimey = 1e9 * DATA.params(2).mesh;
    DATA.load(3).val(2,:,k) = loadtimey;
    DATA.load(3).val(1,:,k) = zeros(size(loadtimey));
end

DATA.load(4).type='stress';
DATA.load(4).box=[.4*L11 .6*L11 ; L21 L21];
for k = 1 : n(3)
    loadtimey = -1e9 * DATA.params(2).mesh;
    DATA.load(4).val(2,:,k) = loadtimey;
    DATA.load(4).val(1,:,k) = zeros(size(loadtimey));
end

% DATA.load(7).type='contact';
% DATA.load(7).box=[X2 (X2+L12) ; L21 Y2];
% DATA.load(7).normal=2;


%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end
