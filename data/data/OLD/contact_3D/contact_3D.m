function DATA = contact_3D()

%
%    TEST CASE FILE: CONTACT  (Raphael's phd)
%		        (Vitse 2015)
%


DATA.dim        = 3;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'contact';   %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:10];
DATA.params(3).mesh = [0:5];

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%
% MESH %
%%%%%%%%

% Mesh refinement
Nh = 1;

% ------ %
% Mesh 1 %
% ------ %
L11 = 8;
L21 = 8;
L31 = 4;

X1 = 0;
Y1 = 0;
Z1 = 0;

DATA.model(1).L=[L11 L21 L31];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1 Z1];
DATA.model(1).Ng=[8 8 8]*Nh;

% ------ %
% Mesh 2 %
% ------ %

L12 = 4;
L22 = 4;
L32 = 4;

X2 = 2;
Y2 = 2;
Z2 = L31 + 1;

DATA.model(2).L=[L12 L22 L32];			% Size of the domain (mm)
DATA.model(2).O = [X2 Y2 Z2];
DATA.model(2).Ng=[4 4 4]*Nh;

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%

DATA.model(1).type              = 'FE3D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  205e9;                    % Young's Modulus
DATA.model(1).material.nu       =  0.3;                       % Poisson ratio
DATA.model(1).material.density  =  8800;                      % density
%DATA.model(1).material.ft       =  3.6e6;                    % limit in compression MPa

DATA.model(2).type              = 'FE3D';
DATA.model(2).behavior.type     = 'elastic';
DATA.model(2).material.E        =  205e9;                     % Young's Modulus
DATA.model(2).material.nu       =  0.3;                       % Poisson ratio
DATA.model(2).material.density  =  8800;                      % density
%DATA.model(2).material.ft       =  3.6e6;                    % limit in compression MPa

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DAMAGE LAW AND PARAMETERS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% [none]

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%

DATA.load(1).type='sym';
DATA.load(1).box=[0 0 ; 0 L21 ; 0 L31];
DATA.load(1).val=zeros(1,n(2),n(3));
DATA.load(1).normal=1;

DATA.load(2).type='sym';
DATA.load(2).box=[L11 L11 ; 0 L21 ; 0 L31];
DATA.load(2).val=zeros(1,n(2),n(3));
DATA.load(2).normal=1;

DATA.load(3).type='sym';
DATA.load(3).box=[0 L11 ; 0 0 ; 0 L31];
DATA.load(3).val=zeros(1,n(2),n(3));
DATA.load(3).normal=2;

DATA.load(4).type='sym';
DATA.load(4).box=[0 L11 ; L21 L21 ; 0 L31];
DATA.load(4).val=zeros(1,n(2),n(3));
DATA.load(4).normal=2;

DATA.load(5).type='sym';
DATA.load(5).box=[0 L11 ; 0 L21 ; 0 0];
DATA.load(5).val=zeros(1,n(2),n(3));
DATA.load(5).normal=3;

DATA.load(6).type='displ';
DATA.load(6).box=[X2 (X2+L12) ; Y2 (Y2+L22) ; (Z2+L32) (Z2+L32)];
DATA.load(6).val=zeros(DATA.dim,n(2),n(3));

DATA.load(7).type='stress';
DATA.load(7).box=[X2 (X2+L12) ; Y2 (Y2+L22) ; Z2 Z2 ];
for k = 1 : n(3)
    loadtimey = 1e9 * DATA.params(2).mesh;
    DATA.load(7).val(3,:,k) = loadtimey;
end

DATA.load(8).type='stress';
DATA.load(8).box=[X2 (X2+L12) ; Y2 (Y2+L22) ; L31 L31];
for k = 1 : n(3)
    loadtimey = -1e9 * DATA.params(2).mesh;
    DATA.load(8).val(3,:,k) = loadtimey;
end

% DATA.load(9).type='contact';
% DATA.load(9).box=[X2 (X2+L12) ; Y2 (Y2+L22) ; L31 Z2];
% DATA.load(9).normal=3;


%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end
