function DATA = cylinder_contact_slip()

%
%    TEST CASE FILE: CONTACT  (Rafael's phd)
%		        (Vitse 2015)
%

DATA.dim        = 2;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'contact';      %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';

step1=0:0.25:1;
step2=1/8:1/8:1;

step=[step1 (step1(end) + step2)];

loadtimex=[    0.0*step1     0.002*sin(2*pi*step2)];
loadtimey=[-0.0025*step1    -0.0025*ones(size(step2))];

% step=[1 2];
% loadtimex=0.001*[0 1];
% loadtimey=-0.0025*[1 1];


DATA.params(2).mesh = 1:length(step);       % increments
DATA.params(3).mesh = 1:3;                  % ???

n=zeros(1,length(DATA.params));
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%%%%%
% MATERIAL %
%%%%%%%%%%%%

DATA.model(1).type              = 'FE2D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  8800;         % density

DATA.model(2).type              = 'FE2D';
DATA.model(2).behavior.type     = 'elastic';
DATA.model(2).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(2).material.nu       =  0.3;          % Poisson ratio
DATA.model(2).material.density  =  8800;         % density

%%%%%%%%
% MESH %
%%%%%%%%

DATA.model(1).mesh = 'specimen_finer2.msh';
DATA.model(2).mesh = 'pad_finer2.msh';

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%

Ls = 40;
Lp = 10;

Hs=10;

Hp=5;

DATA.load(1).type='displ';
DATA.load(1).box=[-Ls/2 Ls/2;-Hs -Hs];
DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

DATA.load(2).type='displ';
DATA.load(2).box=[-Lp/2 Lp/2;Hp Hp];


for k = 1 : n(3)
    DATA.load(2).val(1,:,k) = loadtimex;
    DATA.load(2).val(2,:,k) = loadtimey;
end

R=40;

DATA.load(3).type='contact';
DATA.load(3).box=@(x,y) [-0.3 0.3;0 (R - sqrt(R^2 - x^2))];
DATA.load(3).normal=2;

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

DATA.step=step;

[DATA] = init_data(DATA);

end
