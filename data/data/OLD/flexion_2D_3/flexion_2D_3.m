function DATA = flexion_2D_3()

%
%    TEST CASE FILE: CONTACT  (Rafael's phd)
%		        (Vitse 2015)
%

DATA.dim        = 2;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'none';      %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';

DATA.params(2).name = 't';
DATA.params(2).type = 'time';
DATA.params(2).unit = 's';
DATA.params(2).mesh = 0:0.1:3; % step time

DATA.params(3).name = 'mu';
DATA.params(3).type = 'loading';
DATA.params(3).unit = '-';
DATA.params(3).mesh = 0.1:5;       % 

n=zeros(1,length(DATA.params));
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%%%%%
% MATERIAL %
%%%%%%%%%%%%

DATA.model(1).type              = 'FE2D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e9;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  8800;         % density

% DATA.model(2).type              = 'FE2D';
% DATA.model(2).behavior.type     = 'elastic';
% DATA.model(2).material.E        =  200e3;        % Young's Modulus [MPa]
% DATA.model(2).material.nu       =  0.3;          % Poisson ratio
% DATA.model(2).material.density  =  8800;         % density

%%%%%%%%
% MESH %
%%%%%%%%

L1 = 0.75;
L2 = 0.2;
% L31 = 0.2;

X1 = 0;
Y1 = 0;
% Z1 = 0;

Nx = 30;
Ny = 10;

DATA.model(1).L=[L1 L2];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1];
DATA.model(1).Ng=[Nx Ny];


% DATA.model(1).L=[L11 L21 L31];			% Size of the domain (mm)
% DATA.model(1).O = [X1 Y1 Z1];
% DATA.model(1).Ng=[20*Nh 10 10];


% DATA.model(1).mesh = 'specimen.msh';
% DATA.model(2).mesh = 'pad.msh';

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%
% 
% L11 = 40;
% L21 = 10;

% DATA.load(1).type='sym';
% DATA.load(1).box=[L1/2 L1/2;0 L2];
% DATA.load(1).val=zeros(1,n(2));
% DATA.load(1).normal=1;

% DATA.load(4).type='stress';
% DATA.load(4).box=[-1 1;0 0];
% DATA.load(4).box=[-L1/2 L1/2;0 0];
% loadtimey = -200e3 * DATA.params(2).mesh;
% for k = 1 : n(3)
%     DATA.load(4).val(1,:,k) = zeros(size(loadtimey));
%     DATA.load(4).val(2,:,k) = loadtimey;
% end 

loadtimey = -.1 * DATA.params(2).mesh;


sym = 1;

if sym == 0
    DATA.load(1).type='displ';
    % DATA.load(1).box=[L1-L1/Nx L1 ;0 0+L2/Ny];
    DATA.load(1).box=[L1 L1 ;0 0];

    DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

    DATA.load(2).type='displ';
    DATA.load(2).box=[0 0.0001 ;0 0.0001];
    for k = 1 : n(3)
        DATA.load(2).val(2,:,k)=zeros(size(loadtimey));
    end
    DATA.load(3).type='displ';
    DATA.load(3).box=[L1/2 L1/2 ;L2 L2];

    for k = 1 : n(3)
        DATA.load(3).val(1,:,k) = zeros(size(loadtimey));
        DATA.load(3).val(2,:,k) = loadtimey;
    end
    % DATA.load(3).val=zeros(DATA.dim,n(2),n(3));
else
    
     DATA.load(1).type='displ';
    % DATA.load(1).box=[L1-L1/Nx L1 ;0 0+L2/Ny];
    DATA.load(1).box=[0 0.0001 ;0 0.0001];
    DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

    DATA.load(2).type='sym';
    DATA.load(2).box=[L1 L1; 0.0 L2 ];
    DATA.load(2).val=zeros(1,n(2),n(3));
    DATA.load(2).normal=1;
        
    DATA.load(3).type='stress';
    DATA.load(3).box=[(L1-L1/Nx) L1 ;(L2-L2/Ny) L2];
    for k = 1 : n(3)
        DATA.load(3).val(1,:,k) = zeros(size(loadtimey));
        DATA.load(3).val(2,:,k) = DATA.params(3).mesh(k)*10e7*loadtimey;
    end
end



%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end
