function DATA = flexion_2D_4()

%
%    TEST CASE FILE: 
%

DATA.dim        = 2;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'none';      %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name     = 'M';
DATA.params(2).name     = 't';
DATA.params(2).type     = 'time';
DATA.params(3).name     = 'mu1';
DATA.params(3).type     = 'loading';
DATA.params(2).mesh     = 0:0.1:3;
DATA.params(3).mesh     = 0:4;

DATA.params(4).name     = 'mu2';
DATA.params(4).target   = 'loading';
DATA.params(4).mesh     = 0:4;

n=zeros(1,length(DATA.params));
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%%%%%
% MATERIAL %
%%%%%%%%%%%%

DATA.model(1).type              = 'FE2D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e9;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  8800;         % density

% DATA.model(2).type              = 'FE2D';
% DATA.model(2).behavior.type     = 'elastic';
% DATA.model(2).material.E        =  200e3;        % Young's Modulus [MPa]
% DATA.model(2).material.nu       =  0.3;          % Poisson ratio
% DATA.model(2).material.density  =  8800;         % density

%%%%%%%%
% MESH %
%%%%%%%%

L1 = 0.75;
L2 = 0.2;
% L31 = 0.2;

X1 = 0;
Y1 = 0;
% Z1 = 0;

Nx = 30;
Ny = 10;

DATA.model(1).L=[L1 L2];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1];
DATA.model(1).Ng=[Nx Ny];

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%

loadtimey = -.1 * DATA.params(2).mesh;

sym = 1;

if sym == 0
    %     DATA.load(1).type='displ';
    %     % DATA.load(1).box=[L1-L1/Nx L1 ;0 0+L2/Ny];
    %     DATA.load(1).box=[L1 L1 ;0 0];
    %
    %     DATA.load(1).val=zeros(DATA.dim,n(2),n(3),n(4));
    %
    %     DATA.load(2).type='displ';
    %     DATA.load(2).box=[0 0.0001 ;0 0.0001];
    %     for k = 1 : n(3)
    %         DATA.load(2).val(2,:,k)=zeros(size(loadtimey));
    %     end
    %     DATA.load(3).type='displ';
    %     DATA.load(3).box=[L1/2 L1/2 ;L2 L2];
    %
    %     for k = 1 : n(3)
    %         DATA.load(3).val(1,:,k) = zeros(size(loadtimey));
    %         DATA.load(3).val(2,:,k) = loadtimey;
    %     end
    %     % DATA.load(3).val=zeros(DATA.dim,n(2),n(3));
else
    
    DATA.load(1).type='displ';
    DATA.load(1).box=[0 0.0001 ;0 L2];
    DATA.load(1).val=zeros(DATA.dim,n(2),n(3),n(4));

    DATA.load(3).type='stress';
    DATA.load(3).box=[L1 L1 ;0 L2];
    for k = 1 : n(3)
        for kk = 1: n(4)
            DATA.load(3).val(1,:,k,kk) = zeros(size(loadtimey));
            DATA.load(3).val(2,:,k,kk) = DATA.params(3).mesh(k)*10e7*loadtimey;
        end
    end
    
    DATA.load(2).type='stress';
    DATA.load(2).box=[L1 L1 ;0 L2];
    for k = 1 : n(3)
        for kk = 1: n(4)
            DATA.load(2).val(2,:,k,kk) = zeros(size(loadtimey));
            DATA.load(2).val(1,:,k,kk) = 10*DATA.params(4).mesh(kk)*10e7*loadtimey;
        end
    end
end


%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end
