function DATA = test_2d_cylindrical_contact()

%
%    TEST CASE FILE: CONTACT  (Rafael's phd)
%		        (Vitse 2015)
%

DATA.dim        = 2;		   % Dimension 2d or 3d
DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'contact';      %'none', 'contact', 'reinforcement';
% 'none': just one model
% 'contact': for contact problems
% 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = 0:0.1:1; % step time
DATA.params(3).mesh = 1:5;       % ???

n=zeros(1,length(DATA.params));
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end

%%%%%%%%%%%%
% MATERIAL %
%%%%%%%%%%%%

DATA.model(1).type              = 'FE2D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  8800;         % density

DATA.model(2).type              = 'FE2D';
DATA.model(2).behavior.type     = 'elastic';
DATA.model(2).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(2).material.nu       =  0.3;          % Poisson ratio
DATA.model(2).material.density  =  8800;         % density

%%%%%%%%
% MESH %
%%%%%%%%

DATA.model(1).mesh = 'specimen.msh';
DATA.model(2).mesh = 'pad.msh';

%%%%%%%%%%%%%%%%%%%
% BC AND LOADINGS %
%%%%%%%%%%%%%%%%%%%

L11 = 40;
L21 = 10;

DATA.load(1).type='sym';
DATA.load(1).box=[-L11/2 L11/2;-L21 -L21];
DATA.load(1).val=zeros(1,n(2),n(3));
DATA.load(1).normal=2;

DATA.load(2).type='sym';
DATA.load(2).box=[-L11/2 -L11/2;0 -L21];
DATA.load(2).val=zeros(1,n(2),n(3));
DATA.load(2).normal=1;

DATA.load(3).type='sym';
DATA.load(3).box=[L11/2 L11/2;0 -L21];
DATA.load(3).val=zeros(1,n(2),n(3));
DATA.load(3).normal=1;

DATA.load(4).type='stress';
DATA.load(4).box=[-1 1;0 0];
DATA.load(4).box=[-L11/2 L11/2;0 0];
loadtimey = -200e3 * DATA.params(2).mesh;
for k = 1 : n(3)
    DATA.load(4).val(1,:,k) = zeros(size(loadtimey));
    DATA.load(4).val(2,:,k) = loadtimey;
end

DATA.load(5).type='displ';
DATA.load(5).box=[-L11/8 L11/8;(1+5) (1+5)];
loadtimey = -.1 * DATA.params(2).mesh;
for k = 1 : n(3)
    DATA.load(5).val(1,:,k) = zeros(size(loadtimey));
    DATA.load(5).val(2,:,k) = loadtimey;
end
DATA.load(5).val=zeros(DATA.dim,n(2),n(3));

DATA.load(6).type='stress';
DATA.load(6).box=[-L11/4 L11/4;(1-0.25) (1+0.25)];
loadtimey = 200e3 * DATA.params(2).mesh;
for k = 1 : n(3)
    DATA.load(6).val(1,:,k) = zeros(size(loadtimey));
    DATA.load(6).val(2,:,k) = loadtimey;
end

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end
