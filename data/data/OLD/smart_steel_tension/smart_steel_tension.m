function [DATA] = smart_reinforced_tension()
%
%    TEST CASE FILE: REINFORCED CONCRETE BEAM 
%                   (Vitse 2015)
%

DATA.dim=3;		% Dimension 2d or 3d
DATA.plst = 1;      % 1: PLane STrain and 0: PLane STress

DATA.assembly = 'reinforcement';   %'none', 'contact', 'reinforcement';
   % 'none': just one model
   % 'contact': for contact problems
   % 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:40];
DATA.params(3).mesh = [1:5];

%%%%%%%%
% DIMS %
%%%%%%%%

L11 = 3;
L21 = 0.25;
L31 = 0.30;

X1 = 0;
Y1 = 0;
Z1 = 0;

Nh = 1;

%%%%%%%%%%%%
% MODEL #1 %
%%%%%%%%%%%%

% DATA.model(1).type = 'FE3D';
%         % 'FE3D'
%         % 'FE2D'
%         % 'TRUSS'
% 
 DATA.model(1).L=[L11 L21 L31];			% Size of the domain (mm)
 DATA.model(1).O = [X1 Y1 Z1];
 DATA.model(1).Ng=[20*Nh 10 12];
% 
% DATA.model(1).material.E          = 26.5e9;                  % Young's Modulus
% DATA.model(1).material.nu         = 0.2;                     % Poisson ratio
% DATA.model(1).material.density    = 2550;                    % density
% DATA.model(1).material.ft         = 4.18e6;                   % 10.0e6;       % limit in tension MPa
% DATA.model(1).material.Y0         = (DATA.model(1).material.ft^2)/(2*DATA.model(1).material.E);  % initial threshold for damage activation  
% DATA.model(1).material.Ad         = 2e-4 ;                   % 5e-1;
% 
% DATA.model(1).behavior.type = 'damage'; %'damage';     
%         % 'damage'
%         % 'elasticity'
%         % 'elastoplasticity'
%         % 'viscoplasticity'
%         % 'plasticity'
% DATA.model(1).behavior.F_Y = @(Y,Y0,Adt) 1 - (1 + Adt * (Y - Y0) ).^-1;    % damage law (from [Richard,Ragueneau 12])
% DATA.model(1).behavior.dam_pg = 484;      % (num(elem)-1) * 8 + 4       WARNING !! NUMBERING FROM 0 !!!
% DATA.model(1).behavior.dam_coef = 1;
% 
% DATA.model(1).regularization.type = 'damage_delay';
% DATA.model(1).regularization.tauc = 10 * (DATA.params(2).mesh(2) - DATA.params(2).mesh(1));

%%%%%%%%%%%%
% MODEL #2 %
%%%%%%%%%%%%

DATA.model(1).type                = 'TRUSS';
DATA.model(1).mesh                = 'mesh_steel_small.avs';
DATA.model(1).diam                = 0.008;
DATA.model(1).section             = pi * (DATA.model(1).diam)^2 /4;
DATA.model(1).material.E          = 200e9;                   % Young's Modulus
DATA.model(1).material.nu         = 0.3;                     % Poisson ratio
DATA.model(1).material.density    = 7850;                    % density
DATA.model(1).behavior.type       = 'elastic';

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[0.0 (0.0 + DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; 0.0 L31];
DATA.load(1).val=zeros(3,n(2),n(3));
%DATA.load(1).normal=3;

DATA.load(2).type='sym';
DATA.load(2).box=[(L11-DATA.model(1).L(1) / DATA.model(1).Ng(1)) L11 ; 0.0 L21 ; 0.0 L31];
DATA.load(2).val=zeros(1,n(2),n(3));
DATA.load(2).normal=1;

for k = 1 : n(3)
    %loadtimey1= - (0.00001 + (1+DATA.params(3).mesh(k)/10)*DATA.params(2).mesh / (3e4) .*(sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end))).^2);
    %loadtimey1=  (0.00001 + (1+DATA.params(3).mesh(k)/10)*DATA.params(2).mesh / (3e4) .*(sin(4*pi*DATA.params(2).mesh/DATA.params(2).mesh(end)))) - DATA.params(2).mesh/200000;
    loadtimey1 = (0.00001 + (1+DATA.params(3).mesh(end)/10)*DATA.params(2).mesh/6e4);   
    DATA.load(2).val(1,:,k)= loadtimey1;
end




%dam_boxes: elastic boxes where the boundary conditions are imposed 
%(to avoid damage appearing there)
nb_elem_elast = 2;

DATA.load(1).dam_box=[0.0 (0.0 + nb_elem_elast*DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; 0.0 L31];
DATA.load(2).dam_box=[(L11- nb_elem_elast*DATA.model(1).L(1) / DATA.model(1).Ng(1)) L11 ; 0.0 L21 ; 0.0 L31];





%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

DATA = init_data(DATA); 

end
