function [DATA] = smart_steel()
%
%    TEST CASE FILE: REINFORCED CONCRETE BEAM 
%                   (Vitse 2015)
%

DATA.dim=3;		% Dimension 2d or 3d
DATA.plst = 1;      % 1: PLane STrain and 0: PLane STress

DATA.assembly = 'none';   %'none', 'contact', 'reinforcement';
   % 'none': just one model
   % 'contact': for contact problems
   % 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(3).name = 'mu';
DATA.params(2).mesh = [0:20];
DATA.params(3).mesh = [0:5];


%%%%%%%%
% DIMS %
%%%%%%%%

L11 = 3;
L21 = .25;
L31 = 0.30;

X1 = 0;
Y1 = 0;
Z1 = 0;

Nh = 1;

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[X1+0.0 X1+0.0 ; Y1+0.0 Y1+L21 ; Z1+0.0 Z1+L31];
DATA.load(1).val=zeros(DATA.dim,n(2),n(3));

DATA.load(2).type='displ';
DATA.load(2).box=[X1+3 X1+3 ; Y1+0 Y1+L21 ; Z1+0 Z1+L31];
DATA.load(2).val=zeros(DATA.dim,n(2),n(3));


for k = 1 : n(3)
    loadtimey1=0.0001 + (1+DATA.params(3).mesh(k)/10)*DATA.params(2).mesh / 5e3 .*sin(2*pi*DATA.params(2).mesh/DATA.params(2).mesh(end));
    DATA.load(2).val(3,:,k)= loadtimey1;
    %loadtimey2=0.0001 + (1+DATA.params(3).mesh(k)/10)*DATA.params(2).mesh / 5e4 .*sin(pi*DATA.params(2).mesh/DATA.params(2).mesh(end));
    %DATA.load(3).val(2,:,k)=loadtimey2;
end

%%%%%%%%%%%%
% MODEL #1 %
%%%%%%%%%%%%


DATA.model(1).type                = 'TRUSS';
DATA.model(1).mesh                = 'mesh_steel_small.avs';
DATA.model(1).diam                = 0.008;
DATA.model(1).section             = pi * (DATA.model(1).diam)^2 /4;
DATA.model(1).material.E          = 200e9;                   % Young's Modulus
DATA.model(1).material.nu         = 0.3;                     % Poisson ratio
DATA.model(1).material.density    = 7850;                    % density
DATA.model(1).behavior.type       = 'elastic';

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

DATA = init_data(DATA); 

end
