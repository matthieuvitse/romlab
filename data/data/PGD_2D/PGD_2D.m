function DATA = PGD_2D()

DATA.dim  = 2;		% Dimension 2d or 3d
DATA.plst = 1;      % 1: PLane STrain and 0: PLane STress

DATA.assembly = 'reinforcement';   %'none', 'contact', 'reinforcement';
   
%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(2).type = 'time';
DATA.params(2).unit = 's';       % if none, '-'
DATA.params(2).mesh = [0:99];
DATA.params(3).type = 'loading';
DATA.params(3).unit = '-';       % if none, '-'
DATA.params(3).mesh = [0:10];
DATA.params(4).type = 'loading';
DATA.params(4).unit = '-';       % if none, '-'
DATA.params(4).mesh = [-5:5];
%%%%%%%%
% DIMS %
%%%%%%%%

L11 = 600;
L21 = 280;
L31 = 0;

X1 = 0;
Y1 = 0;
Z1 = 0;

%%%%%%%%%%%%
% MODEL #1 %
%%%%%%%%%%%%

DATA.model(1).type = 'FE2D';
DATA.model(1).mesh                = 'MAIL2.avs';
DATA.model(1).L=[L11 L21 L31];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1 Z1];
DATA.model(1).material.E          = 1*10^9;               % Young's Modulus
DATA.model(1).material.nu         = 0.2;                   % Poisson ratio
DATA.model(1).material.density    = 2550;                 % density

DATA.model(1).behavior.type = 'elastic';        

%%%%%%%%%%%%
% MODEL #2 %
%%%%%%%%%%%%

DATA.model(2).type                = 'TRUSS';
DATA.model(2).mesh                = 'MAIL1.avs';
DATA.model(2).diam                = 10;
DATA.model(2).section             = pi * (DATA.model(2).diam)^2 /4;
DATA.model(2).material.E          = 10^(10);                   % Young's Modulus
DATA.model(2).material.nu         = 0.3;                     % Poisson ratio
DATA.model(2).material.density    = 7850;                    % density
DATA.model(2).behavior.type       = 'elastic';


for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

dist1 = 6;
DATA.load(1).type='displ';
DATA.load(1).box=[0.0 (0.0 + dist1); 0.0 L21 ; 0.0 L31];
DATA.load(1).val=zeros(2,n(2),n(3),n(4));


DATA.load(2).type='stress';
%DATA.load(2).box=[(L11-DATA.model(1).L(1) / DATA.model(1).Ng(1)) L11 ; 0.0 L21 ; 0.0 L31];
DATA.load(2).box=[L11 L11-dist1 ; 0.0 L21 ; 0.0 L31];
DATA.load(2).val=zeros(2,n(2),n(3),n(4));
DATA.load(2).normal=1;

%figure;hold on;
for i_param1 = 1 : numel(DATA.params(3).mesh)
    for i_param2 = 1 : numel(DATA.params(4).mesh)
        loadtimey1 = loading_saw(DATA.params(2).mesh,15,.5*(1+ DATA.params(3).mesh(i_param1)/5),-0.5e-3);
        %DATA.load(2).val(1,:,i_param1,i_param2)= 10000* DATA.params(4).mesh(i_param2)/10 * loadtimey1;
        DATA.load(2).val(1,:,i_param1,i_param2)= 1e10* DATA.params(4).mesh(i_param2)/10 * loadtimey1;
       %plot( 10000* DATA.params(4).mesh(i_param2)/10 * loadtimey1);
        
    end
end
%keyboard
% DATA.load(1).type='displ';
% DATA.load(1).box=[0.0 0.0; 0.0 L21];
% DATA.load(1).val=zeros(2,n(2));
% 
% DATA.load(2).type='displ';
% DATA.load(2).box=[L11 0.0; L11 L21];
% DATA.load(2).val=[50E-3*DATA.params(2).mesh;zeros(1,n(2))];
% 
% nb_motif_y = 10;
% Y_list = linspace(0,L21,nb_motif_y+1);
% Y = Lagrange(linspace(0,1,n(2)),linspace(0,1,nb_motif_y),true);
% for id = 2:(nb_motif_y+1)
%     DATA.load(id).type='displ';
%     DATA.load(id).box=[L11 Y_list(id-1);L11 Y_list(id)];
%     DATA.load(id).val=zeros(2,n(2));
%     DATA.load(id).val(1,:)= 50E-3*Y(id-1,:);
% end

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA); 


% Function
% function Y=Lagrange(X,Base,Norm)
% if nargin<3
%     Norm=false;
% end
% Y=zeros(length(X),length(Base));
% Poly=LagrangePoly(Base);
% for idX=1:length(X)
%     for idBase=1:length(Base)
%         Y(idX,idBase)=Poly{idBase}(X(idX));
%     end
% end
% 
% if Norm
%     Y=Y';
%     Y=Y./repmat(max(abs(Y)),size(Y,1),1);
% end
% 
% 
% function Poly=LagrangePoly(Base)
% Poly=cell(length(Base),1);
% for idx=1:size(Poly,1)
%     list=1:size(Poly,1);
%     list(idx)=[];
%     Poly{idx}=@(X) prod(arrayfun(@(idy) (X-Base(idx))/(Base(idy)-Base(idx)),list));
% end
% end
% end

end