function i = input_ROMlab()
% input parameters of the algorithm
%
i = default();

testcase = evalin('caller','testcase');

%%%%%%%%%
% INPUT %
%%%%%%%%%

    % name of the file to load
    % user comment for naming the file
    % 'LATIN', 'DIRECT', 'CONTACT'
    
i.name   = sprintf('MyKriging%03d',testcase);
i.test   = 'MyKriging';
i.solver   = 'DIRECT';

i.debug          = 0;               % sets up debug options like profile, diary, export along iterations, dbstop if warning or errors, ...

% parallel computing related stuff
i.parallel       = 0;               % 1 to enable parfor loops within code, 0 for serial

% PGD related stuff
i.pgd            = 1;               % PGD resolution of the linear stage (0: no ; 1: yes)
i.orthog         = 1;               % orthogonalization of the PGD basis
i.update_pgd     = 1;
i.crit_update    = 0.8;             % update threshold

% LATIN related stuff
i.MPS            = 0;               
i.rank_trunc_MPS = 0;               % rank of the truncated solution
i.sol_init_MPS   = 'toto.mat';      % file in which the initial solution is stored (in MPS/)
i.max_iter       = 3;               % max number of iteration
i.indic_target   = 1e-12;           % error indicator target for convergence
i.relax          = 1;               % relaxation parameter
i.update_sd      = 0;               % update of the search direction every N iterations

% damage related stuff
i.regularization = 1;               % 0 for no ref, 1 for damage-delay, 2 for nonlocal integral (not implemented yet)
i.unilateral     = 1;               % unilateral effect: crack closing when in compression
i.var_young      = 1;               % enable variability on the young modulus over the specimen (at Gauss point level)
i.var_young_val  = 10;              % max variability (in %)

%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%

i.plot_mesh      = 0;               % write mesh file
i.plot_bc        = 0;
i.plot_sol       = 0;               % write solution file
i.plot_sigeps    = 0;               % compute stress and strain
i.plot_sig_ps    = 0;               % compute principal stresses
i.plot_sig_vm    = 0;               % compute Von Mises stresss
i.plot_damage    = 0;
i.plot_var_E     = 0;
i.exp_pv         = 0;               % standard paraview export
i.exp_pv_name    = 'output';
i.exp_pxdmf      = 0;               % pxdmf export for separated fields

end
