function DATA = beam_reinforced_flexture_2()

DATA.dim  = 3;		% Dimension 2d or 3d
DATA.plst = 1;      % 1: PLane STrain and 0: PLane STress

DATA.assembly = 'reinforcement';   %'none', 'contact', 'reinforcement';
   % 'none': just one model
   % 'contact': for contact problems
   % 'reinforcement': reinforced structures

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(2).type = 'time';
DATA.params(2).unit = 's';       % if none, '-'
DATA.params(2).mesh = [0:99];

%%%%%%%%
% DIMS %
%%%%%%%%

L11 = 3;
L21 = .25;
L31 = 0.30;

X1 = 0;
Y1 = 0;
Z1 = 0;

Nh = 2;

%%%%%%%%%%%%
% MODEL #1 %
%%%%%%%%%%%%


DATA.model(1).type = 'FE3D';
        % 'FE3D'
        % 'FE2D'
        % 'TRUSS'

DATA.model(1).L =[L11 L21 L31];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1 Z1];
DATA.model(1).Ng=[20*Nh 10*Nh 6*Nh];

DATA.model(1).material.E          = 25.42e9;                  % Young's Modulus
DATA.model(1).material.nu         = 0.2;                     % Poisson ratio
DATA.model(1).material.density    = 2550;                    % density
DATA.model(1).material.ft         = 3.6e6;                   % 10.0e6;       % limit in compression MPa
DATA.model(1).material.Y0         = (DATA.model(1).material.ft^2)/(2*DATA.model(1).material.E);  % initial threshold for damage activation  
DATA.model(1).material.Ad         = 2.25e-3;

DATA.model(1).behavior.type = 'damage';        
        % 'damage'
        % 'elasticity'
        % 'elastoplasticity'
        % 'viscoplasticity'
        % 'plasticity'
DATA.model(1).behavior.F_Y = @(Y,Y0,Adt) 1 - (1 + Adt * (Y - Y0) ).^-1;    % damage law (from [Richard,Ragueneau 12])
DATA.model(1).behavior.dam_pg = 484;      % (num(elem)-1) * 8 + 4       WARNING !! NUMBERING FROM 0 !!!
DATA.model(1).behavior.dam_coef = 1;

DATA.model(1).regularization.type = 'damage_delay';
DATA.model(1).regularization.tauc = 10 * (DATA.params(2).mesh(2) - DATA.params(2).mesh(1));

DATA.model(1).substepping.on   = 1;
DATA.model(1).substepping.step = 1;

%%%%%%%%%%%%
% MODEL #2 %
%%%%%%%%%%%%

DATA.model(2).type                = 'TRUSS';
DATA.model(2).mesh                = 'mesh_steel_small.avs';
DATA.model(2).diam                = 0.008;
DATA.model(2).section             = pi * (DATA.model(2).diam)^2 /4;
DATA.model(2).material.E          = 185.35e9;                   % Young's Modulus
DATA.model(2).material.nu         = 0.3;                     % Poisson ratio
DATA.model(2).material.density    = 7850;                    % density
DATA.model(2).behavior.type       = 'elastic';

%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[0.0 (0.0 + DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; 0.0 0];
DATA.load(1).val=zeros(3,n(2));
DATA.load(1).normal = 3;

DATA.load(2).type='sym';
DATA.load(2).box=[DATA.model(1).L(1) (DATA.model(1).L(1) - DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; 0.0 0];
DATA.load(2).val=zeros(1,n(2));
DATA.load(2).normal = 3;

DATA.load(3).type='sym';
DATA.load(3).box=[0.90 (0.90 + DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; L31 L31];
DATA.load(3).val=zeros(1,n(2));
DATA.load(3).normal = 3;

DATA.load(4).type='sym';
DATA.load(4).box=[2.10 (2.10 - DATA.model(1).L(1) / DATA.model(1).Ng(1)) ; 0.0 L21 ; L31 L31];
DATA.load(4).val=zeros(1,n(2));
DATA.load(4).normal = 3;


%loadtimey1 = (1 + 3/10) * 5e-4 * (1+DATA.params(2).mesh/DATA.params(2).mesh(end)) .* (sin(2*pi*DATA.params(2).mesh/(DATA.params(2).mesh(end))));
loadtimey1 = -loading_saw(DATA.params(2).mesh,15,.5*1.5,0);
DATA.load(3).val(1,:)= 3/5 * loadtimey1;
DATA.load(4).val(1,:)= 3/5 * loadtimey1;

% dam_boxes: elastic boxes where the boundary conditions are imposed (to avoid damage appearing there)
nb_elem_elast = 1;   % depth of the box (number of elements)

DATA.load(1).dam_box=[DATA.load(1).box(1,1)  DATA.load(1).box(1,2) ; DATA.load(1).box(2,1) DATA.load(1).box(2,2) ; DATA.load(1).box(3,1) nb_elem_elast*DATA.model(1).L(3)/DATA.model(1).Ng(3)];
DATA.load(2).dam_box=[DATA.load(2).box(1,1)  DATA.load(2).box(1,2) ; DATA.load(2).box(2,1) DATA.load(2).box(2,2) ; DATA.load(2).box(3,1) nb_elem_elast*DATA.model(1).L(3)/DATA.model(1).Ng(3)];

DATA.load(3).dam_box=[DATA.load(3).box(1,1)  DATA.load(3).box(1,2) ; DATA.load(3).box(2,1) DATA.load(3).box(2,2) ; DATA.load(3).box(3,1) DATA.load(3).box(3,1) - nb_elem_elast*DATA.model(1).L(3)/DATA.model(1).Ng(3)];
DATA.load(4).dam_box=[DATA.load(4).box(1,1)  DATA.load(4).box(1,2) ; DATA.load(4).box(2,1) DATA.load(4).box(2,2) ; DATA.load(4).box(3,1) DATA.load(4).box(3,1) - nb_elem_elast*DATA.model(1).L(3)/DATA.model(1).Ng(3)];


%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA); 

end
