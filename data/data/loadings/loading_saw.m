function [ud] = loading_saw(lT,nsub,var1,min_val)
% Example: ud=loading_saw([0:99],8);

ud = zeros(1,numel(lT));

fac0 = 0; %0.05;
for i = 1 : nsub
    fac(i) = (1 + fac0)^i;
end

fac2 = var1 * .00005;



tata = round(lT(end)/nsub);

ud(1) = 0;
cyc = 0;
for i = 2 : numel(lT)

    if cyc == 0
        ud(i) = ud(i-1) - fac2 * fac(1);
        
        if ud(i) < min_val
            ud(i) = ud(i-1) + fac2 * fac(1);
            cyc= 1;
        end
    elseif cyc == 1
        ud(i) = ud(i-1) + fac2 * fac(1) ;
        
        if ud(i) > nsub * .25e-4
            ud(i) = ud(i-1) - fac2 * fac(1);
            cyc= 2;
        end
    elseif cyc == 2
        ud(i) = ud(i-1) - fac2 * fac(1);
        if ud(i) < min_val
            ud(i) = ud(i-1) + fac2 * fac(1) ;
            cyc=  3;
        end
    elseif cyc == 3
        ud(i) = ud(i-1) + fac2 * fac(1);
        if ud(i) > nsub * .5e-4
            ud(i) = ud(i-1) - fac2 * fac(1) ;
            cyc= 4;
        end
    elseif cyc == 4
        ud(i) = ud(i-1) - fac2 * fac(1);
        if ud(i) < min_val
            ud(i) = ud(i-1) + fac2 * fac(1);
            cyc=  5;
        end
    elseif cyc == 5
        ud(i) = ud(i-1) + fac2 * fac(1);
        if ud(i) > nsub * .75e-4
            ud(i) = ud(i-1) - fac2 * fac(1) ;
            cyc= 6;
        end
    elseif cyc == 6
        ud(i) = ud(i-1) - fac2 * fac(1) ;
        if ud(i) < min_val
            ud(i) = ud(i-1) + fac2 * fac(1) ;
            cyc=  7;
        end
    elseif cyc == 7
        ud(i) = ud(i-1) + fac2 * fac(1) ;
        if ud(i) > nsub * 1e-4
            ud(i) = ud(i-1) - fac2 * fac(1) ;
            cyc=  8;
        end
    elseif cyc == 8
        ud(i) = ud(i-1) - fac2 * fac(1) ;
        if ud(i) < min_val
            ud(i) = ud(i-1) + fac2 * fac(1) ;
            cyc=  9;
        end
    elseif cyc == 9
        ud(i) = ud(i-1) + fac2 * fac(1) ;
        if ud(i) > nsub * 1.25e-4
            ud(i) = ud(i-1) - fac2 * fac(1) ;
            cyc=  10;
        end
    else
        ud(i) = ud(i-1) - fac2 * fac(1) ;
        
    end
    
    % for i = 2 : numel(lT)
    %     if i < tata
    %         ud(i) = - fac2 * (fac(1) * i);
    %     elseif i < 2*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(2) * 1);
    %     elseif i < 3.5*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(3) * 1);
    %     elseif i < 5*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(4) * 1);
    %     elseif i < 7*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(5) * 1);
    %     elseif i < 9*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(6) * 1);
    %     elseif i < 11.5*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(7) * 1);
    %     elseif i < 14*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(7) * 1);
    %     elseif i < 17*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(7) * 1);
    %     elseif i < 20*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(7) * 1);
    %     elseif i < 23.5*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(7) * 1);
    %     elseif i < 27*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(7) * 1);
    %     elseif i < 31*tata-1
    %         ud(i) = ud(i-1) - fac2 * (fac(7) * 1);
    %     elseif i < 35*tata-1
    %         ud(i) = ud(i-1) + fac2 * (fac(7) * 1);
    %     else
    %         ud(i) = ud(i-1) - fac2 * (fac(8) * 1);
    %     end
    %end
    
    %figure;plot(ud,'-or')
    
end