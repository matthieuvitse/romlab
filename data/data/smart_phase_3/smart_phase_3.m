function DATA = smart_phase_3()

DATA.dim  = 3;		% Dimension 2d or 3d
DATA.plst = 1;      % 1: PLane STrain and 0: PLane STress

DATA.assembly = 'reinforcement';   %'none', 'contact', 'reinforcement';
   % 'none': just one model
   % 'contact': for contact problems
   % 'reinforcement': reinforced structures
   
%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';
DATA.params(2).name = 't';
DATA.params(2).type = 'time';
DATA.params(2).unit = 's';
loading_1 = load('data/data/smart_phase_3/loading_smart_phase3_y.mat');
loading_2 = load('data/data/smart_phase_3/loading_smart_phase3_z.mat');
DATA.params(2).mesh = [1:numel(loading_1.a32)];
DATA.params(3).name = 'mu1';
DATA.params(3).type = 'loading';
DATA.params(3).unit = '-';
DATA.params(3).mesh = [-5:5];

%%%%%%%%
% DIMS %
%%%%%%%%

X1 = 0;
Y1 = 0;
Z1 = 0;

%%%%%%%%%%%%
% MODEL #1 %
%%%%%%%%%%%%

DATA.model(1).type = 'FE3D';
        % 'FE3D'
        % 'FE2D'
        % 'TRUSS'

DATA.model(1).O  = [X1 Y1 Z1];
DATA.model(1).mesh  = 'mesh.avs';

DATA.model(1).material.E          = 25.42e9;                  % Young's Modulus
DATA.model(1).material.nu         = 0.2;                     % Poisson ratio
DATA.model(1).material.density    = 2550;                    % density
DATA.model(1).material.ft         = 3.6e6;                   % 10.0e6;       % limit in compression MPa
DATA.model(1).material.Y0         = (DATA.model(1).material.ft^2)/(2*DATA.model(1).material.E);  % initial threshold for damage activation  
DATA.model(1).material.Ad         = 2.25e-3 / 35; %15;

warning('off','backtrace')
warning('parameter Ad has been weighted for this example');
warning('on','backtrace')

DATA.model(1).behavior.type = 'damage';        
        % 'damage'
        % 'elasticity'
        % 'elastoplasticity'
        % 'viscoplasticity'
        % 'plasticity'
DATA.model(1).behavior.F_Y      = @(Y,Y0,Adt) 1 - (1 + Adt * (Y - Y0) ).^-1;    % damage law (from [Richard,Ragueneau 12])
DATA.model(1).behavior.dam_pg   = 484;      % (num(elem)-1) * 8 + 4       WARNING !! NUMBERING FROM 0 !!!
DATA.model(1).behavior.dam_coef = 1;

DATA.model(1).regularization.type = 'damage_delay';
DATA.model(1).regularization.tauc = 10 * (DATA.params(2).mesh(2) - DATA.params(2).mesh(1));

DATA.model(1).substepping.on   = 1;
DATA.model(1).substepping.step = 1;

%%%%%%%%%%%%
% MODEL #2 %
%%%%%%%%%%%%

DATA.model(2).type                = 'TRUSS';
DATA.model(2).mesh                = 'mesh_steel.avs';
DATA.model(2).diam                = 0.008;
DATA.model(2).section             = pi * (DATA.model(2).diam)^2 /4;
DATA.model(2).material.E          = 185.35e9;                   % Young's Modulus
DATA.model(2).material.nu         = 0.3;                     % Poisson ratio
DATA.model(2).material.density    = 7850;                    % density
DATA.model(2).behavior.type       = 'elastic';

for i = 2 : length(DATA.params)
    n(i) = length(DATA.params(i).mesh);
end

DATA.load(1).type='displ';
DATA.load(1).box=[-0.12 0 ; 0.0 0.12 ; 1.045 1.26];
DATA.load(1).val=zeros(3,n(2),n(3));

DATA.load(2).type='displ';
DATA.load(2).box=[-0.12 0 ; 0.0 0.12 ; -1.14 -0.925];
DATA.load(2).val=zeros(3,n(2),n(3));

DATA.load(3).type='displ';
DATA.load(3).box=[0.85 1 ; 0.0 0.12 ; 0 0.12];
DATA.load(3).val=zeros(3,n(2),n(3));

for i_p3 = 1 : numel(DATA.params(3).mesh)
    meshk = DATA.params(3).mesh(i_p3);
    loadtimey = 1e-3  * loading_1.a32;
    loadtimez = 1e-3 * (1 + meshk / (5*numel(DATA.params(3).mesh)) ) * loading_2.a31;
    DATA.load(3).val(2,:,i_p3)= loadtimey;
    DATA.load(3).val(3,:,i_p3)= loadtimez;
end

% dam_boxes: elastic boxes where the boundary conditions are imposed (to avoid damage appearing there)
DATA.load(1).dam_box=DATA.load(1).box;
DATA.load(2).dam_box=DATA.load(2).box;
DATA.load(3).dam_box=DATA.load(3).box;

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA); 

end
