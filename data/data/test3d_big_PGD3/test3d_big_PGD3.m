function DATA = Test3d_big_PGD3()
%
%    TEST CASE FILE: 3d big PGD
%


DATA.dim        = 3;		   % Dimension 2d or 3d
%DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'none';      %'none', 'contact', 'reinforcement';

%%%%%%%%
% MESH %
%%%%%%%%

L1 = 3;
L2 = 3;
L3 = 3;

X1 = 0;
Y1 = 0;
Z1 = 0;

Nx = 6;
Ny = 6;
Nz = 6;

% Nx = 15;
% Ny = 15;
% Nz = 15;

% Nx = 21;
% Ny = 21;
% Nz = 21;

% Home made mesher for rectangular domains only...
DATA.model(1).L=[L1 L2 L3];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1 Z1];     % Origine
DATA.model(1).Ng=[Nx Ny Nz];      % number of elements

% ...or use mesh readers

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%


DATA.model(1).type              = 'FE3D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  80;         % density


%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';

% Material parameters
borne = 0.5;
res = 0.1;    % resolution

DATA.variablemat=1;
Ndec = 3;
iparam = 2;
for a1 = 1:Ndec
    for a2 = 1:Ndec
        for a3 = 1:Ndec
            DATA.params(iparam).name = strcat('E',num2str(iparam-1));
            DATA.params(iparam).type = 'young';
            DATA.params(iparam).unit = 'GPa';
            DATA.params(iparam).box = [(a1-1)* L1/Ndec  , a1* L1/Ndec ; ...
                (a2-1)* L2/Ndec  , a2* L2/Ndec ; (a3-1)* L3/Ndec  , a3* L3/Ndec ];
            DATA.params(iparam).mesh = [-borne:res:borne];
            DATA.params(iparam).val = .5*DATA.params(iparam).mesh;
            
            iparam = iparam+1;
        end
    end
end

% Particular Value to compute error :

DATA.particular = zeros(3,length(DATA.params)-1);
for i=1:(length(DATA.params)-1)
    DATA.particular(1,i)=2;
    if mod(i,2)==0
        DATA.particular(2,i)=2;
    else
        DATA.particular(2,i)=10;
    end
    
    DATA.particular(3,i)=2;
end
    DATA.particular(1,length(DATA.params)-1)=10;
    DATA.particular(3,1)=10;
        
%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

n=zeros(1,length(DATA.params));
n(1) = DATA.dim;
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end


DATA.load(1).type='displ';
DATA.load(1).box=[0 0 ; 0 3 ; 0 3];
% DATA.load(1).val=[0 ; 0 ; 0];
for i = 1 : length(n)
    fooU{i} = zeros(n(i),1);
end
DATA.load(1).val=ktensor(fooU);
% keyboard

DATA.load(2).type='stress';
DATA.load(2).box=[(3-0.01) (3+0.01) ; 0 3 ; 0 3];
%DATA.load(2).val=sptensor(n);
% DATA.load(2).val=[10000 ; 0; 0];
for i = 1 : length(n)
    fooF{i} = ones(n(i),1);
end
fooF{1} = [ 1 0 0 ]';

% lt = DATA.params(2).mesh; % If time dependency 
% fooF{2} = sin(2*pi*lt/lt(end))';
DATA.load(2).val=ktensor(fooF);

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end

% %%%%%%%%%%
% % OUTPUT %
% %%%%%%%%%%
% 
% DATA.plot_mesh=1;       % write mesh file
% DATA.plot_sol=1;        % write solution file
% DATA.plot_sigeps=1;     % compute stress and strain
% DATA.plot_vonmises=1;   % compute principal stresses
