function DATA = Test3d_big_PGD_2_param()
%
%    TEST CASE FILE: 3d big PGD
%


DATA.dim        = 3;		   % Dimension 2d or 3d
%DATA.plst       = 1;           % 1: PLane STrain and 0: PLane STress
DATA.assembly   = 'none';      %'none', 'contact', 'reinforcement';

%%%%%%%%
% MESH %
%%%%%%%%

L1 = 4;
L2 = 4;
L3 = 4;

X1 = 0;
Y1 = 0;
Z1 = 0;

Nx = 4;
Ny = 4;
Nz = 4;

% Home made mesher for rectangular domains only...
DATA.model(1).L=[L1 L2 L3];			% Size of the domain (mm)
DATA.model(1).O = [X1 Y1 Z1];     % Origine
DATA.model(1).Ng=[Nx Ny Nz];      % number of elements

% ...or use mesh readers

%%%%%%%%%%%%%
% MATERIALS %
%%%%%%%%%%%%%


DATA.model(1).type              = 'FE3D';
DATA.model(1).behavior.type     = 'elastic';
DATA.model(1).material.E        =  200e3;        % Young's Modulus [MPa]
DATA.model(1).material.nu       =  0.3;          % Poisson ratio
DATA.model(1).material.density  =  80;         % density


%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

DATA.params(1).name = 'M';

% Material parameters

res = 5;    % resolution

DATA.variablemat=1;
DATA.params(2).name = 'E1';
DATA.params(2).type = 'young';
DATA.params(2).unit = 'GPa';
DATA.params(2).box = [0 4 ; 0 2 ; 0 4];
DATA.params(2).mesh = [-res:res];
DATA.params(2).val = .1*DATA.params(2).mesh;
DATA.params(3).name = 'E2';
DATA.params(3).type = 'young';
DATA.params(3).unit = 'GPa';
DATA.params(3).box = [0 4 ; 2 4 ; 0 4];
DATA.params(3).mesh = [-res:res];
DATA.params(3).val = .1*DATA.params(3).mesh;

DATA.params(2).target = 'Young';
DATA.params(3).target = 'Young';
% Particular Value to compute error :

DATA.particular=[ 1 10 ; 10 1]; % Particular values of Ei = E0
        
%%%%%%%%%%%
% LOADING %
%%%%%%%%%%%

n=zeros(1,length(DATA.params));
n(1) = DATA.dim;
for i = 2 : size(n,2)
    n(i) = length(DATA.params(i).mesh);
end


DATA.load(1).type='displ';
DATA.load(1).box=[0 0 ; 0 4 ; 0 4];
% DATA.load(1).val=[0 ; 0 ; 0];
for i = 1 : length(n)
    fooU{i} = zeros(n(i),1);
end
DATA.load(1).val=ktensor(fooU);
% keyboard

DATA.load(2).type='stress';
DATA.load(2).box=[(4-0.01) (4+0.01) ; 0 4 ; 0 4];
%DATA.load(2).val=sptensor(n);
% DATA.load(2).val=[10000 ; 0; 0];
for i = 1 : length(n)
    fooF{i} = ones(n(i),1);
end
fooF{1} = [ 1 0 0 ]';

% lt = DATA.params(2).mesh; % If time dependency 
% fooF{2} = sin(2*pi*lt/lt(end))';
DATA.load(2).val=ktensor(fooF);

%%%%%%%%%%%%%
% INIT DATA %
%%%%%%%%%%%%%

[DATA] = init_data(DATA);

end

% %%%%%%%%%%
% % OUTPUT %
% %%%%%%%%%%
% 
% DATA.plot_mesh=1;       % write mesh file
% DATA.plot_sol=1;        % write solution file
% DATA.plot_sigeps=1;     % compute stress and strain
% DATA.plot_vonmises=1;   % compute principal stresses
