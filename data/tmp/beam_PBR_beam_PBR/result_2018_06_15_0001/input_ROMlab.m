function i = input_ROMlab()
% input parameters of the algorithm
%
i = default();
%
%%%%%%%%%
% INPUT %
%%%%%%%%%
%
testcase = 666;   %14
switch testcase
%
    % name of the file to load
    % user comment for naming the file
    % 'LATIN', 'DIRECT', 'CONTACT'
%
    case 666
        i.name = 'beam_PBR';
        i.test = 'beam_PBR';
        i.solver= 'LATIN';
    case 0
        i.name = 'TEST_beam_reinforced_tension_3';
        i.test = 'TEST';
        i.solver= 'LATIN';
    case 1
        i.name   = 'beam_reinforced_tension_2';
        i.test   = 'tension_2_param';
        i.solver = 'LATIN';
    case 2
        i.name   = 'beam_reinforced_tension_3';
        i.test   = 'tension_3_param';
        i.solver = 'LATIN';
    case 3
        i.name   = 'beam_reinforced_tension_4';
        i.test   = 'tension_4_param';
        i.solver = 'LATIN';
    case 11
        i.name   = 'beam_reinforced_bending_2';
        i.test   = 'bending_2_param';
        i.solver = 'LATIN';
    case 12
        i.name   = 'beam_reinforced_bending_3';
        i.test   = 'bending_3_param';
        i.solver = 'LATIN';
    case 13
        i.name   = 'beam_reinforced_bending_4';
        i.test   = 'bending_4_param';
        i.solver = 'LATIN';
    case 14
        i.name   = 'beam_reinforced_bending_4_V2';
        i.test   = 'bending_4_param_v2';
        i.solver = 'LATIN'; 
    case 15
        i.name   = 'beam_reinforced_bending_4_V3';
        i.test   = 'bending_4_param_v3';
        i.solver = 'LATIN';
    case 21
        i.name   = 'beam_reinforced_tension_2_MPS';
        i.test   = 'tension_2_param';
        i.solver = 'LATIN';
    case 22
        i.name   = 'beam_reinforced_tension_3_MPS';
        i.test   = 'tension_3_param';
        i.solver = 'LATIN';
    case 23
        i.name   = 'beam_reinforced_tension_4_MPS';
        i.test   = 'tension_4_param';
        i.solver = 'LATIN';
    case 101
        i.name   = 'flexion_2D_3';
        i.test   = 'flexion_poutre';
        i.solver = 'PGD';
    case 102
        i.name   = 'flexion_2D_4';
        i.test   = 'flexion-traction_poutre';
        i.solver = 'PGD';
    case 103
        i.name   = 'cylinder_contact_slip';
        i.test   = 'fretting';
        i.solver = 'CONTACT';
    case 104
        i.name   = 'fretting_2D';
        i.test   = 'fretting_2D';
        i.solver = 'DIRECT';
    case 200
        i.name   = 'test3d_big_PGD2';       % 64 param  
        i.test   = 'big_PGD';       % user comment for naming the file
        i.solver = 'bigPGD';           % 'LATIN', 'DIRECT', 'CONTACT' 
    case 201
        i.name   = 'test3d_big_PGD_1_param';       % 1 param
        i.test   = 'big_PGD';       % user comment for naming the file
        i.solver = 'bigPGD';           % 'LATIN', 'DIRECT', 'CONTACT' 
    case 202
        i.name   = 'test3d_big_PGD_2_param';       % 2 param  
        i.test   = 'big_PGD';       % user comment for naming the file
        i.solver = 'bigPGD';           % 'LATIN', 'DIRECT', 'CONTACT' 
    case 208
        i.name   = 'test3d_big_PGD';       % 8 param
        i.test   = 'big_PGD';       % user comment for naming the file
        i.solver = 'bigPGD';           % 'LATIN', 'DIRECT', 'CONTACT'  
    case 227
        i.name   = 'test3d_big_PGD3';       % 27 param
        i.test   = 'big_PGD';       % user comment for naming the file
        i.solver = 'bigPGD';           % 'LATIN', 'DIRECT', 'CONTACT'  
    case 301
        i.name   = 'PGD_2D';
        i.test   = 'PGD_2D';
        i.solver = 'PGD';
    case 402
        i.name   = 'half_beam_reinforced_bending_3';
        i.test   = 'half_beam';
        i.solver = 'LATIN';
    case 403
        i.name   = 'half_beam_reinforced_bending_4';
        i.test   = 'half_beam';
        i.solver = 'LATIN';
    case 502
        i.name   = 'smart_tens_3';
        i.test   = 'smart_tens';
        i.solver = 'LATIN';
    case 601
        i.name   = 'smart_phase_1';
        i.test   = 'smart_phase_1';
        i.solver = 'LATIN';
    case 602
        i.name   = 'smart_phase_2';
        i.test   = 'smart_phase_2';
        i.solver = 'LATIN';
    case 603
        i.name   = 'smart_phase_3';
        i.test   = 'smart_phase_3';
        i.solver = 'LATIN';
     case 702
        i.name   = 'smart_bending_plane_3';
        i.test   = 'smart_bending_plane_3';
        i.solver = 'LATIN';
     case 1000
	     i.name = 'profile_tension_4';
	     i.test = 'no_parallel';
	     i.solver = 'LATIN';
     case 1001
         i.name = 'profile_bending_4';
	     i.test = 'profile';
	     i.solver = 'LATIN';
     case 2001
         i.name = 'bending_LATIN_increm';
	     i.test = 'increm';
	     i.solver = 'LATIN';   
     case 3001
         i.name = 'PAMSIM';
	     i.test = 'PAMSIM';
	     i.solver = 'DIRECT'; 
end
%
i.debug          = 0;               % sets up debug options like profile, diary, export along iterations, dbstop if warning or errors, ...
%
% parallel computing related stuff
i.parallel       = 1;               % 1 to enable parfor loops within code, 0 for serial
%
% 
% PGD related stuff
i.pgd            = 1;               % PGD resolution of the linear stage (0: no ; 1: yes)
i.orthog         = 1;               % orthogonalization of the PGD basis
i.update_pgd     = 1;
i.crit_update    = 0.8;             % update threshold
%
% LATIN related stuff
i.MPS            = 0;
i.rank_trunc_MPS = 0;               % rank of the truncated solution
i.sol_init_MPS   = 'toto.mat';      % file in which the initial solution is stored (in MPS/)
i.max_iter       = 12;               % max number of iteration
i.indic_target   = 1e-12;           % error indicator target for convergence
i.relax          = 1;              % relaxation parameter
i.update_sd      = 0;               % update of the search direction every N iterations
%
% damage related stuff
i.regularization = 1;               % 0 for no ref, 1 for damage-delay, 2 for nonlocal integral (not implemented yet)
i.unilateral     = 1;               % unilateral effect: crack closing when in compression
i.var_young      = 1;               % enable variability on the young modulus over the specimen (at Gauss point level)
i.var_young_val  = 10;               % max variability (+/- X %)

%BigPGD related stuff
i.spatial     =  'fixedP';            % Computation of spacial modes : 'fixedP' = point fixe ; 'eigPb' = eigenvalue problem 
i.tiroir        = 1;                   % Use of "tiroirs" in the fixed point algorith   
i.nkrylov    =1;                    % Number of vector in each Krylov Basis
i.ngroup    =9;                    % if tiroir : number of tiroirs
i.majKrylov = 1;                  % nomber of global iterations btw 2 computations of a new Krylov Basis
i.Rpgd        = 0;                  % =1, use of the pgd approximation of the residual at each iteration
i.modeperX = 3;                % number of parametric functions for each spatial mode
% 
i.rez_target = 0.0001;       % Objective Residual
i.Giter         =2;                % max number of global iterations
i.fixe_pointK =10;                 %  max number of iterations for fixed points algorithm : Krylov basis
i.fixe_pointP =5;                 %  max number of iterations : Param computation
i.stagn_crit =1e-3;            %   Stagnation criterion for fixed points algorithm
% 
%%%%%%%%%%
% OUTPUT %
%%%%%%%%%%
%
i.plot_mesh      = 1;               % write mesh file
i.plot_bc        = 1;
i.plot_sol       = 1;               % write solution file
i.plot_sigeps    = 1;               % compute stress and strain
i.plot_sig_ps    = 0;               % compute principal stresses
i.plot_sig_vm    = 0;               % compute Von Mises stresss
i.plot_damage    = 1;
i.plot_var_E     = 1;
i.exp_pv         = 1;               % standard paraview export
i.exp_pv_name    = 'output';
i.exp_pxdmf      = 1;               % pxdmf export for separated fields
%
end
