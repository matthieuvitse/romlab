try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

# GetSource()

# find view
renderView1 = FindViewOrCreate('RenderView1', viewtype='RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [2556, 1316]

# set active view
SetActiveView(renderView1)


f = open('/u/vitse/Documents/conf_petrus/2/loading.txt','r')
pos1 = float(f.readline())
pos2 = float(f.readline())

# find source
cylinder3 = FindSource('Cylinder3')
cylinder4 = FindSource('Cylinder4')
box1 = FindSource('Box1')
box2 = FindSource('Box2')
# set active source
# SetActiveSource(cylinder3)

act_source = FindSource('output_..pvd')   #GetActiveSource()

startframe = 0
endframe = len(act_source.TimestepValues)-1
timestep_list = act_source.TimestepValues
timestep = timestep_list[0]
endtime = len(timestep_list)
starttime = timestep

AnimationScene1 = GetAnimationScene()

AnimationScene1.EndTime = endtime
AnimationScene1.AnimationTime = starttime
AnimationScene1.PlayMode = 'Snap To TimeSteps'                            
AnimationScene1.StartTime = starttime

for i in range(startframe,endframe):     #(0,len(timestep_list)):     #
    
    timestep = timestep_list[i]
    time = timestep
    renderView1.ViewTime = time
    renderView1.UseCache = 0
    renderView1.CacheKey = time
    
    AnimationScene1.AnimationTime = time
    
    warp_fac = 30
    
    # Properties modified on cylinder3 and 4
    upt = float(f.readline())
    cylinder3.Center = [pos1, 0.125, 0.35 + upt*warp_fac]
    cylinder4.Center = [pos2, 0.125, 0.35 + upt*warp_fac]
    box1.Center      = [1.5, 0.125, 0.5 + upt*warp_fac]
    box2.Center      = [1.5, 0.125, 0.425 + upt*warp_fac]
    
    pic_nbr = "%04d" % (timestep,)
    name = '/u/vitse/Documents/conf_petrus/2/pics/results' + str(pic_nbr) + '.png'
    print(name)
    Render()
    
    viewLayout1 = GetLayout()

    # save screenshot
    SaveScreenshot(name, layout=viewLayout1, magnification=1, quality=100)