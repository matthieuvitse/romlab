#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# find source
output_1pxdmf = FindSource('output_1.pxdmf')

# create a new 'Calculator'
calculator1 = Calculator(Input=output_1pxdmf)

# Properties modified on output_1pxdmf
#output_1pxdmf.PointArrays = ['displacementX_0', 'displacementX_1', 'displacementX_2', 'displacementY_0', 'displacementY_1', 'displacementY_2', 'displacementZ_0', 'displacementZ_1', 'displacementZ_2']
#output_1pxdmf.CellArrays = ['E_0', 'damage_0', 'damage_1', 'damage_2', 'eps_xx_0', 'eps_xx_1', 'eps_xx_2', 'eps_xy_0', 'eps_xy_1', 'eps_xy_2', 'eps_xz_0', 'eps_xz_1', 'eps_xz_2', 'eps_yy_0', 'eps_yy_1', 'eps_yy_2', 'eps_yz_0', 'eps_yz_1', 'eps_yz_2', 'eps_zz_0', 'eps_zz_1', 'eps_zz_2', 'sig_xx_0', 'sig_xx_1', 'sig_xx_2', 'sig_xy_0', 'sig_xy_1', 'sig_xy_2', 'sig_xz_0', 'sig_xz_1', 'sig_xz_2', 'sig_yy_0', 'sig_yy_1', 'sig_yy_2', 'sig_yz_0', 'sig_yz_1', 'sig_yz_2', 'sig_zz_0', 'sig_zz_1', 'sig_zz_2']


# Properties modified on calculator1
calculator1.AttributeMode = 'Cell Data'
calculator1.ResultArrayName = 'VM_stress'
calculator1.Function = '(1/2 * ( (sig_xx - sig_yy)^2 + (sig_yy - sig_zz)^2 + (sig_zz - sig_xx)^2 + 6 * (sig_xy^2 + sig_xz^2 + sig_yz^2) )  )^(1/2)  '


