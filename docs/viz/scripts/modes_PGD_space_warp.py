import os
#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

for i in range(0,10):
    num_mode = i
    #path = '/u/vitse/Documents/ARTICLE/modes/'  
    # Useful stuff: UpdatePipeline() ; Render() ; 


    # create a new 'XDMF Reader'
    #output_1xdmf = XDMFReader(FileNames=['/u/vitse/Documents/ECCOMAS/tension3P_fin/output_1.xdmf'])
    output_1xdmf = FindSource('output_1.xdmf')
   
    # Properties modified on output_1xdmf
    output_1xdmf.GridStatus = ['PGD1']
    
    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # uncomment following to set a specific view size
    renderView1.ViewSize = [1694, 1064]
    renderView1.Background = [1.0, 1.0, 1.0]
    
    # reset view to fit data
    renderView1.ResetCamera()
    
    # show color bar/color legend
    # output_1xdmfDisplay.SetScalarBarVisibility(renderView1, True)
    
    # create a new 'Calculator'
    calculator1 = Calculator(Input=output_1xdmf)
    calculator1.Function = ''
    
    name_array = 'Phi_'+str(num_mode) + '(M)'
# Properties modified on calculator1
    calculator1.ResultArrayName = name_array
    disp_x = 'displacementX_' + str(num_mode)
    disp_y = 'displacementY_' + str(num_mode)
    disp_z = 'displacementZ_' + str(num_mode)
    calc_function = disp_x + '*iHat + ' + disp_y + '*jHat + ' + disp_z + '*kHat'
    calculator1.Function = calc_function
    
    displacementX0LUT = GetColorTransferFunction(name_array)
    
    # show data in view
    calculator1Display = Show(calculator1, renderView1)
    # trace defaults for the display properties.
    calculator1Display.ColorArrayName = ['POINTS', name_array]
    calculator1Display.LookupTable = displacementX0LUT
    calculator1Display.ScalarOpacityUnitDistance = 0.14234628079121475
    
    
    # get opacity transfer function/opacity map for 'displacementX0'
    displacementX0PWF = GetOpacityTransferFunction(name_array)
    displacementX0PWF.Points = [-0.1271581080874911, 0.0, 0.5, 0.0, 0.12716516023036037, 1.0, 0.5, 0.0]
    displacementX0PWF.ScalarRangeInitialized = 1

    # hide data in view
    # Hide(output_1xdmf, renderView1)
   
    # show color bar/color legend
    calculator1Display.SetScalarBarVisibility(renderView1, True)
    
    # Rescale transfer function
    displacementX0LUT.RescaleTransferFunction(-0.127158108087, 0.12716516023)
  
    # Rescale transfer function
    displacementX0PWF.RescaleTransferFunction(-0.127158108087, 0.12716516023)
    
    # set scalar coloring
    ColorBy(calculator1Display, ('POINTS', name_array))
    
    # rescale color and/or opacity maps used to include current data range
    calculator1Display.RescaleTransferFunctionToDataRange(True)
    
    # show color bar/color legend
    calculator1Display.SetScalarBarVisibility(renderView1, True)
    
    # get color transfer function/color map for 'PhiM'
    phiMLUT = GetColorTransferFunction(name_array)
    phiMLUT.RGBPoints = [0.007657067936211031, 0.933593, 0.949218, 0.996094, 32.925422232703454, 0.417975, 0.679683, 0.835935, 65.8431873974707, 0.0312505, 0.316411, 0.609369]
    phiMLUT.ColorSpace = 'HSV'
    phiMLUT.NanColor = [0.247059, 0.0, 0.0]
    phiMLUT.ScalarRangeInitialized = 1.0
    
    
    
    #### uncomment the following to render all views
    # RenderAllViews()
    # alternatively, if you want to write images, you can use SaveScreenshot(...).

    # get display properties
    calculator1Display = GetDisplayProperties(calculator1, view=renderView1)

    # rescale color and/or opacity maps used to exactly fit the current data range
    calculator1Display.RescaleTransferFunctionToDataRange(False)

    warpByVector = WarpByVector(Input=calculator1)
    warpByVector.Vectors = ['POINTS', name_array]
    warpByVector.ScaleFactor = 0.004

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # uncomment following to set a specific view size
    # renderView1.ViewSize = [1727, 1282]

    # show data in view
    warpByVector4Display = Show(warpByVector, renderView1)

    # show color bar/color legend
    warpByVector4Display.SetScalarBarVisibility(renderView1, True)

    # set scalar coloring
    ColorBy(warpByVector4Display, ('POINTS', name_array))

    # rescale color and/or opacity maps used to include current data range
    warpByVector4Display.RescaleTransferFunctionToDataRange(True)

    # show color bar/color legend
    warpByVector4Display.SetScalarBarVisibility(renderView1, True)

    #### saving camera placements for all active views

    # current camera placement for renderView1
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [0.44, -5.061702716503579, 0.06000000000000005]
    renderView1.CameraFocalPoint = [0.44, 0.06, 0.06000000000000005]
    renderView1.CameraViewUp = [0.0, 0.0, 1.0]
    renderView1.CameraParallelScale = 1.603968989725175

    # get opacity transfer function/opacity map for 'PhiM'
    phiMPWF = GetOpacityTransferFunction(name_array)
    phiMPWF.Points = [0.007657067936211031, 0.0, 0.5, 0.0, 65.8431873974707, 1.0, 0.5, 0.0]
    phiMPWF.ScalarRangeInitialized = 1
    phiMPWF.RescaleTransferFunction(0.0, 40.0)
    
    Hide(calculator1)
    Render()

# if (not os.path.isdir(path)):
#     os.mkdir(path)
        
# path_to_save = path + 'mode_' + str(num_mode) + '.eps'
# print path_to_save
# SaveScreenshot(path_to_save, magnification=1, quality=100, view=renderView1)
# ExportView(path_to_save, view=renderView1)
