function [] = generate_exp_MPS(in_out)
    X = [1 : 10];
    Y = [1 : 10];
    XX = repmat(X',numel(Y),1);
    YY = repmat(Y',numel(X),1);
    A = [sort(XX) , YY];
    
    dlmwrite(strcat('data/data/',in_out.name,'/MPS/MPS_exp.txt'),A,'delimiter','\t');

end