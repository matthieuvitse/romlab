function model_i = hooke_model(in_out,DATA,model_i,i_model)

DATA_model_i = DATA.model(i_model);
dnof_loc            = model_i.ndof;
    model_i.k  = spalloc(dnof_loc,dnof_loc, 1);
        
    [B_g,detjac] = grad_op_glob(DATA,model_i,DATA_model_i.type);
    
    model_i.detjac  = detjac;
    [hooke_g,var_E_g]        = hooke_global(in_out,DATA_model_i,model_i,'hooke');
    model_i.var_E_g = var_E_g;
    [hooke_inv_g,~]          = hooke_global(in_out,DATA_model_i,model_i,'invhooke');
    I_g                      = integ_full(DATA_model_i,model_i);
    
    model_i.k =  B_g' * I_g *  hooke_g * B_g;                % assembled stiffness operator
    model_i.k_inv = B_g' * I_g *  hooke_inv_g * B_g;
    disp(['    ... stiffness matrix ',num2str(i_model),' assembled...']);
    
    % LU decomposition of k
    % [m.k_l,m.k_u] = lu(m.k);
    
    [H_1,~] = hooke_global(in_out,DATA_model_i,model_i,'h1');
    
    model_i.h1 = B_g' * I_g * H_1 * B_g;     % assembled h1 integration operator
    disp(['    ... h1 operator ',num2str(i_model),' assembled...']);
    
    model_i.B_g          = B_g;
    model_i.hooke_g      = hooke_g;
    model_i.hooke_inv_g  = hooke_inv_g;
    model_i.I_g          = I_g;
end