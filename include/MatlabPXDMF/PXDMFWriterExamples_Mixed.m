% Matlab example of how to use the writepxdmf
%
% A directory named examples is created with all the outpout files
%
%    By Felipe Bordeu - GEM 2013
%    Felipe.Bordeu@ec-nantes.fr
%    
%    Version date : 29/08/2013
%    Version 1.6.2
%
%
%

% space 1 (x,y) a 2d mesh unstructured  (mixed elemens)
% space 2  (z)  a 1D  structured mesh  (linear element)

% 1 field :
%   1 scalar nodal field (temperature) 2 modes

% the is a bug in th xdmf reader in ParaView. So 1D structured mesh and element
% data are incompatible.

%% 
clear all
close all
clc

%% output directory
dirname = 'pxdmf_examples';
[SUCCESS,MESSAGE,MESSAGEID] = mkdir(dirname);

%%%%%%%%%%%%%%%%%%%
%% nodes is a cell contaning the nodes in each space

nodes = cell(2,1);
%space (x,y)
nodes{1} = [ [0 0 0 ]  
             [1 0 0]
             [2 0 0]
             [3 0 0]
             [4 0 0]
             [5 0 0]
             [6 0 0]
             [4 1 0]
             [5 1 0]             
             [6 1 0]
             [0 2 0]
             [1 1 0]
             [2 2 0]];

% (z) 
% first point origin
% second point spacing
% the spacing must allways be positive (no negative, no zero)
nodes{2} = [ [0   0 0] 
             [0.5 1 1]];

%% elements is a cell contaning the elements in each dimension
% in this case we use structured mesh so only line, quad, and hexa are
% generated

cells = cell(2,1);

%space (x,y)
%elements is a cell contaning the elements in each dimension
% polyvertex element (nodal elements),  Polyline (bars).
% 3 node element (triangle), 4 node element (square)
% 6 node element (wedge), 8 node element (hexa)
cells{1} = [ 1 1 0 ...      % this is a polynodeelement with one node 
             2 2 1 2  ...   % this is a polyline with two nodes
             4   3 7 4  ... % this is a triangle
             5   5 8 9 6  ... % this is a quad
             34  10 12 11];  % this is a XDMF_EDGE_3

% (z) 
% in the case of mixed element types no matrix is posible so a long vector
% is needed
cells{2} = [12 0 0 ];


%% names is a cell contaning the name of each space (the number of names determine the size of the space, 1D, 2D, 3D)
% firs columns names, second comlumns units
names = cell(2,2);

names{1,1} = {'X' 'Y'};
names{1,2} = { 'm' 'm' };

names{2,1} = {'Z'};
names{2,2} = {'m'};



% three fields (temperature)
nodes_fields = cell(2,1);

%% temperature
nodes_fields{1,1} =   1:13; % just to generate randoms modes


nodes_fields{2,1} =  rand(1,prod(cells{2}+1)) ;% just to generate randoms modes
% the number of nodes in each direction is the number of element plus one.

nodes_fields_names =  { 'temperature' };
%%no cell data

cell_fields = {};
cell_fields_names = {};

%% we need to put the from 1 because in the XDMF format the connectivity start from ZERO and not from one.

filename= [dirname '/Ascii_Mixed.pxdmf'];
writepxdmf(filename, nodes, cells, names, nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'rectilinear',[0 1],'mixed',[1 0]);

 
filename= [dirname '/Ascii_Mixed_single.pxdmf'];
writepxdmf(filename, nodes, cells, names, nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'precision','single','rectilinear',[0 1],'mixed',[1 0]);
 
filename= [dirname '/Binary_Mixed.pxdmf'];
writepxdmf(filename, nodes, cells, names, nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'bin',1,'rectilinear',[0 1],'mixed',[1 0]);

filename= [dirname '/Binary_Mixed_single.pxdmf'];
writepxdmf(filename, nodes, cells, names, nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'bin',1, 'precision','single','rectilinear',[0 1],'mixed',[1 0]);
 
%also work for a struct with the data
data.filename = [dirname '/Ascii_Mixed_struct.pxdmf'];
data.nodes = nodes;
data.cells = cells;
data.names = names;
data.nodes_fields = nodes_fields;
data.cell_fields = cell_fields;
data.nodes_fields_names =nodes_fields_names;
data.cell_fields_names = cell_fields_names;
data.verbose = 1;
writepxdmf(data,'rectilinear',[0 1],'mixed',[1 0] );

%% you can use the reader to read data from a pxdmf file 

ReadData = readpxdmf([dirname '/Ascii_Mixed.pxdmf'],'verbose',1);

