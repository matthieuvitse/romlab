% Matlab example of how to use the writepxdmf for a xdmf output
%
% A directory named examples is created with all the outpout files
%
%    By Felipe Bordeu - GEM 2013
%    Felipe.Bordeu@ec-nantes.fr
%    
%    Version date : 26/09/2013
%    Version 1.6.2
%
%
%  2d mesh non structured  (quad elemens)
%
% 3 field : 2 time steps
%   1 scalar nodal field (temperature)
%   1 vector nodal field (despacement) 
%   1 scalar element field (density)   

%% 
clear all
close all
clc

%% output directory
dirname = 'xdmf_examples';
[SUCCESS,MESSAGE,MESSAGEID] = mkdir(dirname);

%% nodes is a cell contaning the nodes in each space

% nodes
nodes = [ [0 0] 
          [1 0]
          [2 0]
          [0 1] 
          [1 1]
          [2 1] ];

%% elements is a cell contaning the elements in each dimension
% 1 node element (nodal element),  2 node element (bar).
% 3 node element (triangle), 4 node element (square)
% 6 node element (wedge), 8 node element (hexa)


% Element
cells = [[1 2 5 4 ]
         [2 3 6 5 ]];
     

% three fields (temperature, despacement)
nodes_fields = cell(1,2);

%% temperature
nodes_fields{1} =   rand(2,size(nodes,1)); % just to genera randoms modes

%% displacement
% in this case the value of the field is stored in the form x1 y1 z1 x2 y2 z2... 
 
nodes_fields{2} =   [ 0 0 1 1 0 1 2 0 1 0 1 1 1 1 1 2 1 1    %  x1 y1 z1 x2 y2 z2...  for the first mode
                        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ];  %  x1 y1 z1 x2 y2 z2...  for the second mode mode

%% names for the fields
nodes_fields_names={ 'temperature' 'displacement' };


%% like the nodes_fields but for the cells 
cell_fields = cell(1) ;
cell_fields{1} = [ 0.9 1 ; 1 1.2];
cell_fields_names= { 'density' };

%% we need to put the from 1 because in the XDMF format the connectivity start from ZERO and not from one.

filename= [dirname '/Ascii.xdmf'];
writexdmf(filename, nodes, cells,  nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'from1',1,'verbose',1);

filename= [dirname '/Ascii_single.xdmf'];
writexdmf(filename, nodes, cells,  nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'from1',1,'precision','single');

filename= [dirname '/Binary.xdmf'];
writexdmf(filename, nodes, cells,  nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'bin',1,'from1',1);


filename= [dirname '/Binary_single.xdmf'];
writexdmf(filename, nodes, cells,  nodes_fields, cell_fields, nodes_fields_names, cell_fields_names, 'bin',1, 'from1',1,'precision','single');

%also work for a struct with the data
% to get the structure form the file .
data  = writexdmf();

data.filename = [dirname '/Ascii_struct.xdmf'];
data.nodes = nodes;
data.cells = cells;
data.nodes_fields = nodes_fields;
data.cell_fields = cell_fields;
data.nodes_fields_names =nodes_fields_names;
data.cell_fields_names = cell_fields_names;
data.verbose = 1;
writexdmf(data,'from1',1);

%% you can use the reader to read data from a pxdmf file 

ReadData = readpxdmf([dirname '/Ascii.xdmf']);

