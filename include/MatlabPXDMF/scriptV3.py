from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'PXDMFReader'
#solpgdpxdmf = PXDMFReader(FileName='/Users/neron/Documents/LMT/MATLAB/esbroufe3d_PGD_LATIN/esbroufe3d/tmp/solpgd.pxdmf')

# or ... 
solpgdpxdmf = GetActiveSource()

# Properties modified on solpgdpxdmf
solpgdpxdmf.VisualizationSpace = ['Dim 0 size : 3 ( X,Y,Z ) ']
#solpgdpxdmf.PointArrays = ['displacementX_0', 'displacementX_1', 'displacementX_2', 'displacementX_3', 'displacementX_4', 'displacementX_5', 'displacementX_6', 'displacementX_7', 'displacementX_8', 'displacementX_9', 'displacementY_0', 'displacementY_1', 'displacementY_2', 'displacementY_3', 'displacementY_4', 'displacementY_5', 'displacementY_6', 'displacementY_7', 'displacementY_8', 'displacementY_9', 'displacementZ_0', 'displacementZ_1', 'displacementZ_2', 'displacementZ_3', 'displacementZ_4', 'displacementZ_5', 'displacementZ_6', 'displacementZ_7', 'displacementZ_8', 'displacementZ_9']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
solpgdpxdmfDisplay = Show(solpgdpxdmf, renderView1)

# show color bar/color legend
solpgdpxdmfDisplay.SetScalarBarVisibility(renderView1, False)

# Properties modified on solpgdpxdmf
solpgdpxdmf.VisualizationTime = ['Dim 1 size : 1 ( t ) ']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# create a new 'Calculator'
calculator1 = Calculator(Input=solpgdpxdmf)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.ResultArrayName = 'Displacement'
calculator1.Function = 'X*iHat + Y*jHat + Z*kHat'

# show data in view
calculator1Display = Show(calculator1, renderView1)

# hide data in view
Hide(solpgdpxdmf, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, False)

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(Input=calculator1)
warpByVector1.Vectors = ['POINTS', 'Displacement']

# show data in view
warpByVector1Display = Show(warpByVector1, renderView1)

# hide data in view
Hide(calculator1, renderView1)

# set scalar coloring
ColorBy(warpByVector1Display, ('POINTS', 'Displacement'))

# show color bar/color legend
warpByVector1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'Displacement'
displacementLUT = GetColorTransferFunction('Displacement')
displacementLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 5e-17, 0.865003, 0.865003, 0.865003, 1e-16, 0.705882, 0.0156863, 0.14902]
displacementLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'Displacement'
displacementPWF = GetOpacityTransferFunction('Displacement')
displacementPWF.Points = [0.0, 0.0, 0.5, 0.0, 1e-16, 1.0, 0.5, 0.0]
displacementPWF.ScalarRangeInitialized = 1

# reset view to fit data
renderView1.ResetCamera()

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [3.5, 5.0, 46.877764558490185]
renderView1.CameraFocalPoint = [3.5, 5.0, 8.0]
renderView1.CameraParallelScale = 10.062305898749054

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).