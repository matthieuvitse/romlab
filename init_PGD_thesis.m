function [m,s_init] = init_solution(in_out,DATA,m)
% Initialization of the process
disp('    ... initialization of the process...')
tic

% convention for resulution

%  /      |      \   /  \     /  \
% |  Kuu  |  Kud  | | Uu |   | Fu |
% | _ _ _ | _ _ _ | | __ | = | __ |
% |       |       | |    |   |    |
% |  Kud  |  Kdd  | | Ud |   | Fd |
%  \      |      /   \  /     \  /

param_redis = m.param_redis;
tol_cp_als = m.tol_cp_als;

allbutM = '';
allbutMmat = '';
for i = 2 : length(m.params)
    if strcmp(m.params(i).type,'loading') || strcmp(m.params(i).type,'time')
        allbutMmat = [ allbutMmat ',:' ];
    end
    allbutM = [ allbutM ',:' ];
end

% monolithic resolution
% ---------------------

%[k,~,~,B_g,hooke_g,~] = computed_coupled_operators(m);
%kuu = k(ddlu,ddlu);
%kud = k(ddlu,ddld);

ddlu = m.dofuu;
ddld = m.dofud;

B_g = m.B_g;
hooke_g = m.hooke_g;
kuu = .01 * m.k(ddlu,ddlu);
kud = m.k(ddlu,ddld);

udd = eval(strcat('double(m.ud(ddld',allbutMmat,'))'));
fuu = eval(strcat('double(m.fu(ddlu',allbutMmat,'))'));

if length(m.params) < 3
    ndof3 = 1;
    flag = 1;
elseif length(m.params) == 3
    flag = 2;
    ndof3 = m.ndofparams(3);
end
if length(m.params) == 4 %&& strcmp( m.params(4).type,'young')
    ndof3 = m.ndofparams(3);
    ndof4 = m.ndofparams(4);
    dof4 = m.params(4).mesh;
    if strcmp( m.params(4).type,'young')
        flag = 4;
    else
        flag = 5;
    end
else
    ndof4 = 1;
    dof4 = 1;
end

parfor (i_init = 1 : ndof3 , in_out.arg_parallel)
%for (i_init = 1 : ndof3)
    for j_init = 1 : ndof4
        switch flag
            case 1
                fu = fuu(:,:,i_init,j_init);
                udd1 = udd(:,:,i_init,j_init);
                Uu(:,:,i_init,j_init) = kuu\(fu - kud*udd1);
                Ud(:,:,i_init,j_init) = udd1;
            case 2
                fu = fuu(:,:,i_init,j_init);
                udd1 = udd(:,:,i_init,j_init);
                Uu(:,:,i_init,j_init) = (kuu)\(fu - kud*udd1);
                Ud(:,:,i_init,j_init) = udd1;
            case 3
                fu = fuu(:,:,i_init,j_init);
                udd1 = udd(:,:,i_init,j_init);
                Uu(:,:,i_init,j_init) = (kuu)\(fu - kud*udd1);
                Ud(:,:,i_init,j_init) = udd1;
            case 4
                fu = fuu(:,:,i_init);
                udd1 = udd(:,:,i_init);
                Uu(:,:,i_init,j_init) = (dof4(j_init)*kuu)\(fu - kud*udd1);
                Ud(:,:,i_init,j_init) = udd1;
            case 5
                fu = fuu(:,:,i_init,j_init);
                udd1 = udd(:,:,i_init,j_init);
                Uu(:,:,i_init,j_init) = (kuu)\(fu - kud*udd1);
                Ud(:,:,i_init,j_init) = udd1;
        end
        
    end
end

U_mono(ddlu,:,:,:) = Uu;
U_mono(ddld,:,:,:) = Ud;

% computation of the globalized (but local) eps and sigma
U_init     = tensor(U_mono);
eps_init   = ttm(U_init,B_g,1);
sigma_init = ttm(eps_init,hooke_g,1);
d_init     = tensor(size(eps_init)); % no damage

if strcmp(DATA.assembly,'contact')
    warning('has to be updated')
    keyboard    
    % decoupled resolution
    % --------------------
    
    % possibily of looping on models with eval functions
    
    % model 1
    ddl1 = m.model(1).dof_corres;
    ddl1u = m.model(1).dofuu;
    ddl1d = m.model(1).dofud;
    %ddl1c = m.model(1).repcontact;
    B1_g = m.model(1).B_g;
    hooke1_g = m.model(1).hooke_g;
    k1uu = m.model(1).k(ddl1u,ddl1u);
    k1ud = m.model(1).k(ddl1u,ddl1d);
    u1bcd = double(m.model(1).ud(ddl1d,:,:));
    f1bcu = double(m.model(1).fu(ddl1u,:,:));
    
    % model 2
    ddl2 = m.model(2).dof_corres;
    ddl2u = m.model(2).dofuu;
    ddl2d = m.model(2).dofud;
    %ddl1c = m.model(2).repcontact;
    B2_g = m.model(2).B_g;
    hooke2_g = m.model(2).hooke_g;
    k2uu = m.model(2).k(ddl2u,ddl2u);
    k2ud = m.model(2).k(ddl2u,ddl2d);
    u2bcd = double(m.model(2).ud(ddl2d,:,:));
    f2bcu = double(m.model(2).fu(ddl2u,:,:));
    
    parfor (i_init = 1 : m.ndofparams(3) , in_out.arg_parallel)
        %for i_init = 1 : m.ndofparams(3)
        f1u = f1bcu(:,:,i_init);
        u1dd = u1bcd(:,:,i_init);
        U1u(:,:,i_init) = k1uu\(f1u - k1ud*u1dd);
        U1d(:,:,i_init) = u1bcd(:,:,i_init);
        
        f2u = f2bcu(:,:,i_init);
        u2dd = u2bcd(:,:,i_init);
        U2u(:,:,i_init) = k2uu\(f2u - k2ud*u2dd);
        U2d(:,:,i_init) = u2bcd(:,:,i_init);
    end
    U1(ddl1u,:,:) = U1u;
    U1(ddl1d,:,:) = U1d;
    U2(ddl2u,:,:) = U2u;
    U2(ddl2d,:,:) = U2d;
    
    % computation of the globalized (but local) eps and sigma
    U1 = tensor(U1);
    eps1 = ttm(U1,B1_g,1);
    sigma1 = ttm(eps1,hooke1_g,1);
    U2 = tensor(U2);
    eps2 = ttm(U2,B2_g,1);
    sigma2 = ttm(eps2,hooke2_g,1);
    
    % storage
    U_init(ddl1,:,:) = U1;
    U_init(ddl2,:,:) = U2;
    
    % marche pas pour l'instant (SNIF :-)).
    % eps_init(pg1,:,:) = eps1;
    % eps_init(pg2,:,:) = eps2;
    % sigma_init(pg1,:,:) = sigma1;
    % sigma_init(pg2,:,:) = sigma2;
    
    % pis-aller de SNIF
    eps_init = ttm(U_init,B_g,1);
    sigma_init = ttm(eps_init,hooke_g,1);
    
    d_init = tensor(size(eps_init)); % no damage
end

switch in_out.solver
    case {'LATIN','PGD'}
        % ktensor of initial solution
        R = 1;
        U_0 = redistribute(cp_als(tensor(U_init),R,'printitn',m.printitn_cp_als,'tol',tol_cp_als),param_redis);
        % we check whether the initial approximation is good or not. (because we then add CA0 modes)
        [U_init,R] = check_approx(m,U_init,U_0,R);
        disp(['        ... done. Rank of initial solution: R = ',num2str(R)]);
        
        m.rank_init = R;
        
        eps_init     = ttm(U_init, B_g , 1);
        sigma_init   = ttm(eps_init,hooke_g,1);
        
        d_init = sptensor(size(eps_init));
end
%U_init.U{1}(ddlu,:) = zeros(numel(ddlu),R);
s_init = sol_class(U_init,eps_init,sigma_init,d_init,'init');

a=toc;
disp(['    ... initialization done... ',num2str(a),' s'])

end
