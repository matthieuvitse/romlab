disp('ROMlab: Loading paths...')
path(path,'bin/');
path(path,'bin/ref');
path(path,'bin/pre');
path(path,'bin/fem');
path(path,'bin/post');
path(path,'bin/solver');
path(path,'bin/pgd');
path(path,'bin/big_pgd');
path(path,'bin/temp_functions');
path(path,'bin/latin');
path(path,'bin/other_functions');
path(path,'bin/damage');
path(path,'bin/MPS');
path(path,'data/');
path(path,'data/data');
path(path,'data/data/loadings');
path(path,'include');
path(path,'include/dependancy_graph');
path(path,'include/MatlabPXDMF');
path(path,'include/tensor_toolbox');
path(path,'include/matlab2tikz/src');
path(path,'post_damage');
disp('    ...paths loaded')
  

