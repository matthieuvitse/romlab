% post treatment of s_linear.eps to get  the damage

clear all
close all
clc

paths;
path(path,'post_damage')

[in_out,DATA,m,s_linear] = load_big_file('post_damage/result_2016_5_20.mat');

if in_out.parallel
    
    delete(gcp('nocreate'));
    parpool;
    in_out.arg_parallel = 3;
else
    in_out.arg_parallel = 0;
end

d = compute_damage_post(in_out,DATA,m,s_linear);
s_linear.d = d;
disp('post_damage: Saving solution... ');
save(name_out,'in_out','DATA','m','s_linear','-v7.3'); 
disp('    ... solution saved');
clear d;
[output] = export_pxdmf(in_out,DATA,m,s_linear);