function [in_out,DATA,m,s] = load_big_file(file)

disp('Loading file... ')
toto    = matfile(file);

DATA    = toto.DATA;
if strcmp(DATA.model(1).behavior,'damage')
    DATA.model(1).behavior.F_Y = @(Y,Y0,Adt) 1 - (1 + Adt * (Y - Y0) ).^-1;
end
m       = toto.m;
in_out  = toto.in_out;
s       = toto.s_linear;

if in_out.plot_damage 
    A = toto.s_local;
    s.d = A.d;
end
disp('    ... file loaded.')
end