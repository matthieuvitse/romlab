function max_deflection = open_csv(newdir)
old_dir = pwd;

cd(newdir)

A = dir('*.csv');

%max_deflection = zeros(99,10,7);
max_deflection = zeros(10,7);

u = 0;
v = 0;

numb2_old = 0;
numb3_old = 0;
for i = 1 : numel(A)
     %numbers = sscanf(A(i).name,'results_%d_%f__%f__0.0.csv');
     numbers =sscanf(A(i).name,'VM_t%d_%f_%f.csv');
     
          if numbers(2) ~= numb2_old
              u = u+1;
              numb2_old = numbers(2);
              v = 0;
          end
          if numbers(3) ~= numb3_old
              v = v+1;
              numb3_old = numbers(3);
          end
     B = csvread(A(i).name,1,2);
     %B2 = B(:,1);
     B2 = B(:,13);
     max_deflection(u,v) = B2;
     %max_deflection(:,u,v) = B2;
end

cd(old_dir)