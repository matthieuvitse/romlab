function [m] = update_hooke_damage()

file = '/usrtmp/vitse/These/results/377447/smart_phase_1_smart_phase_1/result_2016_09_8_0000/result_2016_9_9.mat';
toto    = matfile(file);
m = toto.m;
DATA = toto.DATA;

file2 = '/usrtmp/vitse/These/results/377447/smart_phase_1_smart_phase_1/result_2016_09_8_0000/damage.mat';
tata = matfile(file2);
dam = tata.damage;

hooke_g = m.hooke_g;
hooke_dg = sptensor([m.ndofgp m.ndofgp m.params(2).ndof m.params(3).ndof]);
if DATA.dim == 3
    comp = 6;            % number of components in the elementary eps/sig matrices
end

mparams = m.params;
mbox = m.elem_in_box;
mdofgp = m.ndofgp;
m_model(1).var_E_g = m.model(1).var_E_g;
m_model(1).hooke = DATA.model(1).material.hooke;
m_model(1).elem  = m.model(1).elem;
m_model(2).hooke = DATA.model(2).material.hooke;
m_model(2).elem  = m.model(2).elem;
clear m
clear DATA

var_hooke = m.model(1).var_E_g;
parfor i_param1 = 1 : mparams(3).ndof
    disp(' i m in!!!!!');
    for i_model  = 1 : numel(m_model)
        m_i = m_model(i_model);
        
        hooke_i = m_i.hooke;
        j_hooke = 0;
        hooke_dg_temp = sptensor([mdofgp mdofgp mparams(2).ndof]);
        for i_elem = 1 : numel(m_i.elem)
            npg_i = numel(m_i.elem(i_elem).gp);
            if ~ismember(i_elem,mbox)
                
                for gp = 1 : npg_i
                    j_hooke = j_hooke +1 ;
                    dof_eps_gp       = ((i_elem-1) * npg_i * comp + (gp-1) * comp + 1) : ((i_elem-1) * npg_i* comp + gp * comp);
                    hooke = var_hooke(j_hooke) * hooke_i;
                    
                    for i_time = 1 : mparams(2).ndof
                        hooke_dg_temp(dof_eps_gp,dof_eps_gp,i_time) = sptensor((1 - dam(1,i_time,i_param1)) * hooke) ;
                    end
                end
            else
                j_hooke = j_hooke +npg_i ;
                dof_eps = ((i_elem-1) * npg_i * comp  + 1) : ((i_elem-1) * npg_i* comp + npg_i * comp);
                hooke = hooke_g(dof_eps,dof_eps);

                for i_time = 1 : mparams(2).ndof
                    hooke_dg_temp(dof_eps_gp,dof_eps_gp,i_time) = sptensor(hooke);
                end
            end
        end
    end
    hooke_dg(:,:,:,i_param1) = hooke_dg_temp(:,:,:,1);
end
hooke_dg = hooke_dg;

save('hooke_dg.mat',hooke_dg);


end